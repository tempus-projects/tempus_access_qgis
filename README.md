# <img src="icons/icon_tempus.jpg" alt="Icone Tempus" width="40"/> TempusAccess
**TempusAccess** est un plugin QGIS, distribué sous [licence GPL](https://gitlab.com/tempus-projects/tempus_access_qgis/tree/master/LICENSE), de calcul d'**indicateurs sur l'offre de transport et sur l'accessibilité des territoires permise par cette offre**. Il est développé par le [Cerema](https://www.cerema.fr/fr) et se base sur les outils du projet [Tempus](https://gitlab.com/tempus-projects) développés par le [Cerema](https://www.cerema.fr/fr), [l'IFSTTAR](https://www.ifsttar.fr/accueil/) et la société [Oslandia](https://oslandia.com/).

**La version 2 de TempusAccess est bientôt disponible !** [Consultez les nouveautés](https://gitlab.com/tempus-projects/tempus_access_qgis/-/wikis/release_notes).

## :arrow_forward:  Pour quoi faire ?
TempusAccess permet de construire des indicateurs quantifiant l'offre de transport qui peuvent être utilisés dans différents contextes :  

- Mesures de l'égalité territoriale : accessibilité aux services publics, aux soins, etc. ;
- Qualification détaillée des itinéraires pour différents modes et selon différents critères : temps, confort, correspondances, sécurité, etc., afin d'évaluer des points noirs à améliorer sur les réseaux ;
- Calcul de matrices de coûts pour alimenter des modèles de déplacements ;
- etc.
 
### Un environnement applicatif complet
Ses principaux atouts sont :  
-  le **calcul d'indicateurs** d'offre de transport **via une interface ergonomique** ;  
- le **requêtage direct en SQL**, de la base PostgreSQL sous-jacente, au moyen de fonctions dédiées, ce qui permet de lancer des requêtes plus complexes et de réaliser des traitements par lots qui seraient fastidieux à réaliser via l'interface utilisateur ;  
- l'**intégration automatique de données** dans de nombreux formats : GTFS, Visum, IGN, OpenStreetMap ;  
- une **représentation cartographique** automatique des indicateurs (personnalisable selon les souhaits de l'utilisateur grâce aux fonctions classiques de QGIS).

### Des fonctions de calcul d'itinéraire intégrées
Parmi les indicateurs proposés, certains font appel à des **calculs de plus court chemin** (entre deux points, isochrones), avec la possibilité générer des **solutions intermodales** (par exemple utilisant en combinaison les transports en commun et un véhicule privé, ou encore un système de vélos en libre-service). Pour cela, il est possible de **définir la position initiale des véhicules** et d'en tenir compte dans l'optimisation d'itinéraire. Enfin, TempusAccess permet de **personnaliser le coût généralisé** utilisé pour optimiser les itinéraires, par une somme pondérée du temps et du tarif associé au déplacement réalisé. 

### Une agrégation spatio-temporelle des indicateurs paramétrable
Il est possible de **choisir la période horaire et les jours** sur lesquels les indicateurs sont estimés. Différentes méthodes d'agrégation temporelle des résultats sont proposées : on peut ainsi estimer l'**offre moyenne, l'offre minimum ou l'offre maximum** sur la période et les jours choisis.
De même, pour les indicateurs associés aux isochrones, il est possible d'agréger, selon un minimum, un maximum ou une moyenne, les résultats associés à plusieurs isochrones ayant des noeuds racines différents.

### De nombreux indicateurs sur l'offre de transport
TempusAccess propose différents objets de base : les arrêts, les lignes, les exploitants... Il calcule pour ces objets en un clic un ensemble d'indicateurs sur l'offre de transport, tels que le nombre de passages, l'amplitude horaire, etc. Il calcule également des indicateurs associés à des itinéraires multimodaux, tels que le temps de parcours, le nombre de correspondances et de changements de modes, la distance parcourue, etc. 

[Accédez à la liste complète des objets de base et des indicateurs](https://gitlab.com/tempus-projects/tempus_access_qgis/-/wikis/3_doc_reference/choisir_objets_fr)

## :books: Documentation utilisateur

- Pour accéder à la documentation d'installation et d'utilisation, rendez-vous sur le [wiki du projet](https://gitlab.com/tempus-projects/tempus_access_qgis/wikis).  

- Le [guide de démarrage](https://gitlab.com/tempus-projects/tempus_access_qgis/-/wikis/3_tutos/guide_demarrage_fr)   vous sera sans doute utile pour commencer.

- La [documentation de référence](https://gitlab.com/tempus-projects/tempus_access_qgis/-/wikis/2_ref_doc_fr) vous renseignera sur les formats de données acceptés et sur les indicateurs disponibles.

- Pour avoir une vision plus large des possibilités d'utilisation de TempusAccess, vous pouvez consulter le [tutoriel sur l'analyse de l'accès aux soins en Corse.](https://gitlab.com/tempus-projects/tempus_access_qgis/-/wikis/3_tutos/tuto_complet_corse_fr)

