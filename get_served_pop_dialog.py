# -*- coding: utf-8 -*-
"""
/***************************************************************************
 PluginTempusAccess
                                 A QGIS plugin
 Analyse de l'offre de transport en commun
                              -------------------
        begin                : 2016-10-22
        git sha              : $Format:%H$
        copyright            : (C) 2016 by Cerema
        email                : Aurelie.bousquet@cerema.fr, Patrick.Palmier@cerema.fr, helene.ly@cerema.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# import the PyQt and QGIS libraries
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from qgis.PyQt.QtWidgets import QDialog, QDialogButtonBox
from PyQt5.Qt import *
from PyQt5 import QtSql
from qgis.core import *
from qgis.utils import iface

# Initialize Qt resources from file resources.py
from .config import *
from .functions import *

import sys
import string
import os

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "\\forms")
from get_served_pop_dialog import Ui_Dialog

class get_served_pop_dialog(QDialog): 
    def __init__(self, caller, iface):
        QDialog.__init__(self)
        self.ui= Ui_Dialog()
        self.ui.setupUi(self)
        self.caller = caller
        self.db = caller.db
        self.iface = caller.iface
        
        self.plugin_dir = self.caller.plugin_dir 
        self.icon_dir = self.caller.icon_dir 
        
        self.ui.comboBoxZoningTableName.setModel(self.caller.modelZoningSource)
        self.ui.comboBoxServedAreaTableName.setModel(self.caller.modelPolygonIsochronReq)
        
        self.modelZoningTablePopField = QtSql.QSqlQueryModel()
        self.ui.comboBoxZoningTablePopField.setModel(self.modelZoningTablePopField)
                
        self.connectSlots()
        
    def connectSlots(self):        
        self.ui.comboBoxZoningTableName.currentIndexChanged.connect(self._slotComboBoxPopTableNameIndexChanged)
        self.ui.comboBoxZoningTablePopField.currentIndexChanged.connect(self._slotComboBoxPopTablePopFieldIndexChanged)
        self.ui.pushButtonGenerate.clicked.connect(self._slotPushButtonGenerateClicked)
        self.ui.pushButtonCalculate.clicked.connect(self._slotPushButtonCalculateClicked)
    
    def _slotComboBoxPopTableNameIndexChanged(self, indexChosenLine): 
        s = "SELECT column_name FROM information_schema.columns \
             WHERE table_schema = 'tempus_zoning' AND table_name   = '"+self.ui.comboBoxZoningTableName.currentText()+"' AND data_type = ANY(ARRAY['smallint', 'integer', 'bigint', 'real', 'double precision']) \
             ORDER BY 1"
        self.modelZoningTablePopField.setQuery(unicode(s), self.db)
        
    def _slotComboBoxPopTablePopFieldIndexChanged(self, indexChosenLine): 
        self.ui.lineEditServedTablePopField.setText(self.ui.comboBoxZoningTablePopField.currentText())
        
    def _slotPushButtonCalculateClicked(self): 
        s = "ALTER TABLE tempus_results."+self.ui.comboBoxServedAreaTableName.currentText()+" ADD COLUMN "+self.ui.lineEditServedTablePopField.text()+" real; \
             WITH q AS ( \
                 SELECT iso.id, sum(st_area(st_intersection(pop.geom, iso.geom)) / st_area(pop.geom) * pop.ind) as rel_pop \
                 FROM tempus_results."+self.ui.comboBoxServedAreaTableName.currentText()+" iso JOIN tempus_zoning."+self.ui.comboBoxZoningTableName.currentText()+" pop ON (st_intersects(pop.geom, iso.geom)) \
                 GROUP BY iso.id \
                 ORDER BY iso.id \
             ) \
             UPDATE tempus_results."+self.ui.comboBoxServedAreaTableName.currentText()+" \
             SET "+self.ui.lineEditServedTablePopField.text()+" = q.rel_pop \
             FROM q \
             WHERE q.id = "+self.ui.comboBoxServedAreaTableName.currentText()+".id; "
        print(s)
        q = QtSql.QSqlQuery(self.caller.db)
        q.exec_(unicode(s))
        box.setText(u"La variable de population a été calculée.")
    
    def _slotPushButtonGenerateClicked(self): 
        pass
    
    
    
    
    