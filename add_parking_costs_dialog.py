#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2018-2019 Cerema (https://www.cerema.fr)
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""

# import the PyQt and QGIS libraries
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from qgis.PyQt.QtWidgets import QDialog
from PyQt5.Qt import *
from PyQt5 import QtSql
from qgis.core import *
from qgis.gui import *
from qgis.utils import iface

# Initialize Qt resources from file resources.py
from .config import *
from .functions import *

import sys
import string
import os

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "\\forms")
from add_parking_costs_dialog import Ui_Dialog

class add_parking_costs_dialog(QDialog):

    def __init__(self, caller, iface):
        QDialog.__init__(self)
        self.ui= Ui_Dialog()
        self.ui.setupUi(self)        
        
        self.caller = caller
        self.iface = self.caller.iface
        self.db = caller.db
        
        self.plugin_dir = self.caller.plugin_dir
        self.icon_dir = self.plugin_dir + "/icons"

        self.ui.comboBoxVehicleParkingRule.setModel(self.caller.modelVehicleParkingRule) 
        self.query=""
        
        self.ui.pushButtonExecute.setIcon(QIcon(self.icon_dir + "/icon_exe.svg"))
        self.ui.pushButtonGenerate.setIcon(QIcon(self.icon_dir + "/icon_generate.png"))
        
        self.connectSlots()
    
    def connectSlots(self):        
        self.ui.radioButtonExistingVehicleParkingRule.toggled.connect(self._slotVehicleParkingRuleToggled)
        self.ui.radioButtonNewVehicleParkingRule.toggled.connect(self._slotVehicleParkingRuleToggled)
        self.ui.pushButtonExecute.clicked.connect(self._slotPushButtonExecuteClicked)
        self.ui.pushButtonGenerate.clicked.connect(self._slotPushButtonGenerateClicked)
    
    def _slotVehicleParkingRuleToggled(self):
        if self.ui.radioButtonExistingVehicleParkingRule.isChecked():
            self.ui.comboBoxVehicleParkingRule.setEnabled(True)
            self.ui.lineEditVehicleParkingRule.setEnabled(False)
            self.ui.radioButtonBicycles.setEnabled(False)
            self.ui.radioButtonCars.setEnabled(False)
        elif self.ui.radioButtonNewVehicleParkingRule.isChecked():
            self.ui.comboBoxVehicleParkingRule.setEnabled(False)
            self.ui.lineEditVehicleParkingRule.setEnabled(True)
            self.ui.radioButtonBicycles.setEnabled(True)
            self.ui.radioButtonCars.setEnabled(True)
    
    def prepareQuery(self):        
        if (self.ui.radioButtonExistingVehicleParkingRule.isChecked()):
            vehicle_parking_rule = str(self.caller.modelVehicleParkingRule.record(self.ui.comboBoxVehicleParkingRule.currentIndex()).value("id"))
        else:
            self.query = self.query + "INSERT INTO pgtempus.vehicle_parking_rule(name) VALUES ('"+str(self.ui.lineEditVehicleParkingRule)+"');\n"
            s = "SELECT id FROM pgtempus.vehicle_parking_rule WHERE name = '"+str(self.ui.lineEditVehicleParkingRule)+"'"
            q=QtSql.QSqlQuery(unicode(s), self.db)
            q.next()
            vehicle_parking_rule = str(q.value(0))
            
        for layer in self.iface.layerTreeView().selectedLayers():
            if (layer.name() == "Noeuds"):
                features = layer.selectedFeatures()
                if (features == []):
                    box = QMessageBox()
                    box.setText(u"Aucun noeud sélectionné")
                    box.exec_()
                    return False
                else:
                    for f in features:
                        # If the pair (node_id, vehicle_parking_rule) is not already in the table, it is inserted, else, it is updated
                        self.query = self.query + "\n\
                                    INSERT INTO tempus_networks.vehicle_parking_cost(node_id, vehicle_parking_rule, leaving_time, leaving_fare, taking_time, taking_fare) \
                                    (\n\
                                        SELECT "+str(f["id"])+", "+vehicle_parking_rule+", '"+self.ui.timeEditLeaving.time().toString("hh:mm:ss")+"'::time, "+str(self.ui.doubleSpinBoxLeavingFare.value())+", '"+self.ui.timeEditTaking.time().toString("hh:mm:ss")+"'::time, "+str(self.ui.doubleSpinBoxTakingFare.value())+"\n\
                                    )\n\
                                    EXCEPT\n\
                                    (\n\
                                        SELECT node_id, vehicle_parking_rule, '"+self.ui.timeEditLeaving.time().toString("hh:mm:ss")+"'::time, "+str(self.ui.doubleSpinBoxLeavingFare.value())+", '"+self.ui.timeEditTaking.time().toString("hh:mm:ss")+"'::time, "+str(self.ui.doubleSpinBoxTakingFare.value())+"\n\
                                        FROM tempus_networks.vehicle_parking_cost\n\
                                    );\n\
                                    UPDATE tempus_networks.vehicle_parking_cost\n\
                                    SET leaving_time = '"+self.ui.timeEditLeaving.time().toString("hh:mm:ss")+"'::time, leaving_fare = "+str(self.ui.doubleSpinBoxLeavingFare.value())+", taking_time = '"+self.ui.timeEditTaking.time().toString("hh:mm:ss")+"'::time, taking_fare = "+str(self.ui.doubleSpinBoxTakingFare.value())+"\n\
                                    WHERE node_id = "+str(f["id"])+" AND vehicle_parking_rule = "+vehicle_parking_rule+";\n"
                    return True
    
    def _slotPushButtonExecuteClicked(self):
        if (self.prepareQuery()):
            q=QtSql.QSqlQuery(unicode(self.query), self.db)
            q.exec_()
        self.caller.iface.mapCanvas().refreshAllLayers()
    
    def _slotPushButtonGenerateClicked(self):
        if (self.prepareQuery()):
            NomFichierComplet=["", ""]
            NomFichierComplet = QFileDialog.getSaveFileName(caption = "Enregistrer sous...", directory=self.plugin_dir, filter = "SQL files (*.sql)")
            if (NomFichierComplet[0]!=""):
                f = open(NomFichierComplet[0], "a")
                f.write(unicode(self.query))
                f.close()
                
                box = QMessageBox()
                box.setText(u"La requête a été générée dans le fichier cible")
                box.exec_()
    