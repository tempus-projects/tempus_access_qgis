#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2018-2019 Cerema (https://www.cerema.fr)
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""

# import the PyQt and QGIS libraries
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from qgis.PyQt.QtWidgets import QDialog
from PyQt5.Qt import *
from PyQt5 import QtSql
from qgis.core import *
from qgis.utils import iface

# Initialize Qt resources from file resources.py
from .config import *
from .functions import *

import sys
import string
import os

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "\\forms")
from set_vehicles_dialog import Ui_Dialog

class set_vehicles_dialog(QDialog):

    def __init__(self, caller, iface):
        QDialog.__init__(self)
        self.ui= Ui_Dialog()
        self.ui.setupUi(self)        
        
        self.caller = caller
        self.iface = self.caller.iface
        self.db = caller.db
        
        self.plugin_dir = self.caller.plugin_dir
        self.icon_dir = self.plugin_dir + "/icons"

        self.ui.comboBoxTransportMode.setModel(self.caller.modelVehiclesTransportMode)        
        self.ui.pushButtonAddSelection.setIcon(QIcon(self.icon_dir + "/icon_add_veh.png"))
        self.ui.pushButtonRemoveSelection.setIcon(QIcon(self.icon_dir + "/icon_remove_veh.png"))
        
        self.connectSlots()
    
    def connectSlots(self):        
        self.ui.pushButtonAddSelection.clicked.connect(self._slotPushButtonAddSelectionClicked)
        self.ui.pushButtonRemoveSelection.clicked.connect(self._slotPushButtonRemoveSelectionClicked)
        self.ui.pushButtonClear.clicked.connect(self._slotPushButtonClearClicked)
    
    def _slotPushButtonAddSelectionClicked(self):
        transport_mode_id = self.caller.modelVehiclesTransportMode.record(self.ui.comboBoxTransportMode.currentIndex()).value("id")
        for layer in self.iface.layerTreeView().selectedLayers():
            if (layer.name() == "Noeuds"):
                features = layer.selectedFeatures()
                if (features != []):
                    for f in features:
                        s1="SELECT count(*) FROM tempus_networks.vehicle_parking_cost WHERE node_id = "+str(f["id"])+" AND vehicle_parking_rule = (SELECT vehicle_parking_rule FROM pgtempus.transport_mode WHERE id = "+str(transport_mode_id)+")"
                        q=QtSql.QSqlQuery(self.db)
                        q.exec_(unicode(s1))
                        q.next()
                        if (int(q.value(0)) == 0):
                            iface.messageBar().pushMessage("Error", "Tentative d'insertion d'un véhicule sur un noeud où le stationnement n'est pas autorisé. Définir d'abord le stationnement.", level=Qgis.Warning)
                        else:
                            s2="INSERT INTO pgtempus.vehicle(node_id, transport_mode_id) VALUES("+str(f["id"])+", "+str(transport_mode_id)+")"
                            q=QtSql.QSqlQuery(self.db)
                            q.exec_(unicode(s2))
                    self.caller.iface.mapCanvas().refreshAllLayers()
                else:
                    iface.messageBar().pushMessage("Error", "Aucun noeud n'est sélectionné.", level=Qgis.Warning)
            else:
                iface.messageBar().pushMessage("Error", "La couche des noeuds n'est pas sélectionnée.", level=Qgis.Warning)
    
    def _slotPushButtonRemoveSelectionClicked(self):
        transport_mode_id = self.caller.modelVehiclesTransportMode.record(self.ui.comboBoxTransportMode.currentIndex()).value("id")
        for layer in self.iface.layerTreeView().selectedLayers():
            if (layer.name() == "Noeuds"):
                features = layer.selectedFeatures()
                if (features != []):
                    for f in features:
                        s="DELETE FROM pgtempus.vehicle WHERE node_id = "+str(f["id"])+" AND transport_mode_id = "+str(transport_mode_id)
                        q=QtSql.QSqlQuery(self.db)
                        q.exec_(unicode(s) )
                    self.caller.iface.mapCanvas().refreshAllLayers()
                else:
                    iface.messageBar().pushMessage("Error", "Aucun noeud n'est sélectionné.", level=Qgis.Warning)
            else:
                iface.messageBar().pushMessage("Error", "La couche des noeuds n'est pas sélectionnée.", level=Qgis.Warning)
    
        
    def _slotPushButtonClearClicked(self):
        transport_mode_id = self.caller.modelVehiclesTransportMode.record(self.ui.comboBoxTransportMode.currentIndex()).value("id")
        s="DELETE FROM pgtempus.vehicle WHERE transport_mode_id = "+str(transport_mode_id)
        q=QtSql.QSqlQuery(self.db)
        q.exec_(unicode(s))
        self.caller.iface.mapCanvas().refreshAllLayers()
    
    