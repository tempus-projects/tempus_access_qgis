# -*- coding: utf-8 -*-
"""
/***************************************************************************
 PluginTempusAccess
                                 A QGIS plugin
 Analyse de l'offre de transport en commun
                              -------------------
        begin                : 2016-10-22
        git sha              : $Format:%H$
        copyright            : (C) 2016 by Cerema
        email                : Aurelie.bousquet@cerema.fr, Patrick.Palmier@cerema.fr, helene.ly@cerema.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# import the PyQt and QGIS libraries
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from qgis.PyQt.QtWidgets import QDialog, QDialogButtonBox
from PyQt5.Qt import *
from PyQt5 import QtSql
from qgis.core import *
from qgis.utils import iface

# Initialize Qt resources from file resources.py
from .config import *
from .functions import *

import sys
import string
import os

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "\\forms")
from export_dialog import Ui_Dialog

class export_dialog(QDialog): 

    def __init__(self, caller, iface):
        QDialog.__init__(self)
        self.ui= Ui_Dialog()
        self.ui.setupUi(self)
        self.caller = caller
        self.db = caller.db
        self.iface = caller.iface
        
        self.plugin_dir = self.caller.plugin_dir 
        self.icon_dir = self.caller.icon_dir 
        
        self.ui.listViewSources.setModel(self.caller.modelPTSource)
        self.ui.comboBoxFormat.setModel(self.caller.modelExportFormat)
        
        self.ui.pushButtonExecute.setIcon(QIcon(self.icon_dir + "/icon_exe.svg")) 
        self.ui.pushButtonGenerate.setIcon(QIcon(self.icon_dir + "/icon_generate.png")) 
        
        self._connectSlots()
    
    def _connectSlots(self):
        self.ui.comboBoxFormat.currentIndexChanged.connect(self._slotComboBoxFormatCurrentIndexChanged)
        self.ui.pushButtonExecute.clicked.connect(self._slotPushButtonExecuteClicked)
        self.ui.pushButtonGenerate.clicked.connect(self._slotPushButtonGenerateClicked)
        self.ui.pushButtonChoose.clicked.connect(self._slotPushButtonChooseClicked)
    
    def setDataType(self, string):
        self.data_type = string
        if (string == 'road'):
            self.ui.listViewSources.setModel(self.caller.modelRoadSource)
            self.model = self.caller.modelRoadSource
        elif (string == 'pt'):
            self.ui.listViewSources.setModel(self.caller.modelPTSource)
            self.model = self.caller.modelPTSource
        elif (string == 'poi'):
            self.ui.listViewSources.setModel(self.caller.modelPOISource)    
            self.model = self.caller.modelPOISource        
        elif (string == 'zoning'):
            self.ui.listViewSources.setModel(self.caller.modelZoningSource)
            self.model = self.caller.modelZoningSource
    
    def _slotComboBoxFormatCurrentIndexChanged(self, indexChosenLine):
        self.format = self.caller.modelExportFormat.record(indexChosenLine).value("data_format")
        self.path_type = self.caller.modelExportFormat.record(self.ui.comboBoxFormat.currentIndex()).value("path_type")
    
    def prepareQuery(self):
        if (self.ui.listViewSources.selectionModel().hasSelection()):
            s = ""
            

            self.cmd=[PYTHON, TEMPUSLOADER, "--action", "export", 
                                            "--data-type", self.data_type, 
                                            "--data-format", self.format,
                                            "--dbstring", "host="+self.caller.db.hostName()+" user="+self.caller.db.userName()+" dbname="+self.caller.db.databaseName()+" port="+str(self.caller.db.port())]            
            self.cmd.append("--source-name")
            for item in self.ui.listViewSources.selectionModel().selectedRows():
                self.cmd.append(self.model.record(item.row()).value("name"))
            self.cmd.append("--path")
            if (self.path_type=="directory"):
                self.cmd.append(self.cheminComplet)
            else:
                self.cmd.append(self.cheminComplet[0])
        else:
            box = QMessageBox()
            box.setModal(True)
            box.setText(u"Sélectionnez au moins une source de données à exporter") 
        
    def _slotPushButtonExecuteClicked(self):
        if self.cheminComplet != "": 
            self.prepareQuery()
            rc = execute_external_cmd( self.cmd )
            box = QMessageBox()
            box.setModal(True)
            print(rc)
            if (rc==0):
                box.setText(u"L'export de la source est terminé.") 
            else:
                box.setText(u"Erreur pendant l'export.")
            box.exec_()
    
    def _slotPushButtonGenerateClicked(self):
        self.prepareQuery()
        line_cmd = transform_external_cmd( self.cmd )
        
        NomFichierComplet=["", ""]
        NomFichierComplet = QFileDialog.getSaveFileName(caption = "Enregistrer sous...", directory=self.plugin_dir, filter = "BATCH files (*.bat)")
        if (NomFichierComplet[0]!=""):
            f = open(NomFichierComplet[0], "w")
            f.write("call C:/OSGeo4W64/bin/o4w_env.bat\ncall py3_env.bat\n" + unicode(line_cmd) + "\npause")
            f.close()
            
            box = QMessageBox()
            box.setText(u"La requête a été générée dans le fichier cible")
            box.exec_()
        
    def updatePushButtons(self):
        if (self.ui.labelFile.text() != ''):
            self.ui.pushButtonExecute.setEnabled(True)
            self.ui.pushButtonGenerate.setEnabled(True)
        else:
            self.ui.pushButtonExecute.setEnabled(False)
            self.ui.pushButtonGenerate.setEnabled(False)
    
    def _slotPushButtonChooseClicked(self):
        if (self.path_type=="directory"):
            self.cheminComplet = QFileDialog.getExistingDirectory(options=QFileDialog.ShowDirsOnly, directory=self.caller.last_dir)
            self.caller.last_dir = os.path.dirname(self.cheminComplet)
            self.ui.labelFile.setText(os.path.basename(self.cheminComplet))        
        else:
            self.cheminComplet = QFileDialog.getSaveFileName(caption = "Enregistrer sous...", directory=self.plugin_dir, filter = "Zip files (*.zip)")
            self.caller.last_dir = os.path.dirname(self.cheminComplet[0])
            self.ui.labelFile.setText(os.path.basename(self.cheminComplet[0]))        
        self.updatePushButtons()    
    
    def _slotPushButtonGenerateClicked(self):
        self.prepareQuery()
        line_cmd = transform_external_cmd( self.cmd )
        
        NomFichierComplet=["", ""]
        NomFichierComplet = QFileDialog.getSaveFileName(caption = "Enregistrer sous...", directory=self.plugin_dir, filter = "BATCH files (*.bat)")
        if (NomFichierComplet[0]!=""):
            f = open(NomFichierComplet[0], "w")
            f.write("call C:/OSGeo4W64/bin/o4w_env.bat\ncall py3_env.bat\n" + unicode(line_cmd) + "\npause")
            f.close()
            
            box = QMessageBox()
            box.setText(u"La requête a été générée dans le fichier cible")
            box.exec_()
    
        
        