# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Code\Tempus\tempus_access_qgis\forms\get_served_pop_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(481, 173)
        self.layoutWidget_2 = QtWidgets.QWidget(Dialog)
        self.layoutWidget_2.setGeometry(QtCore.QRect(10, 10, 461, 121))
        self.layoutWidget_2.setObjectName("layoutWidget_2")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.layoutWidget_2)
        self.gridLayout_3.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.label_5 = QtWidgets.QLabel(self.layoutWidget_2)
        self.label_5.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_5.setObjectName("label_5")
        self.gridLayout_3.addWidget(self.label_5, 4, 0, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_3.addItem(spacerItem, 2, 1, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.layoutWidget_2)
        self.label_2.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_2.setObjectName("label_2")
        self.gridLayout_3.addWidget(self.label_2, 1, 0, 1, 1)
        self.comboBoxServedAreaTableName = QtWidgets.QComboBox(self.layoutWidget_2)
        self.comboBoxServedAreaTableName.setObjectName("comboBoxServedAreaTableName")
        self.gridLayout_3.addWidget(self.comboBoxServedAreaTableName, 3, 1, 1, 1)
        self.comboBoxZoningTableName = QtWidgets.QComboBox(self.layoutWidget_2)
        self.comboBoxZoningTableName.setObjectName("comboBoxZoningTableName")
        self.gridLayout_3.addWidget(self.comboBoxZoningTableName, 0, 1, 1, 1)
        self.comboBoxZoningTablePopField = QtWidgets.QComboBox(self.layoutWidget_2)
        self.comboBoxZoningTablePopField.setObjectName("comboBoxZoningTablePopField")
        self.gridLayout_3.addWidget(self.comboBoxZoningTablePopField, 1, 1, 1, 1)
        self.label_6 = QtWidgets.QLabel(self.layoutWidget_2)
        self.label_6.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_6.setObjectName("label_6")
        self.gridLayout_3.addWidget(self.label_6, 0, 0, 1, 1)
        self.lineEditServedTablePopField = QtWidgets.QLineEdit(self.layoutWidget_2)
        self.lineEditServedTablePopField.setObjectName("lineEditServedTablePopField")
        self.gridLayout_3.addWidget(self.lineEditServedTablePopField, 4, 1, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.layoutWidget_2)
        self.label_3.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_3.setObjectName("label_3")
        self.gridLayout_3.addWidget(self.label_3, 3, 0, 1, 1)
        self.pushButtonCalculate = QtWidgets.QPushButton(Dialog)
        self.pushButtonCalculate.setGeometry(QtCore.QRect(360, 140, 111, 23))
        self.pushButtonCalculate.setObjectName("pushButtonCalculate")
        self.pushButtonGenerate = QtWidgets.QPushButton(Dialog)
        self.pushButtonGenerate.setGeometry(QtCore.QRect(240, 140, 111, 23))
        self.pushButtonGenerate.setObjectName("pushButtonGenerate")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Calcul des populations desservies"))
        self.label_5.setText(_translate("Dialog", "Champ population à créer"))
        self.label_2.setText(_translate("Dialog", "Champ population"))
        self.label_6.setText(_translate("Dialog", "Table des données de population"))
        self.label_3.setText(_translate("Dialog", "Table des zones de desserte"))
        self.pushButtonCalculate.setText(_translate("Dialog", "Calculer"))
        self.pushButtonGenerate.setText(_translate("Dialog", "Générer la requête"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

