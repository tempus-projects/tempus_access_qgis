# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Code\Tempus\tempus_access_qgis\forms\db_properties_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(260, 139)
        self.layoutWidget = QtWidgets.QWidget(Dialog)
        self.layoutWidget.setGeometry(QtCore.QRect(10, 10, 241, 91))
        self.layoutWidget.setObjectName("layoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.layoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.label_197 = QtWidgets.QLabel(self.layoutWidget)
        self.label_197.setStyleSheet("color: rgb(0, 0, 0);")
        self.label_197.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.label_197.setObjectName("label_197")
        self.gridLayout.addWidget(self.label_197, 0, 0, 1, 1)
        self.labelNetworksDataSize = QtWidgets.QLabel(self.layoutWidget)
        self.labelNetworksDataSize.setObjectName("labelNetworksDataSize")
        self.gridLayout.addWidget(self.labelNetworksDataSize, 0, 1, 1, 1)
        self.label_196 = QtWidgets.QLabel(self.layoutWidget)
        self.label_196.setStyleSheet("color: rgb(0, 0, 0);")
        self.label_196.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.label_196.setObjectName("label_196")
        self.gridLayout.addWidget(self.label_196, 1, 0, 1, 1)
        self.labelResultsDataSize = QtWidgets.QLabel(self.layoutWidget)
        self.labelResultsDataSize.setObjectName("labelResultsDataSize")
        self.gridLayout.addWidget(self.labelResultsDataSize, 1, 1, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.layoutWidget)
        self.label_2.setStyleSheet("color: rgb(0, 0, 0);")
        self.label_2.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 2, 0, 1, 1)
        self.labelCostsDataSize = QtWidgets.QLabel(self.layoutWidget)
        self.labelCostsDataSize.setObjectName("labelCostsDataSize")
        self.gridLayout.addWidget(self.labelCostsDataSize, 2, 1, 1, 1)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(90, 110, 161, 23))
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Close)
        self.buttonBox.setObjectName("buttonBox")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Propriétés de la base"))
        self.label_197.setToolTip(_translate("Dialog", "<html><head/><body><p>Cette fenêtre permet de surveiller l\'espace disque utilisé par la base de données. Afin d\'éviter de saturer le disque dur de la machine, il est peut être nécessaire de faire du vide quand l\'espace occupé par la base est trop important (suppression d\'indicateurs calculés, de données auxiliaires, etc.). </p></body></html>"))
        self.label_197.setText(_translate("Dialog", "Taille base de données de réseaux"))
        self.labelNetworksDataSize.setText(_translate("Dialog", "..."))
        self.label_196.setText(_translate("Dialog", "Taille base de données d\'indicateurs"))
        self.labelResultsDataSize.setText(_translate("Dialog", "..."))
        self.label_2.setText(_translate("Dialog", "Taille base de données de coûts"))
        self.labelCostsDataSize.setText(_translate("Dialog", "..."))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

