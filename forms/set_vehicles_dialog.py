# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Code\Tempus\tempus_access_qgis\forms\set_vehicles_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(476, 68)
        self.pushButtonClear = QtWidgets.QPushButton(Dialog)
        self.pushButtonClear.setGeometry(QtCore.QRect(380, 40, 82, 23))
        self.pushButtonClear.setObjectName("pushButtonClear")
        self.layoutWidget = QtWidgets.QWidget(Dialog)
        self.layoutWidget.setGeometry(QtCore.QRect(11, 10, 451, 25))
        self.layoutWidget.setObjectName("layoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.layoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.labelReq = QtWidgets.QLabel(self.layoutWidget)
        self.labelReq.setEnabled(True)
        self.labelReq.setObjectName("labelReq")
        self.horizontalLayout.addWidget(self.labelReq)
        self.comboBoxTransportMode = QtWidgets.QComboBox(self.layoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.comboBoxTransportMode.sizePolicy().hasHeightForWidth())
        self.comboBoxTransportMode.setSizePolicy(sizePolicy)
        self.comboBoxTransportMode.setObjectName("comboBoxTransportMode")
        self.horizontalLayout.addWidget(self.comboBoxTransportMode)
        self.pushButtonAddSelection = QtWidgets.QPushButton(self.layoutWidget)
        self.pushButtonAddSelection.setMaximumSize(QtCore.QSize(30, 16777215))
        self.pushButtonAddSelection.setText("")
        self.pushButtonAddSelection.setObjectName("pushButtonAddSelection")
        self.horizontalLayout.addWidget(self.pushButtonAddSelection)
        self.pushButtonRemoveSelection = QtWidgets.QPushButton(self.layoutWidget)
        self.pushButtonRemoveSelection.setEnabled(True)
        self.pushButtonRemoveSelection.setMaximumSize(QtCore.QSize(30, 16777215))
        self.pushButtonRemoveSelection.setText("")
        self.pushButtonRemoveSelection.setObjectName("pushButtonRemoveSelection")
        self.horizontalLayout.addWidget(self.pushButtonRemoveSelection)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Positionner des véhicules dans le réseau"))
        self.pushButtonClear.setToolTip(_translate("Dialog", "<html><head/><body><p>Supprime tous les véhicules du réseau</p></body></html>"))
        self.pushButtonClear.setText(_translate("Dialog", "Tout supprimer"))
        self.labelReq.setText(_translate("Dialog", "Mode de transport"))
        self.pushButtonAddSelection.setToolTip(_translate("Dialog", "<html><head/><body><p>Ajoute des véhicules pour le mode choisi, sur les noeuds sélectionnés</p></body></html>"))
        self.pushButtonRemoveSelection.setToolTip(_translate("Dialog", "<html><head/><body><p>Supprime les véhicules situés sur les noeuds sélectionnés, pour le mode choisi</p></body></html>"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

