# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Code\Tempus\tempus_access_qgis\forms\config_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(649, 142)
        self.layoutWidget = QtWidgets.QWidget(Dialog)
        self.layoutWidget.setGeometry(QtCore.QRect(10, 10, 631, 91))
        self.layoutWidget.setObjectName("layoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.layoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.pushButton_exe2 = QtWidgets.QPushButton(self.layoutWidget)
        self.pushButton_exe2.setMaximumSize(QtCore.QSize(35, 16777215))
        self.pushButton_exe2.setObjectName("pushButton_exe2")
        self.gridLayout.addWidget(self.pushButton_exe2, 1, 2, 1, 1)
        self.lineEdit_path = QtWidgets.QLineEdit(self.layoutWidget)
        self.lineEdit_path.setObjectName("lineEdit_path")
        self.gridLayout.addWidget(self.lineEdit_path, 0, 1, 1, 1)
        self.lineEdit_path2 = QtWidgets.QLineEdit(self.layoutWidget)
        self.lineEdit_path2.setObjectName("lineEdit_path2")
        self.gridLayout.addWidget(self.lineEdit_path2, 1, 1, 1, 1)
        self.label_197 = QtWidgets.QLabel(self.layoutWidget)
        self.label_197.setStyleSheet("color: rgb(0, 0, 0);")
        self.label_197.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_197.setObjectName("label_197")
        self.gridLayout.addWidget(self.label_197, 0, 0, 1, 1)
        self.label_196 = QtWidgets.QLabel(self.layoutWidget)
        self.label_196.setStyleSheet("color: rgb(0, 0, 0);")
        self.label_196.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_196.setObjectName("label_196")
        self.gridLayout.addWidget(self.label_196, 1, 0, 1, 1)
        self.pushButton_exe1 = QtWidgets.QPushButton(self.layoutWidget)
        self.pushButton_exe1.setMaximumSize(QtCore.QSize(35, 16777215))
        self.pushButton_exe1.setObjectName("pushButton_exe1")
        self.gridLayout.addWidget(self.pushButton_exe1, 0, 2, 1, 1)
        self.label = QtWidgets.QLabel(self.layoutWidget)
        self.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 2, 0, 1, 1)
        self.lineEdit = QtWidgets.QLineEdit(self.layoutWidget)
        self.lineEdit.setText("")
        self.lineEdit.setObjectName("lineEdit")
        self.gridLayout.addWidget(self.lineEdit, 2, 1, 1, 1)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(480, 110, 161, 23))
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Close)
        self.buttonBox.setObjectName("buttonBox")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Configuration des exécutables"))
        self.pushButton_exe2.setText(_translate("Dialog", "..."))
        self.lineEdit_path.setToolTip(_translate("Dialog", "<html><head/><body><p>Par défaut, les exécutable du dossier &quot;exe&quot; du plugin sont utilisés. </p></body></html>"))
        self.lineEdit_path2.setToolTip(_translate("Dialog", "<html><head/><body><p>Par défaut, les exécutable du dossier &quot;exe&quot; du plugin sont utilisés. </p></body></html>"))
        self.label_197.setToolTip(_translate("Dialog", "<html><head/><body><p>Cette fenêtre permet de surveiller l\'espace disque utilisé par la base de données. Afin d\'éviter de saturer le disque dur de la machine, il est peut être nécessaire de faire du vide quand l\'espace occupé par la base est trop important (suppression d\'indicateurs calculés, de données auxiliaires, etc.). </p></body></html>"))
        self.label_197.setText(_translate("Dialog", "Dossier contenant les exécutables createdb, dropdb, initdb, pg_restore et pg_dump"))
        self.label_196.setText(_translate("Dialog", "Dossier contenant les exécutables shp2pgsql, pgsql2shp et psql"))
        self.pushButton_exe1.setText(_translate("Dialog", "..."))
        self.label.setText(_translate("Dialog", "Nom du schéma de stockage temporaire des données"))
        self.lineEdit.setToolTip(_translate("Dialog", "<html><head/><body><p>Par défaut, _temp.</p></body></html>"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

