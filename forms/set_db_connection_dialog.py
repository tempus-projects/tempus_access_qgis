# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Code\Tempus\tempus_access_qgis\forms\set_db_connection_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(418, 199)
        self.layoutWidget_155 = QtWidgets.QWidget(Dialog)
        self.layoutWidget_155.setGeometry(QtCore.QRect(10, 10, 391, 141))
        self.layoutWidget_155.setObjectName("layoutWidget_155")
        self.gridLayout_16 = QtWidgets.QGridLayout(self.layoutWidget_155)
        self.gridLayout_16.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_16.setObjectName("gridLayout_16")
        self.label_192 = QtWidgets.QLabel(self.layoutWidget_155)
        self.label_192.setStyleSheet("color: rgb(0, 0, 0);")
        self.label_192.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_192.setObjectName("label_192")
        self.gridLayout_16.addWidget(self.label_192, 0, 0, 1, 1)
        self.label_194 = QtWidgets.QLabel(self.layoutWidget_155)
        self.label_194.setStyleSheet("color: rgb(0, 0, 0);")
        self.label_194.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_194.setObjectName("label_194")
        self.gridLayout_16.addWidget(self.label_194, 3, 0, 1, 1)
        self.lineEdit_port = QtWidgets.QLineEdit(self.layoutWidget_155)
        self.lineEdit_port.setObjectName("lineEdit_port")
        self.gridLayout_16.addWidget(self.lineEdit_port, 1, 1, 1, 2)
        self.lineEdit_host = QtWidgets.QLineEdit(self.layoutWidget_155)
        self.lineEdit_host.setStyleSheet("color: rgb(0, 0, 0);")
        self.lineEdit_host.setObjectName("lineEdit_host")
        self.gridLayout_16.addWidget(self.lineEdit_host, 0, 1, 1, 2)
        self.lineEdit_login = QtWidgets.QLineEdit(self.layoutWidget_155)
        self.lineEdit_login.setStyleSheet("color: rgb(0, 0, 0);")
        self.lineEdit_login.setText("")
        self.lineEdit_login.setObjectName("lineEdit_login")
        self.gridLayout_16.addWidget(self.lineEdit_login, 2, 1, 1, 2)
        self.lineEdit_pwd = QtWidgets.QLineEdit(self.layoutWidget_155)
        self.lineEdit_pwd.setStyleSheet("color: rgb(0, 0, 0);")
        self.lineEdit_pwd.setText("")
        self.lineEdit_pwd.setObjectName("lineEdit_pwd")
        self.gridLayout_16.addWidget(self.lineEdit_pwd, 3, 1, 1, 2)
        self.label_193 = QtWidgets.QLabel(self.layoutWidget_155)
        self.label_193.setStyleSheet("color: rgb(0, 0, 0);")
        self.label_193.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_193.setObjectName("label_193")
        self.gridLayout_16.addWidget(self.label_193, 2, 0, 1, 1)
        self.label = QtWidgets.QLabel(self.layoutWidget_155)
        self.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label.setObjectName("label")
        self.gridLayout_16.addWidget(self.label, 1, 0, 1, 1)
        self.horizontalLayoutWidget = QtWidgets.QWidget(Dialog)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(10, 160, 391, 31))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.pushButtonRestart = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.pushButtonRestart.setObjectName("pushButtonRestart")
        self.horizontalLayout.addWidget(self.pushButtonRestart)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.buttonBox = QtWidgets.QDialogButtonBox(self.horizontalLayoutWidget)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Apply|QtWidgets.QDialogButtonBox.Cancel)
        self.buttonBox.setObjectName("buttonBox")
        self.horizontalLayout.addWidget(self.buttonBox)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)
        Dialog.setTabOrder(self.lineEdit_host, self.lineEdit_port)
        Dialog.setTabOrder(self.lineEdit_port, self.lineEdit_login)
        Dialog.setTabOrder(self.lineEdit_login, self.lineEdit_pwd)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Paramètres de connexion au serveur de bases de données"))
        self.label_192.setText(_translate("Dialog", "Serveur"))
        self.label_194.setText(_translate("Dialog", "Mot de passe"))
        self.lineEdit_port.setText(_translate("Dialog", "55432"))
        self.lineEdit_host.setToolTip(_translate("Dialog", "<html><head/><body><p>GTFSAnalyst utilise une base de données PostgreSQL-PostGIS pour le stockage des données et le calcul <br/>des indicateurs. Par défaut, elle est configurée pour être stockée localement sur la machine (installation PostgreSQL &quot;PGLite&quot;, base &quot;gtfsanalyst&quot; sur le port 55432 avec le nom d\'utilisateur de la session Windows et sans mot de passe).</p><p>Ce paramétrage peut être modifié ici, pour configurer le stockage de la base de données de l\'application<br/>sur un autre serveur ou dans une autre base.</p><p><br/></p></body></html>"))
        self.lineEdit_host.setText(_translate("Dialog", "localhost"))
        self.label_193.setText(_translate("Dialog", "Login"))
        self.label.setText(_translate("Dialog", "Port"))
        self.pushButtonRestart.setText(_translate("Dialog", "Redémarrer (pglite)"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

