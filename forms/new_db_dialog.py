# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Code\Tempus\tempus_access_qgis\forms\new_db_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(508, 144)
        self.layoutWidget_2 = QtWidgets.QWidget(Dialog)
        self.layoutWidget_2.setGeometry(QtCore.QRect(10, 10, 491, 61))
        self.layoutWidget_2.setObjectName("layoutWidget_2")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.layoutWidget_2)
        self.gridLayout_3.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.label_6 = QtWidgets.QLabel(self.layoutWidget_2)
        self.label_6.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_6.setObjectName("label_6")
        self.gridLayout_3.addWidget(self.label_6, 0, 0, 1, 1)
        self.lineEditNewDB = QtWidgets.QLineEdit(self.layoutWidget_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lineEditNewDB.sizePolicy().hasHeightForWidth())
        self.lineEditNewDB.setSizePolicy(sizePolicy)
        self.lineEditNewDB.setObjectName("lineEditNewDB")
        self.gridLayout_3.addWidget(self.lineEditNewDB, 0, 1, 1, 2)
        self.label = QtWidgets.QLabel(self.layoutWidget_2)
        self.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label.setObjectName("label")
        self.gridLayout_3.addWidget(self.label, 1, 0, 1, 1)
        self.spinBoxSRID = QtWidgets.QSpinBox(self.layoutWidget_2)
        self.spinBoxSRID.setMinimum(1000)
        self.spinBoxSRID.setMaximum(10000)
        self.spinBoxSRID.setProperty("value", 2154)
        self.spinBoxSRID.setObjectName("spinBoxSRID")
        self.gridLayout_3.addWidget(self.spinBoxSRID, 1, 1, 1, 2)
        self.layoutWidget = QtWidgets.QWidget(Dialog)
        self.layoutWidget.setGeometry(QtCore.QRect(180, 80, 321, 20))
        self.layoutWidget.setObjectName("layoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.layoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.radioButton_base_vierge = QtWidgets.QRadioButton(self.layoutWidget)
        self.radioButton_base_vierge.setChecked(True)
        self.radioButton_base_vierge.setObjectName("radioButton_base_vierge")
        self.horizontalLayout.addWidget(self.radioButton_base_vierge)
        self.radioButton_restore = QtWidgets.QRadioButton(self.layoutWidget)
        self.radioButton_restore.setObjectName("radioButton_restore")
        self.horizontalLayout.addWidget(self.radioButton_restore)
        self.layoutWidget1 = QtWidgets.QWidget(Dialog)
        self.layoutWidget1.setGeometry(QtCore.QRect(177, 110, 321, 25))
        self.layoutWidget1.setObjectName("layoutWidget1")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.layoutWidget1)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.pushButtonGenerate = QtWidgets.QPushButton(self.layoutWidget1)
        self.pushButtonGenerate.setObjectName("pushButtonGenerate")
        self.horizontalLayout_2.addWidget(self.pushButtonGenerate)
        self.pushButtonExecute = QtWidgets.QPushButton(self.layoutWidget1)
        self.pushButtonExecute.setObjectName("pushButtonExecute")
        self.horizontalLayout_2.addWidget(self.pushButtonExecute)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Nouvelle base de données"))
        self.label_6.setText(_translate("Dialog", "tempusaccess_"))
        self.label.setText(_translate("Dialog", "SRID"))
        self.radioButton_base_vierge.setText(_translate("Dialog", "Créer une base vierge"))
        self.radioButton_restore.setText(_translate("Dialog", "Restaurer une sauvegarde"))
        self.pushButtonGenerate.setText(_translate("Dialog", "  Générer la requête"))
        self.pushButtonExecute.setToolTip(_translate("Dialog", "<html><head/><body><p>Saisir un nom de base pour activer la fonction. Si la base existe déjà, efface toutes les données présentes dans la base.</p></body></html>"))
        self.pushButtonExecute.setText(_translate("Dialog", "   Exécuter"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

