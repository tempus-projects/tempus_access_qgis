# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Code\Tempus\tempus_access_qgis\forms\mapmatch_pt_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(323, 181)
        self.layoutWidget_2 = QtWidgets.QWidget(Dialog)
        self.layoutWidget_2.setGeometry(QtCore.QRect(20, 10, 291, 121))
        self.layoutWidget_2.setObjectName("layoutWidget_2")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.layoutWidget_2)
        self.gridLayout_3.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.comboBoxPTSource = QtWidgets.QComboBox(self.layoutWidget_2)
        self.comboBoxPTSource.setObjectName("comboBoxPTSource")
        self.gridLayout_3.addWidget(self.comboBoxPTSource, 0, 1, 1, 1)
        self.label_5 = QtWidgets.QLabel(self.layoutWidget_2)
        self.label_5.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_5.setObjectName("label_5")
        self.gridLayout_3.addWidget(self.label_5, 0, 0, 1, 1)
        self.label = QtWidgets.QLabel(self.layoutWidget_2)
        self.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label.setObjectName("label")
        self.gridLayout_3.addWidget(self.label, 1, 0, 1, 1)
        self.listViewPTTripTypes = QtWidgets.QListView(self.layoutWidget_2)
        self.listViewPTTripTypes.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
        self.listViewPTTripTypes.setObjectName("listViewPTTripTypes")
        self.gridLayout_3.addWidget(self.listViewPTTripTypes, 1, 1, 1, 1)
        self.pushButtonOK = QtWidgets.QPushButton(Dialog)
        self.pushButtonOK.setGeometry(QtCore.QRect(230, 140, 81, 31))
        self.pushButtonOK.setObjectName("pushButtonOK")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Recaler les tracés sur les réseaux routiers"))
        self.label_5.setText(_translate("Dialog", "Réseau TC"))
        self.label.setText(_translate("Dialog", "Types services"))
        self.pushButtonOK.setText(_translate("Dialog", "OK"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

