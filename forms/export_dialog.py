# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Code\Tempus\tempus_access_qgis\forms\export_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(388, 212)
        self.verticalLayoutWidget = QtWidgets.QWidget(Dialog)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(10, 10, 371, 162))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.verticalLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.labelFile = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.labelFile.setText("")
        self.labelFile.setObjectName("labelFile")
        self.gridLayout.addWidget(self.labelFile, 2, 2, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_2.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 2, 0, 1, 1)
        self.comboBoxFormat = QtWidgets.QComboBox(self.verticalLayoutWidget)
        self.comboBoxFormat.setObjectName("comboBoxFormat")
        self.gridLayout.addWidget(self.comboBoxFormat, 1, 1, 1, 2)
        self.label_3 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_3.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 1, 0, 1, 1)
        self.pushButtonChoose = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.pushButtonChoose.setObjectName("pushButtonChoose")
        self.gridLayout.addWidget(self.pushButtonChoose, 2, 1, 1, 1)
        self.listViewSources = QtWidgets.QListView(self.verticalLayoutWidget)
        self.listViewSources.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
        self.listViewSources.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.listViewSources.setObjectName("listViewSources")
        self.gridLayout.addWidget(self.listViewSources, 0, 1, 1, 2)
        self.widget = QtWidgets.QWidget(Dialog)
        self.widget.setGeometry(QtCore.QRect(10, 180, 371, 25))
        self.widget.setObjectName("widget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.widget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.pushButtonGenerate = QtWidgets.QPushButton(self.widget)
        self.pushButtonGenerate.setEnabled(False)
        self.pushButtonGenerate.setObjectName("pushButtonGenerate")
        self.horizontalLayout.addWidget(self.pushButtonGenerate)
        self.pushButtonExecute = QtWidgets.QPushButton(self.widget)
        self.pushButtonExecute.setEnabled(False)
        self.pushButtonExecute.setObjectName("pushButtonExecute")
        self.horizontalLayout.addWidget(self.pushButtonExecute)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Exporter une offre de transport collectif"))
        self.label.setText(_translate("Dialog", "Sources à exporter"))
        self.label_2.setText(_translate("Dialog", "Fichier d\'export"))
        self.label_3.setText(_translate("Dialog", "Format"))
        self.pushButtonChoose.setText(_translate("Dialog", "Parcourir..."))
        self.pushButtonGenerate.setText(_translate("Dialog", "  Générer la requête..."))
        self.pushButtonExecute.setText(_translate("Dialog", "  Exporter"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

