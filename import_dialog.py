# -*- coding: utf-8 -*-
"""
/***************************************************************************
 PluginTempusAccess
                                 A QGIS plugin
 Analyse de l'offre de transport en commun
                              -------------------
        begin                : 2016-10-22
        git sha              : $Format:%H$
        copyright            : (C) 2016 by Cerema
        email                : Aurelie.bousquet@cerema.fr, Patrick.Palmier@cerema.fr, helene.ly@cerema.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# import the PyQt and QGIS libraries
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from qgis.PyQt.QtWidgets import QDialog, QDialogButtonBox
from PyQt5.Qt import *
from PyQt5 import QtSql
from qgis.core import *
from qgis.utils import iface

# Initialize Qt resources from file resources.py
from .config import *
from .functions import *

import sys
import string
import os

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "\\forms")
from import_dialog import Ui_Dialog

class import_dialog(QDialog): 

    def __init__(self, caller, iface):
        QDialog.__init__(self) 
        self.ui = Ui_Dialog() 
        self.ui.setupUi(self)
        
        self.caller = caller 
        self.iface = self.caller.iface 
        
        self.plugin_dir = self.caller.plugin_dir 
        self.icon_dir = self.caller.icon_dir 
        
        self.ui.comboBoxFormat.setModel(self.caller.modelImportFormat) 
        self.ui.comboBoxFormatVersion.setModel(self.caller.modelImportFormatVersion) 
        self.ui.comboBoxEncoding.setModel(self.caller.modelEncoding) 
        self.ui.comboBoxPOIType.setModel(self.caller.modelPOIType)         
        self.ui.comboBoxFormat.setCurrentIndex(0) 
        self.ui.comboBoxFormatVersion.setCurrentIndex(0) 
        self.ui.comboBoxEncoding.setCurrentIndex(0) 
        self.ui.comboBoxPOIType.setCurrentIndex(0) 
        
        self._connectSlots() 
        
        self.ui.pushButtonExecute.setIcon(QIcon(self.icon_dir + "/icon_exe.svg"))
        self.ui.pushButtonGenerate.setIcon(QIcon(self.icon_dir + "/icon_generate.png"))
    
    def _connectSlots(self):
        self.ui.comboBoxFormat.currentIndexChanged.connect(self._slotComboBoxFormatCurrentIndexChanged)
        self.ui.comboBoxFormatVersion.currentIndexChanged.connect(self._slotComboBoxFormatVersionCurrentIndexChanged)
        self.ui.pushButtonChoose.clicked.connect(self._slotPushButtonChooseClicked)
        self.ui.pushButtonGenerate.clicked.connect(self._slotPushButtonGenerateClicked)
        self.ui.pushButtonExecute.clicked.connect(self._slotPushButtonImportClicked)
        self.ui.lineEditSourceName.textChanged.connect(self._slotLineEditSourceNameTextChanged)
        self.ui.checkBoxMerge.toggled.connect(self._slotCheckBoxMergeToggled) 
        self.ui.comboBoxSource.currentIndexChanged.connect(self._slotComboBoxSourceCurrentIndexChanged)
        self.ui.checkBoxGeomSimplify.toggled.connect(self._slotCheckBoxGeomSimplifyToggled) 
        self.ui.checkBoxLink.toggled.connect(self._slotCheckBoxLinkToggled)
        self.ui.checkBoxFilter.toggled.connect(self._slotCheckBoxFilterToggled)
        self.ui.checkBoxPrefix.toggled.connect(self._slotCheckBoxPrefixToggled)
    
    def setDataType(self, string):
        self.data_type = string
        self.ui.labelFile.setText("")
        self.cheminFichierComplet=""
        self.ui.lineEditSourceComment.setText("")
        self.ui.lineEditPrefix.setEnabled(False)
        self.ui.lineEditPrefix.setText("")
        self.ui.lineEditFilter.setEnabled(False)
        self.ui.lineEditFilter.setText("")
        self.ui.labelFile.setText("")
        self.ui.pushButtonExecute.setEnabled(False)
        self.ui.labelVisumModes1.setEnabled(False)
        self.ui.labelVisumModes2.setEnabled(False)
        self.ui.lineEditVisumModes.setEnabled(False)
        self.ui.lineEditVisumModes.setText("")
        self.ui.lineEditIdField.setText("")
        self.ui.lineEditIdField.setEnabled(False)
        self.ui.labelIdField.setEnabled(False)
        self.ui.lineEditNameField.setText("")
        self.ui.lineEditNameField.setEnabled(False)
        self.ui.labelNameField.setEnabled(False) 
        self.ui.checkBoxMerge.setChecked(False)
        
        if (string == 'road'):
            self.ui.comboBoxSource.setModel(self.caller.modelRoadSource)
            self.ui.labelIdField.setEnabled(False)
            self.ui.lineEditIdField.setEnabled(False)
            self.ui.labelNameField.setEnabled(False)
            self.ui.lineEditNameField.setEnabled(False)
            self.ui.labelPOIType.setEnabled(False)
            self.ui.comboBoxPOIType.setEnabled(False) 
            self.ui.checkBoxLink.setChecked(False) 
            self.ui.checkBoxLink.setEnabled(False)
            self.ui.spinBoxRadius.setEnabled(False)
            self.ui.labelMetres2.setEnabled(False)
            self.ui.checkBoxFilter.setEnabled(True)
            self.ui.checkBoxFilter.setChecked(False)
            self.ui.checkBoxGeomSimplify.setEnabled(True)
            self.ui.checkBoxGeomSimplify.setChecked(False)
            self.ui.labelMetres1.setEnabled(False)
        
        elif (string == 'pt'):
            self.ui.comboBoxSource.setModel(self.caller.modelPTSource)
            self.ui.labelIdField.setEnabled(False)
            self.ui.lineEditIdField.setEnabled(False)
            self.ui.labelNameField.setEnabled(False)
            self.ui.lineEditNameField.setEnabled(False)
            self.ui.labelPOIType.setEnabled(False)
            self.ui.comboBoxPOIType.setEnabled(False) 
            self.ui.checkBoxLink.setChecked(True)
            self.ui.checkBoxLink.setEnabled(True)
            self.ui.spinBoxRadius.setEnabled(True)
            self.ui.labelMetres2.setEnabled(True)
            self.ui.checkBoxFilter.setEnabled(False)
            self.ui.checkBoxFilter.setChecked(False)
            self.ui.checkBoxGeomSimplify.setEnabled(False)
            self.ui.checkBoxGeomSimplify.setChecked(False)
            self.ui.labelMetres1.setEnabled(False)
        
        elif (string == 'poi'):
            self.ui.comboBoxSource.setModel(self.caller.modelPOISource)            
            self.ui.checkBoxLink.setChecked(True)
            self.ui.checkBoxLink.setEnabled(True)
            self.ui.spinBoxRadius.setEnabled(True)
            self.ui.labelMetres2.setEnabled(True)
            self.ui.checkBoxFilter.setEnabled(True)
            self.ui.checkBoxFilter.setChecked(False)
            self.ui.checkBoxGeomSimplify.setEnabled(False)
            self.ui.checkBoxGeomSimplify.setChecked(False)
            self.ui.labelMetres1.setEnabled(False)
        
        elif (string == 'zoning'):
            self.ui.comboBoxSource.setModel(self.caller.modelZoningSource)
            self.ui.labelPOIType.setEnabled(False)
            self.ui.comboBoxPOIType.setEnabled(False) 
            self.ui.checkBoxLink.setChecked(False)
            self.ui.checkBoxLink.setEnabled(False)
            self.ui.spinBoxRadius.setEnabled(False)
            self.ui.labelMetres2.setEnabled(False)
            self.ui.checkBoxFilter.setEnabled(False)
            self.ui.checkBoxFilter.setChecked(False)
            self.ui.checkBoxGeomSimplify.setEnabled(True)
            self.ui.checkBoxGeomSimplify.setChecked(False)
            self.ui.labelMetres1.setEnabled(False)            
        
        elif (string == 'barrier'):
            self.ui.comboBoxSource.setModel(self.caller.modelBarrierSource)
            self.ui.labelPOIType.setEnabled(False)
            self.ui.comboBoxPOIType.setEnabled(False) 
            self.ui.checkBoxLink.setChecked(False)
            self.ui.checkBoxLink.setEnabled(False)
            self.ui.spinBoxRadius.setEnabled(False)
            self.ui.labelMetres2.setEnabled(False)
            self.ui.checkBoxFilter.setEnabled(False)
            self.ui.checkBoxFilter.setChecked(False)
            self.ui.checkBoxGeomSimplify.setEnabled(True)
            self.ui.checkBoxGeomSimplify.setChecked(False)
            self.ui.labelMetres1.setEnabled(False)
        
        self._slotComboBoxFormatCurrentIndexChanged(0) 
    
    def updatePushButtons(self):
        if (self.ui.labelFile.text() != ''):
            self.ui.pushButtonExecute.setEnabled(True)
            self.ui.pushButtonGenerate.setEnabled(True)
        else:
            self.ui.pushButtonExecute.setEnabled(False)
            self.ui.pushButtonGenerate.setEnabled(False)
    
    def reinit_window(self):
        self.caller.import_dialog.hide()
        if (self.data_type == "road"):
            self.caller.import_road()
        elif (self.data_type == "pt"):
            self.caller.import_pt()
        elif (self.data_type == "poi"):
            self.caller.import_poi()
        elif (self.data_type == "zoning"):
            self.caller.import_zoning()
        elif (self.data_type == "barrier"):
            self.caller.import_barrier()
        elif (self.data_type == "indic"):
            self.caller.import_indic()
    
    def _slotLineEditSourceNameTextChanged(self):
        self.updatePushButtons() 
    
    def _slotCheckBoxMergeToggled(self):
        if (self.ui.checkBoxMerge.isChecked()):
            self.ui.lineEditSourceName.setText(self.ui.comboBoxSource.currentText())
            self.ui.lineEditSourceName.setEnabled(False)
            self.ui.comboBoxSource.setEnabled(True)
        else:
            self.ui.lineEditSourceName.setText("")
            self.ui.lineEditSourceName.setEnabled(True)
            self.ui.comboBoxSource.setEnabled(False)
    
    def _slotCheckBoxFilterToggled(self):
        if (self.ui.checkBoxFilter.isChecked()):
            self.ui.lineEditFilter.setEnabled(True)
        else:
            self.ui.lineEditFilter.setEnabled(False)
    
    def _slotCheckBoxLinkToggled(self):
        if (self.ui.checkBoxLink.isChecked()):
            self.ui.spinBoxRadius.setEnabled(True)
            self.ui.labelMetres2.setEnabled(True)
        else:
            self.ui.spinBoxRadius.setEnabled(False)
            self.ui.labelMetres2.setEnabled(False)
    
    def _slotCheckBoxGeomSimplifyToggled(self):
        if (self.ui.checkBoxGeomSimplify.isChecked()):
            self.ui.doubleSpinBoxGeomSimplify.setEnabled(True)
            self.ui.labelMetres1.setEnabled(True)
        else:
            self.ui.doubleSpinBoxGeomSimplify.setEnabled(False)
            self.ui.labelMetres1.setEnabled(False)

    def _slotComboBoxSourceCurrentIndexChanged(self, indexChosenLine):
        if (self.ui.checkBoxMerge.isChecked()):
            self.ui.lineEditSourceName.setText(self.ui.comboBoxSource.currentText())
    
    def _slotCheckBoxPrefixToggled(self):
        if (self.ui.checkBoxPrefix.isChecked()):
            self.lineEditPrefix.setEnabled(True)
        else:
            self.lineEditPrefix.setEnabled(False)
    
    def _slotComboBoxFormatVersionCurrentIndexChanged(self, indexChosenLine):
        if (indexChosenLine >= 0):
            self.path_type = self.caller.modelImportFormatVersion.record(self.ui.comboBoxFormatVersion.currentIndex()).value("path_type")
            self.ui.comboBoxEncoding.setCurrentIndex(self.caller.modelEncoding.match(self.caller.modelEncoding.index(0,0), 0, self.caller.modelImportFormatVersion.record(self.ui.comboBoxFormatVersion.currentIndex()).value("default_encoding"))[0].row())
            if (self.caller.modelImportFormatVersion.record(indexChosenLine).value("default_srid") != None):
                self.ui.spinBoxSRID.setValue(self.caller.modelImportFormatVersion.record(indexChosenLine).value("default_srid"))
        
    def _slotComboBoxFormatCurrentIndexChanged(self, indexChosenLine):
        if (indexChosenLine >= 0):
            self.ui.pushButtonChoose.setEnabled(True)
            self.format = self.caller.modelImportFormat.record(self.ui.comboBoxFormat.currentIndex()).value("data_format")             
            self.ui.labelSRID.setEnabled(True)
            self.ui.spinBoxSRID.setEnabled(True)
            self.ui.labelEncoding.setEnabled(True)
            self.ui.comboBoxEncoding.setEnabled(True)
            
            self.caller.modelImportFormatVersion.setQuery("SELECT model_version, default_srid, default_encoding, path_type FROM pgtempus.data_format WHERE data_type = '"+self.data_type+"_import' AND data_format = '"+str(self.format)+"' ORDER BY model_version DESC", self.caller.db)
            self.ui.comboBoxFormatVersion.setCurrentIndex(0) 
            
            if ( self.data_type == "road" ):
                self.ui.checkBoxFilter.setEnabled(False)
                self.ui.checkBoxFilter.setChecked(False)                 
                if (self.format == "ign_bdtopo"):
                    self.ui.labelChoose.setText("Choisir le dossier")
                    self.ui.pushButtonChoose.setToolTip("Le dossier doit contenir les fichiers TRONCON_DE_ROUTE.shp et NON_COMMUNICATION.shp.")
                elif (self.format == "visum"):
                    self.ui.labelChoose.setText("Choisir le dossier")
                    self.ui.pushButtonChoose.setToolTip("Le dossier doit contenir les fichiers .")
                    self.ui.lineEditVisumModes.setText('P,B,V,T')
                    self.ui.lineEditVisumModes.setEnabled(True)
                    self.ui.labelVisumModes1.setEnabled(True)
                    self.ui.labelVisumModes2.setEnabled(True)         
            
            elif ( self.data_type == "zoning" ):
                if (self.format == 'ign_adminexpress'):
                    self.ui.labelIdField.setEnabled(False)
                    self.ui.lineEditIdField.setEnabled(False)
                    self.ui.labelNameField.setEnabled(False)
                    self.ui.lineEditIdField.setEnabled(False)
                    self.ui.labelChoose.setText(u'Choisir le dossier')
                    self.ui.pushButtonChoose.setToolTip("Le dossier doit contenir les fichiers COMMUNE.shp, DEPARTEMENT.shp et REGION.shp.")                                
                elif ( self.format == "generic" ):
                    self.ui.labelChoose.setText(u'Choisir le dossier')
                    self.ui.pushButtonChoose.setToolTip("Le dossier doit contenir le fichier zoning.shp.")
                    self.ui.lineEditIdField.setEnabled(True)
                    self.ui.labelIdField.setEnabled(True)
                    self.ui.lineEditNameField.setEnabled(True)
                    self.ui.labelNameField.setEnabled(True)
            
            elif ( self.data_type == "pt" ):
                if (self.format == 'gtfs'): 
                    self.ui.labelChoose.setText(u'Choisir le fichier .zip')
                    self.ui.checkBoxPrefix.setEnabled(False)
                    self.ui.lineEditPrefix.setEnabled(False)
                    self.ui.labelSRID.setEnabled(False)
                    self.ui.spinBoxSRID.setEnabled(False)
                    self.ui.labelEncoding.setEnabled(False)
                    self.ui.comboBoxEncoding.setEnabled(False)                
                elif ( self.format == "sncf" ):
                    self.ui.labelChoose.setText(u'Choisir le dossier')
                    self.ui.pushButtonChoose.setToolTip("Le dossier doit contenir les fichiers suivants : \n\
                                                                 gtfs_ter.zip, \n\
                                                                 gtfs_ic.zip, \n\
                                                                 gtfs_tgv.zip, \n\
                                                                 appariement_ign_arrets_fer.shp, \n\
                                                                 ref_stops.shp, \n\
                                                                 NOEUD_FERRE.shp, \n\
                                                                 TRONCON_VOIE_FERREE.shp \n\
                                                                 et, de façon facultative, le fichier transfers.csv.")
                    self.ui.checkBoxPrefix.setEnabled(True)
                    self.ui.lineEditPrefix.setEnabled(True)
                    self.ui.labelSRID.setEnabled(True)
                    self.ui.spinBoxSRID.setEnabled(True)
                    self.ui.labelEncoding.setEnabled(True)
                    self.ui.comboBoxEncoding.setEnabled(True)       
                elif ( self.format == "tempus" ):
                    self.ui.labelChoose.setText(u'Choisir le dossier')
                    self.ui.checkBoxPrefix.setEnabled(True)
                    self.ui.lineEditPrefix.setEnabled(True)
                    self.ui.labelSRID.setEnabled(True)
                    self.ui.spinBoxSRID.setEnabled(True)
                    self.ui.labelEncoding.setEnabled(True)
                    self.ui.comboBoxEncoding.setEnabled(True)                 
            
            elif ( self.data_type == "poi" ):
                if (self.format == "generic"):
                    self.ui.labelChoose.setText( u"Choisir le dossier" )
                    self.ui.labelIdField.setEnabled(True)
                    self.ui.lineEditIdField.setEnabled(True)
                    self.ui.labelNameField.setEnabled(True)
                    self.ui.lineEditNameField.setEnabled(True)
                    self.ui.labelPOIType.setEnabled(True)
                    self.ui.comboBoxPOIType.setEnabled(True)                          
                else:
                    self.ui.labelChoose.setText( u"Choisir le dossier" )
                    self.ui.labelIdField.setEnabled(False)
                    self.ui.lineEditIdField.setEnabled(False)
                    self.ui.labelNameField.setEnabled(False)
                    self.ui.lineEditNameField.setEnabled(False)
                    self.ui.labelPOIType.setEnabled(False)
                    self.ui.comboBoxPOIType.setEnabled(False)
            
            elif (self.data_type == "barrier"): 
                self.caller.modelImportFormatVersion.setQuery("SELECT model_version, default_srid, default_encoding, path_type FROM pgtempus.data_format WHERE data_type = 'barrier_import' AND data_format = '"+str(self.format)+"' ORDER BY model_version DESC", self.caller.db) 
                if (self.format == "ign_bdtopo"):
                    self.ui.labelChoose.setText( u"Choisir le dossier" )
    
    def _slotPushButtonChooseClicked(self):
        if (self.path_type=="directory"):
            self.cheminComplet = QFileDialog.getExistingDirectory(options=QFileDialog.ShowDirsOnly, directory=self.caller.last_dir)
            self.caller.last_dir = os.path.dirname(self.cheminComplet)
            self.ui.labelFile.setText(os.path.basename(self.cheminComplet))        
        else:
            self.cheminComplet = QFileDialog.getOpenFileName(caption = "Choisir un fichier "+self.path_type, directory=self.caller.last_dir, filter = "(*"+self.path_type+")")
            self.caller.last_dir = os.path.dirname(self.cheminComplet[0])
            self.ui.labelFile.setText(os.path.basename(self.cheminComplet[0]))
        
        self.updatePushButtons()
    
    def prepareQuery(self):   
        # Check if mandatory fields are filled
        rc = 0           
        if ( self.data_type == "road" ):
            if (self.format == "visum"):
                if (self.ui.lineEditVisumModes.text() == ""):
                    rc = 1
        elif ( self.data_type == "zoning" ):
            if ( self.format == "generic" ):
                if (self.ui.lineEditIdField.text() ==  ""):
                    rc = 1  
        elif ( self.data_type == "poi" ):
            if (self.format == "generic"):
                if (self.ui.lineEditIdField.text() == ""):
                    rc = 1
        
        box = QMessageBox()
        box.setModal(True)
        if (rc != 0):
            box.setText(u"Un ou plusieurs champs obligatoires ne sont pas renseignés.")
            box.exec_()
            
        else:
            self.cmd=[ PYTHON, TEMPUSLOADER, "--action", "import", \
                                             "--data-type", self.data_type, \
                                             "--data-format", self.format, \
                                             "--source-name", unicode(self.ui.lineEditSourceName.text()), \
                                             "--encoding", self.caller.modelEncoding.record(self.ui.comboBoxEncoding.currentIndex()).value("mod_lib"), \
                                             "--source-srid", str(self.ui.spinBoxSRID.value()), \
                                             "--target-srid", str(self.caller.srid), \
                                             "--dbstring", "host="+self.caller.db.hostName()+" user="+self.caller.db.userName()+" dbname="+self.caller.db.databaseName()+" port="+str(self.caller.db.port()) ]
            
            if ( self.ui.lineEditPrefix.text()!="" ):
                self.cmd.append( "--prefix" )
                self.cmd.append(unicode( self.ui.lineEditPrefix.text() ))
            if ( self.ui.checkBoxGeomSimplify.isChecked() ):
                self.cmd.append( "--geom-simplify" )
                self.cmd.append(str(self.ui.doubleSpinBoxGeomSimplify.value()))
            if ( self.ui.comboBoxFormatVersion.currentText()!=""):
                self.cmd.append( "--model-version" )
                self.cmd.append( str(self.caller.modelImportFormatVersion.record(self.ui.comboBoxFormatVersion.currentIndex()).value("model_version")) )
            if ( (self.ui.checkBoxFilter.isChecked()) and (self.ui.lineEditFilter.text() != "") ):
                self.cmd.append( "--filter" )
                self.cmd.append( unicode(self.ui.lineEditFilter.text()) )
            if ( self.ui.lineEditIdField.text() != "" ):
                self.cmd.append( "--id-field" )
                self.cmd.append( unicode(self.ui.lineEditIdField.text()) )
            if ( self.ui.lineEditNameField.text() != "" ):
                self.cmd.append( "--name-field" )
                self.cmd.append( unicode(self.ui.lineEditNameField.text()) )
            if (self.ui.lineEditSourceComment.text()!= "" ):
                self.cmd.append( "--source-comment" )
                self.cmd.append(self.ui.lineEditSourceComment.text())        
            if ( self.ui.lineEditVisumModes.text() != "" ):
                self.cmd.append("--visum-modes")
                self.visum_modes = unicode(self.ui.lineEditVisumModes.text())
            if ( self.ui.checkBoxNoClean.isChecked() ):
                self.cmd.append("--noclean")
            if self.ui.checkBoxMerge.isChecked():
                self.cmd.append("--merge")
            if (self.ui.comboBoxPOIType.isEnabled() == True):
                self.cmd.append("--poi-type")
                self.cmd.append( str(self.caller.modelPOIType.record(self.ui.comboBoxPOIType.currentIndex()).value("id")) )
            if (self.ui.checkBoxLink.isChecked() == True):
                self.cmd.append("--link")
                self.cmd.append( "--max-dist" )
                self.cmd.append( str(self.ui.spinBoxRadius.value()) )
            self.cmd.append("--path")
            if (self.path_type=="directory"):
                self.cmd.append(self.cheminComplet)
            else:
                self.cmd.append(self.cheminComplet[0])
        return rc
    
    def _slotPushButtonImportClicked(self):
        if self.cheminComplet != "": 
            failed = self.prepareQuery()
            if (failed == 0):
                rc = execute_external_cmd( self.cmd )
                box = QMessageBox()
                box.setModal(True)
                if (rc==0):
                    self.caller.manage_db_dialog._slotPushButtonLoadClicked()
                    box.setText(u"L'import de la source est terminé.")
                else:
                    box.setText(u"Erreur pendant l'import.\n Pour en savoir plus, générer la requête et la relancer en ligne de commande.")
                box.exec_()
        self.reinit_window()
    
    def _slotPushButtonGenerateClicked(self):
        rc = self.prepareQuery()
        if (rc == 0):
            line_cmd = transform_external_cmd( self.cmd )
        
            NomFichierComplet=["", ""]
            NomFichierComplet = QFileDialog.getSaveFileName(caption = "Enregistrer sous...", directory=self.plugin_dir, filter = "BATCH files (*.bat)")
            if (NomFichierComplet[0]!=""):
                f = open(NomFichierComplet[0], "w")
                f.write("call C:/OSGeo4W64/bin/o4w_env.bat\ncall py3_env.bat\n" + unicode(line_cmd) + "\npause")
                f.close()
                
                box = QMessageBox()
                box.setText(u"La requête a été générée dans le fichier cible")
                box.exec_()
        self.reinit_window()
