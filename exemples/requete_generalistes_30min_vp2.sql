WITH q AS
(
	SELECT array_agg(id) as dest FROM tempus_networks.poi_geom WHERE type = 113
)
SELECT pgtempus.tempus_create_o_or_d_indicator_layer(
														p_root_nodes := q.dest::bigint[],
														p_max_cost := 900::real, 
														p_root_mode := 4::smallint, 
														p_time_constraint_dep :=False::boolean,
														p_sources_id := ARRAY[1, 2]::smallint[], 
														p_transport_modes := ARRAY[4]::smallint[], 
														p_pt_trip_types := ARRAY[]::smallint[], 
														p_pt_agencies_id := ARRAY[]::smallint[], 
														p_days := ARRAY['2019-04-09']::date[],
														p_times := ARRAY['17:00:00']::time[],
														p_day_ag := 1::smallint,
														p_time_ag := 1::smallint,
														p_node_ag := 2::smallint,
														p_debug := False, 
														p_walking_speed := 3.6::real,
														p_bicycle_speed := 11.0::real, 
														p_pt_time_weight := 3.6::real,
														p_waiting_time_weight := 1.0::real, 
														p_indiv_mode_time_weight := 1.0::real,
														p_fare_weight := 0.0::real, 
														p_pt_transfer_weight := 0.0::real,
														p_mode_change_weight := 0.0::real, 
														p_max_walking_time := NULL::interval,
														p_max_bicycle_time := NULL::interval, 
														p_max_pt_transfers := NULL::integer,
														p_max_mode_changes := NULL::integer,
														p_pt_security_times := False, 
														p_table_name_isochron := 'isochrone'
												    )
FROM q;


