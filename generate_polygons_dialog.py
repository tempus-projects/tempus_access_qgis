# -*- coding: utf-8 -*-
"""
/***************************************************************************
 PluginTempusAccess
                                 A QGIS plugin
 Analyse de l'offre de transport en commun
                              -------------------
        begin                : 2016-10-22
        git sha              : $Format:%H$
        copyright            : (C) 2016 by Cerema
        email                : Aurelie.bousquet@cerema.fr, Patrick.Palmier@cerema.fr, helene.ly@cerema.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# import the PyQt and QGIS libraries
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from qgis.PyQt.QtWidgets import QDialog, QDialogButtonBox
from PyQt5.Qt import *
from PyQt5 import QtSql
from qgis.core import *
from qgis.utils import iface
from qgis import processing

# Initialize Qt resources from file resources.py
from .config import *
from .functions import *

import sys
import string
import os

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "\\forms")
from generate_polygons_dialog import Ui_Dialog

class generate_polygons_dialog(QDialog): 
    def __init__(self, caller, iface):
        QDialog.__init__(self)
        self.ui= Ui_Dialog()
        self.ui.setupUi(self)
        self.caller = caller
        self.db = caller.db
        self.iface = caller.iface
        
        self.plugin_dir = self.caller.plugin_dir 
        self.icon_dir = self.caller.icon_dir 
        
        self.ui.listViewBarrierSources.setModel(self.caller.modelBarrierSource)
        self.ui.comboBoxIsochronTable.setModel(self.caller.modelAgregatedIsochronReq)
        
        self.connectSlots()
    
    def connectSlots(self):
        self.ui.comboBoxIsochronTable.currentIndexChanged.connect(self._slotComboBoxIsochronTableIndexChanged) 
        self.ui.spinBoxPixelSize.valueChanged.connect(self._slotSpinBoxPixelSizeValueChanged) 
        self.ui.pushButtonCalculateRaster.clicked.connect(self._slotPushButtonCalculateRasterClicked) 
        self.ui.pushButtonCalculatePolygons.clicked.connect(self._slotPushButtonCalculatePolygonsClicked) 
        self.ui.pushButtonImport.clicked.connect(self._slotPushButtonImportClicked)
    
    def _slotSpinBoxPixelSizeValueChanged(self, value):
        s = "SELECT (( st_xmax(st_extent(geom)) - st_xmin(st_extent(geom)) )/"+str(value)+")::integer as nb_pix_x, \
                    (( st_ymax(st_extent(geom)) - st_ymin(st_extent(geom)) )/"+str(value)+")::integer as nb_pix_y \
             FROM tempus_results."+self.ui.comboBoxIsochronTable.currentText();
        q=QtSql.QSqlQuery(self.db)
        q.exec_(unicode(s))
        q.next()
        self.ui.labelXPixelsNum.setText(str(q.value(0))) 
        self.ui.labelYPixelsNum.setText(str(q.value(1))) 
    
    def _slotComboBoxIsochronTableIndexChanged(self, indexChosenLine):
        if (indexChosenLine>0):
            self._slotSpinBoxPixelSizeValueChanged(200)
            s = "SELECT min(cost), max(cost), (max(cost)-min(cost))/5 FROM tempus_results."+self.ui.comboBoxIsochronTable.currentText();
            q=QtSql.QSqlQuery(self.db)
            q.exec_(unicode(s))
            q.next()
            self.ui.doubleSpinBoxMinValue.setValue(q.value(0))
            self.ui.doubleSpinBoxMaxValue.setValue(q.value(1))
            self.ui.doubleSpinBoxInterval.setValue(q.value(2))
            
            s = "SELECT st_xmin(st_extent(geom)) as xmin, \
                        st_xmax(st_extent(geom)) as xmax, \
                        st_ymin(st_extent(geom)) as ymin, \
                        st_ymax(st_extent(geom)) as ymax \
                 FROM tempus_results."+self.ui.comboBoxIsochronTable.currentText();
            q = QtSql.QSqlQuery(self.db)
            q.exec_(unicode(s))
            q.next()
            self.x_min = q.value(0)
            self.x_max = q.value(1)
            self.y_min = q.value(2)
            self.y_max = q.value(3)
    
    def _slotPushButtonCalculateRasterClicked(self):
        if (self.ui.checkBoxBarrier.isChecked()):
            self.traversabilite = True
        else:
            self.traversabilite = False
        processing.execAlgorithmDialog("Networks:linear_interpolation", { 'COUT_I' : '\"pred_cost\"', \
                                                          'COUT_J' : '\"cost\"', \
                                                          'DECIMALES' : 5, \
                                                          'DIFFUSION' : '\"diffusion\"', \
                                                          'FENETRE' : str(self.x_min)+','+str(self.x_max)+','+str(self.y_min)+','+str(self.y_max)+' [EPSG:'+str(self.caller.srid)+']', \
                                                          'IND_VALUES' : '\'.\'', \
                                                          'INTRAVERSABLE' : self.traversabilite, \
                                                          'NB_PIXELS_X' : self.ui.labelXPixelsNum.text(), \
                                                          'NB_PIXELS_Y' : self.ui.labelYPixelsNum.text(), \
                                                          'RAYON' : 500, \
                                                          'RESEAU' : 'dbname=\'tempusaccess_corse\' host=localhost port=55432 user=\'aurelie-p.bousquet\' key=\'arc_id\' checkPrimaryKeyUnicity=\'1\' table=\"tempus_results\".\"'+self.ui.comboBoxIsochronTable.currentText()+'\" (geom) sql=', \
                                                          'RESULTAT' : self.plugin_dir + '/resultats/resultat.tif', \
                                                          'SENS' : '\'1\'', \
                                                          'TAILLE_PIXEL_X' : -1, \
                                                          'TAILLE_PIXEL_Y' : -1, \
                                                          'TRAVERSABILITE' : '\"crossability\"', \
                                                          'VITESSE_DIFFUSION' : '4.0' \
                                                        }
                                      )  
    
    def _slotPushButtonCalculatePolygonsClicked(self):
        processing.execAlgorithmDialog("Networks:isovalues_polygons", { 'BANDE' : 1, \
                                                                        'CONTOURS' : self.plugin_dir + '/resultats/zoning.shp', \
                                                                        'IND_VALUES' : False, \
                                                                        'INTERVALLE' : self.ui.doubleSpinBoxInterval.value(), \
                                                                        'MAXI' : self.ui.doubleSpinBoxMaxValue.value(), \
                                                                        'MINI' : self.ui.doubleSpinBoxMinValue.value(), \
                                                                        'NOVALUE' : -9999, \
                                                                        'POLYGONS' : True, \
                                                                        'RASTER' : self.plugin_dir + '/resultats/resultat.tif' \
                                                                      }
                                      )  
    
    def _slotPushButtonImportClicked(self):
        self.cmd=[ PYTHON, TEMPUSLOADER, "--action", "import", \
                                         "--data-type", "zoning", \
                                         "--data-format", "generic", \
                                         "--model-version", "0", \
                                         "--source-name", self.ui.comboBoxIsochronTable.currentText()+"_contours", \
                                         "--encoding", "UTF-8", \
                                         "--source-srid", str(self.caller.srid), \
                                         "--target-srid", str(self.caller.srid), \
                                         "--id-field", "id", \
                                         "--dbstring", "host="+self.caller.db.hostName()+" user="+self.caller.db.userName()+" dbname="+self.caller.db.databaseName()+" port="+str(self.caller.db.port()), \
                                         "--path", self.plugin_dir + "/resultats" ]
        rc = execute_external_cmd( self.cmd )
        box = QMessageBox()
        box.setModal(True)
        if (rc==0):
            s = "DROP TABLE IF EXISTS tempus_results."+self.ui.comboBoxIsochronTable.currentText()+"_contours; \
                 ALTER TABLE tempus_zoning."+self.ui.comboBoxIsochronTable.currentText()+"_contours SET SCHEMA tempus_results; \
                 DELETE FROM tempus_results.catalog WHERE table_name = '"+self.ui.comboBoxIsochronTable.currentText()+"_contours'; \
                 INSERT INTO tempus_results.catalog(table_name, base_obj_type_id, result_type_id) VALUES ('"+self.ui.comboBoxIsochronTable.currentText()+"_contours', 5, 7); \
                 ALTER TABLE tempus_results."+self.ui.comboBoxIsochronTable.currentText()+"_contours RENAME COLUMN original_id TO cost; \
                 ALTER TABLE tempus_results."+self.ui.comboBoxIsochronTable.currentText()+"_contours ALTER COLUMN cost TYPE real USING cost::real; \
                 ALTER TABLE tempus_results."+self.ui.comboBoxIsochronTable.currentText()+"_contours ADD COLUMN symbol_color real;"
            q = QtSql.QSqlQuery(self.caller.db)
            q.exec_(unicode(s))
            box.setText(u"L'import de la source est terminé.")
        else:
            box.setText(u"Erreur pendant l'import.\n Pour en savoir plus, générer la requête et la relancer en ligne de commande.")
        box.exec_() 



    