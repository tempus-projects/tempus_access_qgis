# -*- coding: utf-8 -*-
"""
/***************************************************************************
 PluginTempusAccess
                                 A QGIS plugin
 Analyse de l'offre de transport en commun
                              -------------------
        begin                : 2016-10-22
        git sha              : $Format:%H$
        copyright            : (C) 2016 by Cerema
        email                : Aurelie.bousquet@cerema.fr, Patrick.Palmier@cerema.fr, helene.ly@cerema.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# import the PyQt and QGIS libraries
from PyQt5 import Qt, QtSql
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from qgis.core import *
from qgis.gui import *
from qgis.PyQt.QtWidgets import QDialog

# Initialize Qt resources from file resources.py
from .config import *
from .functions import *

import sys
import string
import os

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "\\forms")
from mapmatch_pt_dialog import Ui_Dialog

class mapmatch_pt_dialog(QDialog): 

    def __init__(self, caller, iface):
        QDialog.__init__(self)
        self.ui= Ui_Dialog()
        self.ui.setupUi(self)
        self.caller = caller
        self.db = caller.db
        self.iface = caller.iface
        
        self.ui.comboBoxPTSource.setModel(self.caller.modelPTSource)
        self.ui.comboBoxPTSource.setCurrentIndex(0)
        self.modelPTTripTypes = QtSql.QSqlQueryModel()
        self.ui.listViewPTTripTypes.setModel(self.modelPTTripTypes)
        
        self._connectSlots()
    
    def _connectSlots(self):
        self.ui.pushButtonOK.clicked.connect(self._slotPushButtonOKClicked)     
        self.ui.comboBoxPTSource.currentIndexChanged.connect(self._slotComboBoxPTSourceIndexChanged)
    
    def _slotPushButtonOKClicked(self):
        self.PTTripTypes = []
        if (self.ui.listViewPTTripTypes.selectionModel().hasSelection()):
            for item in self.ui.listViewPTTripTypes.selectionModel().selectedRows():
                self.PTTripTypes.append(self.modelPTTripTypes.record(item.row()).value("id"))
        
        self.PTSource = self.caller.modelPTSource.record(self.ui.comboBoxPTSource.currentIndex()).value("id")
        
        query="SELECT tempus_parameters.map_match_pt_sections(\n\
                                                               table_name1 := '"+TABLE_MM_PATH_TREE_STEP+"',\n\
                                                               table_name2 := '"+TABLE_MM_PATH+"',\n\
                                                               param_transport_modes := ARRAY[4,5]::smallint[],\n\
                                                               param_pt_network := "+str(self.PTSource)+"::smallint,\n\
                                                               param_pt_trip_types := ARRAY"+str(self.PTTripTypes)+"::smallint[]\n\
                                                            );\n" 
        print(query)
        q=QtSql.QSqlQuery(self.caller.db)
        q.exec_(unicode(query))
    
    def _slotComboBoxPTSourceIndexChanged(self):
        s="SELECT DISTINCT pt_trip_type.name, pt_trip_type.id \
            FROM tempus_costs.pt_trip_type JOIN tempus_networks.pt_trip ON ( pt_trip_type.id = ANY(pt_trip.pt_trip_types_id) ) \
            JOIN tempus_networks.pt_route ON (pt_trip.pt_route_id = pt_route.id) \
            JOIN tempus_networks.pt_agency ON (pt_agency.id = pt_route.pt_agency_id)  \
            WHERE pt_agency.source_id = "+str(self.caller.modelPTSource.record(self.ui.comboBoxPTSource.currentIndex()).value("id"))+"\
            ORDER BY id"
        print(s)
        self.modelPTTripTypes.setQuery(unicode(s), self.db)
        self.ui.listViewPTTripTypes.selectAll() 
        
        