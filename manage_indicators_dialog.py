#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2018-2019 Cerema (https://www.cerema.fr)
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""

# import the PyQt and QGIS libraries
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from qgis.PyQt.QtWidgets import QDialog
from PyQt5.Qt import *
from PyQt5 import QtSql
from qgis.core import *
from qgis.utils import iface

# Initialize Qt resources from file resources.py
from .config import *
from .functions import *

import sys
import string
import os

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "\\forms")
from manage_indicators_dialog import Ui_Dialog

class manage_indicators_dialog(QDialog): 

    def __init__(self, caller, iface):
        QDialog.__init__(self)
        self.ui= Ui_Dialog()
        self.ui.setupUi(self)        
        
        self.caller = caller
        self.iface = self.caller.iface
        self.db = caller.db
        
        self.plugin_dir = self.caller.plugin_dir
        self.icon_dir = self.plugin_dir + "/icons"
        
        self.modelO = QtSql.QSqlQueryModel()
        self.modelD = QtSql.QSqlQueryModel()
        self.modelDay = QtSql.QSqlQueryModel()
        self.modelDepTime = QtSql.QSqlQueryModel()
        self.modelArrTime = QtSql.QSqlQueryModel()
        self.ui.comboBoxO.setModel(self.modelO) 
        self.ui.comboBoxD.setModel(self.modelD) 
        self.ui.comboBoxDay.setModel(self.modelDay) 
        self.ui.comboBoxDepTime.setModel(self.modelDepTime)
        self.ui.comboBoxArrTime.setModel(self.modelArrTime)
        
        self.ui.comboBoxColorIndic.setModel(self.caller.modelColorIndic)
        self.ui.comboBoxSizeIndic.setModel(self.caller.modelSizeIndic)
        self.ui.comboBoxReq.setModel(self.caller.modelReq)
        self.ui.comboBoxObjType.setModel(self.caller.modelObjType)
        self.ui.comboBoxObjSubType.setModel(self.caller.modelObjSubType)
        
        self.ui.pushButtonReqDisplay.setIcon(QIcon(self.icon_dir + "/icon_open.png"))
        self.ui.pushButtonReqDelete.setIcon(QIcon(self.icon_dir + "/icon_delete.png"))
        self.ui.pushButtonReqRename.setIcon(QIcon(self.icon_dir + "/icon_rename.png"))
        self.ui.pushButtonSaveComments.setIcon(QIcon(self.icon_dir + "/icon_save.png"))
        self.ui.pushButtonUpdate.setIcon(QIcon(self.icon_dir + "/icon_update.png"))
        self.ui.pushButtonFilter.setIcon(QIcon(self.icon_dir + "/icon_update.png"))
        
        self.connectSlots()
        
    def connectSlots(self):        
        self.ui.comboBoxObjType.currentIndexChanged.connect(self._slotComboBoxObjTypeIndexChanged)
        self.ui.comboBoxObjSubType.currentIndexChanged.connect(self._slotComboBoxObjSubTypeIndexChanged)
        self.ui.radioButton_leave_after.toggled.connect(self.list_req)
        self.ui.radioButton_arrive_before.toggled.connect(self.list_req)
        self.ui.comboBoxReq.currentIndexChanged.connect(self._slotComboBoxReqIndexChanged)
        self.ui.pushButtonReqDelete.clicked.connect(self._slotPushButtonReqDeleteClicked)
        self.ui.pushButtonReqRename.clicked.connect(self._slotPushButtonReqRenameClicked)
        self.ui.pushButtonSaveComments.clicked.connect(self._slotPushButtonSaveCommentsClicked)
        self.ui.comboBoxSizeIndic.currentIndexChanged.connect(self._slotComboBoxSizeIndicIndexChanged)
        self.ui.comboBoxColorIndic.currentIndexChanged.connect(self._slotComboBoxColorIndicIndexChanged)
        self.ui.pushButtonReqDisplay.clicked.connect(self._slotPushButtonReqDisplayClicked)
        self.ui.pushButtonFilter.clicked.connect(self._slotPushButtonFilterClicked)
        self.ui.pushButtonUpdate.clicked.connect(self.refreshReq)
        self.ui.radioButtonColorByMode.toggled.connect(self._slotRadioButtonsColorToggled)
        self.ui.radioButtonColorGrade.toggled.connect(self._slotRadioButtonsColorToggled)
    
    def _slotComboBoxObjTypeIndexChanged(self, indexChosenLine):
        self.caller.modelObjSubType.setQuery("SELECT subtype_name, obj_subtype_id FROM pgtempus.base_obj_subtype WHERE obj_type_id = "+str(self.caller.modelObjType.record(indexChosenLine).value("id")), self.caller.db)         
        
        if (self.caller.modelObjType.record(indexChosenLine).value("id") == 4) or (self.caller.modelObjType.record(indexChosenLine).value("id") == 5):
            self.ui.radioButton_leave_after.setEnabled(True)
            self.ui.radioButton_arrive_before.setEnabled(True)
        else:
            self.ui.radioButton_leave_after.setEnabled(False)
            self.ui.radioButton_arrive_before.setEnabled(False)
            
    def _slotComboBoxObjSubTypeIndexChanged(self, indexChosenLine):
        self.list_req()
        
    def list_req(self):
        if (self.ui.comboBoxObjSubType.currentIndex()>0):
            if (self.ui.radioButton_leave_after.isChecked()):
                time_constraint = 1
            else:
                time_constraint = 2
            s= "( \
                                             SELECT table_name, catalog.id, base_obj_type_id, base_obj_type.def_name, base_obj_subtype_id, time_constraint, comment \
                                             FROM tempus_results.catalog JOIN pgtempus.base_obj_type ON (base_obj_type.id = catalog.base_obj_type_id) \
                                             WHERE base_obj_type_id = "+str(self.caller.modelObjType.record(self.ui.comboBoxObjType.currentIndex()).value("id"))+" \
                                               AND base_obj_subtype_id = "+str(self.caller.modelObjSubType.record(self.ui.comboBoxObjSubType.currentIndex()).value("obj_subtype_id"))+" \
                                               AND time_constraint = "+str(time_constraint)+" \
                                            ) UNION \
                                            SELECT '', -1, null, null, null, null, null \
                                            ORDER BY 2"
            self.caller.modelReq.setQuery( s, self.caller.db )    
    
    def _slotComboBoxReqIndexChanged(self, indexChosenLine):
        id = self.caller.modelReq.record(self.ui.comboBoxReq.currentIndex()).value("id")        
        if ( (id != None) and (id > 0) ): 
            self.base_obj_type_id = self.caller.modelReq.record(self.ui.comboBoxReq.currentIndex()).value("base_obj_type_id")
            self.base_obj_def_name = self.caller.modelReq.record(self.ui.comboBoxReq.currentIndex()).value("def_name")
            self.base_obj_subtype_id = self.caller.modelReq.record(self.ui.comboBoxReq.currentIndex()).value("base_obj_subtype_id")
            table_name = self.caller.modelReq.record(self.ui.comboBoxReq.currentIndex()).value("table_name")
            self.ui.pushButtonReqDisplay.setEnabled(True)
            self.ui.pushButtonReqRename.setEnabled(True)
            self.ui.pushButtonReqDelete.setEnabled(True)
            self.ui.pushButtonReqDelete.setToolTip("Supprimer ce calcul stocké")
            self.ui.pushButtonSaveComments.setEnabled(True)
            
            self.ui.textEditComments.setEnabled(True)
            self.ui.labelComments.setEnabled(True)
            
            if ( (self.base_obj_def_name == "pt_stop") or (self.base_obj_def_name == "pt_route") or (self.base_obj_def_name == "pt_agency") ):
                self.modelO.clear()
                self.modelD.clear()
                self.modelDay.clear()
                self.modelDepTime.clear()
                self.modelArrTime.clear()
                self.ui.groupBoxFilter.setEnabled(False)
                if ( self.base_obj_def_name== "pt_stop" ):
                    self.ui.groupBoxColor.setEnabled(True)
                    self.ui.radioButtonColorByMode.setEnabled(False)
                    self.ui.radioButtonColorGrade.setEnabled(True)
                    self.ui.radioButtonColorGrade.setChecked(True)
                    self.ui.groupBoxSize.setEnabled(True)
                elif (self.base_obj_def_name=="pt_route"):
                    if (self.base_obj_subtype_id == 1) or (self.base_obj_subtype_id == 2):
                        self.ui.groupBoxColor.setEnabled(True)
                        self.ui.radioButtonColorByMode.setEnabled(False)
                        self.ui.radioButtonColorGrade.setEnabled(True)
                        self.ui.radioButtonColorGrade.setChecked(True)
                        self.ui.groupBoxSize.setEnabled(True)
                    else:
                        self.ui.groupBoxColor.setEnabled(False)
                        self.ui.groupBoxSize.setEnabled(False)
                elif ((self.base_obj_def_name=="pt_agency") or (self.base_obj_def_name=="od") or (self.base_obj_def_name == "o_or_d") ):
                    self.ui.groupBoxColor.setEnabled(False)
                    self.ui.groupBoxSize.setEnabled(False)   
            
            elif ( (self.base_obj_def_name == "od") or (self.base_obj_def_name == "o_or_d") ):
                self.ui.groupBoxFilter.setEnabled(True)
                self.ui.groupBoxSize.setEnabled(False) 
                self.ui.pushButtonReqDisplay.setEnabled(True)
                
                if (self.base_obj_def_name == "od"):
                    self.ui.groupBoxColor.setEnabled(False)
                    s = "SELECT DISTINCT path_o_node::character varying FROM tempus_results."+table_name+" UNION SELECT '' ORDER BY 1"
                    self.modelO.setQuery(s, self.db)
                    self.ui.comboBoxO.setEnabled(True)
                    s = "SELECT DISTINCT path_d_node::character varying FROM tempus_results."+table_name+" UNION SELECT '' ORDER BY 1"
                    self.modelD.setQuery(s, self.db)
                    self.ui.comboBoxD.setEnabled(True)
                    if (self.base_obj_subtype_id <= 3):
                        s = "SELECT DISTINCT theo_dep_time::date::character varying FROM tempus_results."+table_name+" WHERE theo_dep_time IS NOT NULL \
                        UNION SELECT DISTINCT theo_arr_time::date::character varying FROM tempus_results."+table_name+" WHERE theo_arr_time IS NOT NULL \
                        UNION SELECT '' ORDER BY 1"
                        self.modelDay.setQuery(s, self.db)
                        self.ui.comboBoxDay.setEnabled(True)
                        s = "SELECT DISTINCT theo_dep_time::time::character varying FROM tempus_results."+table_name+" WHERE theo_dep_time IS NOT NULL \
                        UNION SELECT '' ORDER BY 1"
                        self.modelDepTime.setQuery(s, self.db)
                        self.ui.comboBoxDepTime.setEnabled(True)
                        s = "SELECT DISTINCT theo_arr_time::time::character varying FROM tempus_results."+table_name+" WHERE theo_arr_time IS NOT NULL \
                        UNION SELECT '' ORDER BY 1"
                        self.modelArrTime.setQuery(s, self.db)
                        self.ui.comboBoxArrTime.setEnabled(True)
                    elif (self.base_obj_subtype_id == 4):
                        self.modelDay.clear()
                        self.ui.comboBoxDay.setEnabled(False)
                        self.modelDepTime.clear()
                        self.modelArrTime.clear()
                        self.ui.comboBoxDepTime.setEnabled(False)
                        self.ui.comboBoxArrTime.setEnabled(False)
                
                elif (self.base_obj_def_name == "o_or_d"):
                    self.ui.groupBoxColor.setEnabled(True)
                    
                    if ((self.base_obj_subtype_id == 1) or (self.base_obj_subtype_id == 2)): # detailed_paths_trees or root_day_time
                        if (self.base_obj_subtype_id == 1):
                            self.ui.radioButtonColorByMode.setEnabled(True)
                            self.ui.radioButtonColorByMode.setChecked(True)
                            self.ui.radioButtonColorGrade.setEnabled(False)
                        else:
                            self.ui.radioButtonColorByMode.setEnabled(False)
                            self.ui.radioButtonColorGrade.setEnabled(True)
                            self.ui.radioButtonColorGrade.setChecked(True)
                        s = "SELECT DISTINCT (root_timestamp::date)::character varying FROM tempus_results."+table_name+" UNION SELECT '' ORDER BY 1"
                        self.modelDay.setQuery(s, self.db)
                        self.ui.comboBoxDay.setEnabled(True)
                        
                        if (self.ui.radioButton_leave_after.isChecked()): # leave after
                            s = "SELECT DISTINCT root_node::character varying FROM tempus_results."+table_name+" UNION SELECT '' ORDER BY 1"
                            self.modelO.setQuery(s, self.db)
                            self.modelD.clear()
                            self.ui.comboBoxO.setEnabled(True)
                            self.ui.labelO.setEnabled(True)
                            self.ui.comboBoxD.setEnabled(False)
                            self.ui.labelD.setEnabled(False)
                            s = "SELECT DISTINCT (root_timestamp::time)::character varying FROM tempus_results."+table_name+" UNION SELECT '' ORDER BY 1"
                            self.modelDepTime.setQuery(s, self.db)
                            self.modelArrTime.clear()
                            self.ui.comboBoxDepTime.setEnabled(True)
                            self.ui.labelDepartureTime.setEnabled(True)
                            self.ui.comboBoxArrTime.setEnabled(False)
                            self.ui.labelArrivalTime.setEnabled(False)
                            
                        elif (self.ui.radioButton_arrive_before.isChecked()): # arrive before
                            s = "SELECT DISTINCT root_node::character varying FROM tempus_results."+table_name+" UNION SELECT '' ORDER BY 1"
                            self.modelD.setQuery(s, self.db)
                            self.modelO.clear()
                            self.ui.comboBoxD.setEnabled(True)
                            self.ui.labelD.setEnabled(True)
                            self.ui.comboBoxO.setEnabled(False)
                            self.ui.labelO.setEnabled(False)
                            s = "SELECT DISTINCT (root_timestamp::time)::character varying FROM tempus_results."+table_name+" UNION SELECT '' ORDER BY 1"
                            self.modelDepTime.clear()
                            self.modelArrTime.setQuery(s, self.db)
                            self.ui.comboBoxDepTime.setEnabled(False)
                            self.ui.labelDepartureTime.setEnabled(False)
                            self.ui.comboBoxArrTime.setEnabled(True)
                            self.ui.labelArrivalTime.setEnabled(True)

                    else: # agregated
                        self.ui.radioButtonColorByMode.setEnabled(False)
                        self.ui.radioButtonColorGrade.setChecked(True)
                        self.modelDay.clear()
                        self.ui.comboBoxDay.setEnabled(False)
                        self.modelO.clear()
                        self.modelD.clear()
                        self.modelDepTime.clear()
                        self.modelArrTime.clear()
                        self.ui.comboBoxO.setEnabled(False)
                        self.ui.comboBoxD.setEnabled(False)
                        self.ui.comboBoxDepTime.setEnabled(False)
                        self.ui.comboBoxArrTime.setEnabled(False)
                    
            # Update color and size indicators
            self.updateReqIndicators()        
            
            # Display comments of the current layer
            s="SELECT coalesce(pg_catalog.obj_description((SELECT 'tempus_results."+table_name+"'::regclass::oid)), '');"
            q=QtSql.QSqlQuery(unicode(s), self.db)
            q.next()
            self.ui.textEditComments.setPlainText(q.value(0))
            
        else:
            self.ui.groupBoxColor.setEnabled(False)
            self.ui.groupBoxSize.setEnabled(False) 
            self.ui.textEditComments.setEnabled(False)
            self.ui.labelComments.setEnabled(False)
            
            self.ui.pushButtonReqDisplay.setEnabled(False)
            self.ui.pushButtonReqRename.setEnabled(False)
            self.ui.pushButtonReqDelete.setEnabled(True)
            self.ui.pushButtonReqDelete.setToolTip("Supprimer tous les calculs stockés")
            self.ui.pushButtonSaveComments.setEnabled(False)
            
            self.caller.modelSizeIndic.clear()
            self.caller.modelColorIndic.clear()
            self.ui.textEditComments.setText('')
    
    def _slotPushButtonReqDeleteClicked(self):
        id = self.caller.modelReq.record(self.ui.comboBoxReq.currentIndex()).value("id")        
        if ( (id != None) and (id > 0) ): 
            ret = QMessageBox.question(self, "Tempus Access", u"Le calcul stocké courant va être supprimé. \n Êtes vous certain(e) de vouloir faire cette opération ?", QMessageBox.Ok | QMessageBox.Cancel,QMessageBox.Cancel)
            if (ret == QMessageBox.Ok):
                for layer in self.caller.node_indicators.findLayers():
                    if (layer.name()==self.ui.comboBoxReq.currentText()):
                        QgsProject.instance().removeMapLayer(layer.layer().id())
                            
                s="DROP TABLE tempus_results."+self.ui.comboBoxReq.currentText()+";\
                DELETE FROM tempus_results.catalog WHERE table_name = '"+self.ui.comboBoxReq.currentText()+"';"
                q=QtSql.QSqlQuery(self.db)
                q.exec_(unicode(s))
        else:
            ret = QMessageBox.question(self, "Tempus Access", u"Tous les calculs stockés vont être supprimés. \n Êtes vous certain(e) de vouloir faire cette opération ?", QMessageBox.Ok | QMessageBox.Cancel,QMessageBox.Cancel)
            if (ret == QMessageBox.Ok):
                for layer in self.caller.node_indicators.findLayers():
                    QgsProject.instance().removeMapLayer(layer.layer().id())
                               
                s = "SELECT table_name \
                     FROM information_schema.tables \
                     WHERE table_schema = 'tempus_results' AND table_name !='catalog'"
                q=QtSql.QSqlQuery(unicode(s), self.db)
                while q.next():
                    t = "DROP TABLE tempus_results."+q.value(0)+";"
                    r=QtSql.QSqlQuery(self.db)
                    r.exec_(unicode(t))
                s = "DELETE FROM tempus_results.catalog;"
                q=QtSql.QSqlQuery(self.db)
                q.exec_(unicode(s))
            
        self.refreshReq()
        self._slotComboBoxReqIndexChanged(0)
    
    def _slotPushButtonReqRenameClicked(self):
        old_name = self.caller.modelReq.record(self.ui.comboBoxReq.currentIndex()).value("table_name")
        new_name = self.ui.comboBoxReq.currentText()
        s="ALTER TABLE tempus_results."+old_name+" RENAME TO "+new_name+";\
        UPDATE tempus_results.catalog SET table_name = '"+new_name+"' WHERE table_name = '"+self.caller.modelReq.record(self.ui.comboBoxReq.currentIndex()).value("table_name")+"';\
        ALTER TABLE tempus_results."+new_name+"\
        RENAME CONSTRAINT "+old_name+"_pkey TO "+new_name+"_pkey";
        q=QtSql.QSqlQuery(self.db)
        q.exec_(unicode(s))
        
        for layer in self.caller.node_indicators.findLayers():
            if (layer.name()==old_name):
                QgsProject.instance().removeMapLayer(layer.layer().id())
        
        self.refreshReq()
        
        self.ui.comboBoxReq.setCurrentIndex(self.caller.modelReq.match(self.caller.modelReq.index(0,0), 0, new_name)[0].row())
        
    def _slotPushButtonSaveCommentsClicked(self):
        s="COMMENT ON TABLE tempus_results."+self.caller.modelReq.record(self.ui.comboBoxReq.currentIndex()).value("table_name")+" IS '"+unicode(self.ui.textEditComments.toPlainText())+"';"
        
        q=QtSql.QSqlQuery(self.db)
        q.exec_(unicode(s))
    
    def _slotComboBoxSizeIndicIndexChanged(self, indexChosenLine):
        if (indexChosenLine>0):
            s="SELECT coalesce(min("+self.caller.modelSizeIndic.record(indexChosenLine).value("col_name")+"), 0), coalesce(max("+self.caller.modelSizeIndic.record(indexChosenLine).value("col_name")+"), 0) FROM tempus_results."+self.ui.comboBoxReq.currentText()
            q=QtSql.QSqlQuery(unicode(s), self.db)
            q.next()
            self.ui.spinBoxSizeIndicMinValue.setEnabled(True)
            self.ui.spinBoxSizeIndicMaxValue.setEnabled(True)
            self.ui.doubleSpinBoxSize.setEnabled(True)
            self.ui.label.setEnabled(True)
            self.ui.spinBoxSizeIndicMinValue.setValue(max(0,q.value(0)-1))
            self.ui.spinBoxSizeIndicMaxValue.setValue(q.value(1))
        else:
            self.ui.spinBoxSizeIndicMinValue.setEnabled(False) 
            self.ui.spinBoxSizeIndicMaxValue.setEnabled(False)
            self.ui.doubleSpinBoxSize.setEnabled(False)
            self.ui.label.setEnabled(False)
            self.ui.spinBoxSizeIndicMinValue.setValue(0)
            self.ui.spinBoxSizeIndicMaxValue.setValue(0)
    
    def _slotRadioButtonsColorToggled(self):
        if (self.ui.radioButtonColorByMode.isChecked()):
            self.ui.labelColorIndic.setEnabled(False)
            self.ui.comboBoxColorIndic.setEnabled(False)
            self.ui.labelMinColor.setEnabled(False)
            self.ui.spinBoxColorIndicMinValue.setEnabled(False)
            self.ui.labelMaxColor.setEnabled(False)
            self.ui.spinBoxColorIndicMaxValue.setEnabled(False)
        else:
            self.ui.labelColorIndic.setEnabled(True)
            self.ui.comboBoxColorIndic.setEnabled(True)
            self.ui.labelMinColor.setEnabled(True)
            self.ui.spinBoxColorIndicMinValue.setEnabled(True)
            self.ui.labelMaxColor.setEnabled(True)
            self.ui.spinBoxColorIndicMaxValue.setEnabled(True)
    
    def _slotComboBoxColorIndicIndexChanged(self, indexChosenLine):
        if (indexChosenLine>0):
            col_name = self.caller.modelColorIndic.record(indexChosenLine).value("col_name")
            s="SELECT coalesce(min("+col_name+"), 0), coalesce(max("+col_name+"),0) FROM tempus_results."+self.ui.comboBoxReq.currentText()+" WHERE "+col_name+" != 'Infinity'::double precision"
            q=QtSql.QSqlQuery(unicode(s), self.db)
            q.next()
            self.ui.spinBoxColorIndicMinValue.setEnabled(True)
            self.ui.spinBoxColorIndicMaxValue.setEnabled(True) 
            self.ui.spinBoxColorIndicMinValue.setValue(max(q.value(0),0))
            self.ui.spinBoxColorIndicMaxValue.setValue(q.value(1))
        else:
            self.ui.spinBoxColorIndicMinValue.setEnabled(False)
            self.ui.spinBoxColorIndicMaxValue.setEnabled(False) 
    
    def _slotPushButtonFilterClicked(self):
        if (self.base_obj_def_name == "od"):
            origin_layers = QgsProject.instance().mapLayersByName("Origines")
            destination_layers = QgsProject.instance().mapLayersByName("Destinations")
            
            if len(origin_layers)>0 and len(destination_layers)>0:
                origin_layer = origin_layers[0]
                destination_layer = destination_layers[0]
                if (self.ui.comboBoxO.currentText()!=""):
                    s_orig = "id = " + self.ui.comboBoxO.currentText()
                else:  
                    s_orig="id = ANY (ARRAY"+str(self.caller.orig_nodes)+"::bigint[])" 
                if (self.ui.comboBoxD.currentText()!=""):
                    s_dest = "id = " + self.ui.comboBoxD.currentText()
                else:
                    s_dest = "id = ANY (ARRAY"+str(self.caller.dest_nodes)+"::bigint[])"
                origin_layer.setSubsetString(s_orig)
                destination_layer.setSubsetString(s_dest)            
            
            if (self.base_obj_subtype_id == 1):
                list = QgsProject.instance().mapLayersByName("Arcs composant les chemins")
                if len(list)>0:
                    layer = list[0]
                    s = "in_paths = True"
                    if (self.ui.comboBoxO.currentText() != ""):
                        s = s + " AND path_o_node = " + self.ui.comboBoxO.currentText()
                    if (self.ui.comboBoxD.currentText() != ""):
                        s = s + " AND path_d_node = " + self.ui.comboBoxD.currentText()
                    if (self.ui.comboBoxDay.currentText() != ""):
                        s = s + " AND (theo_dep_time::date = '" + self.ui.comboBoxDay.currentText() + "' OR theo_arr_time::date = '" + self.ui.comboBoxDay.currentText() + "')"
                    if (self.ui.comboBoxDepTime.currentText() != ""):
                        s = s + " AND theo_dep_time::time = '" + self.ui.comboBoxDepTime.currentText() + "'"
                    if (self.ui.comboBoxArrTime.currentText() != ""):
                        s = s + " AND theo_arr_time::time = '" + self.ui.comboBoxArrTime.currentText() + "'"
                    #print(s)
                    layer.setSubsetString(s)
            elif (self.base_obj_subtype_id == 2):
                list = QgsProject.instance().mapLayersByName("Trajets monomodaux")
                if len(list)>0:
                    layer = list[0]
                    s = ""
                    if (self.ui.comboBoxO.currentText() != ""):
                        s = s + " AND path_o_node = " + self.ui.comboBoxO.currentText()
                    if (self.ui.comboBoxD.currentText() != ""):
                        s = s + " AND path_d_node = " + self.ui.comboBoxD.currentText()
                    if (self.ui.comboBoxDay.currentText() != ""):
                        s = s + " AND (theo_dep_time::date = '" + self.ui.comboBoxDay.currentText() + "' OR theo_arr_time::day = '" + self.ui.comboBoxDay.currentText() + "')"
                    if (self.ui.comboBoxDepTime.currentText() != ""):
                        s = s + " AND theo_dep_time::time = '" + self.ui.comboBoxDepTime.currentText() + "'"
                    if (self.ui.comboBoxArrTime.currentText() != ""):
                        s = s + " AND theo_arr_time::time = '" + self.ui.comboBoxArrTime.currentText() + "'"
                    s = s[5:]
                    #print(s)
                    layer.setSubsetString(s)
            elif (self.base_obj_subtype_id == 3):
                list = QgsProject.instance().mapLayersByName("Chemins multimodaux")
                if len(list)>0:
                    layer = list[0]
                    s = ""
                    if (self.ui.comboBoxO.currentText() != ""):
                        s = s + " AND path_o_node = " + self.ui.comboBoxO.currentText()
                    if (self.ui.comboBoxD.currentText() != ""):
                        s = s + " AND path_d_node = " + self.ui.comboBoxD.currentText()
                    if (self.ui.comboBoxDay.currentText() != ""):
                        s = s + " AND (theo_dep_time::date = '" + self.ui.comboBoxDay.currentText() + "' OR theo_arr_time::date = '" + self.ui.comboBoxDay.currentText() + "')"
                    if (self.ui.comboBoxDepTime.currentText() != ""):
                        s = s + " AND theo_dep_time::time = '" + self.ui.comboBoxDepTime.currentText() + "'"
                    if (self.ui.comboBoxArrTime.currentText() != ""):
                        s = s + " AND theo_arr_time::time = '" + self.ui.comboBoxArrTime.currentText() + "'"
                    s = s[5:]
                    #print(s)
                    layer.setSubsetString(s)
            elif (self.base_obj_subtype_id == 4):
                list = QgsProject.instance().mapLayersByName("OD agrégées par jour")
                if len(list)>0:
                    layer = list[0]
                    s = ""
                    if (self.ui.comboBoxO.currentText() != ""):
                        s = s + " AND path_o_node = " + self.ui.comboBoxO.currentText()
                    if (self.ui.comboBoxD.currentText() != ""):
                        s = s + " AND path_d_node = " + self.ui.comboBoxD.currentText()
                    if (self.ui.comboBoxDay.currentText() != ""):
                        s = s + " AND (theo_dep_time::date = '" + self.ui.comboBoxDay.currentText() + "' OR theo_arr_time::date = '" + self.ui.comboBoxDay.currentText() + "')"
                    #print(s)
                    s = s[5:]
                    layer.setSubsetString(s)
            elif (self.base_obj_subtype_id == 5):
                list = QgsProject.instance().mapLayersByName("OD agrégées")
                if len(list)>0:
                    layer = list[0]
                    s = ""
                    if (self.ui.comboBoxO.currentText() != ""):
                        s = s + " AND path_o_node = " + self.ui.comboBoxO.currentText()
                    if (self.ui.comboBoxD.currentText() != ""):
                        s = s + " AND path_d_node = " + self.ui.comboBoxD.currentText()
                    #print(s)
                    s = s[5:]
                    layer.setSubsetString(s) 
        
        elif (self.base_obj_def_name == "o_or_d"):
            origin_layers = QgsProject.instance().mapLayersByName("Origines")
            destination_layers = QgsProject.instance().mapLayersByName("Destinations")
            
            if len(origin_layers)>0 and len(destination_layers)>0:
                origin_layer = origin_layers[0]
                destination_layer = destination_layers[0]
                if (self.base_obj_subtype_id == 1) or (self.base_obj_subtype_id == 2): # Leave after, from origin
                    if (self.ui.comboBoxO.currentText()!=""):
                        s_orig = "id = " + self.ui.comboBoxO.currentText()
                    else: 
                        s_orig="id = ANY (ARRAY"+str(self.caller.orig_nodes)+"::bigint[])"        
                    s_dest = "(1=2)"
                if (self.base_obj_subtype_id == 4) or (self.base_obj_subtype_id == 5): # Arrive before, to destination
                    if (self.ui.comboBoxD.currentText()!=""):
                        s_dest = "id = " + self.ui.comboBoxD.currentText()
                    else:
                        s_dest = "id = ANY (ARRAY"+str(self.caller.dest_nodes)+"::bigint[])"
                    s_orig = "(1=2)"
                origin_layer.setSubsetString(s_orig)
                destination_layer.setSubsetString(s_dest)
            
            if (self.base_obj_subtype_id == 1) or (self.base_obj_subtype_id == 4):
                list = QgsProject.instance().mapLayersByName("Détail des arborescences")                
            elif (self.base_obj_subtype_id == 2) or (self.base_obj_subtype_id == 5):
                list = QgsProject.instance().mapLayersByName("Isochrones désagrégées")
            if len(list)>0:
                layer = list[0]
                s = ""
                if (self.base_obj_subtype_id == 1) or (self.base_obj_subtype_id == 2):
                    if (self.ui.comboBoxO.currentText() != ""):
                        s = s + " AND root_node = " + self.ui.comboBoxO.currentText()
                        
                    if (self.ui.comboBoxDepTime.currentText() != ""):
                        s = s + " AND root_timestamp::time = '" + self.ui.comboBoxDepTime.currentText() + "'"
                        s2 = "id = " + self.ui.comboBoxO.currentText()
                elif (self.base_obj_subtype_id == 4) or (self.base_obj_subtype_id == 5):
                    if (self.ui.comboBoxD.currentText() != ""):
                        s = s + " AND root_node = " + self.ui.comboBoxD.currentText()
                        s2 = "id = " + self.ui.comboBoxD.currentText()
                    if (self.ui.comboBoxArrTime.currentText() != ""):
                        s = s + " AND root_timestamp::time = '" + self.ui.comboBoxArrTime.currentText() + "'"
                if (self.ui.comboBoxDay.currentText() != ""):
                    s = s + " AND root_timestamp::date = '" + self.ui.comboBoxDay.currentText() + "'"
                
                s = s[5:] 
                print(s)
                layer.setSubsetString(s) 
    
    def _slotPushButtonReqDisplayClicked(self):
        # Define symbol size and color values
        size_indic_name=self.caller.modelSizeIndic.record(self.ui.comboBoxSizeIndic.currentIndex()).value("col_name")
        if ( size_indic_name!="" and size_indic_name != None ):
            s="SELECT pgtempus.tempus_map_indicator('"+self.ui.comboBoxReq.currentText()+"', '"+size_indic_name+"', 'size', "+str(self.ui.spinBoxSizeIndicMinValue.value())+", "+str(self.ui.spinBoxSizeIndicMaxValue.value())+", "+str(self.ui.doubleSpinBoxSize.value())+")"
            q=QtSql.QSqlQuery(self.db)
            q.exec_(unicode(s))
        else:
            s="UPDATE tempus_results."+self.ui.comboBoxReq.currentText()+" SET symbol_size = 1"
            q=QtSql.QSqlQuery(self.db)
            q.exec_(unicode(s)) 
        
        color_indic_name=self.caller.modelColorIndic.record(self.ui.comboBoxColorIndic.currentIndex()).value("col_name")
        if ( ( self.ui.radioButtonColorGrade.isChecked() ) and ( color_indic_name!="" and color_indic_name != None ) ):
            s="SELECT pgtempus.tempus_map_indicator('"+self.ui.comboBoxReq.currentText()+"', '"+color_indic_name+"', 'color', "+str(self.ui.spinBoxColorIndicMinValue.value())+", "+str(self.ui.spinBoxColorIndicMaxValue.value())+", 1)"
            q=QtSql.QSqlQuery(self.db)
            q.exec_(unicode(s))
        else:
            s="UPDATE tempus_results."+self.ui.comboBoxReq.currentText()+" SET symbol_color = 1"
            q=QtSql.QSqlQuery(self.db)
            q.exec_(unicode(s)) 
        
        # Make all others layers unvisible
        for layer in self.caller.node_indicators.findLayers():
            layer.setItemVisibilityChecked(False)
        
        s=""
        # Display layer
        if (self.base_obj_def_name=="pt_stop"):
            self.caller.dockwidget.ui.comboBoxObjType.setCurrentIndex(self.caller.dockwidget.ui.comboBoxObjType.findText(u"Arrêts"))
            if (self.base_obj_subtype_id == 1): # stop point
                layer = self.indicDisplay(self.ui.comboBoxReq.currentText(), self.ui.comboBoxReq.currentText(), self.plugin_dir + '/styles/indic/pt_stop_point.qml', 'gid', "geom", s)
                field_to_value_relation(layer, "source_id", self.caller.source_layer, "name")
                field_to_value_relation(layer, "pt_stop_area_node_id", self.caller.poi_layer, "name")
            elif (self.base_obj_subtype_id == 2): # stop area
                layer = self.indicDisplay(self.ui.comboBoxReq.currentText(), self.ui.comboBoxReq.currentText(), self.plugin_dir + '/styles/indic/pt_stop_area.qml', 'gid', "geom", s)
                field_to_value_relation(layer, "source_id", self.caller.source_layer, "name")
        elif (self.base_obj_def_name =="pt_route"):
            self.caller.dockwidget.ui.comboBoxObjType.setCurrentIndex(self.caller.dockwidget.ui.comboBoxObjType.findText(u"Lignes"))
            if (self.base_obj_subtype_id == 1): # PT trip
                layer = self.indicDisplay(self.ui.comboBoxReq.currentText(), self.ui.comboBoxReq.currentText()+"_nodes", self.plugin_dir + '/styles/indic/pt_route_trip_node.qml', 'gid', "pt_stops_geom", s)
                field_to_value_relation(layer, "source_id", self.caller.source_layer, "name")   
                field_to_value_relation(layer, "pt_agency_id", self.caller.pt_agency_layer, "name")   
                field_to_value_relation(layer, "pt_route_id", self.caller.pt_route_layer, "long_name")   
                layer = self.indicDisplay(self.ui.comboBoxReq.currentText(), self.ui.comboBoxReq.currentText()+"_arcs", self.plugin_dir + '/styles/indic/pt_route_trip_arc.qml', 'gid', "pt_arcs_geom", s)
                field_to_value_relation(layer, "source_id", self.caller.source_layer, "name")   
                field_to_value_relation(layer, "pt_agency_id", self.caller.pt_agency_layer, "name")   
                field_to_value_relation(layer, "pt_route_id", self.caller.pt_route_layer, "long_name")  
            elif (self.base_obj_subtype_id == 2): # PT route path
                layer = self.indicDisplay(self.ui.comboBoxReq.currentText(), self.ui.comboBoxReq.currentText()+"_nodes", self.plugin_dir + '/styles/indic/pt_route_path_node.qml', 'id', "pt_stops_geom", s)
                field_to_value_relation(layer, "source_id", self.caller.source_layer, "name")   
                field_to_value_relation(layer, "pt_agency_id", self.caller.pt_agency_layer, "name")   
                field_to_value_relation(layer, "pt_route_id", self.caller.pt_route_layer, "long_name")   
                layer = self.indicDisplay(self.ui.comboBoxReq.currentText(), self.ui.comboBoxReq.currentText()+"_arcs", self.plugin_dir + '/styles/indic/pt_route_path_arc.qml', 'id', "pt_arcs_geom", s)
                field_to_value_relation(layer, "source_id", self.caller.source_layer, "name")   
                field_to_value_relation(layer, "pt_agency_id", self.caller.pt_agency_layer, "name")   
                field_to_value_relation(layer, "pt_route_id", self.caller.pt_route_layer, "long_name")   
            elif (self.base_obj_subtype_id == 3): # PT route
                layer = self.indicDisplay(self.ui.comboBoxReq.currentText(), self.ui.comboBoxReq.currentText(), self.plugin_dir+'/styles/indic/pt_route.qml', 'gid', None, s)
                field_to_value_relation(layer, "source_id", self.caller.source_layer, "name")                
        elif (self.base_obj_def_name == "pt_agency"):
            self.caller.dockwidget.ui.comboBoxObjType.setCurrentIndex(self.caller.dockwidget.ui.comboBoxObjType.findText(u"Expoitants"))
            layer = self.indicDisplay(self.ui.comboBoxReq.currentText(), self.ui.comboBoxReq.currentText(), self.plugin_dir+'/styles/indic/pt_agency.qml', 'gid', None, s)
            field_to_value_relation(layer, "source_id", self.caller.source_layer, "name")                
        
        elif (self.base_obj_def_name == "od" or self.base_obj_def_name=="o_or_d"):
            self.caller.addODVehLayers() 
            for layer in self.caller.node_indicators.findLayers():
                if (layer.name()== "Destinations" or layer.name() == "Origines"):
                    layer.setItemVisibilityChecked(True)
            
            if (self.base_obj_def_name == "od"):
                self.caller.dockwidget.ui.comboBoxObjType.setCurrentIndex(self.caller.dockwidget.ui.comboBoxObjType.findText(u"OD"))
                
                s_orig = "SELECT DISTINCT path_o_node as id FROM tempus_results." + self.ui.comboBoxReq.currentText()
                q=QtSql.QSqlQuery(unicode(s_orig), self.caller.db)
                while q.next():
                    self.caller.orig_nodes.append(q.value(0))
                
                s_dest = "SELECT DISTINCT path_d_node as id FROM tempus_results." + self.ui.comboBoxReq.currentText()
                q=QtSql.QSqlQuery(unicode(s_dest), self.caller.db)
                while q.next():
                    self.caller.dest_nodes.append(q.value(0))                
                
                if ( self.base_obj_subtype_id == 1 ): # arc    
                    layer = self.indicDisplay( self.ui.comboBoxReq.currentText(), "Arcs composant les chemins", self.plugin_dir + "/styles/indic/od_arc.qml", "arc_id", "geom_arc", "in_paths = true" ) 
                    field_to_value_relation(layer, "transport_mode_id", self.caller.transport_mode_layer, "name")
                    field_to_value_relation(layer, "orig_mode", self.caller.transport_mode_layer, "name")
                    field_to_value_relation(layer, "dest_mode", self.caller.transport_mode_layer, "name")
                
                elif ( self.base_obj_subtype_id == 2 ): # trip
                    layer = self.indicDisplay(self.ui.comboBoxReq.currentText(), "Trajets monomodaux", self.plugin_dir + "/styles/indic/od_trip.qml", "id", "geom_arc", s)
                    field_to_value_relation(layer, "transport_mode_id", self.caller.transport_mode_layer, "name")
                    field_to_value_relation(layer, "orig_mode", self.caller.transport_mode_layer, "name")
                    field_to_value_relation(layer, "dest_mode", self.caller.transport_mode_layer, "name")
                    field_to_value_relation(layer, "pt_agency_id", self.caller.pt_agency_layer, "name")
                    field_to_value_relation(layer, "pt_route_id", self.caller.pt_route_layer, "long_name")                                
                
                elif ( self.base_obj_subtype_id == 3 ): # path
                    layer = self.indicDisplay(self.ui.comboBoxReq.currentText(), "Chemins multimodaux", self.plugin_dir + "/styles/indic/od_path.qml", "id", "geom", s)
                    field_to_value_relation(layer, "orig_mode", self.caller.transport_mode_layer, "name")
                    field_to_value_relation(layer, "dest_mode", self.caller.transport_mode_layer, "name")
                
                elif ( self.base_obj_subtype_id == 4 ): # od
                    layer = self.indicDisplay(self.ui.comboBoxReq.currentText(), "OD agrégées", self.plugin_dir + "/styles/indic/od.qml", "id", "geom", s)
                    field_to_value_relation(layer, "orig_mode", self.caller.transport_mode_layer, "name")
                    field_to_value_relation(layer, "dest_mode", self.caller.transport_mode_layer, "name")
                
                layer.attributeTableConfig().setSortExpression("cost")
                layer.attributeTableConfig().setSortOrder(Qt.AscendingOrder)
             
            elif (self.base_obj_def_name=="o_or_d"):
                self.caller.dockwidget.ui.comboBoxObjType.setCurrentIndex(self.caller.dockwidget.ui.comboBoxObjType.findText(u"O ou D"))

                s_orig = "SELECT DISTINCT root_node as id FROM tempus_results." + self.ui.comboBoxReq.currentText()
                q=QtSql.QSqlQuery(unicode(s_orig), self.caller.db)
                while q.next():
                    if (self.base_obj_subtype_id <= 3):
                        self.caller.orig_nodes.append(q.value(0))
                    else:
                        self.caller.dest_nodes.append(q.value(0))
                
                if (self.base_obj_subtype_id == 1) or (self.base_obj_subtype_id == 4):
                    layer = self.indicDisplay(self.ui.comboBoxReq.currentText(), "Détail des arborescences", self.plugin_dir + "/styles/indic/o_or_d_isochron_root_day_time_by_mode.qml", "id", "geom", s)
                    field_to_value_relation(layer, "root_mode", self.caller.transport_mode_layer, "name")
                    field_to_value_relation(layer, "transport_mode_id", self.caller.transport_mode_layer, "name")
                elif (self.base_obj_subtype_id == 2) or (self.base_obj_subtype_id == 5):
                    layer = self.indicDisplay(self.ui.comboBoxReq.currentText(), "Isochrones désagrégées", self.plugin_dir + "/styles/indic/o_or_d_isochron_root_day_time_by_cost.qml", "id", "geom", s) 
                    field_to_value_relation(layer, "root_mode", self.caller.transport_mode_layer, "name")
                    field_to_value_relation(layer, "transport_mode_id", self.caller.transport_mode_layer, "name")
                elif (self.base_obj_subtype_id == 3) or (self.base_obj_subtype_id == 6):
                    layer = self.indicDisplay(self.ui.comboBoxReq.currentText(), "Isochrones agrégées", self.plugin_dir + "/styles/indic/o_or_d_isochron_global.qml", "id", "geom", s)
                elif (self.base_obj_subtype_id == 7):
                    layer = self.indicDisplay(self.ui.comboBoxReq.currentText(), "Polygones isochrones", self.plugin_dir + "/styles/indic/o_or_d_isochron_polygons.qml", "id", "geom", s)
            
            self.iface.setActiveLayer(layer)
            #self._slotPushButtonFilterClicked()
            
    def indicDisplay(self, table_name, layer_alias, layer_style_path, col_id, col_geom, filter):
        # Remove layer having the same alias
        layerList = [layer for layer in QgsProject.instance().mapLayers().values() if (layer.name()==layer_alias)]
        for lyr in layerList:
            QgsProject.instance().removeMapLayer(lyr.id())
        
        # Create layer
        uri=QgsDataSourceUri()
        uri.setConnection(self.db.hostName(), str(self.db.port()), self.db.databaseName(), self.db.userName(), self.db.password())
        uri.setDataSource("tempus_results", table_name, col_geom, "", col_id)
        layer = QgsVectorLayer(uri.uri(), layer_alias, "postgres")
        layer.setProviderEncoding(u'UTF-8')
        layer.dataProvider().setEncoding(u'UTF-8')
        
        # Add in the layer manager
        QgsProject.instance().addMapLayer(layer, False)
        node_layer = QgsLayerTreeLayer(layer)
        self.caller.node_indicators.addChildNode(node_layer)            
        self.caller.node_indicators.setExpanded(True)
        node_layer.setExpanded(False)
        self.caller.node_networks.setExpanded(False)
        #self.caller.node_zoning.setExpanded(False)
        
        # Set subset if necessary
        layer.setSubsetString(filter)
        
        # Set style
        if (layer_style_path != ''):
            layer.loadNamedStyle(layer_style_path)
            QgsProject.instance().layerTreeRoot().findLayer(layer.id()).setItemVisibilityChecked(True)
        
        # Zoom to layer
        list = [layer]
        self.caller.zoomToLayersList(list, True)
        
        return layer
    
    def refreshReq(self):
        self.caller.modelReq.setQuery( "( \
                                         SELECT table_name, catalog.id, base_obj_type_id, base_obj_type.def_name, base_obj_subtype_id, comment \
                                         FROM tempus_results.catalog JOIN pgtempus.base_obj_type ON (base_obj_type.id = catalog.base_obj_type_id) \
                                        ) UNION \
                                        SELECT '', -1, null, null, null, null \
                                        ORDER BY 2", self.caller.db )
        self.caller.modelIsochronReq.setQuery( "( \
                                                 SELECT table_name, catalog.id, base_obj_type_id, base_obj_type.def_name, base_obj_subtype_id, comment \
                                                 FROM tempus_results.catalog JOIN pgtempus.base_obj_type ON (base_obj_type.id = catalog.base_obj_type_id) \
                                                 WHERE base_obj_type_id = 5\
                                                ) UNION \
                                                SELECT '', -1, null, null, null, null \
                                                ORDER BY 2", self.caller.db )
        self.caller.modelAgregatedIsochronReq.setQuery( "( \
                                                             SELECT table_name, catalog.id, base_obj_type_id, base_obj_type.def_name, base_obj_subtype_id, comment \
                                                             FROM tempus_results.catalog JOIN pgtempus.base_obj_type ON (base_obj_type.id = catalog.base_obj_type_id) \
                                                             WHERE base_obj_type_id = 5 AND base_obj_subtype_id = 3\
                                                         ) UNION \
                                                         SELECT '', -1, null, null, null, null \
                                                         ORDER BY 2", self.caller.db )
        self.caller.modelPolygonIsochronReq.setQuery( "( \
                                                            SELECT table_name, catalog.id, base_obj_type_id, base_obj_type.def_name, base_obj_subtype_id, comment \
                                                            FROM tempus_results.catalog JOIN pgtempus.base_obj_type ON (base_obj_type.id = catalog.base_obj_type_id) \
                                                            WHERE base_obj_type_id = 5 AND base_obj_subtype_id = 7\
                                                       ) UNION \
                                                       SELECT '', -1, null, null, null, null \
                                                       ORDER BY 2", self.caller.db )
    
    def updateReqIndicators(self):
        s="SELECT name, id, col_name FROM pgtempus.indic \
           WHERE map_size = TRUE AND col_name IN \
           (\
                SELECT column_name \
                FROM information_schema.columns \
                WHERE table_schema = 'tempus_results' AND table_name = '"+self.ui.comboBoxReq.currentText()+"'\
           )\
           UNION \
           (\
                SELECT '', -1, '' \
           )\
          ORDER BY 2"
        self.caller.modelSizeIndic.setQuery(s, self.db)
        
        s="SELECT name, id, col_name FROM pgtempus.indic \
           WHERE map_color = TRUE AND col_name IN \
           (\
                SELECT column_name \
                FROM information_schema.columns \
                WHERE table_schema = 'tempus_results' AND table_name = '"+self.ui.comboBoxReq.currentText()+"'\
           )\
           UNION \
           (\
                SELECT '', -1, '' \
           )\
          ORDER BY 2"
        self.caller.modelColorIndic.setQuery(s,self.db)

    def _slotPushButtonAddClicked(self):
        if (self.ui.comboBoxReq.currentText()!= "") and (self.ui.comboBoxBarrierSource.currentText()!=""):
            box = QMessageBox()
            box.setWindowFlags(Qt.WindowStaysOnTopHint)
            box.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel);
            box.setText(u"Vous vous apprêtez à intégrer de nouveaux arcs correspondant aux barrières physiques dans la couche résultats. Confirmez-vous la requête ?")
            ret = box.exec_()
            if (ret == QMessageBox.Ok):
                s = "WITH selected_barriers AS (\
                        WITH extent AS (\
                            SELECT st_setsrid(st_extent(geom), 2154) as geom FROM tempus_results."+self.ui.comboBoxReq.currentText()+"\
                        )\
                        SELECT arc.id, arc.node_from_id, arc.node_to_id, diffusion, crossability, arc.geom \
                        FROM tempus_networks.arc JOIN extent ON (st_intersects(arc.geom, extent.geom)) \
                        WHERE type = 3 AND source_id = (SELECT id FROM pgtempus.source WHERE name = '"+self.ui.comboBoxBarrierSource.currentText()+"') \
                    )\
                    INSERT INTO tempus_results."+self.ui.comboBoxReq.currentText()+"(arc_id, pred_node_id, node_id, diffusion, crossability, geom) \
                    SELECT * FROM selected_barriers;"
                q=QtSql.QSqlQuery(self.db)
                q.exec_(unicode(s))
                if (self.base_obj_subtype_id == 1) or (self.base_obj_subtype_id == 5):
                    layer_name = "Isochrones par noeud racine, par jour et par heure"
                elif (self.base_obj_subtype_id == 2) or (self.base_obj_subtype_id == 6):
                    layer_name = "Isochrones par noeud racine et par jour"
                elif (self.base_obj_subtype_id == 3) or (self.base_obj_subtype_id == 7):
                    layer_name = "Isochrones par noeud racine"
                elif (self.base_obj_subtype_id == 4) or (self.base_obj_subtype_id == 8):
                    layer_name = "Isochrone complètement agrégée"
                list = QgsProject.instance().mapLayersByName(layer_name)
                if len(list)>0:
                    self.caller.zoomToLayersList(list, True)
            
    def _slotPushButtonDeleteClicked(self):
        if (self.ui.comboBoxReq.currentText()!= ""):
            box = QMessageBox()
            box.setWindowFlags(Qt.WindowStaysOnTopHint)
            box.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel);
            box.setText(u"Vous vous apprêtez à supprimer tous les arcs correspondant aux barrières physiques dans la couche résultats. Confirmez-vous la requête ?")
            ret = box.exec_()
            if (ret == QMessageBox.Ok):
                s = "DELETE FROM tempus_results."+self.ui.comboBoxReq.currentText()+"\
                     WHERE cost is null and pred_cost is null;"
                q=QtSql.QSqlQuery(self.db)
                q.exec_(unicode(s))
                if (self.base_obj_subtype_id == 1) or (self.base_obj_subtype_id == 5):
                    layer_name = "Isochrones par noeud racine, par jour et par heure"
                elif (self.base_obj_subtype_id == 2) or (self.base_obj_subtype_id == 6):
                    layer_name = "Isochrones par noeud racine et par jour"
                elif (self.base_obj_subtype_id == 3) or (self.base_obj_subtype_id == 7):
                    layer_name = "Isochrones par noeud racine"
                elif (self.base_obj_subtype_id == 4) or (self.base_obj_subtype_id == 8):
                    layer_name = "Isochrone complètement agrégée"
                list = QgsProject.instance().mapLayersByName(layer_name)
                if len(list)>0:
                    self.caller.zoomToLayersList(list, True)


