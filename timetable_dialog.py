# -*- coding: utf-8 -*-
"""
/***************************************************************************
 PluginTempusAccess
                                 A QGIS plugin
 Analyse de l'offre de transport en commun
                              -------------------
        begin                : 2016-10-22
        git sha              : $Format:%H$
        copyright            : (C) 2016 by Cerema
        email                : Aurelie.bousquet@cerema.fr, Patrick.Palmier@cerema.fr, helene.ly@cerema.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# import the PyQt and QGIS libraries
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from qgis.PyQt.QtWidgets import QDialog
from PyQt5.Qt import *
from PyQt5 import QtSql
from qgis.core import *
from qgis.utils import iface

# Initialize Qt resources from file resources.py
from .config import *
from .functions import *

import sys
import string
import os

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "\\forms")
from timetable_dialog import Ui_Dialog

class timetable_dialog(QDialog): 

    def __init__(self, caller, iface):
        QDialog.__init__(self)
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.caller = caller
        self.db = self.caller.db
        
        self.modelTimetable = QtSql.QSqlQueryModel()
        self.ui.tableViewTimetable.setModel(self.modelTimetable)
        self.ui.comboBoxPTStop.setModel(self.caller.modelPTStop)
        
        self._connectSlots()
    
    
    def _connectSlots(self):
        self.ui.comboBoxPTStop.currentIndexChanged.connect(self.updateTimetable)
        
        
    def updateTimetable(self):
        r="SELECT source.name as source_name, \
                  pt_agency.name as pt_agency_name, \
                  pt_route.long_name as pt_route_long_name, \
                  pt_trip.direction_headsign as direction, \
                  pt_stop.name as next_stop, \
                  pt_trip_type.name as pt_trip_type_name, \
                  time_from as leaving_time, \
                  time_to as arriving_time \
           FROM tempus_networks.pt_section_trip_stop_times p JOIN pgtempus.pt_trip_type ON (pt_trip_type.id = ANY(p.pt_trip_types_id)) \
                                                             JOIN tempus_networks.pt_trip ON (pt_trip.id = p.pt_trip_id) \
                                                             JOIN tempus_networks.pt_agency ON (pt_agency.id = p.pt_agency_id) \
                                                             JOIN pgtempus.source ON (pt_agency.source_id = source.id) \
                                                             JOIN tempus_networks.pt_route ON (pt_route.id = p.pt_route_id) \
                                                             JOIN tempus_networks.pt_stop ON (pt_stop.node_id = p.node_to_id) \
           WHERE day='"+self.caller.dockwidget.ui.calendarWidget.selectedDate().toString("yyyy-MM-dd")+"'::date \
             AND node_from_id = "+str(self.caller.modelPTStop.record(self.ui.comboBoxPTStop.currentIndex()).value("id"))+" and pt_trip_type.id >0 \
           ORDER BY time_from, pt_agency.id, pt_route.id"
        #print(r)
        self.modelTimetable.setQuery(unicode(r), self.db)
        self.ui.tableViewTimetable.setModel(self.modelTimetable)
        self.modelTimetable.setHeaderData(0, Qt.Horizontal, "Source") 
        self.modelTimetable.setHeaderData(1, Qt.Horizontal, "Exploitant") 
        self.modelTimetable.setHeaderData(2, Qt.Horizontal, "Ligne") 
        self.modelTimetable.setHeaderData(3, Qt.Horizontal, "Direction") 
        self.modelTimetable.setHeaderData(4, Qt.Horizontal, "Prochain arrêt") 
        self.modelTimetable.setHeaderData(5, Qt.Horizontal, "Type service") 
        self.modelTimetable.setHeaderData(6, Qt.Horizontal, "Heure passage") 
        self.modelTimetable.setHeaderData(7, Qt.Horizontal, "Heure au prochain arrêt")
        self.ui.tableViewTimetable.setColumnWidth(7,160)
        