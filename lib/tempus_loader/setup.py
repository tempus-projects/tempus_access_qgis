#!/usr/bin/env python
# -*- coding: utf-8 -*-
from setuptools import setup
from distutils.spawn import find_executable

assert find_executable('psql'), 'psql executable not found!!'

setup(
    name='tempus_loader',
    version='2.0.0',
    description="Tempus data loader",
    long_description="Tempus data loader",
    classifiers=[
        "Programming Language :: Python",
        "Operating System :: OS Independent",
        "Intended Audience :: Developers",
        "Topic :: System :: Software Distribution",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],

    keywords='',
    author='Tempus Team',
    author_email='infos@oslandia.com',
    maintainer='Cerema',
    maintainer_email='modelisation-deplacements@cerema.fr',

    license='LGPL',
    packages=['src', 'src/provider'],
    package_data={
        'src/provider': ['sql/*.sql'],
    },
    include_package_data=True,
    zip_safe=False,
    install_requires=('pglite'),
    extras_require={
        "develop": ()
    },

    entry_points=dict(console_scripts=[
        'tempus_loader=src.tempus_loader:main',
    ]),
)
