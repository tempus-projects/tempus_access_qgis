#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Tempus zoning data importer

import provider
import sys  

def import_zoning_generic(args, shape_options):
    """ Load areas data into a TempusAccess database with a generic method."""
    subs = {}
    if args.source_name is None:
        sys.stderr.write("A road network name must be supplied. Use --source-name\n")
        sys.exit(1)        
    subs['source_name'] = args.source_name[0]
         
    if args.prefix is None:
        args.prefix = ''
        
    if args.source_srid is None:
        subs["source_srid"] = "2154"
    else:
        subs["source_srid"] = args.source_srid
        
    if args.target_srid is None:
        subs["target_srid"] = "2154"
    else: 
        subs["target_srid"] = args.target_srid
        
    if args.source_comment and args.source_comment is not None:
        subs["source_comment"] = args.source_comment
    else:
        subs["source_comment"] = ""
        
    if args.noclean:
        subs["no_clean"] = "True"
    else:
        subs["no_clean"] = "False"
    
    if args.merge:
        subs["merge"] = "True"
    else:
        subs["merge"] = "False"
    
    if args.geom_simplify and args.geom_simplify is not None: 
        subs["geom_simplify"] = args.geom_simplify
    else: 
        subs["geom_simplify"] = "0"
    
    if args.noclean:
        subs["no_clean"] = "True"
    else:
        subs["no_clean"] = "False"
    
    subs["temp_schema"]=provider.config.TEMPSCHEMA
        
    if args.id_field is None:
        sys.stderr.write("An ID field name must be supplied. Use --id-field\n")
        sys.exit(1)
    subs["id_field"]=args.id_field
    
    if args.name_field is None:
        subs["name_field"]=''
    else:
        subs["name_field"] = args.name_field
    
    shape_options['S'] = False # There are multipolygons with two parts, so, the -S option (which converts multi geometries to simple ones) cannot be used
    tempusi = provider.ImportZoningGeneric(path=args.path[0], prefix=args.prefix, dbstring=args.dbstring, logfile=args.logfile, options=shape_options, subs=subs)
    return tempusi.run()

def import_zoning_ign_adminexpress(args, shape_options):
    """ Load IGN AdminExpress areas data into a TempusAccess database."""
    subs = {}
    if args.source_name is None:
        sys.stderr.write("A road network name must be supplied. Use --source-name\n")
        sys.exit(1)        
    subs['source_name'] = args.source_name[0]
        
    if args.prefix is None:
        args.prefix = ''
        
    if args.source_srid is None:
        subs["source_srid"] = "2154"
    else:
        subs["source_srid"] = args.source_srid
        
    if args.target_srid is None:
        subs["target_srid"] = "2154"
    else: 
        subs["target_srid"] = args.target_srid
        
    if args.source_comment and args.source_comment is not None:
        subs["source_comment"] = args.source_comment
    else:
        subs["source_comment"] = ""
        
    if args.noclean:
        subs["no_clean"] = "True"
    else:
        subs["no_clean"] = "False"
    
    if args.merge:
        subs["merge"] = "True"
    else:
        subs["merge"] = "False"
    
    if args.geom_simplify and args.geom_simplify is not None: 
        subs["geom_simplify"] = args.geom_simplify
    else: 
        subs["geom_simplify"] = "0"
    
    if args.noclean:
        subs["no_clean"] = "True"
    else:
        subs["no_clean"] = "False"
    
    subs["temp_schema"]=provider.config.TEMPSCHEMA
    shape_options['S'] = False # There are multipolygons with two parts, so, the -S option (which converts multi geometries to simple ones) cannot be used
    adminexpi = provider.ImportZoningIGNAdminExpress(path=args.path[0], prefix=args.prefix, dbstring=args.dbstring, logfile=args.logfile, options=shape_options, subs=subs)
    return adminexpi.run()
    
    