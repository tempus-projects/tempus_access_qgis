#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Tempus data exporter

import provider

# def export_road_network(args):
    # subs = {}
    # if args.source_name is None:
        # sys.stderr.write("The road network name must be specified with --source-name !\n")
        # sys.exit(1)
    # subs["source_name"] = args.source_name
    # subs["target_srid"] = args.target_srid
    # subs["temp_schema"] = provider.config.TEMPSCHEMA
    # roade = provider.ExportRoadTempus(path=args.path, dbstring=args.dbstring, sep=",", text_format = '.txt', encoding=args.encoding, copymode=args.copymode, logfile=args.logfile, subs=subs)
    # return roade.run()


def export_pt_gtfs(args):
    subs = {}
    if args.source_name is None:
        sys.stderr.write("The PT data source name must be specified with --source_name !\n")
        sys.exit(1)
    s="["
    for i in range(len(args.source_name)):
        s = s + "'" + args.source_name[i] + "', "
    s = s[:len(s) - 2] + "]"
    print(s)
    subs["source_name"] = s
    subs["target_srid"] = str(args.target_srid)
    subs["temp_schema"] = provider.config.TEMPSCHEMA
    pte = provider.ExportPTGTFS(path=args.path[0], dbstring=args.dbstring, sep=",", text_format = '.txt', encoding=args.encoding, copymode=args.copymode, logfile=args.logfile, subs=subs)
    return pte.run()    

def export_pt_tempus(args):
    subs = {}
    if args.source_name is None:
        sys.stderr.write("The PT data source name must be specified with --source_name !\n")
        sys.exit(1)
    subs["source_name"] = args.source_name
    subs["target_srid"] = str(args.target_srid)
    subs["temp_schema"] = provider.config.TEMPSCHEMA
    pte = provider.ExportPTTempus(path=args.path[0], dbstring=args.dbstring, sep=",", text_format = '.txt', encoding=args.encoding, copymode=args.copymode, logfile=args.logfile, subs=subs)
    return pte.run()    

def export_poi_generic(args):
    subs = {}
    if args.source_name is None:
        sys.stderr.write("The POI data source name must be specified with --source_name !\n")
        sys.exit(1)
    s="["
    for i in range(len(args.source_name)):
        s = s + "'" + args.source_name[i] + "', "
    s = s[:len(s) - 2] + "]"
    print(s)
    subs["source_name"] = s
    subs["target_srid"] = str(args.target_srid)
    subs["temp_schema"] = provider.config.TEMPSCHEMA
    pte = provider.ExportPOIGeneric(path=args.path[0], dbstring=args.dbstring, sep=",", text_format = '.txt', encoding=args.encoding, copymode=args.copymode, logfile=args.logfile, subs=subs)
    return pte.run()   
    
    