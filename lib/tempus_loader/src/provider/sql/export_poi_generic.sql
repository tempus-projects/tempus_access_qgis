CREATE TABLE %(temp_schema).poi AS
(
    SELECT poi.node_id as id, poi.name, poi.type, poi.comment, node.geom
    FROM tempus_networks.poi JOIN tempus_networks.node ON (node.id = poi.node_id)
                             JOIN pgtempus.source ON (node.source_id = source.id)
    WHERE source.name = ANY(ARRAY%(source_name))
);
