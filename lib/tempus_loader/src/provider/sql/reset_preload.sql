/*
        Substitutions options
        %(temp_schema): name of schema containing temporary data
*/

DROP SCHEMA IF EXISTS %(temp_schema) CASCADE;
CREATE SCHEMA %(temp_schema); 

CREATE TABLE %(temp_schema).data_format
(
    data_type character varying,
    data_format character varying, 
    name character varying,
    model_version character varying,
    default_encoding character varying,
    default_srid integer, 
    path_type character varying
); 
COMMENT ON TABLE %(temp_schema).data_format IS 'Plugin system table: do not modify!'; 


CREATE TABLE %(temp_schema).agregate
(
    id integer,
    name character varying,
    func_name character varying
); 
COMMENT ON TABLE %(temp_schema).agregate IS 'Plugin system table: do not modify !';

CREATE TABLE %(temp_schema).modality
(
    var character varying, 
    mod_code integer, 
    mod_lib character varying,
    needs_pt boolean, 
    CONSTRAINT modality_pkey PRIMARY KEY (var, mod_code)
); 
COMMENT ON TABLE %(temp_schema).modality IS 'Plugin system table: do not modify !';

CREATE TABLE %(temp_schema).base_obj_type
(
  id integer NOT NULL,
  name character varying,
  def_name character varying,
  needs_pt boolean, 
  CONSTRAINT obj_type_pkey PRIMARY KEY (id)
); 
COMMENT ON TABLE %(temp_schema).base_obj_type
  IS 'Plugin system table: do not modify !';

COMMENT ON COLUMN %(temp_schema).base_obj_type.name IS 'Object name';
COMMENT ON COLUMN %(temp_schema).base_obj_type.def_name IS 'Default name of the layer'; 

CREATE TABLE %(temp_schema).base_obj_subtype
(
  obj_type_id integer NOT NULL, 
  obj_subtype_id integer NOT NULL, 
  subtype_name character varying, 
  CONSTRAINT obj_subtype_pkey PRIMARY KEY (obj_type_id, obj_subtype_id)
); 
COMMENT ON TABLE %(temp_schema).base_obj_subtype
  IS 'Plugin system table: do not modify !';

CREATE TABLE %(temp_schema).indic
(
    id integer PRIMARY KEY,
    name character varying,
    map_size boolean,
    map_color boolean,
    col_name character varying
);
COMMENT ON TABLE %(temp_schema).indic
  IS 'Plugin system table: do not modify !';
 
CREATE TABLE %(temp_schema).holiday_period
(
  id serial NOT NULL,
  name character varying,
  start_date date,
  end_date date
);
