
do $$
begin
raise notice '==== TempusAccess indicators function for PT stops ===';
end$$;

DROP FUNCTION IF EXISTS pgtempus.tempus_create_pt_stop_indicator_layer( character varying, character varying, integer[], integer[], integer[], date[], integer, time, time, boolean );

CREATE OR REPLACE FUNCTION pgtempus.tempus_create_pt_stop_indicator_layer(
                                                                    p_table_name_pt_stop_point character varying,
                                                                    p_table_name_pt_stop_area character varying,
                                                                    p_sources_id smallint[],
                                                                    p_pt_trip_types integer[], 
                                                                    p_pt_agencies integer[], 
                                                                    p_days date[], 
                                                                    p_day_ag integer DEFAULT 1,
                                                                    p_time_start time DEFAULT '00:00:00'::time,
                                                                    p_time_end time DEFAULT '23:59:00'::time, 
                                                                    p_time_constraint_dep boolean DEFAULT True
                                                                  ) 
RETURNS void AS 
$BODY$
DECLARE
    s character varying;
    t character varying;
    day_ag_str character varying;
BEGIN
    -- Mandatory parameters
    IF (
            p_sources_id is null OR p_pt_trip_types is null OR p_pt_agencies is null OR p_days is null OR 
            array_length(p_sources_id, 1)=0 OR array_length(p_pt_trip_types, 1)=0 OR array_length(p_pt_agencies, 1)=0 OR array_length(p_days, 1)=0
       )
    THEN 
        RAISE EXCEPTION 'Parameters 1 to 4 must be non-empty arrays';
    END IF;
    
    IF p_time_constraint_dep = True
    THEN
        t = $$
              AND (coalesce(e1.time_from, e2.time_from) <= '$$ || p_time_end::character varying || $$')
              AND (coalesce(e1.time_from, e2.time_from) >= '$$ || p_time_start::character varying || $$')
            $$;    
    ELSE
        t = $$
              AND (coalesce(e1.time_to, e2.time_to) <= '$$ || p_time_end::character varying || $$')
              AND (coalesce(e1.time_to, e2.time_to) >= '$$ || p_time_start::character varying || $$')
            $$;    
    END IF;
    
    -- Filtered data
    s = $$
        DROP TABLE IF EXISTS filtered_data;
        CREATE TEMPORARY TABLE filtered_data AS
        (
            SELECT coalesce(e1.node_from_id, e2.node_to_id) as id, 
                   unnest(coalesce(e1.pt_trip_types_id, e2.pt_trip_types_id)) as pt_trip_type_id, 
                   coalesce(e1.pt_agency_id, e2.pt_agency_id) as pt_agency_id, 
                   coalesce(e1.pt_route_id, e2.pt_route_id) as pt_route_id, 
                   e2.time_to, 
                   e1.time_from, 
                   coalesce(e1.day, e2.day) as day
            FROM tempus_networks.pt_section_trip_stop_times e1 FULL JOIN tempus_networks.pt_section_trip_stop_times e2 
                                                            ON (
                                                                e1.node_from_id = e2.node_to_id AND
                                                                e1.pt_trip_id = e2.pt_trip_id AND
                                                                e1.day = e2.day
                                                               ) 
            WHERE coalesce(e1.source_id, e2.source_id) = ANY('$$ || p_sources_id::character varying || $$')
              AND coalesce(e1.pt_trip_types_id, e2.pt_trip_types_id) && '$$ || p_pt_trip_types::character varying || $$' = True
              AND coalesce(e1.pt_agency_id, e2.pt_agency_id) = ANY('$$ || p_pt_agencies::character varying || $$')
              AND coalesce(e1.day, e2.day) = ANY('$$ || p_days::character varying || $$')
              $$ || t || $$
        )
        $$; 
    RAISE NOTICE 'Filtered data:\n%', s; 
    EXECUTE(s);
    
    -- Daily aggregated data
    s= $$
            DROP TABLE IF EXISTS daily_aggregated_data; 
            CREATE TEMPORARY TABLE daily_aggregated_data AS
            (
                SELECT id, 
                       pt_trip_type_id, 
                       array_agg(DISTINCT pt_agency_id) as pt_agencies_id, 
                       array_agg(DISTINCT pt_route_id) as pt_routes_id, 
                       day, 
                       max(time_from) - min(time_from) as time_ampl_dep, 
                       max(time_to) - min(time_to) as time_ampl_arr,
                       min(time_from) as first_dep, 
                       max(time_from) as last_dep,
                       min(time_to) as first_arr, 
                       max(time_to) as last_arr, 
                       count(distinct time_from) as dep_num, 
                       count(distinct time_to) as arr_num
                FROM filtered_data
                GROUP BY id, pt_trip_type_id, day
            );
        $$;
    RAISE NOTICE 'Daily aggregated data:\n%', s;
    EXECUTE(s);
    
    -- Aggregating all days together
    IF p_day_ag IS NULL THEN
        p_day_ag=1;
    END IF;
    SELECT INTO day_ag_str 
                func_name 
    FROM pgtempus.agregate WHERE id = p_day_ag;
    
    s = $$
        DROP TABLE IF EXISTS tempus_results.$$ || p_table_name_pt_stop_point || $$ CASCADE;
        CREATE TABLE tempus_results.$$ || p_table_name_pt_stop_point || $$ AS
        (        
            SELECT row_number() over() as gid,
                   d.id, 
                   pt_stop_geom.name,
                   pt_stop_geom.pt_stop_area_node_id,
                   pt_stop_geom.source_id, 
                   pt_stop_geom.original_id, 
                   pt_stop_geom.traffic_rules,
                   pt_stop_geom.pt_fare_zone_id, 
                   d.pt_trip_type_id, 
                   pt_trip_type.name as pt_trip_type_name, 
                   pt_trip_type.gtfs_codes, 
                   array_agg(DISTINCT pt_agency.id) as pt_agencies_id,
                   array_agg(DISTINCT pt_agency.name) as pt_agencies_name, 
                   array_agg(DISTINCT pt_route.id) as pt_routes_id,
                   array_agg(DISTINCT pt_route.short_name) as pt_routes_short_name,
                   array_agg(DISTINCT pt_route.long_name) as pt_routes_long_name, 
                   $$ || array_length(p_days, 1)::character varying || $$::smallint as nb_req_days, 
                   array_length(array_remove(array_agg(DISTINCT CASE WHEN d.id IS NOT NULL THEN day END ORDER BY CASE WHEN d.id IS NOT NULL THEN day END), NULL), 1)::smallint as nb_pt_serv_days,
                   array_remove(array_agg(DISTINCT CASE WHEN d.id IS NOT NULL THEN day END ORDER BY CASE WHEN d.id IS NOT NULL THEN day END), NULL) as pt_serv_days,
                   (extract(epoch from $$ || day_ag_str || $$(time_ampl_dep))/60)::integer AS time_ampl_dep, 
                   (extract(epoch from $$ || day_ag_str || $$(time_ampl_arr))/60)::integer AS time_ampl_arr, 
                   date_trunc('second', $$ || day_ag_str || $$(first_dep))::time AS first_dep, 
                   date_trunc('second', $$ || day_ag_str || $$(last_dep))::time AS last_dep, 
                   date_trunc('second', $$ || day_ag_str || $$(first_arr))::time AS first_arr, 
                   date_trunc('second', $$ || day_ag_str || $$(last_arr))::time AS last_arr, 
                   round($$ || day_ag_str || $$(dep_num),2) AS dep_num, 
                   round($$ || day_ag_str || $$(arr_num),2) AS arr_num,
                   pt_stop_geom.pt_stop_geom as geom
            FROM daily_aggregated_data d JOIN tempus_networks.pt_stop_geom ON (d.id = pt_stop_geom.id)
                                         JOIN tempus_networks.pt_route ON (pt_route.id = ANY(d.pt_routes_id))
                                         JOIN tempus_networks.pt_agency ON (pt_agency.id = ANY(d.pt_agencies_id))
                                         JOIN pgtempus.pt_trip_type ON (d.pt_trip_type_id = pt_trip_type.id)
            GROUP BY d.id, 
                     d.pt_trip_type_id,  
                     pt_trip_type.name,  
                     pt_trip_type.gtfs_codes,
                     pt_stop_geom.name, 
                     pt_stop_geom.pt_stop_area_node_id,
                     pt_stop_geom.original_id, 
                     pt_stop_geom.traffic_rules, 
                     pt_stop_geom.pt_fare_zone_id, 
                     pt_stop_geom.source_id, 
                     pt_stop_geom.pt_stop_geom
            ORDER BY d.id, d.pt_trip_type_id
        );
        DELETE FROM tempus_results.catalog WHERE table_name = '$$ || p_table_name_pt_stop_point || $$';
        INSERT INTO tempus_results.catalog( table_name, base_obj_type_id, base_obj_subtype_id) VALUES ('$$ || p_table_name_pt_stop_point || $$', 1, 1);  
        $$;
    RAISE NOTICE '%', s; 
    EXECUTE(s);
    
    -- Add mapping fields and key
    s = $$
            ALTER TABLE tempus_results.$$ || p_table_name_pt_stop_point || $$ 
            ADD COLUMN symbol_size real, 
            ADD COLUMN symbol_color real, 
            ADD CONSTRAINT $$ || p_table_name_pt_stop_point || $$_pkey PRIMARY KEY(gid);
        $$;
    RAISE NOTICE '%', s; 
    EXECUTE(s);
    
    -- PT stations
    s = $$
            DROP TABLE IF EXISTS tempus_results.$$ || p_table_name_pt_stop_area || $$;
            CREATE TABLE tempus_results.$$ || p_table_name_pt_stop_area || $$ AS
            (
                SELECT row_number() over() as gid, * 
				FROM
				(
					SELECT pt_stop_area_node_id as id, 
                           poi.name,
						   node.source_id, 
                           array(SELECT distinct unnest(array_cat_agg(pt_agencies_id))) as pt_agencies_id, 
                           array(SELECT distinct unnest(array_cat_agg(pt_agencies_name))) as pt_agencies_name, 
						   pt_trip_type_id, 
						   pt_trip_type_name, 
						   gtfs_codes, 
						   node.geom, 
						   extract(epoch from (max(last_dep::time) - min(first_dep::time))::interval)/60 as time_ampl_dep, 
						   extract(epoch from (max(last_arr::time) - min(first_arr::time))::interval)/60 as time_ampl_arr, 
						   date_trunc('second', min(first_dep::time))::time as first_dep, 
						   date_trunc('second', max(last_dep::time))::time as last_dep, 
						   date_trunc('second', min(first_arr::time))::time as first_arr, 
						   date_trunc('second', max(last_arr::time))::time as last_arr, 
						   sum(dep_num) as dep_num, 
						   sum(arr_num) as arr_num 
					FROM tempus_results.$$ || p_table_name_pt_stop_point || $$ stop_point JOIN tempus_networks.poi ON (poi.node_id = stop_point.pt_stop_area_node_id)
                                                                                          JOIN tempus_networks.node ON (node.id = stop_point.pt_stop_area_node_id)
                    WHERE poi.type = 1
					GROUP BY pt_stop_area_node_id, poi.name, pt_trip_type_id, pt_trip_type_name, gtfs_codes, node.geom, node.source_id
					ORDER BY pt_stop_area_node_id, pt_trip_type_id 
				) q
            ); 
            DELETE FROM tempus_results.catalog WHERE table_name = '$$ || p_table_name_pt_stop_area || $$';
            INSERT INTO tempus_results.catalog( table_name, base_obj_type_id, base_obj_subtype_id) VALUES ('$$ || p_table_name_pt_stop_area || $$', 1, 2); 
        $$;
    EXECUTE(s);
    RAISE NOTICE '%', s;
    EXECUTE(s);
    
     -- Add mapping fields and key
    s = $$
            ALTER TABLE tempus_results.$$ || p_table_name_pt_stop_area || $$ 
            ADD COLUMN symbol_size real, 
            ADD COLUMN symbol_color real, 
            ADD CONSTRAINT $$ || p_table_name_pt_stop_area || $$_pkey PRIMARY KEY(gid);
        $$;
    RAISE NOTICE '%', s; 
    EXECUTE(s);
    
    RETURN;    
END;
$BODY$
LANGUAGE plpgsql; 

