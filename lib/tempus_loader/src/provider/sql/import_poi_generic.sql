/*

Generic POI importer
Parameters:
    %(source_name)
    %(target_srid)
    %(filter)
    %(link)
    %(max_dist)
    %(temp_schema)
    %(id_field)
    %(name_field)
    %(poi_type)
    %(merge)
*/

DO
$$
BEGIN
    IF %(merge) = True THEN
        RAISE notice '==== 0. Duplicate suppression ===';
        DELETE FROM %(temp_schema).poi
        WHERE id IN 
        (
            SELECT original_id 
            FROM tempus_networks.node
            WHERE source_id = (SELECT id FROM pgtempus.source WHERE name = '%(source_name)')
        );
    END IF;
END
$$;

DO
$$
BEGIN
RAISE notice '==== 1. Make geometries valid ===';
END
$$;

SELECT pgtempus.tempus_correction_geom('%(temp_schema)', 'poi', 'geom');


DO
$$
BEGIN
RAISE notice '==== 2. Create nodes and POIs ===';
END
$$;

DROP TABLE IF EXISTS %(temp_schema).node; 
CREATE TABLE %(temp_schema).node
(
    id bigserial PRIMARY KEY,
    original_id character varying, 
    name character varying,
    geom Geometry(PointZ, %(target_srid))
);
ALTER SEQUENCE %(temp_schema).node_id_seq MINVALUE 0;
SELECT setval('%(temp_schema).node_id_seq', (SELECT coalesce(max(id), 0) FROM tempus_networks.node));

INSERT INTO %(temp_schema).node( original_id, name, geom )
    SELECT %(id_field), %(name_field), st_force3DZ(st_transform(geom, %(target_srid))) 
    FROM %(temp_schema).poi
    WHERE %(filter);
    
CREATE INDEX ON %(temp_schema).node USING btree(original_id);
CREATE INDEX ON %(temp_schema).node USING gist(geom);

INSERT INTO tempus_networks.node(id, original_id, source_id, geom)
(
    SELECT id, 
           original_id,
           (SELECT id FROM pgtempus.source WHERE name = '%(source_name)'),
           geom
    FROM %(temp_schema).node
);

-- New POI
INSERT INTO tempus_networks.poi(node_id, name, type)
SELECT id, name, coalesce(%(poi_type), 8)
FROM %(temp_schema).node;

DO
$$
BEGIN
RAISE notice '==== 3. Attach to the road network ===';
END
$$;

DO
$$
BEGIN
    IF ( '%(link)'::character varying = 'True'::character varying ) THEN
        CREATE INDEX ON tempus_networks.arc USING gist( geography(st_transform(geom, 4326)) ); 
        PERFORM pgtempus.tempus_attach_node_to_a_road_section( node.id, (SELECT array_agg(id) FROM pgtempus.source WHERE type = 1), %(max_dist), ARRAY[1,4]::smallint[] )
        FROM %(temp_schema).node;
        DROP INDEX tempus_networks.arc_geography_idx; 
        PERFORM setval('tempus_networks.arc_id_seq', (SELECT coalesce(max(id)+1, 1) FROM tempus_networks.arc), false); 
    END IF;
END
$$;

