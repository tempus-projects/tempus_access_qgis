
do $$
begin
raise notice '==== Restore constraints and indexes ===';
end$$;

SELECT pgtempus.tempus_restore_foreign_keys_and_indexes('tempus_networks.pt_stop');
SELECT pgtempus.tempus_restore_foreign_keys_and_indexes('tempus_networks.pt_agency');
SELECT pgtempus.tempus_restore_foreign_keys_and_indexes('tempus_networks.pt_trip');
SELECT pgtempus.tempus_restore_foreign_keys_and_indexes('tempus_networks.pt_route');
SELECT pgtempus.tempus_restore_foreign_keys_and_indexes('tempus_networks.pt_fare_rule');
SELECT pgtempus.tempus_restore_foreign_keys_and_indexes('tempus_networks.pt_unit_fare');
SELECT pgtempus.tempus_restore_foreign_keys_and_indexes('tempus_networks.pt_od_fare');
SELECT pgtempus.tempus_restore_foreign_keys_and_indexes('tempus_networks.pt_zonal_fare');
SELECT pgtempus.tempus_restore_foreign_keys_and_indexes('tempus_networks.pt_fare_zone');
SELECT pgtempus.tempus_restore_foreign_keys_and_indexes('tempus_networks.pt_section');
SELECT pgtempus.tempus_restore_foreign_keys_and_indexes('tempus_networks.pt_section_stop_times');

do $$
begin
raise notice '==== Restore triggers ===';
end$$;

SELECT pgtempus.tempus_restore_triggers();



