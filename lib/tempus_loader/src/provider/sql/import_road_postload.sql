
do $$
begin
raise notice '==== Clean road network, restore constraints and indexes ===';
end$$;


SELECT pgtempus.tempus_restore_foreign_keys_and_indexes('tempus_networks.node');
SELECT pgtempus.tempus_restore_foreign_keys_and_indexes('tempus_networks.arc');
-- Delete nodes not referenced in any arc
DELETE FROM tempus_networks.node
WHERE id IN
(
	SELECT id
    FROM tempus_networks.node
    EXCEPT
    (
        SELECT node_from_id
        FROM tempus_networks.arc
        union
        SELECT node_to_id
        FROM tempus_networks.arc
    )
); 

SELECT pgtempus.tempus_restore_foreign_keys_and_indexes('tempus_networks.arcs_sequence');
SELECT pgtempus.tempus_restore_foreign_keys_and_indexes('tempus_networks.road_section_attributes');
SELECT pgtempus.tempus_restore_foreign_keys_and_indexes('tempus_networks.road_section_speed');
-- Delete arcs referencing non existing nodes
DELETE FROM tempus_networks.arc
WHERE id IN
(
	SELECT arc.id
	FROM tempus_networks.arc LEFT JOIN tempus_networks.node node_from ON (arc.node_from_id = node_from.id)
                             LEFT JOIN tempus_networks.node node_to ON (arc.node_to_id = node_to.id)
	WHERE node_to.id IS NULL or node_from.id IS NULL
); 

SELECT pgtempus.tempus_restore_foreign_keys_and_indexes('tempus_networks.arcs_sequence_time');
SELECT pgtempus.tempus_restore_foreign_keys_and_indexes('tempus_networks.arcs_sequence_toll');
-- Delete arc sequences referencing non existing arcs
DELETE FROM tempus_networks.arcs_sequence
WHERE id IN
(
	SELECT distinct id
	FROM
	(
		SELECT q.id, q.arc_id, q.rang, s.node_from_id, s.node_to_id
		FROM (
                SELECT id, unnest(arcs_id) as arc_id, array_search(unnest(arcs_id), arcs_id) as rang
                FROM tempus_networks.arcs_sequence
             ) as q LEFT JOIN tempus_networks.arc as s ON s.id=q.arc_id
	) t 
	WHERE node_from_id IS NULL 
);

do $$
begin
raise notice '==== Restore triggers ===';
end$$;

SELECT pgtempus.tempus_restore_triggers();


