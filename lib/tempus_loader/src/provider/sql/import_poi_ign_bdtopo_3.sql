/*

BDTopo 3 POI importer
Parameters:
    %(source_name)
    %(target_srid)
    %(filter)
    %(link)
    %(max_dist)
    %(temp_schema)
    %(merge)
*/

DO
$$
BEGIN
    IF %(merge) = True THEN
        RAISE notice '==== 0. Duplicate suppression ===';
        DELETE FROM %(temp_schema).equipement_de_transport
        WHERE id IN 
        (
            SELECT original_id 
            FROM tempus_networks.node
            WHERE source_id = (SELECT id FROM pgtempus.source WHERE name = '%(source_name)')
        );
    END IF;
END
$$;

DO
$$
BEGIN
RAISE notice '==== 1. Make geometries valid ===';
END
$$;

SELECT pgtempus.tempus_correction_geom('%(temp_schema)', 'equipement_de_transport', 'geom');

DO
$$
BEGIN
RAISE notice '==== 2. Create nodes and POIs ===';
END
$$;

DROP TABLE IF EXISTS %(temp_schema).node; 
CREATE TABLE %(temp_schema).node
(
    id bigserial PRIMARY KEY,
    original_id character varying, 
    geom Geometry(PointZ, %(target_srid))
);
ALTER SEQUENCE %(temp_schema).node_id_seq MINVALUE 0;
SELECT setval('%(temp_schema).node_id_seq', (SELECT coalesce(max(id), 0) FROM tempus_networks.node));

INSERT INTO %(temp_schema).node( original_id, geom )
    SELECT id, st_force3DZ(st_transform(st_pointonsurface(geom), %(target_srid))) 
    FROM %(temp_schema).equipement_de_transport
    WHERE %(filter);
    
CREATE INDEX ON %(temp_schema).node USING btree(original_id);
CREATE INDEX ON %(temp_schema).node USING gist(geom);

DROP TABLE IF EXISTS %(temp_schema).poi; 
CREATE TABLE %(temp_schema).poi
(
    node_id serial, 
    name character varying,
    type integer
);

INSERT INTO %(temp_schema).poi(
                                node_id, 
                                name,
                                type
                              )
    SELECT node.id,
           toponyme,
           CASE WHEN nature = 'Parking' THEN 3
                WHEN nature = 'Gare voyageurs et fret' or nature = 'Gare routière' or nature = 'Gare maritime' or nature = 'Arrêt voyageurs' or nature = 'Aérogare' or nature = 'Port' THEN 1
           END
    FROM %(temp_schema).equipement_de_transport JOIN %(temp_schema).node ON (node.original_id = equipement_de_transport.id)
    WHERE etat = 'En service';

-- New nodes
INSERT INTO tempus_networks.node(id, source_id, geom)
(
    SELECT id,  
           (SELECT id FROM pgtempus.source WHERE name = '%(source_name)'),
           geom
    FROM %(temp_schema).node
);

-- New POI
INSERT INTO tempus_networks.poi(node_id, name, type)
SELECT node_id, name, type FROM %(temp_schema).poi;

DO
$$
BEGIN
RAISE notice '==== 3. Attach to the road network ===';
END
$$;

DO
$$
BEGIN
    IF ( '%(link)'::character varying = 'True'::character varying ) THEN
        CREATE INDEX ON tempus_networks.arc USING gist( geography(st_transform(geom, 4326)) ); 
        
        PERFORM pgtempus.tempus_attach_node_to_a_road_section( node.id, (SELECT array_agg(id) FROM pgtempus.source WHERE type = 1), %(max_dist), ARRAY[1,4]::smallint[] )
        FROM %(temp_schema).node;
        
        PERFORM pgtempus.tempus_set_node_traffic_rules( node.id )
        FROM %(temp_schema).node;
        
        DROP INDEX tempus_networks.arc_geography_idx; 
        PERFORM setval('tempus_networks.arc_id_seq', (SELECT coalesce(max(id)+1, 1) FROM tempus_networks.arc), false); 
    END IF;
END
$$;

