do $$
begin
RAISE NOTICE '==== TempusAccess indicators function for ODs ==='; 
end$$;

DROP FUNCTION IF EXISTS pgtempus.tempus_create_od_indicator_layer(
                                                            bigint[], 
                                                            bigint[], 
                                                            smallint, 
                                                            smallint, 
                                                            boolean, 
                                                            smallint[], 
                                                            smallint[],
                                                            smallint[],
                                                            smallint[],
                                                            date[], 
                                                            time[], 
                                                            smallint, 
                                                            smallint, 
                                                            boolean, 
                                                            boolean, 
                                                            real, 
                                                            real, 
                                                            real, 
                                                            real, 
                                                            real, 
                                                            real, 
                                                            real, 
                                                            real, 
                                                            interval, 
                                                            interval, 
                                                            smallint, 
                                                            smallint, 
                                                            boolean, 
                                                            character varying, 
                                                            character varying, 
                                                            character varying, 
                                                            character varying 
                                                          );
CREATE FUNCTION pgtempus.tempus_create_od_indicator_layer (
                                                            p_o_nodes bigint[], 
                                                            p_d_nodes bigint[], 
                                                            p_o_mode smallint, 
                                                            p_d_mode smallint, 
                                                            p_time_constraint_dep boolean, 
                                                            p_sources_id smallint[], 
                                                            p_transport_modes smallint[], 
                                                            p_pt_trip_types smallint[], 
                                                            p_pt_agencies_id smallint[], 
                                                            p_days date[] DEFAULT ARRAY['2000-01-01'::date], 
                                                            p_times time[] DEFAULT ARRAY['00:00:00'::time], 
                                                            p_day_ag smallint DEFAULT 1, 
                                                            p_time_ag smallint DEFAULT 1, 
                                                            p_debug boolean DEFAULT False, 
                                                            p_matrix boolean DEFAULT True,                                                                                                       
                                                            p_walking_speed real DEFAULT 3.6, 
                                                            p_bicycle_speed real DEFAULT 11, 
                                                            p_pt_time_weight real DEFAULT 1, 
                                                            p_waiting_time_weight real DEFAULT 1, 
                                                            p_indiv_mode_time_weight real DEFAULT 1, 
                                                            p_fare_weight real DEFAULT 0, 
                                                            p_pt_transfer_weight real DEFAULT 0, 
                                                            p_mode_change_weight real DEFAULT 0, 
                                                            p_max_walking_time interval DEFAULT NULL, 
                                                            p_max_bicycle_time interval DEFAULT NULL,  
                                                            p_max_pt_transfers smallint DEFAULT NULL, 
                                                            p_max_mode_changes smallint DEFAULT NULL, 
                                                            p_pt_security_times boolean DEFAULT False, 
                                                            p_table_name_od_arc character varying DEFAULT 'od_arc', 
                                                            p_table_name_od_trip character varying DEFAULT 'od_trajet', 
                                                            p_table_name_od_path character varying DEFAULT 'od_chemin', 
                                                            p_table_name_od character varying DEFAULT 'od' 
                                                          )
RETURNS void AS
$BODY$

DECLARE
    error boolean;
    s1 character varying;
    s character varying; 
    day_ag_str character varying;
    r record;
    q record; 
    q_id integer;
    nb_res smallint;
    time_constraint smallint; 
    source_filter1 character varying;
    source_filter2 character varying;
    traffic_rules_filter character varying;
    traffic_rules_filter2 character varying;
    traffic_rules_filter3 character varying; 
    toll_rules_filter character varying;
    transport_mode_filter character varying;   
    pt_trip_types_filter character varying;
    pt_trip_types_filter2 character varying;
    pt_agencies_filter character varying;
    pt_agencies_filter2 character varying;

BEGIN

    IF (p_time_constraint_dep) THEN 
        time_constraint = 1;
    ELSE
        time_constraint = 2;
    END IF;
    
    IF (p_matrix is False) AND ((array_upper(p_o_nodes, 1) != array_upper(p_d_nodes, 1))) 
    THEN RAISE EXCEPTION 'Origins and destinations arrays must have the same dimension';
    END IF;
    
    s=$$
        -- Arcs table
        DROP TABLE IF EXISTS tempus_results.$$ || p_table_name_od_arc || $$; 
        CREATE TABLE tempus_results.$$ || p_table_name_od_arc || $$
        (
            id serial PRIMARY KEY, 
            query_id integer, 
            path_o_node bigint,
            path_d_node bigint,
            orig_mode smallint,
            dest_mode smallint,
            theo_dep_time timestamp, 
            theo_arr_time timestamp, 
            obj_id bigint,
            pred_obj_id bigint,
            arc_id bigint,
            pred_node_id bigint,
            node_id bigint,             
            transport_mode_id smallint,
            automaton_state integer, 
            pred_cost real,
            cost real, 
            pt_trip_id bigint,
            waiting_time time,
            departure_time time,
            arrival_time time,
            total_time time, 
            total_bicycle_time time,
            total_walking_time time,
            total_mode_changes smallint, 
            total_pt_transfers smallint, 
            total_fare real,
            in_paths boolean,
            geom_arc Geometry(LinestringZ, 2154), 
            geom_node Geometry(PointZ, 2154),
            UNIQUE(query_id, arc_id, transport_mode_id, automaton_state)
        ); 
        COMMENT ON COLUMN tempus_results.$$ || p_table_name_od_arc || $$.query_id IS 'Unique ID of shortest paths query';
        COMMENT ON COLUMN tempus_results.$$ || p_table_name_od_arc || $$.path_o_node IS 'ID of the origin node (when leave after... constraint is used)';
        COMMENT ON COLUMN tempus_results.$$ || p_table_name_od_arc || $$.path_d_node IS 'ID of the destination node (when arrive before... constraint is used)';
        COMMENT ON COLUMN tempus_results.$$ || p_table_name_od_arc || $$.theo_dep_time IS 'Theoretical departure time (when leave after... constraint is used)';
        COMMENT ON COLUMN tempus_results.$$ || p_table_name_od_arc || $$.theo_arr_time IS 'Theoretical arrival time (when arrive before... constraint is used)';
        COMMENT ON COLUMN tempus_results.$$ || p_table_name_od_arc || $$.obj_id IS 'Current object (triplet node, mode, state) ID';
        COMMENT ON COLUMN tempus_results.$$ || p_table_name_od_arc || $$.pred_obj_id IS 'Predecessor object (triplet node, mode, state) ID in the shortest path tree'; 
        COMMENT ON COLUMN tempus_results.$$ || p_table_name_od_arc || $$.arc_id IS 'Arc ID';
        COMMENT ON COLUMN tempus_results.$$ || p_table_name_od_arc || $$.pred_node_id IS 'Predecessor node in the path';
        COMMENT ON COLUMN tempus_results.$$ || p_table_name_od_arc || $$.node_id IS 'Current node';
        COMMENT ON COLUMN tempus_results.$$ || p_table_name_od_arc || $$.transport_mode_id IS 'Transport mode used on the current arc';
        COMMENT ON COLUMN tempus_results.$$ || p_table_name_od_arc || $$.automaton_state IS 'State of the automaton associated to the labeled object (for debugging only).';
        COMMENT ON COLUMN tempus_results.$$ || p_table_name_od_arc || $$.cost IS 'Value of the cost from the root node to the current node';
        COMMENT ON COLUMN tempus_results.$$ || p_table_name_od_arc || $$.pred_cost IS 'Value of the cost from the root node to the predecessor node';
        COMMENT ON COLUMN tempus_results.$$ || p_table_name_od_arc || $$.pt_trip_id IS 'PT trip ID used on the current arc';
        COMMENT ON COLUMN tempus_results.$$ || p_table_name_od_arc || $$.waiting_time IS 'Wait time at the origin of the arc, before departure';
        COMMENT ON COLUMN tempus_results.$$ || p_table_name_od_arc || $$.departure_time IS 'Departure time of the origin of the arc';
        COMMENT ON COLUMN tempus_results.$$ || p_table_name_od_arc || $$.arrival_time IS 'Arrival time at the destination of the arc'; 
        COMMENT ON COLUMN tempus_results.$$ || p_table_name_od_arc || $$.total_bicycle_time IS 'Cumulated bicycle time since departure (when leave after... constraint is used) or since arrival (when arrive before... constraint is used)';
        COMMENT ON COLUMN tempus_results.$$ || p_table_name_od_arc || $$.total_walking_time IS 'Cumulated walking time since departure (when leave after... constraint is used) or since arrival (when arrive before... constraint is used)';
        COMMENT ON COLUMN tempus_results.$$ || p_table_name_od_arc || $$.total_mode_changes IS 'Number of mode changes since departure (when leave after... constraint is used) or since arrival (when arrive before... constraint is used)';
        COMMENT ON COLUMN tempus_results.$$ || p_table_name_od_arc || $$.total_pt_transfers IS 'Number of PT transfers since departure (when leave after... constraint is used) or since arrival (when arrive before... constraint is used)';
        COMMENT ON COLUMN tempus_results.$$ || p_table_name_od_arc || $$.total_fare IS 'Cumulated fare since departure (when leave after... constraint is used) or since arrival (when arrive before... constraint is used)';
        COMMENT ON COLUMN tempus_results.$$ || p_table_name_od_arc || $$.in_paths IS 'True if the explored object belongs to the requested paths, false otherwise';
        DELETE FROM tempus_results.catalog WHERE table_name = '$$ || p_table_name_od_arc || $$';
        INSERT INTO tempus_results.catalog(table_name, base_obj_type_id, base_obj_subtype_id, time_constraint) VALUES ( '$$ || p_table_name_od_arc || $$', 4, 1, $$ || time_constraint::character varying || $$ );
        
        -- Path legs table
        DROP TABLE IF EXISTS tempus_results.$$ || p_table_name_od_trip || $$;
        CREATE TABLE tempus_results.$$ || p_table_name_od_trip || $$
        (
            id serial PRIMARY KEY, 
            query_id integer,
            path_o_node bigint,
            path_d_node bigint,
            orig_mode smallint,
            dest_mode smallint,
            theo_dep_time timestamp,
            theo_arr_time timestamp, 
            trip_num smallint,
            o_node bigint,
            d_node bigint,
            o_pt_stop_name character varying,
            d_pt_stop_name character varying,
            o_cost real,
            d_cost real,
            waiting_time time,
            departure_time time, 
            arrival_time time,
            travel_time time,
            transport_mode_id smallint,
            pt_trip_types_id integer[],
            pt_trip_types_name character varying[],
            gtfs_codes smallint[], 
            pt_route_id integer,
            pt_agency_id integer,
            geom_arc geometry(MultiLinestringZ, 2154), 
            geom_node geometry(PointZ, 2154)
        );
        DELETE FROM tempus_results.catalog WHERE table_name = '$$ || p_table_name_od_trip || $$';
        INSERT INTO tempus_results.catalog(table_name, base_obj_type_id, base_obj_subtype_id, time_constraint) VALUES ( '$$ || p_table_name_od_trip || $$', 4, 2, $$ || time_constraint::character varying || $$ );
        
        -- Paths table
        DROP TABLE IF EXISTS tempus_results.$$ || p_table_name_od_path || $$; 
        CREATE TABLE tempus_results.$$ || p_table_name_od_path || $$
        (
            id serial PRIMARY KEY, 
            query_id integer, 
            path_o_node bigint,
            path_d_node bigint,
            orig_mode smallint,
            dest_mode smallint,
            theo_dep_time timestamp,
            theo_arr_time timestamp,
            departure_time time, 
            arrival_time time,
            travel_time time,
            waiting_time time,
            fare real,
            cost real,
            mode_changes smallint,
            pt_transfers smallint, 
            geom geometry(MultiLinestringZ, 2154)
        );
        DELETE FROM tempus_results.catalog WHERE table_name = '$$ || p_table_name_od_path || $$';
        INSERT INTO tempus_results.catalog(table_name, base_obj_type_id, base_obj_subtype_id, time_constraint) VALUES ( '$$ || p_table_name_od_path || $$', 4, 3, $$ || time_constraint::character varying || $$ );
        
        -- OD table 
        DROP TABLE IF EXISTS tempus_results.$$ || p_table_name_od || $$;
        CREATE TABLE tempus_results.$$ || p_table_name_od || $$
        (
            path_o_node bigint,
            path_d_node bigint,
            orig_mode smallint,
            dest_mode smallint,
            first_dep time,
            last_dep time,
            first_arr time,
            last_arr time,
            cost real,
            fare real, 
            num_paths smallint,
            waiting_time time,
            travel_time time,
            mode_changes real,
            pt_transfers real,
            geom Geometry(LinestringZ, 2154), 
            PRIMARY KEY(path_o_node, path_d_node, orig_mode, dest_mode)
        );
        DELETE FROM tempus_results.catalog WHERE table_name = '$$ || p_table_name_od || $$';
        INSERT INTO tempus_results.catalog(table_name, base_obj_type_id, base_obj_subtype_id, time_constraint) VALUES ( '$$ || p_table_name_od || $$', 4, 4, $$ || time_constraint::character varying || $$ );
    $$;
    EXECUTE(s);
    
    IF p_day_ag IS NULL THEN
        p_day_ag=1;
    END IF;
    SELECT INTO day_ag_str 
                func_name 
    FROM pgtempus.agregate WHERE id = p_day_ag;
    
    IF p_time_ag IS NULL THEN
        p_time_ag=1;
    END IF; 
    
    -- Define filters
    source_filter1 = $$(source_id = ANY('$$ || p_sources_id::character varying || $$'::smallint[]))$$;
    source_filter2 = $$('$$ || p_sources_id::character varying || $$'::smallint[] @> sources_id)$$;
    traffic_rules_filter = $$(traffic_rules IS NULL OR traffic_rules && (SELECT array_agg(traffic_rule) FROM pgtempus.transport_mode WHERE traffic_rule IS NOT NULL AND id = ANY('$$ || p_transport_modes::character varying || $$'::smallint[])))$$;
    traffic_rules_filter2 = $$(arc_complete.traffic_rules IS NULL OR arc_complete.traffic_rules && (SELECT array_agg(traffic_rule) FROM pgtempus.transport_mode WHERE traffic_rule IS NOT NULL AND id = ANY('$$ || p_transport_modes::character varying || $$'::smallint[])))$$;
    traffic_rules_filter3 = $$(traffic_rules && (SELECT array_agg(traffic_rule) FROM pgtempus.transport_mode WHERE traffic_rule IS NOT NULL AND id = ANY('$$ || p_transport_modes::character varying || $$'::smallint[])))$$;
    toll_rules_filter = $$(toll_rules IS NULL OR toll_rules && (SELECT array_agg(toll_rule) FROM pgtempus.transport_mode WHERE toll_rule IS NOT NULL AND id = ANY('$$ || p_transport_modes::character varying || $$'::smallint[])))$$;
    transport_mode_filter = $$(transport_mode.id = ANY('$$ || p_transport_modes::character varying || $$'::smallint[]))$$;
    pt_trip_types_filter = $$((pt_trip_types_id IS NULL) OR (pt_trip_types_id && '$$ || p_pt_trip_types::character varying || $$'::smallint[]))$$;
    pt_trip_types_filter2 = $$((t.pt_trip_types_id IS NULL) OR (t.pt_trip_types_id && '$$ || p_pt_trip_types::character varying || $$'::smallint[]))$$;
    pt_agencies_filter = $$((pt_agencies_id IS NULL) OR (pt_agencies_id && '$$ || p_pt_agencies_id::character varying || $$'::smallint[]))$$;
    pt_agencies_filter2 = $$(t.pt_agency_id = ANY('$$ || p_pt_agencies_id::character varying || $$'::smallint[]))$$;
    
    -- Check if all origins and destinations exist
    s=$$
    (
        SELECT id
        FROM tempus_networks.node_complete
        WHERE id = ANY('$$ || p_o_nodes::character varying || $$') OR id = ANY('$$ || p_d_nodes::character varying || $$')
    )
    EXCEPT
    (
        SELECT id
        FROM tempus_networks.node_complete
        WHERE $$ || source_filter1 || $$ 
          AND $$ || traffic_rules_filter || $$
    )
    $$;
    --RAISE NOTICE '%', s;
    FOR r IN EXECUTE(s)
    LOOP RAISE WARNING 'Node %, referenced in origins or destinations, is not defined', r.id;
    END LOOP;

    -- Check if initial and final modes exist
    s=$$
    (
        SELECT id
        FROM pgtempus.transport_mode
        WHERE id = $$ || p_o_mode || $$ or id = $$ || p_d_mode || $$
    )
    EXCEPT
    (
        SELECT id
        FROM pgtempus.transport_mode
        WHERE $$ || transport_mode_filter || $$ 
    )
    $$;
    --RAISE NOTICE '%', s;
    
    FOR r IN EXECUTE(s)
    LOOP RAISE EXCEPTION 'Transport mode % is not defined', r.id;
    END LOOP;
    -- Check if all arcs have their corresponding nodes
    s=$$
    ( 
        SELECT node_from_id as id
        FROM tempus_networks.arc_complete
        WHERE $$ || source_filter2 || $$ 
          AND $$ || traffic_rules_filter || $$ 
          AND $$ || pt_trip_types_filter || $$ 
          AND $$ || pt_agencies_filter || $$ 
        UNION
        SELECT node_to_id
        FROM tempus_networks.arc_complete
        WHERE $$ || source_filter2 || $$ 
          AND $$ || traffic_rules_filter || $$ 
          AND $$ || pt_trip_types_filter || $$ 
          AND $$ || pt_agencies_filter || $$ 
    ) 
    EXCEPT
    ( 
        SELECT id
        FROM tempus_networks.node_complete
        WHERE $$ || source_filter1 || $$ 
          AND $$ || traffic_rules_filter || $$ 
    ) 
    $$;
    --RAISE NOTICE '%', s;
    FOR r IN EXECUTE(s)
    LOOP RAISE WARNING 'Node %, referenced from the arcs table, is not defined', r.id;
    END LOOP;
    
    -- Check if all road sections costs have their corresponding arcs and transport modes
    s=$$
    (
        -- Walking and bicycle costs
        SELECT arc_complete.id as arc_id,
               transport_mode.id::integer as transport_mode_id
        FROM tempus_networks.arc_complete JOIN pgtempus.transport_mode ON (transport_mode.traffic_rule = ANY(arc_complete.traffic_rules))
        WHERE transport_mode.id = ANY('$$ || p_transport_modes::character varying || $$') 
          AND transport_mode.category = ANY(ARRAY['Walking'::pgtempus.transport_mode_category, 'Bicycle'::pgtempus.transport_mode_category])
          AND arc_complete.type = 1
          AND $$ || source_filter2 || $$ 
          AND $$ || traffic_rules_filter || $$ 
          AND $$ || pt_trip_types_filter || $$ 
          AND $$ || pt_agencies_filter || $$ 
        UNION
        -- Car costs
        SELECT arc_complete.id as arc_id,
               transport_mode.id::integer as transport_mode_id
        FROM tempus_networks.arc_complete JOIN pgtempus.transport_mode ON (transport_mode.traffic_rule = ANY(arc_complete.traffic_rules))
                                          LEFT JOIN tempus_networks.road_section_speed ON (road_section_speed.arc_id = arc_complete.id AND road_section_speed.speed_rule = transport_mode.speed_rule)
        WHERE transport_mode.id = ANY('$$ || p_transport_modes::character varying || $$') 
          AND transport_mode.category = 'Car'::pgtempus.transport_mode_category
          AND arc_complete.type = 1
          AND $$ || source_filter2 || $$ 
          AND $$ || traffic_rules_filter || $$ 
          AND $$ || pt_trip_types_filter || $$ 
          AND $$ || pt_agencies_filter || $$ 
    )
    EXCEPT
    (
        SELECT arc_complete.id, transport_mode.id
        FROM tempus_networks.arc_complete CROSS JOIN pgtempus.transport_mode
        WHERE $$ || source_filter2 || $$ 
          AND $$ || traffic_rules_filter || $$ 
          AND $$ || pt_trip_types_filter || $$ 
          AND $$ || pt_agencies_filter || $$ 
          AND $$ || transport_mode_filter || $$ 
    )
    $$;
    --RAISE NOTICE '%', s;
    FOR r IN EXECUTE(s)
    LOOP
        RAISE EXCEPTION 'The pair (Arc, Transport mode) = (%, %), referenced from the road sections costs table, is not defined', r.arc_id, r.transport_mode_id;
    END LOOP;
    
    -- Check if all arcs sequences have their corresponding arcs 
    s=$$
    (
        SELECT unnest(penalized_path.arcs_id) as arc_id
        FROM tempus_networks.penalized_path
        WHERE $$ || source_filter2 || $$ 
          AND $$ || traffic_rules_filter || $$ 
          AND $$ || toll_rules_filter || $$ 
    )
    EXCEPT
    (
        SELECT id
        FROM tempus_networks.arc_complete
        WHERE $$ || source_filter2 || $$ 
          AND $$ || traffic_rules_filter || $$ 
          AND $$ || pt_trip_types_filter || $$ 
          AND $$ || pt_agencies_filter || $$ 
    )
    $$;
    --RAISE NOTICE '%', s;
    FOR r IN EXECUTE(s)
    LOOP
        RAISE WARNING 'The arc %, referenced from the arcs sequences table, is not defined', r.arc_id;
    END LOOP;
    
    IF (p_debug) THEN SET pgtempus.debug = on;
    ELSE 
    SET pgtempus.debug = off;
    END IF;
    
    RAISE NOTICE 'Graph building...';    
    PERFORM pgtempus.tempus_build_multimodal_graph(
                                                    'g', 
                                                    $$
                                                      SELECT id, 
                                                             name::text, 
                                                             category::text, 
                                                             traffic_rule, 
                                                             toll_rule, 
                                                             vehicle_parking_rule
                                                      FROM pgtempus.transport_mode
                                                      WHERE $$ || transport_mode_filter, 
                                                    $$
                                                      SELECT id, 
                                                             st_x(geom)::real as x,
                                                             st_y(geom)::real as y, 
                                                             st_z(geom)::real as z
                                                      FROM tempus_networks.node_complete
                                                      WHERE arc_types != ARRAY[3]::smallint[] 
                                                        AND $$ || source_filter1 || $$ 
                                                        AND $$ || traffic_rules_filter,
                                                    $$
                                                      SELECT id, 
                                                             node_from_id, 
                                                             node_to_id, 
                                                             type, 
                                                             traffic_rules
                                                      FROM tempus_networks.arc_complete
                                                      WHERE type = ANY(ARRAY[1,2]) 
                                                        AND $$ || source_filter2 || $$ 
                                                        AND $$ || traffic_rules_filter || $$ 
                                                        AND $$ || pt_trip_types_filter || $$ 
                                                        AND $$ || pt_agencies_filter
                                                  );
    RAISE NOTICE 'Assigning road costs...';
    PERFORM pgtempus.tempus_set_static_road_section_costs(
                                                            'g',
                                                            $$
                                                                -- Walking costs
                                                                SELECT arc_complete.id as arc_id,
                                                                       transport_mode.id as transport_mode_id, 
                                                                       CASE WHEN arc_complete.length IS NULL THEN 2147483647
                                                                            ELSE (arc_complete.length/1000) / $$ || p_walking_speed::character varying || $$ * 3600 
                                                                       END::integer as travel_time, 
                                                                       0::real as fare
                                                                FROM tempus_networks.arc_complete JOIN pgtempus.transport_mode ON (transport_mode.traffic_rule = ANY(arc_complete.traffic_rules))
                                                                WHERE transport_mode.id = ANY('$$ || p_transport_modes::character varying || $$') 
                                                                      AND transport_mode.category = 'Walking'::pgtempus.transport_mode_category 
                                                                      AND arc_complete.type = 1 
                                                                      AND $$ || source_filter2 || $$ 
                                                                      AND $$ || traffic_rules_filter || $$ 
                                                                UNION
                                                                -- Bicycle costs
                                                                SELECT arc_complete.id as arc_id,
                                                                       transport_mode.id as transport_mode_id, 
                                                                       CASE WHEN arc_complete.length IS NULL THEN 2147483647
                                                                            ELSE (arc_complete.length/1000) / $$ || p_bicycle_speed::character varying || $$ * 3600 
                                                                       END::integer as travel_time, 
                                                                       0::real as fare
                                                                FROM tempus_networks.arc_complete JOIN pgtempus.transport_mode ON (transport_mode.traffic_rule = ANY(arc_complete.traffic_rules))
                                                                WHERE transport_mode.id = ANY('$$ || p_transport_modes::character varying || $$') 
                                                                      AND transport_mode.category = 'Bicycle'::pgtempus.transport_mode_category 
                                                                      AND arc_complete.type = 1 
                                                                      AND $$ || source_filter2 || $$ 
                                                                      AND $$ || traffic_rules_filter || $$ 
                                                                UNION
                                                                -- Car costs
                                                                SELECT arc_complete.id as arc_id,
                                                                       transport_mode.id as transport_mode_id, 
                                                                       CASE WHEN arc_complete.length IS NULL OR road_section_speed.value = 0 THEN 2147483647
                                                                            ELSE (arc_complete.length/1000) / COALESCE(road_section_speed.value, 30) * 3600 
                                                                       END::integer as duration, 
                                                                       0::real as additional_cost
                                                                FROM tempus_networks.arc_complete JOIN pgtempus.transport_mode ON ( transport_mode.traffic_rule = ANY(arc_complete.traffic_rules) )
                                                                                                  LEFT JOIN tempus_networks.road_section_speed ON (road_section_speed.arc_id = arc_complete.id AND road_section_speed.speed_rule = transport_mode.speed_rule)
                                                                WHERE transport_mode.id = ANY('$$ || p_transport_modes::character varying || $$') 
                                                                      AND transport_mode.category = 'Car'::pgtempus.transport_mode_category 
                                                                      AND arc_complete.type = 1 
                                                                      AND $$ || source_filter2 || $$ 
                                                                      AND $$ || traffic_rules_filter
                                                         ); 
    RAISE NOTICE 'Assigning arcs sequence time penalties...'; 
    PERFORM pgtempus.tempus_set_arcs_sequence_costs(
                                                        'g',
                                                        $$
                                                            SELECT arcs_id,
                                                                   traffic_rules, 
                                                                   time_value, 
                                                                   toll_rules,
                                                                   toll_value
                                                            FROM tempus_networks.penalized_path JOIN pgtempus.transport_mode ON ( transport_mode.traffic_rule = ANY(penalized_path.traffic_rules) )
                                                            WHERE transport_mode.id = ANY('$$ || p_transport_modes::character varying || $$') 
                                                              AND $$ || source_filter2 || $$ 
                                                              AND $$ || traffic_rules_filter3 || $$ 
                                                              AND $$ || toll_rules_filter
                                                   ); 
    RAISE NOTICE 'Assigning parking costs...';  
    PERFORM pgtempus.tempus_set_parking_costs(
                                                'g', 
                                                $$
                                                    SELECT v.node_id, 
                                                           v.vehicle_parking_rule, 
                                                           extract(epoch from v.leaving_time)::integer as leaving_time, 
                                                           v.leaving_fare, 
                                                           extract(epoch from v.taking_time)::integer as taking_time, 
                                                           v.taking_fare
                                                    FROM tempus_networks.vehicle_parking_cost v JOIN pgtempus.transport_mode ON (transport_mode.vehicle_parking_rule = v.vehicle_parking_rule)
                                                                                                JOIN tempus_networks.node_complete  ON (node_complete.id = v.node_id)
                                                    WHERE transport_mode.id = ANY('$$ || p_transport_modes::character varying || $$') 
                                                      AND node_complete.arc_types != ARRAY[3]::smallint[] 
                                                      AND $$ || source_filter1 || $$ 
                                                      AND $$ || traffic_rules_filter
                                             );
    RAISE NOTICE 'Assigning vehicles positions...';
    PERFORM pgtempus.tempus_set_vehicles_positions(
                                                        'g', 
                                                        $$
                                                            SELECT v.node_id, v.transport_mode_id
                                                            FROM pgtempus.vehicle v JOIN tempus_networks.node_complete ON ( v.node_id = node_complete.id )
                                                            WHERE transport_mode_id = ANY('$$ || p_transport_modes::character varying || $$') 
                                                              AND node_complete.arc_types != ARRAY[3]::smallint[] 
                                                              AND $$ || source_filter1 || $$ 
                                                              AND $$ || traffic_rules_filter
                                                  );  
    
    IF (p_pt_security_times) THEN 
        RAISE NOTICE 'Assigning PT boarding security times...';
        PERFORM pgtempus.tempus_set_pt_boarding_security_times(
                                                                    'g', 
                                                                    $$
                                                                        SELECT pt_stop.node_id, 
                                                                               extract(epoch from security_time)::smallint as pt_boarding_security_time
                                                                        FROM tempus_networks.pt_stop JOIN tempus_networks.node_complete ON (pt_stop.node_id = node_complete.id)
                                                                        WHERE node_complete.arc_types != ARRAY[3]::smallint[] 
                                                                          AND $$ || source_filter1 || $$ 
                                                                          AND $$ || traffic_rules_filter || $$ 
                                                                          AND $$ || pt_trip_types_filter || $$ 
                                                                          AND $$ || pt_agencies_filter 
                                                              );
    END IF;

    FOR index_day IN 1..array_upper(p_days, 1)
    LOOP        
        RAISE NOTICE 'Assigning PT timetables (different for each day)...';
        PERFORM pgtempus.tempus_set_pt_section_timetable(
                                                            'g', 
                                                            $$
                                                                SELECT t.id as arc_id, 
                                                                       t.time_from as departure, 
                                                                       t.time_to as arrival, 
                                                                       t.pt_trip_id, 
                                                                       t.traffic_rules
                                                                FROM tempus_networks.pt_section_trip_stop_times t JOIN tempus_networks.arc_complete ON (arc_complete.id = t.id)
                                                                WHERE day = '$$ || p_days[index_day]::character varying || $$'::date 
                                                                  AND $$ || source_filter2 || $$ 
                                                                  AND $$ || traffic_rules_filter2 || $$ 
                                                                  AND $$ || pt_trip_types_filter2 || $$ 
                                                                  AND $$ || pt_agencies_filter2|| $$ 
                                                                ORDER BY t.id, time_from, time_to
                                                            $$, 
                                                            p_days[index_day]::timestamp
                                                        );

        FOR index_time IN 1..array_upper(p_times, 1)
        LOOP
            IF (p_time_constraint_dep is True) THEN
                FOR index_orig IN 1..array_upper(p_o_nodes, 1)
                LOOP
                    error = False;
                    -- Check if origin exists
                    s = $$
                        SELECT count(id)
                        FROM tempus_networks.node_complete
                        WHERE $$ || source_filter1 || $$ 
                          AND $$ || traffic_rules_filter || $$
                          AND id = $$ || p_o_nodes[index_orig];
                    FOR r IN EXECUTE(s)
                    LOOP 
                        IF (r.count = 0) THEN
                            RAISE WARNING 'Node % referenced in origins is not defined. Corresponding query will be skipped.', p_o_nodes[index_orig];
                            error = True;
                        END IF;
                    END LOOP;
                    CONTINUE WHEN (error = True); 
                    
                    IF (p_matrix) THEN
                        -- Check if destinations exist
                        s = $$
                            SELECT array_agg(id) as d_nodes
                            FROM tempus_networks.node_complete
                            WHERE $$ || source_filter1 || $$ 
                              AND $$ || traffic_rules_filter || $$
                              AND id = ANY('$$ || p_d_nodes::character varying || $$')
                        $$;
                        FOR r IN EXECUTE(s)
                        LOOP 
                            IF array_upper(r.d_nodes, 1) = 0 THEN
                                RAISE WARNING 'No destinations defined. Corresponding query will be skipped.';
                                error = True;
                            ELSE
                                p_d_nodes = r.d_nodes;
                            END IF;
                        END LOOP;
                        CONTINUE WHEN (error = True);
                        
                        RAISE NOTICE 'Origin = %, destinations = %', p_o_nodes[index_orig], p_d_nodes;
                        
                        PERFORM pgtempus.tempus_compute_forward_query ( 
                                                                            p_o_nodes[index_orig], 
                                                                            p_d_nodes, 
                                                                            p_o_mode, 
                                                                            p_d_mode,
                                                                            p_transport_modes, 
                                                                            p_time_constraint_dep, 
                                                                            p_debug, 
                                                                            p_days[index_day], 
                                                                            p_times[index_time], 
                                                                            p_pt_time_weight, 
                                                                            p_waiting_time_weight, 
                                                                            p_indiv_mode_time_weight, 
                                                                            p_fare_weight, 
                                                                            p_pt_transfer_weight, 
                                                                            p_mode_change_weight, 
                                                                            p_max_walking_time, 
                                                                            p_max_bicycle_time, 
                                                                            p_max_pt_transfers, 
                                                                            p_max_mode_changes, 
                                                                            p_table_name_od_arc, 
                                                                            p_table_name_od_trip, 
                                                                            p_table_name_od_path
                                                                      ); 
                    ELSE
                        -- Check if destination exists
                        s = $$
                            SELECT count(id)
                            FROM tempus_networks.node_complete
                            WHERE $$ || source_filter1 || $$ 
                              AND $$ || traffic_rules_filter || $$ 
                              AND id = $$ || p_d_nodes[index_orig];
                        FOR r IN EXECUTE(s)
                        LOOP 
                            IF (r.count = 0) THEN
                                RAISE WARNING 'Node % referenced in destinations is not defined. Corresponding query will be skipped.', p_d_nodes[index_orig];
                                error = True;
                            END IF;
                        END LOOP;
                        CONTINUE WHEN (error = True);
                        
                        RAISE NOTICE 'Origin = %, destination = %', p_o_nodes[index_orig], p_d_nodes[index_orig];
                        
                        PERFORM pgtempus.tempus_compute_forward_query ( 
                                                                            p_o_nodes[index_orig], 
                                                                            ARRAY[p_d_nodes[index_orig]], 
                                                                            p_o_mode, 
                                                                            p_d_mode,
                                                                            p_transport_modes, 
                                                                            p_time_constraint_dep, 
                                                                            p_debug, 
                                                                            p_days[index_day], 
                                                                            p_times[index_time], 
                                                                            p_pt_time_weight, 
                                                                            p_waiting_time_weight, 
                                                                            p_indiv_mode_time_weight, 
                                                                            p_fare_weight, 
                                                                            p_pt_transfer_weight, 
                                                                            p_mode_change_weight, 
                                                                            p_max_walking_time, 
                                                                            p_max_bicycle_time, 
                                                                            p_max_pt_transfers, 
                                                                            p_max_mode_changes, 
                                                                            p_table_name_od_arc, 
                                                                            p_table_name_od_trip, 
                                                                            p_table_name_od_path
                                                                      );
                    END IF;
                END LOOP;
            ELSE
                FOR index_dest IN 1..array_upper(p_d_nodes, 1)
                LOOP
                    error = False; 
                    -- Check if origin exists
                    s = $$
                        SELECT count(id)
                        FROM tempus_networks.node_complete
                        WHERE $$ || source_filter1 || $$ 
                          AND $$ || traffic_rules_filter || $$ 
                          AND id = $$ || p_o_nodes[index_dest] || $$;$$;
                    FOR r IN EXECUTE(s)
                    LOOP 
                        IF (r.count = 0) THEN
                            RAISE WARNING 'Node % referenced in destinations is not defined. Corresponding query will be skipped.', p_o_nodes[index_dest];
                            error = True;
                        END IF;
                    END LOOP;
                    CONTINUE WHEN (error = True);
                    
                    IF (p_matrix) THEN
                        -- Check if origins exist
                        s = $$
                                SELECT array_agg(id) as o_nodes
                                FROM tempus_networks.node_complete
                                WHERE $$ || source_filter1 || $$ 
                                  AND $$ || traffic_rules_filter || $$ 
                                  AND id = ANY('$$ || p_o_nodes::character varying || $$')$$;
                        FOR r IN EXECUTE(s)
                        LOOP 
                            IF array_upper(r.o_nodes, 1) = 0 THEN
                                RAISE WARNING 'No destinations defined. Corresponding query will be skipped.';
                                error = True;
                            ELSE
                                p_o_nodes = r.o_nodes;
                            END IF;
                        END LOOP;
                        CONTINUE WHEN (error = True);
                        
                        RAISE NOTICE 'Origins = %, destination = %', p_o_nodes, p_d_nodes[index_dest];
                        
                        PERFORM pgtempus.tempus_compute_backward_query( 
                                                                            p_o_nodes, 
                                                                            p_d_nodes[index_dest], 
                                                                            p_o_mode, 
                                                                            p_d_mode,
                                                                            p_transport_modes, 
                                                                            p_time_constraint_dep, 
                                                                            p_debug, 
                                                                            p_days[index_day], 
                                                                            p_times[index_time], 
                                                                            p_pt_time_weight, 
                                                                            p_waiting_time_weight, 
                                                                            p_indiv_mode_time_weight, 
                                                                            p_fare_weight, 
                                                                            p_pt_transfer_weight, 
                                                                            p_mode_change_weight, 
                                                                            p_max_walking_time, 
                                                                            p_max_bicycle_time, 
                                                                            p_max_pt_transfers, 
                                                                            p_max_mode_changes, 
                                                                            p_table_name_od_arc, 
                                                                            p_table_name_od_trip, 
                                                                            p_table_name_od_path
                                                                      );
                    ELSE
                        -- Check if destination exists
                        s = $$SELECT count(id)
                              FROM tempus_networks.node_complete
                              WHERE $$ || source_filter1 || $$ 
                                AND $$ || traffic_rules_filter || $$ 
                                AND id = $$ || p_d_nodes[index_dest]::character varying || $$;$$;
                        FOR r IN EXECUTE(s)
                        LOOP 
                            IF (r.count = 0) THEN
                                RAISE WARNING 'Node % referenced in destinations is not defined. Corresponding query will be skipped.', p_d_nodes[index_dest];
                                error = True;
                            END IF;
                        END LOOP;
                        CONTINUE WHEN (error = True); 
                        
                        RAISE NOTICE 'Origins = %, destination = %', p_o_nodes[index_dest], p_d_nodes[index_dest];
                        
                        PERFORM pgtempus.tempus_compute_backward_query ( 
                                                                            ARRAY[p_o_nodes[index_dest]], 
                                                                            p_d_nodes[index_dest], 
                                                                            p_o_mode, 
                                                                            p_d_mode,
                                                                            p_transport_modes, 
                                                                            p_time_constraint_dep, 
                                                                            p_debug, 
                                                                            p_days[index_day], 
                                                                            p_times[index_time], 
                                                                            p_pt_time_weight, 
                                                                            p_waiting_time_weight, 
                                                                            p_indiv_mode_time_weight, 
                                                                            p_fare_weight, 
                                                                            p_pt_transfer_weight, 
                                                                            p_mode_change_weight, 
                                                                            p_max_walking_time, 
                                                                            p_max_bicycle_time, 
                                                                            p_max_pt_transfers, 
                                                                            p_max_mode_changes, 
                                                                            p_table_name_od_arc, 
                                                                            p_table_name_od_trip, 
                                                                            p_table_name_od_path
                                                                       );
                    END IF;
                END LOOP;
            END IF; 
        END LOOP;     
    END LOOP;     
    PERFORM pgtempus.tempus_create_od_layer( p_table_name_od_path, p_table_name_od, p_time_ag, p_day_ag, p_debug );     
    PERFORM pgtempus.tempus_unload_graph('g'); 
    RETURN;
END; 
$BODY$
LANGUAGE plpgsql; 


DROP FUNCTION IF EXISTS pgtempus.tempus_compute_forward_query ( 
                                                        bigint, 
                                                        bigint[], 
                                                        smallint, 
                                                        smallint, 
                                                        smallint[], 
                                                        boolean, 
                                                        boolean, 
                                                        date, 
                                                        time, 
                                                        real, 
                                                        real, 
                                                        real, 
                                                        real, 
                                                        real, 
                                                        real, 
                                                        interval, 
                                                        interval, 
                                                        smallint, 
                                                        smallint, 
                                                        character varying, 
                                                        character varying, 
                                                        character varying 
                                                       );

CREATE FUNCTION pgtempus.tempus_compute_forward_query( 
                                                p_o_node bigint,
                                                p_d_nodes bigint[],
                                                p_o_mode smallint,
                                                p_d_mode smallint,
                                                p_transport_modes smallint[],
                                                p_time_constraint_dep boolean,
                                                p_debug boolean,
                                                p_day date,
                                                p_time time,
                                                p_pt_time_weight real,
                                                p_waiting_time_weight real,
                                                p_indiv_mode_time_weight real,
                                                p_fare_weight real,
                                                p_pt_transfer_weight real,
                                                p_mode_change_weight real,
                                                p_max_walking_time interval,
                                                p_max_bicycle_time interval,
                                                p_max_pt_transfers smallint,
                                                p_max_mode_changes smallint, 
                                                p_table_name_od_arc character varying, 
                                                p_table_name_od_trip character varying,
                                                p_table_name_od_path character varying
                                              )
RETURNS void AS
$BODY$
DECLARE
    s1 character varying;
    s character varying;
    d_nodes bigint[];
    q record;
    r record;
    q_id smallint;
BEGIN
    d_nodes = array(select unnest(p_d_nodes) except select p_o_node); 
    s1= $$  
            SELECT node_id, min(arrival_time) as arrival_time FROM pgtempus.tempus_one_to_many_paths(
                                                                                                        graph_id := 'g',
                                                                                                        algo_name := 'multimodal_ls'::text, 
                                                                                                        orig_node := $$ || p_o_node::character varying || $$::bigint, 
                                                                                                        dest_nodes := '$$ || d_nodes::character varying || $$'::bigint[], 
                                                                                                        orig_transport_mode := $$ || p_o_mode || $$::smallint, 
                                                                                                        dest_transport_mode := $$ || p_d_mode || $$::smallint, 
                                                                                                        max_cost := null::real,
                                                                                                        allowed_transport_modes := '$$ || p_transport_modes::character varying || $$'::bigint[], 
                                                                                                        departure_time := '$$ || (p_day + p_time)::character varying || $$',
                                                                                                        pt_time_weight := $$ || p_pt_time_weight || $$::real, 
                                                                                                        waiting_time_weight := $$ || p_waiting_time_weight || $$::real, 
                                                                                                        indiv_mode_time_weight := $$ || p_indiv_mode_time_weight || $$::real, 
                                                                                                        fare_weight := $$ || p_fare_weight || $$::real,
                                                                                                        pt_transfer_weight := $$ || p_pt_transfer_weight || $$::real, 
                                                                                                        mode_change_weight := $$ || p_mode_change_weight || $$::real, 
                                                                                                        max_walking_time := $$ || CASE WHEN p_max_walking_time IS NULL THEN 'NULL' ELSE $$'$$ || p_max_walking_time::character varying || $$'$$ END || $$::interval, 
                                                                                                        max_bicycle_time := $$ || CASE WHEN p_max_bicycle_time IS NULL THEN 'NULL' ELSE $$'$$ || p_max_bicycle_time::character varying || $$'$$ END || $$::interval, 
                                                                                                        max_pt_transfers := $$ || CASE WHEN p_max_pt_transfers IS NULL THEN 'NULL' ELSE $$'$$ || p_max_pt_transfers::character varying || $$'$$ END || $$::smallint, 
                                                                                                        max_mode_changes := $$ || CASE WHEN p_max_mode_changes IS NULL THEN 'NULL' ELSE $$'$$ || p_max_mode_changes::character varying || $$'$$ END || $$::smallint
                                                                                                    )
            WHERE arrival_time is not null AND node_id = ANY('$$ || p_d_nodes::character varying || $$') AND node_id != $$ || p_o_node::character varying || $$ AND transport_mode_id = $$ || p_d_mode || $$
            GROUP BY node_id;
        $$;
    IF (p_debug) THEN RAISE NOTICE '%', s1; END IF;
    FOR r IN EXECUTE(s1) 
    LOOP
        s = $$SELECT coalesce(max(query_id)+1, 1) as id FROM tempus_results.$$ || p_table_name_od_arc;
        FOR q IN EXECUTE(s)
        LOOP 
            q_id = q.id;
        END LOOP;
        s=$$
            -- Path trees
            INSERT INTO tempus_results.$$ || p_table_name_od_arc || $$(
                                                                       query_id, 
                                                                       path_o_node,
                                                                       path_d_node,
                                                                       orig_mode,
                                                                       dest_mode, 
                                                                       theo_dep_time, 
                                                                       obj_id, 
                                                                       pred_obj_id, 
                                                                       arc_id, 
                                                                       pred_node_id, 
                                                                       node_id,
                                                                       transport_mode_id, 
                                                                       automaton_state,
                                                                       pred_cost,
                                                                       cost, 
                                                                       pt_trip_id, 
                                                                       waiting_time, 
                                                                       departure_time, 
                                                                       arrival_time, 
                                                                       total_time,
                                                                       total_bicycle_time,
                                                                       total_walking_time,
                                                                       total_mode_changes, 
                                                                       total_pt_transfers,
                                                                       total_fare,
                                                                       in_paths, 
                                                                       geom_arc, 
                                                                       geom_node
                                                                    )
                SELECT $$ || q_id || $$, 
                       $$ || p_o_node::character varying || $$::bigint, 
                       $$ || r.node_id::character varying || $$::bigint, 
                       $$ || p_o_mode::character varying || $$::smallint, 
                       $$ || p_d_mode::character varying || $$::smallint,
                       '$$ || (p_day + p_time)::character varying || $$'::timestamp, 
                       p.obj_id,
                       p.pred_obj_id,
                       p.arc_id, 
                       p.pred_node_id, 
                       p.node_id, 
                       p.transport_mode_id::smallint,  
                       p.automaton_state,
                       p.pred_cost,
                       p.cost,
                       p.pt_trip_id, 
                       p.waiting_time::time, 
                       p.departure_time::time, 
                       p.arrival_time::time, 
                       p.total_time::time,
                       p.total_bicycle_time::time, 
                       p.total_walking_time::time,
                       p.total_mode_changes::smallint, 
                       p.total_pt_transfers::smallint, 
                       p.total_fare::real, 
                       p.in_paths,
                       arc.geom, 
                       node.geom
                FROM pgtempus.tempus_many_to_one_paths(
                                                            graph_id := 'g',
                                                            algo_name := 'multimodal_ls'::text, 
                                                            dest_node := $$ || r.node_id::character varying || $$::bigint, 
                                                            orig_nodes := '{$$ || p_o_node::character varying || $$}'::bigint[], 
                                                            dest_transport_mode := $$ || p_d_mode || $$::smallint, 
                                                            orig_transport_mode := $$ || p_o_mode || $$::smallint, 
                                                            max_cost := null::real,
                                                            allowed_transport_modes := '$$ || p_transport_modes::character varying || $$'::bigint[], 
                                                            arrival_time := '$$ || (p_day + r.arrival_time)::character varying || $$',
                                                            pt_time_weight := $$ || p_pt_time_weight || $$::real, 
                                                            waiting_time_weight := $$ || p_waiting_time_weight || $$::real, 
                                                            indiv_mode_time_weight := $$ || p_indiv_mode_time_weight || $$::real, 
                                                            fare_weight := $$ || p_fare_weight || $$::real,
                                                            pt_transfer_weight := $$ || p_pt_transfer_weight || $$::real, 
                                                            mode_change_weight := $$ || p_mode_change_weight || $$::real, 
                                                            max_walking_time := $$ || CASE WHEN p_max_walking_time IS NULL THEN 'NULL' ELSE $$'$$ || p_max_walking_time::character varying || $$'$$ END || $$::interval, 
                                                            max_bicycle_time := $$ || CASE WHEN p_max_bicycle_time IS NULL THEN 'NULL' ELSE $$'$$ || p_max_bicycle_time::character varying || $$'$$ END || $$::interval, 
                                                            max_pt_transfers := $$ || CASE WHEN p_max_pt_transfers IS NULL THEN 'NULL' ELSE $$'$$ || p_max_pt_transfers::character varying || $$'$$ END || $$::smallint, 
                                                            max_mode_changes := $$ || CASE WHEN p_max_mode_changes IS NULL THEN 'NULL' ELSE $$'$$ || p_max_mode_changes::character varying || $$'$$ END || $$::smallint
                                                      ) p JOIN tempus_networks.node ON ( p.node_id = node.id)
                                                          LEFT JOIN tempus_networks.arc ON ( arc.id = p.arc_id ) 
                ORDER BY p.obj_id; 
                DELETE FROM tempus_results.catalog WHERE table_name = '$$ || p_table_name_od_arc || $$';
                INSERT INTO tempus_results.catalog(table_name, base_obj_type_id, base_obj_subtype_id, time_constraint) VALUES ( '$$ || p_table_name_od_arc || $$', 4, 1, $$ || time_constraint::character varying || $$ );
          $$;
        IF (p_debug) THEN RAISE NOTICE '%', s; END IF;
        EXECUTE(s); 
        
        PERFORM pgtempus.tempus_create_path_trips_layer( q_id, p_table_name_od_arc, p_table_name_od_trip, p_o_node, r.node_id, p_o_mode, p_d_mode, p_day, p_time, p_time_constraint_dep, p_debug );                         
        PERFORM pgtempus.tempus_create_paths_layer( q_id, p_table_name_od_arc, p_table_name_od_path, p_o_node, r.node_id, p_o_mode, p_d_mode, p_day, p_time, p_time_constraint_dep, p_debug ); 

    
    END LOOP; 
END;
$BODY$
LANGUAGE plpgsql;                                                       


DROP FUNCTION IF EXISTS pgtempus.tempus_compute_backward_query ( 
                                                            bigint[], 
                                                            bigint, 
                                                            smallint, 
                                                            smallint, 
                                                            smallint[], 
                                                            boolean, 
                                                            boolean, 
                                                            date, 
                                                            time, 
                                                            real, 
                                                            real, 
                                                            real, 
                                                            real, 
                                                            real, 
                                                            real, 
                                                            interval, 
                                                            interval, 
                                                            smallint, 
                                                            smallint, 
                                                            character varying, 
                                                            character varying, 
                                                            character varying
                                                        );

CREATE FUNCTION pgtempus.tempus_compute_backward_query( 
                                                p_o_nodes bigint[],
                                                p_d_node bigint,
                                                p_o_mode smallint,
                                                p_d_mode smallint,
                                                p_transport_modes smallint[],
                                                p_time_constraint_dep boolean,
                                                p_debug boolean,
                                                p_day date,
                                                p_time time,
                                                p_pt_time_weight real,
                                                p_waiting_time_weight real,
                                                p_indiv_mode_time_weight real,
                                                p_fare_weight real,
                                                p_pt_transfer_weight real,
                                                p_mode_change_weight real,
                                                p_max_walking_time interval,
                                                p_max_bicycle_time interval,
                                                p_max_pt_transfers smallint,
                                                p_max_mode_changes smallint, 
                                                p_table_name_od_arc character varying, 
                                                p_table_name_od_trip character varying,
                                                p_table_name_od_path character varying
                                              )
RETURNS void AS
$BODY$
DECLARE
    s1 character varying;
    s character varying;
    d_nodes bigint[];
    q record;
    r record;
    q_id smallint;
BEGIN
    s1= $$  
            SELECT node_id, max(departure_time) as departure_time FROM pgtempus.tempus_many_to_one_paths(
                                                                                                            graph_id := 'g',
                                                                                                            algo_name := 'multimodal_ls'::text, 
                                                                                                            dest_node := $$ || p_d_node::character varying || $$::bigint, 
                                                                                                            orig_nodes := '$$ || p_o_nodes::character varying || $$'::bigint[], 
                                                                                                            dest_transport_mode := $$ || p_d_mode || $$::smallint, 
                                                                                                            orig_transport_mode := $$ || p_o_mode || $$::smallint, 
                                                                                                            max_cost := null::real,
                                                                                                            allowed_transport_modes := '$$ || p_transport_modes::character varying || $$'::bigint[], 
                                                                                                            arrival_time := '$$ || (p_day + p_time)::character varying || $$',
                                                                                                            pt_time_weight := $$ || p_pt_time_weight || $$::real, 
                                                                                                            waiting_time_weight := $$ || p_waiting_time_weight || $$::real, 
                                                                                                            indiv_mode_time_weight := $$ || p_indiv_mode_time_weight || $$::real, 
                                                                                                            fare_weight := $$ || p_fare_weight || $$::real,
                                                                                                            pt_transfer_weight := $$ || p_pt_transfer_weight || $$::real, 
                                                                                                            mode_change_weight := $$ || p_mode_change_weight || $$::real, 
                                                                                                            max_walking_time := $$ || CASE WHEN p_max_walking_time IS NULL THEN 'NULL' ELSE p_max_walking_time::character varying END || $$::interval, 
                                                                                                            max_bicycle_time := $$ || CASE WHEN p_max_bicycle_time IS NULL THEN 'NULL' ELSE p_max_bicycle_time::character varying END || $$::interval, 
                                                                                                            max_pt_transfers := $$ || CASE WHEN p_max_pt_transfers IS NULL THEN 'NULL' ELSE p_max_pt_transfers::character varying END || $$::smallint, 
                                                                                                            max_mode_changes := $$ || CASE WHEN p_max_mode_changes IS NULL THEN 'NULL' ELSE p_max_mode_changes::character varying END || $$::smallint
                                                                                                        )
            WHERE departure_time is not null AND node_id = ANY('$$ || p_o_nodes::character varying || $$') AND node_id != $$ || p_d_node::character varying || $$ AND transport_mode_id = $$ || p_o_mode || $$
            GROUP BY node_id;
    $$;
    
    IF (p_debug) THEN RAISE NOTICE '%', s1; END IF;
    FOR r IN EXECUTE(s1) 
    LOOP
        s = $$SELECT coalesce(max(query_id)+1, 1) as id FROM tempus_results.$$ || p_table_name_od_arc;
        FOR q IN EXECUTE(s)
        LOOP 
            q_id = q.id; 
        END LOOP;
        s=$$
        -- Path trees
        INSERT INTO tempus_results.$$ || p_table_name_od_arc || $$(
                                                                   query_id, 
                                                                   path_o_node,
                                                                   path_d_node,
                                                                   orig_mode,
                                                                   dest_mode, 
                                                                   theo_arr_time, 
                                                                   obj_id, 
                                                                   pred_obj_id, 
                                                                   arc_id, 
                                                                   pred_node_id, 
                                                                   node_id,
                                                                   transport_mode_id, 
                                                                   automaton_state,
                                                                   pred_cost,
                                                                   cost, 
                                                                   pt_trip_id, 
                                                                   waiting_time, 
                                                                   departure_time, 
                                                                   arrival_time, 
                                                                   total_time, 
                                                                   total_bicycle_time,
                                                                   total_walking_time,
                                                                   total_mode_changes, 
                                                                   total_pt_transfers,
                                                                   total_fare,
                                                                   in_paths, 
                                                                   geom_arc, 
                                                                   geom_node
                                                                )
        SELECT $$ || q_id || $$, 
               $$ || r.node_id::character varying || $$::bigint, 
               $$ || p_d_node || $$::bigint, 
               $$ || p_o_mode || $$::smallint,
               $$ || p_d_mode || $$::smallint, 
               '$$ || (p_day + p_time)::character varying || $$', 
               p.obj_id,
               p.pred_obj_id,
               p.arc_id, 
               p.pred_node_id, 
               p.node_id, 
               p.transport_mode_id::smallint,  
               p.automaton_state,
               p.pred_cost,
               p.cost,
               p.pt_trip_id, 
               p.waiting_time::time, 
               p.departure_time::time, 
               p.arrival_time::time, 
               p.total_time::time,
               p.total_bicycle_time::time, 
               p.total_walking_time::time,
               p.total_mode_changes::smallint, 
               p.total_pt_transfers::smallint, 
               p.total_fare::real, 
               p.in_paths, 
               arc.geom, 
               node.geom 
        FROM pgtempus.tempus_one_to_many_paths(
                                                graph_id := 'g',
                                                algo_name := 'multimodal_ls'::text, 
                                                orig_node := $$ || r.node_id::character varying || $$, 
                                                dest_nodes := '{$$ || p_d_node::character varying || $$}', 
                                                orig_transport_mode := $$ || p_o_mode || $$::smallint, 
                                                dest_transport_mode := $$ || p_d_mode || $$::smallint, 
                                                max_cost := null::real,
                                                allowed_transport_modes := '$$ || p_transport_modes::character varying || $$'::bigint[], 
                                                departure_time := '$$ || (p_day + r.departure_time)::character varying || $$',
                                                pt_time_weight := $$ || p_pt_time_weight || $$::real, 
                                                waiting_time_weight := $$ || p_waiting_time_weight || $$::real, 
                                                indiv_mode_time_weight := $$ || p_indiv_mode_time_weight || $$::real, 
                                                fare_weight := $$ || p_fare_weight || $$::real,
                                                pt_transfer_weight := $$ || p_pt_transfer_weight || $$::real, 
                                                mode_change_weight := $$ || p_mode_change_weight || $$::real, 
                                                max_walking_time := $$ || CASE WHEN p_max_walking_time IS NULL THEN 'NULL' ELSE p_max_walking_time::character varying END || $$::interval, 
                                                max_bicycle_time := $$ || CASE WHEN p_max_bicycle_time IS NULL THEN 'NULL' ELSE p_max_bicycle_time::character varying END || $$::interval, 
                                                max_pt_transfers := $$ || CASE WHEN p_max_pt_transfers IS NULL THEN 'NULL' ELSE p_max_pt_transfers::character varying END || $$::smallint, 
                                                max_mode_changes := $$ || CASE WHEN p_max_mode_changes IS NULL THEN 'NULL' ELSE p_max_mode_changes::character varying END || $$::smallint
                                              ) p JOIN tempus_networks.node ON ( p.node_id = node.id ) 
                                                  LEFT JOIN tempus_networks.arc ON ( arc.id = p.arc_id ) 
        ORDER BY p.obj_id;  
        DELETE FROM tempus_results.catalog WHERE table_name = '$$ || p_table_name_od_arc || $$';
        INSERT INTO tempus_results.catalog(table_name, base_obj_type_id, base_obj_subtype_id, time_constraint) VALUES ( '$$ || p_table_name_od_arc || $$', 4, 1, $$ || time_constraint::character varying || $$ );
        $$; 
        IF (p_debug) THEN RAISE NOTICE '%', s; END IF;
        EXECUTE(s);
        
        PERFORM pgtempus.tempus_create_path_trips_layer( q_id, p_table_name_od_arc, p_table_name_od_trip, r.node_id, p_d_node, p_o_mode, p_d_mode, p_day, p_time, p_time_constraint_dep, p_debug );  
        PERFORM pgtempus.tempus_create_paths_layer( q_id, p_table_name_od_arc, p_table_name_od_path, r.node_id, p_d_node, p_o_mode, p_d_mode, p_day, p_time, p_time_constraint_dep, p_debug );                        
    END LOOP;
END;
$BODY$
LANGUAGE plpgsql;


DROP FUNCTION IF EXISTS pgtempus.tempus_create_path_trips_layer(
                                                            integer, 
                                                            character varying, 
                                                            character varying, 
                                                            bigint, 
                                                            bigint, 
                                                            smallint, 
                                                            smallint, 
                                                            date, 
                                                            time, 
                                                            boolean, 
                                                            boolean
                                                        );
CREATE FUNCTION pgtempus.tempus_create_path_trips_layer(
                                                    query_id integer, 
                                                    p_table_name_od_arc character varying,
                                                    p_table_name_od_trip character varying,
                                                    p_o_node bigint, 
                                                    p_d_node bigint, 
                                                    p_o_mode smallint,
                                                    p_d_mode smallint, 
                                                    p_day date, 
                                                    p_time time, 
                                                    p_time_constraint_dep boolean, 
                                                    p_debug boolean
                                                )
RETURNS void AS
$BODY$
    
DECLARE
    d integer;
    t integer;
    orig integer;
    dest integer;
    s character varying; 
    
BEGIN 
    
    
    IF p_time_constraint_dep is True THEN
        s = $$
        INSERT INTO tempus_results.$$ || p_table_name_od_trip || $$(
                                                                    query_id, 
                                                                    path_o_node, 
                                                                    path_d_node, 
                                                                    orig_mode,
                                                                    dest_mode,
                                                                    theo_dep_time, 
                                                                    trip_num,
                                                                    o_node, 
                                                                    d_node, 
                                                                    o_pt_stop_name, 
                                                                    d_pt_stop_name,
                                                                    o_cost, 
                                                                    d_cost, 
                                                                    waiting_time, 
                                                                    departure_time, 
                                                                    arrival_time, 
                                                                    travel_time, 
                                                                    transport_mode_id, 
                                                                    pt_trip_types_id, 
                                                                    gtfs_codes, 
                                                                    pt_trip_types_name, 
                                                                    pt_route_id, 
                                                                    pt_agency_id, 
                                                                    geom_arc,
                                                                    geom_node
                                                                  )
        (
            WITH query AS 
            (
                WITH q3 AS (
                    WITH q2 AS (
                        WITH q1 AS (
                            WITH RECURSIVE path AS (
                                SELECT * 
                                FROM tempus_results.od_arc
                                WHERE query_id = $$ || query_id ::character varying || $$ AND in_paths = True AND path_o_node = $$ || p_o_node::character varying || $$::bigint AND theo_dep_time = '$$ || (p_day + p_time)::character varying || $$' 
                                AND node_id = $$ || p_o_node::character varying || $$ AND transport_mode_id = $$ || p_d_mode || $$
                                UNION 
                                SELECT p.* 
                                FROM tempus_results.$$ || p_table_name_od_arc || $$ p JOIN path ON (p.obj_id = path.pred_obj_id AND p.path_o_node = path.path_o_node AND p.theo_dep_time = path.theo_dep_time)
                                WHERE p.query_id = $$ || query_id ::character varying || $$ AND p.in_paths = True AND p.path_o_node = $$ || p_o_node::character varying || $$::bigint AND p.theo_dep_time = '$$ || (p_day + p_time)::character varying || $$'
                            ) 
                           SELECT *, 
                           CASE WHEN LAG (transport_mode_id) OVER (ORDER BY cost) <> transport_mode_id OR LAG (coalesce(pt_trip_id, 0)) OVER (ORDER BY cost) <> coalesce(pt_trip_id, 0) THEN 1 END AS is_reset
                           FROM path
                        )
                        SELECT q1.*, count(is_reset) over (ORDER BY cost) as grp
                        FROM q1
                    )
                    SELECT grp, 
                           last_value(node_id) OVER (PARTITION BY grp) as o_node, 
                           first_value(pred_node_id) OVER (PARTITION BY grp) as d_node, 
                           first_value(waiting_time) OVER (PARTITION BY grp) as waiting_time,
                           last_value(departure_time::time) OVER (PARTITION BY grp) as departure_time,
                           first_value(arrival_time::time) OVER (PARTITION BY grp) as arrival_time,
                           first_value(pred_cost) OVER (PARTITION BY grp) as o_cost, 
                           last_value(cost) OVER (PARTITION BY grp) as d_cost,
                           (first_value(arrival_time::time) OVER (PARTITION BY grp) - last_value(departure_time::time) OVER (PARTITION BY grp)) as travel_time, 
                           transport_mode_id, 
                           pt_trip_id,
                           geom_arc, 
                           last_value(geom_node) OVER (PARTITION BY grp) as geom_node
                    FROM q2
                    WHERE pred_obj_id IS NOT NULL
                    ORDER BY grp
                )
                SELECT grp as trip_num, o_node, d_node, waiting_time, departure_time, arrival_time, travel_time, o_cost, d_cost, transport_mode_id, pt_trip_id, st_union(geom_arc) as geom_arc, geom_node
                FROM q3
                GROUP BY grp, o_node, d_node, waiting_time, departure_time, arrival_time, travel_time, o_cost, d_cost, transport_mode_id, pt_trip_id, geom_node
                ORDER BY d_cost
            )       
            SELECT $$ || query_id::character varying || $$, 
                   $$ || p_o_node::character varying || $$ as path_o_node, 
                   $$ || p_d_node::character varying || $$ as path_d_node, 
                   $$ || p_o_mode::character varying || $$ as orig_mode, 
                   $$ || p_d_mode::character varying || $$ as dest_mode, 
                   '$$ || (p_day + p_time)::character varying || $$' as theo_dep_time, 
                   trip_num,
                   o_node,
                   d_node,
                   pt_stop_o.name as o_pt_stop_name,
                   pt_stop_d.name as d_pt_stop_name,
                   o_cost, 
                   d_cost, 
                   waiting_time::time,
                   departure_time::time, 
                   arrival_time::time,
                   (arrival_time - departure_time)::time as travel_time,
                   transport_mode_id, 
                   pt_trip.pt_trip_types_id, 
                   array(SELECT DISTINCT unnest(array_cat_agg(pt_trip_type.gtfs_codes)) FROM pgtempus.pt_trip_type WHERE pt_trip_type.id = ANY(pt_trip.pt_trip_types_id)) as gtfs_codes, 
                   (SELECT array_agg(DISTINCT pt_trip_type.name) FROM pgtempus.pt_trip_type WHERE pt_trip_type.id = ANY(pt_trip.pt_trip_types_id)) as pt_trip_types_name, 
                   pt_trip.pt_route_id, 
                   pt_route.pt_agency_id, 
                   st_force3D(st_multi(query.geom_arc)) as geom_arc, 
                   st_force3D(query.geom_node) as geom_node
            FROM query LEFT JOIN tempus_networks.pt_trip ON (pt_trip.id = query.pt_trip_id)
                       LEFT JOIN tempus_networks.pt_route ON (pt_route.id = pt_trip.pt_route_id)
                       LEFT JOIN tempus_networks.pt_stop pt_stop_o ON (pt_stop_o.node_id = query.o_node)
                       LEFT JOIN tempus_networks.pt_stop pt_stop_d ON (pt_stop_d.node_id = query.d_node)
            ORDER BY d_cost
        ); 
        DELETE FROM tempus_results.catalog WHERE table_name = '$$ || p_table_name_od_trip || $$';
        INSERT INTO tempus_results.catalog(table_name, base_obj_type_id, base_obj_subtype_id, time_constraint) VALUES ( '$$ || p_table_name_od_trip || $$', 4, 2, $$ || time_constraint::character varying || $$ );
        $$;
    ELSE 
        s = $$
        INSERT INTO tempus_results.$$ || p_table_name_od_trip || $$(
                                                                    query_id, 
                                                                    path_o_node, 
                                                                    path_d_node, 
                                                                    orig_mode,
                                                                    dest_mode,
                                                                    theo_arr_time, 
                                                                    trip_num,
                                                                    o_node, 
                                                                    d_node, 
                                                                    o_pt_stop_name, 
                                                                    d_pt_stop_name, 
                                                                    o_cost, 
                                                                    d_cost, 
                                                                    waiting_time, 
                                                                    departure_time, 
                                                                    arrival_time, 
                                                                    travel_time, 
                                                                    transport_mode_id, 
                                                                    pt_trip_types_id, 
                                                                    gtfs_codes, 
                                                                    pt_trip_types_name, 
                                                                    pt_route_id, 
                                                                    pt_agency_id, 
                                                                    geom_arc,
                                                                    geom_node
                                                                  )
        (
            WITH query AS 
            (
                WITH q3 AS (
                    WITH q2 AS (
                        WITH q1 AS (
                            WITH RECURSIVE path AS (
                                SELECT * 
                                FROM tempus_results.$$ || p_table_name_od_arc || $$
                                WHERE query_id = $$ || query_id ::character varying || $$ AND in_paths = True AND path_d_node = $$ || p_d_node::character varying || $$::bigint AND theo_arr_time = '$$ || (p_day + p_time)::character varying || $$' 
                                AND node_id = $$ || p_d_node::character varying || $$ AND transport_mode_id = $$ || p_o_mode || $$
                                UNION 
                                SELECT p.* 
                                FROM tempus_results.$$ || p_table_name_od_arc || $$ p JOIN path ON (p.obj_id = path.pred_obj_id AND p.path_d_node = path.path_d_node AND p.theo_arr_time = path.theo_arr_time)
                                WHERE p.query_id = $$ || query_id ::character varying || $$ AND p.in_paths = True AND p.path_d_node = $$ || p_d_node::character varying || $$::bigint AND p.theo_arr_time = '$$ || (p_day + p_time)::character varying || $$'
                            ) 
                           SELECT *, 
                           CASE WHEN LAG (transport_mode_id) OVER (ORDER BY cost) <> transport_mode_id OR LAG (coalesce(pt_trip_id, 0)) OVER (ORDER BY cost) <> coalesce(pt_trip_id, 0) THEN 1 END AS is_reset
                           FROM path
                        )
                        SELECT q1.*, count(is_reset) over (ORDER BY cost) as grp
                        FROM q1
                    )
                    SELECT grp, 
                           first_value(pred_node_id) OVER (PARTITION BY grp) as o_node, 
                           last_value(node_id) OVER (PARTITION BY grp) as d_node, 
                           first_value(waiting_time) OVER (PARTITION BY grp) as waiting_time,
                           first_value(departure_time::time) OVER (PARTITION BY grp) as departure_time,
                           last_value(arrival_time::time) OVER (PARTITION BY grp) as arrival_time,
                           first_value(pred_cost) OVER (PARTITION BY grp) as o_cost, 
                           last_value(cost) OVER (PARTITION BY grp) as d_cost,
                           (last_value(arrival_time::time) OVER (PARTITION BY grp) - first_value(departure_time::time) OVER (PARTITION BY grp)) as travel_time, 
                           transport_mode_id, 
                           pt_trip_id,
                           geom_arc, 
                           first_value(geom_node) OVER (PARTITION BY grp) as geom_node
                    FROM q2
                    WHERE pred_obj_id IS NOT NULL
                    ORDER BY grp
                )
                SELECT grp as trip_num, o_node, d_node, waiting_time, departure_time, arrival_time, travel_time, o_cost, d_cost, transport_mode_id, pt_trip_id, st_union(geom_arc) as geom_arc, geom_node
                FROM q3
                GROUP BY grp, o_node, d_node, waiting_time, departure_time, arrival_time, travel_time, o_cost, d_cost, transport_mode_id, pt_trip_id, geom_node
                ORDER BY d_cost
            )       
            SELECT $$ || query_id::character varying || $$, 
                   $$ || p_o_node::character varying || $$ as path_o_node, 
                   $$ || p_d_node::character varying || $$ as path_d_node, 
                   $$ || p_o_mode::character varying || $$ as orig_mode, 
                   $$ || p_d_mode::character varying || $$ as dest_mode, 
                   '$$ || (p_day + p_time)::character varying || $$' as theo_arr_time, 
                   trip_num,
                   o_node,
                   d_node,
                   pt_stop_o.name as o_pt_stop_name,
                   pt_stop_d.name as d_pt_stop_name,
                   o_cost, 
                   d_cost,
                   waiting_time::time,
                   departure_time::time, 
                   arrival_time::time,
                   (arrival_time - departure_time)::time as travel_time,
                   transport_mode_id, 
                   pt_trip.pt_trip_types_id, 
                   array(SELECT DISTINCT unnest(array_cat_agg(pt_trip_type.gtfs_codes)) FROM pgtempus.pt_trip_type WHERE pt_trip_type.id = ANY(pt_trip.pt_trip_types_id)) as gtfs_codes, 
                   (SELECT array_agg(DISTINCT pt_trip_type.name) FROM pgtempus.pt_trip_type WHERE pt_trip_type.id = ANY(pt_trip.pt_trip_types_id)) as pt_trip_types_name, 
                   pt_trip.pt_route_id as pt_route_id, 
                   pt_route.pt_agency_id as pt_agency_id, 
                   st_force3D(st_multi(geom_arc)) as geom_arc, 
                   st_force3D(geom_node) as geom_node
            FROM query LEFT JOIN tempus_networks.pt_trip ON (pt_trip.id = query.pt_trip_id)
                       LEFT JOIN tempus_networks.pt_route ON (pt_route.id = pt_trip.pt_route_id)
                       LEFT JOIN tempus_networks.pt_stop pt_stop_o ON (pt_stop_o.node_id = query.o_node)
                       LEFT JOIN tempus_networks.pt_stop pt_stop_d ON (pt_stop_d.node_id = query.d_node)
            ORDER BY o_cost 
        ); 
        DELETE FROM tempus_results.catalog WHERE table_name = '$$ || p_table_name_od_trip || $$';
        INSERT INTO tempus_results.catalog(table_name, base_obj_type_id, base_obj_subtype_id, time_constraint) VALUES ( '$$ || p_table_name_od_trip || $$', 4, 2, $$ || time_constraint::character varying || $$ );
    $$;
    END IF;
    IF (p_debug) THEN RAISE NOTICE '%', s; END IF;
    EXECUTE(s);
    
    RETURN;
END;
$BODY$
LANGUAGE plpgsql; 


DROP FUNCTION IF EXISTS pgtempus.tempus_create_paths_layer( 
                                                    integer, 
                                                    character varying, 
                                                    character varying, 
                                                    bigint, 
                                                    bigint, 
                                                    smallint, 
                                                    smallint, 
                                                    date, 
                                                    time, 
                                                    boolean, 
                                                    boolean
                                                   );
CREATE FUNCTION pgtempus.tempus_create_paths_layer(
                                            query_id integer, 
                                            p_table_name_od_arc character varying,
                                            p_table_name_od_path character varying,
                                            p_o_node bigint, 
                                            p_d_node bigint, 
                                            p_o_mode smallint,
                                            p_d_mode smallint,
                                            p_day date, 
                                            p_time time, 
                                            p_time_constraint_dep boolean, 
                                            p_debug boolean
                                           )
RETURNS void AS
$BODY$
DECLARE
    d integer;
    t integer;
    orig integer;
    dest integer;
    s character varying; 
BEGIN
    
    IF p_time_constraint_dep is True THEN
        s=$$
            INSERT INTO tempus_results.$$ || p_table_name_od_path || $$ (
                                                                        query_id, 
                                                                        path_o_node, 
                                                                        path_d_node, 
                                                                        orig_mode,
                                                                        dest_mode,
                                                                        theo_dep_time, 
                                                                        departure_time, 
                                                                        arrival_time, 
                                                                        waiting_time,
                                                                        travel_time, 
                                                                        fare,
                                                                        cost, 
                                                                        pt_transfers, 
                                                                        mode_changes, 
                                                                        geom
                                                                      )
            WITH q1 AS (
                WITH RECURSIVE path AS (
                    SELECT * 
                    FROM tempus_results.od_arc
                    WHERE query_id = $$ || query_id ::character varying || $$ AND in_paths = True AND path_o_node = $$ || p_o_node::character varying || $$::bigint AND theo_dep_time = '$$ || (p_day + p_time)::character varying || $$' 
                    AND node_id = $$ || p_o_node::character varying || $$ AND transport_mode_id = $$ || p_d_mode || $$
                    UNION 
                    SELECT p.* 
                    FROM tempus_results.$$ || p_table_name_od_arc || $$ p JOIN path ON (p.obj_id = path.pred_obj_id AND p.path_o_node = path.path_o_node AND p.theo_dep_time = path.theo_dep_time)
                    WHERE p.query_id = $$ || query_id ::character varying || $$ AND p.in_paths = True AND p.path_o_node = $$ || p_o_node::character varying || $$::bigint AND p.theo_dep_time = '$$ || (p_day + p_time)::character varying || $$'
                ) 
                SELECT query_id, 
                       first_value(departure_time::time) OVER () as departure_time,
                       last_value(arrival_time::time) OVER () as arrival_time,
                       waiting_time,
                       transport_mode_id, 
                       first_value(total_fare) OVER () as fare,
                       first_value(cost) OVER () as cost,
                       first_value(total_pt_transfers) OVER () as pt_transfers,
                       first_value(total_mode_changes) OVER () as mode_changes,
                       geom_arc as geom
                FROM path
				WHERE pred_obj_id is not null
            )
            SELECT DISTINCT ON (departure_time, arrival_time, waiting_time, fare, cost, pt_transfers, mode_changes)
                   query_id, 
                   $$ || p_o_node::character varying || $$ as path_o_node, 
                   $$ || p_d_node::character varying || $$ as path_d_node, 
                   $$ || p_o_mode::character varying || $$ as orig_mode, 
                   $$ || p_d_mode::character varying || $$ as dest_mode, 
                   '$$ || (p_day + p_time)::character varying || $$' as theo_dep_time, 
                   departure_time, 
                   arrival_time, 
                   sum(waiting_time) as waiting_time,
                   arrival_time - departure_time, 
                   fare, 
                   cost, 
                   pt_transfers,
                   mode_changes, 
                   st_multi(st_force3D(st_union(geom))) as geom
            FROM q1
            GROUP BY query_id, departure_time, arrival_time, fare, cost, pt_transfers, mode_changes;
            DELETE FROM tempus_results.catalog WHERE table_name = '$$ || p_table_name_od_path || $$';
            INSERT INTO tempus_results.catalog(table_name, base_obj_type_id, base_obj_subtype_id, time_constraint) VALUES ( '$$ || p_table_name_od_path || $$', 4, 3, $$ || time_constraint::character varying || $$ );
        $$; 
    ELSE
        s=$$
            INSERT INTO tempus_results.$$ || p_table_name_od_path || $$ (
                                                                        query_id, 
                                                                        path_o_node, 
                                                                        path_d_node, 
                                                                        orig_mode,
                                                                        dest_mode,
                                                                        theo_arr_time, 
                                                                        departure_time, 
                                                                        arrival_time, 
                                                                        waiting_time,
                                                                        travel_time,  
                                                                        fare,
                                                                        cost,
                                                                        pt_transfers, 
                                                                        mode_changes, 
                                                                        geom
                                                                      )
            WITH q1 AS (
                WITH RECURSIVE path AS (
                    SELECT * 
                    FROM tempus_results.$$ || p_table_name_od_arc || $$
                    WHERE query_id = $$ || query_id ::character varying || $$ AND in_paths = True AND path_d_node = $$ || p_d_node::character varying || $$::bigint AND theo_arr_time = '$$ || (p_day + p_time)::character varying || $$' 
                    AND node_id = $$ || p_d_node::character varying || $$ AND transport_mode_id = $$ || p_o_mode || $$
                    UNION 
                    SELECT p.* 
                    FROM tempus_results.$$ || p_table_name_od_arc || $$ p JOIN path ON (p.obj_id = path.pred_obj_id AND p.path_d_node = path.path_d_node AND p.theo_arr_time = path.theo_arr_time)
                    WHERE p.query_id = $$ || query_id ::character varying || $$ AND p.in_paths = True AND p.path_d_node = $$ || p_d_node::character varying || $$::bigint AND p.theo_arr_time = '$$ || (p_day + p_time)::character varying || $$'
                )
                SELECT query_id, 
                       last_value(departure_time::time) OVER () as departure_time,
                       first_value(arrival_time::time) OVER () as arrival_time,
                       waiting_time,
                       first_value(total_fare) OVER () as fare,
                       first_value(cost) OVER () as cost,
                       first_value(total_pt_transfers) OVER () as pt_transfers,
                       first_value(total_mode_changes) OVER () as mode_changes,
                       geom_arc as geom
                FROM path
				WHERE pred_obj_id is not null
            )
            SELECT DISTINCT ON (departure_time, arrival_time, waiting_time, fare, cost, pt_transfers, mode_changes) 
                   query_id, 
                   $$ || p_o_node::character varying || $$ as path_o_node, 
                   $$ || p_d_node::character varying || $$ as path_d_node, 
                   $$ || p_o_mode::character varying || $$ as orig_mode, 
                   $$ || p_d_mode::character varying || $$ as dest_mode, 
                   '$$ || (p_day + p_time)::character varying || $$' as theo_arr_time, 
                   departure_time, 
                   arrival_time, 
                   sum(waiting_time) as waiting_time,
                   arrival_time - departure_time, 
                   fare, 
                   cost, 
                   pt_transfers,
                   mode_changes, 
                   st_multi(st_force3D(st_union(geom))) as geom
            FROM q1
            GROUP BY query_id, departure_time, arrival_time, fare, cost, pt_transfers, mode_changes;
            DELETE FROM tempus_results.catalog WHERE table_name = '$$ || p_table_name_od_path || $$';
            INSERT INTO tempus_results.catalog(table_name, base_obj_type_id, base_obj_subtype_id, time_constraint) VALUES ( '$$ || p_table_name_od_path || $$', 4, 3, $$ || time_constraint::character varying || $$ );
        $$;
    END IF;
    IF (p_debug) THEN RAISE NOTICE '%', s; END IF;
    EXECUTE(s); 

    RETURN;
END;
$BODY$
LANGUAGE plpgsql;


DROP FUNCTION IF EXISTS pgtempus.tempus_create_od_layer( 
                                                  character varying,
                                                  character varying,
                                                  smallint,
                                                  smallint,
                                                  boolean
                                                );
CREATE FUNCTION pgtempus.tempus_create_od_layer(
                                            p_table_name_od_path character varying,
                                            p_table_name_od character varying,
                                            p_time_ag smallint,
                                            p_day_ag smallint, 
                                            p_debug boolean
                                        )
  RETURNS void AS
$BODY$
DECLARE
    s character varying; 
    day_ag_str character varying;
    time_ag_str character varying; 
BEGIN
    SELECT INTO time_ag_str 
                func_name 
    FROM pgtempus.agregate 
    WHERE id = p_time_ag;
    
    SELECT INTO day_ag_str 
                func_name 
    FROM pgtempus.agregate 
    WHERE id = p_day_ag;
    -- All days aggregation 
    s = $$ 
        WITH od_day AS (
            WITH t AS (
                SELECT path_o_node, 
                       path_d_node, 
                       orig_mode, 
                       dest_mode, 
                       ((coalesce(theo_dep_time, theo_arr_time))::timestamp)::date as d, 
                       departure_time, 
                       arrival_time, 
                       waiting_time, 
                       travel_time, 
                       fare, 
                       cost, 
                       pt_transfers, 
                       mode_changes
                FROM tempus_results.$$ || p_table_name_od_path || $$
                GROUP BY path_o_node, 
                         path_d_node, 
                         orig_mode, 
                         dest_mode, 
                         ((coalesce(theo_dep_time, theo_arr_time))::timestamp)::date, 
                         departure_time, 
                         arrival_time, 
                         waiting_time, 
                         travel_time, 
                         fare, 
                         cost, 
                         pt_transfers, 
                         mode_changes
            )
            SELECT t.path_o_node, 
                   t.path_d_node, 
                   t.orig_mode,
                   t.dest_mode,
                   t.d, 
                   min(t.departure_time::time) as first_dep,
                   max(t.departure_time::time) as last_dep,
                   min(t.arrival_time::time) as first_arr,
                   max(t.arrival_time::time) as last_arr, 
                   count(*)::numeric(6,2) as num_paths,
                   $$ || time_ag_str || $$(t.cost)::real as cost,
                   $$ || time_ag_str || $$(t.fare)::real as fare,
                   $$ || time_ag_str || $$(t.waiting_time::interval)::time as waiting_time,
                   $$ || time_ag_str || $$(t.travel_time::interval)::time as travel_time,
                   $$ || time_ag_str || $$(t.pt_transfers)::numeric(6,2) as pt_transfers,
                   $$ || time_ag_str || $$(t.mode_changes)::numeric(6,2) as mode_changes,
                   st_makeline(node_o.geom, node_d.geom) as geom
            FROM t JOIN tempus_networks.node node_o ON ( t.path_o_node = node_o.id )
                   JOIN tempus_networks.node node_d ON ( t.path_d_node = node_d.id )
            GROUP BY path_o_node, 
                     path_d_node, 
                     orig_mode, 
                     dest_mode, 
                     d, 
                     node_o.geom, 
                     node_d.geom
        )        
        INSERT INTO tempus_results.$$ || p_table_name_od || $$(path_o_node, path_d_node, orig_mode, dest_mode, first_dep, last_dep, first_arr, last_arr, cost, fare, num_paths, waiting_time, travel_time, pt_transfers, mode_changes, geom) 
        SELECT path_o_node, 
               path_d_node, 
               orig_mode, 
               dest_mode, 
               $$ || day_ag_str || $$(first_dep::time) as first_dep,
               $$ || day_ag_str || $$(last_dep::time) as last_dep,
               $$ || day_ag_str || $$(first_arr::time) as first_arr,
               $$ || day_ag_str || $$(last_arr::time) as last_arr,
               round($$ || day_ag_str || $$(cost)::numeric,2) as cost,
               round($$ || day_ag_str || $$(fare)::numeric,2) as fare,
               round($$ || day_ag_str || $$(num_paths)::numeric,2) as num_paths,
               $$ || day_ag_str || $$(waiting_time::time) as waiting_time,
               $$ || day_ag_str || $$(travel_time::time) as travel_time,
               round($$ || day_ag_str || $$(pt_transfers)::numeric,2) as pt_transfers,
               round($$ || day_ag_str || $$(mode_changes)::numeric,2) as mode_changes, 
               geom
        FROM od_day
        GROUP BY path_o_node, path_d_node, orig_mode, dest_mode, geom;
    $$;
    IF (p_debug) THEN RAISE NOTICE '%', s; END IF;
    EXECUTE(s);
    
    RETURN;
END;
$BODY$
LANGUAGE plpgsql; 


