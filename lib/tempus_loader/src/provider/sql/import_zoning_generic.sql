
DO
$$
BEGIN
RAISE notice '==== 1. Make geometries valid ===';
END
$$;

SELECT pgtempus.tempus_correction_geom('%(temp_schema)', 'zoning', 'geom');

DO
$$
BEGIN
RAISE notice '==== 2. Rename table and fields, reproject and comment ===';
END
$$;

DROP TABLE IF EXISTS tempus_zoning.%(source_name);
CREATE TABLE tempus_zoning.%(source_name)
AS
SELECT *
FROM %(temp_schema).zoning
ORDER BY gid; 

UPDATE tempus_zoning.%(source_name)
SET geom = st_simplify(geom, %(geom_simplify));

ALTER TABLE tempus_zoning.%(source_name)
RENAME COLUMN %(id_field) TO original_id;

ALTER TABLE tempus_zoning.%(source_name)
RENAME COLUMN gid TO id;

DO
$$
BEGIN
    IF (('%(name_field)'!='name') AND ('%(name_field)'!=NULL) AND ('%(name_field)'!='')) THEN
        ALTER TABLE tempus_zoning.%(source_name)
        RENAME COLUMN %(name_field) TO name;
    END IF; 
END;
$$;

ALTER TABLE tempus_zoning.%(source_name)
ALTER COLUMN geom TYPE Geometry(MultiPolygon, %(target_srid)) USING st_transform(st_multi(geom), %(target_srid)) ;

SELECT pgtempus.tempus_correction_geom('tempus_zoning', '%(source_name)', 'geom'); 

ALTER TABLE tempus_zoning.%(source_name)
ALTER COLUMN original_id TYPE character varying USING original_id::character varying;

COMMENT ON TABLE tempus_zoning.%(source_name) IS '%(source_comment)'; 

do $$
begin
raise notice '==== 3. Create indexes ===';
end$$;

CREATE INDEX %(source_name)_original_id_idx ON tempus_zoning.%(source_name) USING btree (original_id);
CREATE INDEX %(source_name)_geom_idx ON tempus_zoning.%(source_name) USING gist(geom);

