/*

INSEE BPE POI importer
Parameters:
    %(source_name)
    %(target_srid)
    %(filter)
    %(max_dist)
    %(link)
    %(temp_schema)
    %(merge)
*/
-- No duplicate suppression, since there is no ID in the original file
-- To test: could be a duplicate detection based on X and Y (is there several POI having the same X and Y in BPE ?)

DO
$$
BEGIN
RAISE notice '==== 1. Create new POIs and nodes ===';
END
$$;

DROP TABLE IF EXISTS %(temp_schema).node; 
CREATE TABLE %(temp_schema).node
(
    id bigserial PRIMARY KEY,
    typequ character varying(4),
    qualite_xy character varying,
    geom Geometry(PointZ, %(target_srid))
);
ALTER SEQUENCE %(temp_schema).node_id_seq MINVALUE 0;
SELECT setval('%(temp_schema).node_id_seq', (SELECT coalesce(max(id), 0) FROM tempus_networks.node));
    
CREATE INDEX ON %(temp_schema).node USING gist(geom);
CREATE INDEX ON %(temp_schema)."contours-iris" USING btree(insee_com, iris);

INSERT INTO %(temp_schema).node( typequ, qualite_xy, geom )
    SELECT typequ, qualite_xy, st_force3DZ(st_transform(st_setsrid(st_makepoint(lambert_x::real, lambert_y::real), 2154), %(target_srid))) 
    FROM %(temp_schema).bpe_ensemble_xy
    WHERE (qualite_xy != 'Non géolocalisé' AND qualite_xy != 'type_équipement_non_géolocalisé_cette_année') AND %(filter)
    UNION
    SELECT typequ, 'Centroïde de l''IRIS', st_force3DZ(st_pointonsurface("contours-iris".geom)) 
    FROM %(temp_schema).bpe_ensemble_xy JOIN %(temp_schema)."contours-iris" ON ( ("contours-iris".insee_com || '_' || "contours-iris".iris = bpe_ensemble_xy.dciris) OR  ("contours-iris".insee_com = bpe_ensemble_xy.dciris) )
    WHERE (qualite_xy = 'Non géolocalisé' or qualite_xy = 'type_équipement_non_géolocalisé_cette_année') AND %(filter);
-- Non geolocalised points are localised at the centroid of the IRIS polygon

CREATE TABLE %(temp_schema).poi_type(
    id smallserial,
    original_id character varying(4), 
    name character varying
);
ALTER SEQUENCE %(temp_schema).poi_type_id_seq MINVALUE 0;
SELECT setval('%(temp_schema).poi_type_id_seq', (SELECT coalesce(max(id), 0) FROM pgtempus.poi_type));

INSERT INTO %(temp_schema).poi_type ( original_id, name )
SELECT cod_mod, lib_mod
FROM %(temp_schema).varmod_ensemble_xy
WHERE cod_var = 'TYPEQU';

DROP TABLE IF EXISTS %(temp_schema).poi; 
CREATE TABLE %(temp_schema).poi
(
    node_id serial, 
    name character varying,
    type integer, 
    comment character varying
);

INSERT INTO %(temp_schema).poi(
                                node_id, 
                                name,
                                type, 
                                comment
                              )
    SELECT node.id,
           node.typequ,
           poi_type.id, 
           'Qualite XY = ' || node.qualite_xy
    FROM %(temp_schema).node JOIN %(temp_schema).poi_type ON (poi_type.original_id = node.typequ);

-- New POI types
INSERT INTO pgtempus.poi_type(id, name, abstract)
SELECT id, name, False FROM %(temp_schema).poi_type;
SELECT setval('pgtempus.poi_type_id_seq', (SELECT coalesce(max(id), 0) FROM pgtempus.poi_type));

-- New nodes
INSERT INTO tempus_networks.node(id, source_id, geom)
(
    SELECT id,  
           (SELECT id FROM pgtempus.source WHERE name = '%(source_name)'), 
           geom
    FROM %(temp_schema).node
);

-- New POI
INSERT INTO tempus_networks.poi(node_id, name, type, comment)
SELECT node_id, name, type, comment FROM %(temp_schema).poi;

DO
$$
BEGIN
RAISE notice '==== 2. Attach to the road network ===';
END
$$;

DO
$$
BEGIN
    IF ( '%(link)'::character varying = 'True'::character varying ) THEN
        CREATE INDEX ON tempus_networks.arc USING gist( geography(st_transform(geom, 4326)) ); 
        
        PERFORM pgtempus.tempus_attach_node_to_a_road_section( node.id, (SELECT array_agg(id) FROM pgtempus.source WHERE type = 1), %(max_dist), ARRAY[1,4]::smallint[] )
        FROM %(temp_schema).node;
        
        PERFORM pgtempus.tempus_set_node_traffic_rules( node.id )
        FROM %(temp_schema).node;
        
        DROP INDEX tempus_networks.arc_geography_idx; 
        PERFORM setval('tempus_networks.arc_id_seq', (SELECT coalesce(max(id)+1, 1) FROM tempus_networks.arc), false); 
    END IF;
END
$$;

