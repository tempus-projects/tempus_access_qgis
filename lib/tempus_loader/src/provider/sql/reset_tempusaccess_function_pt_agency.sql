
do $$
begin
raise notice '==== TempusAccess indicators function for PT agencies ==='; 
end$$;



DROP FUNCTION IF EXISTS pgtempus.tempus_create_pt_agency_indicator_layer( character varying, integer[], integer[], date[], integer, time, time, boolean );

CREATE OR REPLACE FUNCTION pgtempus.tempus_create_pt_agency_indicator_layer(
                                                                       p_table_name_pt_agency character varying,
                                                                       p_sources_id smallint[], 
                                                                       p_pt_trip_types integer[],
                                                                       p_days date[],
                                                                       p_day_ag integer,
                                                                       p_time_start time,
                                                                       p_time_end time,
                                                                       p_time_constraint_dep boolean
                                                                    )
RETURNS void AS
$BODY$
DECLARE
    s character varying; 
	t character varying; 
    day_ag_str character varying; 
BEGIN
    -- Mandatory parameters
    IF (
            p_sources_id is null OR p_pt_trip_types is null OR p_days is null OR 
            array_length(p_sources_id, 1)=0 OR array_length(p_pt_trip_types, 1)=0 OR array_length(p_days, 1)=0
       )
    THEN 
        RAISE EXCEPTION 'Parameters 1 to 4 must be non-empty arrays';
    END IF;
    
    IF p_time_constraint_dep = True
    THEN
        t = $$
              AND (time_from <= '$$ || p_time_end::character varying || $$')
              AND (time_from >= '$$ || p_time_start::character varying || $$')
            $$;    
    ELSE
        t = $$
              AND (time_to <= '$$ || p_time_end::character varying || $$')
              AND (time_to >= '$$ || p_time_start::character varying || $$')
            $$;    
    END IF; 
    
    -- Filtered data
    s = $$
        DROP TABLE IF EXISTS filtered_data;
        CREATE TEMPORARY TABLE filtered_data AS
        (
            SELECT pt_agency_id as id, 
                   unnest(pt_trip_types_id) as pt_trip_type_id, 
                   pt_trip_id,
                   day
            FROM tempus_networks.pt_section_trip_stop_times
            WHERE source_id = ANY('$$ || p_sources_id::character varying || $$')
              AND pt_trip_types_id && '$$ || p_pt_trip_types::character varying || $$'
              AND day = ANY('$$ || p_days::character varying || $$')
              $$ || t || $$
        )
        $$; 
    RAISE NOTICE 'Filtered data:\n%', s; 
    EXECUTE(s);
    
    -- Daily aggregated data
    s = $$
        DROP TABLE IF EXISTS daily_agregated_data;
        CREATE TEMPORARY TABLE daily_agregated_data AS
        (
            SELECT d.id, 
                   d.pt_trip_type_id, 
                   array_agg(DISTINCT pt_trip_id) as pt_trips_id, 
                   sum(pt_trip_geom.total_dist) as veh_km, 
                   day
            FROM filtered_data d JOIN tempus_networks.pt_trip_geom ON ( pt_trip_geom.id = d.pt_trip_id)
            GROUP BY d.id, pt_trip_type_id, day
        )
        $$; 
    RAISE NOTICE 'Daily agregated data:\n%', s; 
    EXECUTE(s);
    
    -- Aggregating all days together
    IF p_day_ag IS NULL THEN
        p_day_ag=1;
    END IF;
    SELECT INTO day_ag_str 
                func_name 
    FROM pgtempus.agregate WHERE id = p_day_ag;
	
    s = $$
        DROP TABLE IF EXISTS tempus_results.$$ || p_table_name_pt_agency || $$ CASCADE;
        CREATE TABLE tempus_results.$$ || p_table_name_pt_agency || $$ AS
        (        
            SELECT row_number() over() as gid, q.*
            FROM 
            (
                -- Aggregate days together 
                SELECT pt_agency_complete.id, 
                       pt_agency_complete.original_id, 
                       pt_agency_complete.name, 
                       d.pt_trip_type_id,
                       pt_trip_type.name as pt_trip_type_name, 
                       pt_trip_type.gtfs_codes, 
                       pt_agency_complete.source_id,  
                       '$$ || p_days::character varying || $$'::date[] as days,
                       array_remove(array_agg(DISTINCT CASE WHEN d.id IS NOT NULL THEN day END ORDER BY CASE WHEN d.id IS NOT NULL THEN day END), NULL) as pt_serv_days,                    
                       round($$ || day_ag_str || $$( d.veh_km ), 2) as veh_km 
                FROM daily_agregated_data d JOIN tempus_networks.pt_agency_complete ON ( d.id = pt_agency_complete.id )
                                            JOIN pgtempus.pt_trip_type ON (d.pt_trip_type_id = pt_trip_type.id)
                GROUP BY pt_agency_complete.id, 
                         pt_agency_complete.original_id, 
                         pt_agency_complete.name, 
                         d.pt_trip_type_id, 
                         pt_trip_type.name, 
                         pt_trip_type.gtfs_codes, 
                         pt_agency_complete.source_id
                ORDER BY pt_agency_complete.id
            ) q
        ); 
        DELETE FROM tempus_results.catalog WHERE table_name = '$$ || p_table_name_pt_agency || $$';
        INSERT INTO tempus_results.catalog( table_name, base_obj_type_id, base_obj_subtype_id ) VALUES ( '$$ || p_table_name_pt_agency || $$', 3, 1 )
        $$;
    RAISE NOTICE '%', s; 
    EXECUTE(s);
    
    -- Add key
    s = $$
            ALTER TABLE tempus_results.$$ || p_table_name_pt_agency || $$ ADD CONSTRAINT $$ || p_table_name_pt_agency || $$_pkey PRIMARY KEY(gid);
        $$;
    RAISE NOTICE '%', s; 
    EXECUTE(s);
    
    RETURN;
END; 

$BODY$
LANGUAGE plpgsql; 
  
  