
do $$
begin
raise notice '==== Restore constraints, triggers and indexes ===';
end $$;

ALTER TABLE tempus_networks.node ADD CONSTRAINT node_source_id_fkey FOREIGN KEY (source_id) REFERENCES pgtempus.source (id) ON UPDATE CASCADE ON DELETE CASCADE;
CREATE INDEX IF NOT EXISTS node_id_idx ON tempus_networks.node(id);
CREATE INDEX IF NOT EXISTS node_source_id_idx ON tempus_networks.node(source_id);
CREATE INDEX IF NOT EXISTS node_geom_idx ON tempus_networks.node USING gist (geom); 

ALTER TABLE tempus_networks.arc ADD CONSTRAINT arc_node_from_id_fkey FOREIGN KEY (node_from_id) REFERENCES tempus_networks.node ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE tempus_networks.arc ADD CONSTRAINT arc_node_to_id_fkey FOREIGN KEY (node_to_id) REFERENCES tempus_networks.node ON UPDATE CASCADE ON DELETE CASCADE;
CREATE INDEX IF NOT EXISTS arc_id_idx ON tempus_networks.arc(id);
CREATE INDEX IF NOT EXISTS arc_node_from_id_idx ON tempus_networks.arc(node_from_id);
CREATE INDEX IF NOT EXISTS arc_node_to_id_idx ON tempus_networks.arc(node_to_id);
CREATE INDEX IF NOT EXISTS arc_geom_idx ON tempus_networks.arc USING gist (geom); 

SELECT pgtempus.tempus_restore_triggers();
    

