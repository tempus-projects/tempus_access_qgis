do $$
begin
raise notice '==== Remove constraints and indexes ===';
end$$;

SELECT pgtempus.tempus_remove_foreign_keys_and_indexes('tempus_networks.node');
SELECT pgtempus.tempus_remove_foreign_keys_and_indexes('tempus_networks.arc');
SELECT pgtempus.tempus_remove_foreign_keys_and_indexes('tempus_networks.arcs_sequence');
SELECT pgtempus.tempus_remove_foreign_keys_and_indexes('tempus_networks.arcs_sequence_time');
SELECT pgtempus.tempus_remove_foreign_keys_and_indexes('tempus_networks.arcs_sequence_toll');
SELECT pgtempus.tempus_remove_foreign_keys_and_indexes('tempus_networks.road_section_speed');
SELECT pgtempus.tempus_remove_foreign_keys_and_indexes('tempus_networks.road_section_attributes');

do $$
begin
raise notice '==== Remove triggers ===';
end$$;

SELECT pgtempus.tempus_remove_triggers();

do $$
begin
raise notice '==== Create new source if needed ===';
end$$;

do
$$
begin
    IF '%(merge)' = 'False' THEN
        DELETE FROM pgtempus.source
        WHERE name = '%(source_name)';
        
        INSERT INTO pgtempus.source(id, name, type, comment)
        VALUES((SELECT coalesce(max(id)+1, 1) FROM pgtempus.source), '%(source_name)'::character varying, 1, '%(source_comment)'::character varying);
        
        ALTER SEQUENCE pgtempus.source_id_seq MINVALUE 0;
        PERFORM setval('pgtempus.source_id_seq', (SELECT coalesce(max(id), 0) FROM pgtempus.source));
    END IF;
end
$$;

