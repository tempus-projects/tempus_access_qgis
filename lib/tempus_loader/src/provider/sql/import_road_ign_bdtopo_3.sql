-- Tempus - Road BDTopo SQL import Wrapper
/*
        Substitutions options
        %(source_name): name of the road network
        %(merge): IF True then the current network is merged with an existing one (objects having same "original_id" are merged)
*/

DO
$$
BEGIN
    IF %(merge) = True THEN
        RAISE notice '==== 0. Doubles suppression ===';
        DELETE FROM %(temp_schema).troncon_de_route
        WHERE id IN 
        (
            SELECT arc.original_id 
            FROM tempus_networks.arc JOIN tempus_networks.node node_from ON (node_from.id = arc.node_from_id)
                                     JOIN tempus_networks.node node_to ON (node_to.id = arc.node_to_id)
            WHERE node_from.source_id = (SELECT id FROM pgtempus.source WHERE name = '%(source_name)') AND node_from.source_id = node_to.source_id
        );
        
        DELETE FROM %(temp_schema).non_communication
        WHERE id IN 
        (
            SELECT arcs_sequence.original_id 
            FROM tempus_networks.arcs_sequence 
            WHERE sources_id = ARRAY[(SELECT id FROM pgtempus.source WHERE name = '%(source_name)')]
        );
    END IF;
END
$$;

DO
$$
BEGIN
RAISE notice '==== 1. Make geometries valid ===';
END
$$;

SELECT pgtempus.tempus_correction_geom('%(temp_schema)', 'troncon_de_route', 'geom');

DO
$$
BEGIN
RAISE notice '==== 2. Build graph topology ===';
END
$$;

DROP TABLE IF EXISTS %(temp_schema).node;
CREATE TABLE %(temp_schema).node
(
    id bigserial,
    geom Geometry(PointZ, %(target_srid))
);
ALTER SEQUENCE %(temp_schema).node_id_seq MINVALUE 0;
SELECT setval('%(temp_schema).node_id_seq', (SELECT coalesce(max(id), 0) FROM tempus_networks.node)); 

INSERT INTO %(temp_schema).node ( geom )
(
    SELECT DISTINCT st_force3DZ(st_transform(st_startpoint(st_linemerge(geom)), %(target_srid))) 
    FROM %(temp_schema).troncon_de_route
    UNION 
    SELECT DISTINCT st_force3DZ(st_transform(st_endpoint(st_linemerge(geom)), %(target_srid)))
    FROM %(temp_schema).troncon_de_route
);
CREATE INDEX ON %(temp_schema).node USING btree(id);
CREATE INDEX ON %(temp_schema).node USING gist(geom);

ALTER TABLE %(temp_schema).troncon_de_route
    ADD COLUMN id_ft bigserial, 
    ADD COLUMN id_tf bigserial,
    ADD COLUMN start_geom geometry(pointZ, %(target_srid)), 
    ADD COLUMN end_geom geometry(pointZ, %(target_srid)),
    ADD COLUMN start_node bigint,
    ADD COLUMN end_node bigint;

UPDATE %(temp_schema).troncon_de_route
SET id_ft = id_ft + (SELECT coalesce(max(id), 0) FROM tempus_networks.arc);

UPDATE %(temp_schema).troncon_de_route
SET id_tf = id_tf + (SELECT max(id_ft) FROM %(temp_schema).troncon_de_route);

CREATE INDEX troncon_de_route_id_idx ON %(temp_schema).troncon_de_route(id); 

UPDATE %(temp_schema).troncon_de_route
SET start_geom = st_force3DZ(st_transform(st_startpoint(st_linemerge(geom)), %(target_srid))), 
    end_geom = st_force3DZ(st_transform(st_endpoint(st_linemerge(geom)), %(target_srid)));

-- remove arcs that are reduced to a node
DELETE FROM %(temp_schema).troncon_de_route
WHERE st_equals(start_geom, end_geom);

CREATE INDEX ON %(temp_schema).troncon_de_route using gist(start_geom);
CREATE INDEX ON %(temp_schema).troncon_de_route using gist(end_geom);
CREATE INDEX ON %(temp_schema).troncon_de_route using gist(geom);

-- resolve start and end node ids
UPDATE %(temp_schema).troncon_de_route
SET start_node = ( SELECT n1.id FROM %(temp_schema).node n1 WHERE st_equals(n1.geom, start_geom) LIMIT 1 ), 
    end_node = ( SELECT n1.id FROM %(temp_schema).node n1 WHERE st_equals(n1.geom, end_geom) LIMIT 1 );

CREATE TABLE %(temp_schema).arc AS 
(
    SELECT DISTINCT ON (node_from_id, node_to_id) *
    FROM
    (
    SELECT
            id_ft as id, 
            ARRAY[(SELECT id FROM pgtempus.source WHERE name = '%(source_name)')] as sources_id,
            id as original_id, 
            start_node as node_from_id, 
            end_node as node_to_id, 
            round(st_length(geom)::numeric, 2) as length, 
            1 as type,
            COALESCE (
                        -- Walking
                        CASE WHEN (nature!='Autoroute' AND nature!='Quasi-autoroute' AND nature!='Type autoroutier' AND nature!='Bretelle') 
                             THEN (SELECT array_agg(id) FROM pgtempus.traffic_rule WHERE name = 'Walking')
                             ELSE ARRAY[]::smallint[]
                        END ||
                        -- Wheelchair
                        CASE WHEN (nature!='Autoroute' AND nature!='Quasi-autoroute' AND nature!='Type autoroutier' AND nature!='Bretelle' AND nature != 'Escalier' AND nature != 'Route empierrée') 
                             THEN (SELECT array_agg(id) FROM pgtempus.traffic_rule WHERE name = 'Wheelchair')::smallint[]
                             ELSE ARRAY[]::smallint[]
                        END || 
                        -- Bicycle 
                        CASE WHEN (sens = 'Sens direct' or sens = 'Double sens' or sens = 'Sans objet') 
                                  AND (nature!='Autoroute' AND nature!='Quasi-autoroute' AND nature!='Type autoroutier' AND nature!='Bretelle' AND nature!='Escalier') 
                                  AND ((fermeture is null) OR (position('cycle' in fermeture) = 0)) 
                             THEN ARRAY[(SELECT id FROM pgtempus.traffic_rule WHERE name = 'Bicycle')]::smallint[]
                             ELSE ARRAY[]::smallint[]
                        END ||               
                        -- Individual cars with toll, taxi, individual cars without toll
                        CASE WHEN (sens = 'Sens direct' or sens = 'Double sens' or sens = 'Sans objet') AND (acces_vl != 'Physiquement impossible') AND (vit_moy_vl>0)
                             THEN
                                    CASE WHEN (acces_vl = 'A péage') THEN (SELECT array_agg(id) FROM pgtempus.traffic_rule WHERE name = 'Car accepting toll roads' or name = 'Taxi')::smallint[]
                                         ELSE (SELECT array_agg(id) FROM pgtempus.traffic_rule WHERE name = 'Car accepting toll roads' or name = 'Car refusing toll roads' or  name = 'Taxi')::smallint[]
                                    END
                             ELSE ARRAY[]::smallint[]
                        END, 
                        ARRAY[]::smallint[]
            )::smallint[] as traffic_rules, 
            CASE WHEN (nature='Autoroute' OR nature='Quasi-autoroute' OR nature='Type autoroutier' OR nature='Bretelle' OR nature='Bac ou liaison maritime') THEN 0
                 ELSE 3
            END as diffusion, 
            CASE WHEN (nature='Autoroute' OR nature='Quasi-autoroute' OR nature='Type autoroutier' OR nature='Bretelle') THEN 3
                 ELSE 0
            END as crossability, 
            ST_LineMerge(st_force3DZ(st_transform(st_simplify( st_linemerge(geom), %(geom_simplify), True ), %(target_srid)))) as geom 
    FROM %(temp_schema).troncon_de_route
    WHERE etat='En service' AND id_ft IS NOT NULL AND start_node != end_node
    UNION
    SELECT
            id_tf as id, 
            ARRAY[(SELECT id FROM pgtempus.source WHERE name = '%(source_name)')],
            id as original_id, 
            end_node, 
            start_node, 
            round(st_length(geom)::numeric, 2) as length, 
            1, 
            COALESCE (
                        -- Walking
                        CASE WHEN (nature!='Autoroute' AND nature!='Quasi-autoroute' AND nature!='Type autoroutier' AND nature!='Bretelle') 
                             THEN (SELECT array_agg(id) FROM pgtempus.traffic_rule WHERE name = 'Walking')
                             ELSE ARRAY[]::smallint[]
                        END ||
                        -- Wheelchair
                        CASE WHEN (nature!='Autoroute' AND nature!='Quasi-autoroute' AND nature!='Type autoroutier' AND nature!='Bretelle' AND nature != 'Escalier' AND nature != 'Route empierrée') 
                             THEN (SELECT array_agg(id) FROM pgtempus.traffic_rule WHERE name = 'Wheelchair')::smallint[]
                             ELSE ARRAY[]::smallint[]
                        END || 
                        -- Bicycle 
                        CASE WHEN (sens = 'Sens inverse' or sens = 'Double sens' or sens = 'Sans objet') 
                                  AND (nature!='Autoroute' AND nature!='Quasi-autoroute' AND nature!='Type autoroutier' AND nature!='Bretelle' AND nature!='Escalier') 
                                  AND ((fermeture is null) OR (position('cycle' in fermeture) = 0)) 
                             THEN ARRAY[(SELECT id FROM pgtempus.traffic_rule WHERE name = 'Bicycle')]::smallint[]
                             ELSE ARRAY[]::smallint[]
                        END ||               
                        -- Individual cars with toll, taxi, individual cars without toll
                        CASE WHEN (sens = 'Sens inverse' or sens = 'Double sens' or sens = 'Sans objet') AND (acces_vl != 'Physiquement impossible') AND (vit_moy_vl>0)
                             THEN
                                    CASE WHEN (acces_vl = 'A péage') THEN (SELECT array_agg(id) FROM pgtempus.traffic_rule WHERE name = 'Car accepting toll roads' or name = 'Taxi')::smallint[]
                                         ELSE (SELECT array_agg(id) FROM pgtempus.traffic_rule WHERE name = 'Car accepting toll roads' or name = 'Car refusing toll roads' or  name = 'Taxi')::smallint[]
                                    END
                             ELSE ARRAY[]::smallint[]
                        END, 
                        ARRAY[]::smallint[]
            )::smallint[] as traffic_rules, 
            CASE WHEN (nature='Autoroute' OR nature='Quasi-autoroute' OR nature='Type autoroutier' OR nature='Bretelle' OR nature='Bac ou liaison maritime') THEN 0
                 ELSE 3
            END, 
            CASE WHEN (nature='Autoroute' OR nature='Quasi-autoroute' OR nature='Type autoroutier' OR nature='Bretelle') THEN 3
                 ELSE 0
            END, 
            st_reverse(ST_LineMerge(st_force3DZ(st_transform(st_simplify( st_linemerge(geom), %(geom_simplify), True ), %(target_srid))))) as geom
    FROM %(temp_schema).troncon_de_route
    WHERE etat='En service' AND id_tf IS NOT NULL AND start_node != end_node
    ) q
);

CREATE INDEX ON %(temp_schema).arc using btree(node_from_id);
CREATE INDEX ON %(temp_schema).arc using btree(node_to_id);


do $$
begin
raise notice '==== 3. Road node table ===';
end $$;

INSERT INTO tempus_networks.node(id, source_id, traffic_rules, geom)
(
    SELECT node.id, 
           (SELECT id FROM pgtempus.source WHERE name = '%(source_name)') as source_id, 
           array_agg(DISTINCT q.traffic_rule ORDER BY q.traffic_rule) as traffic_rules, 
           ST_Force3DZ(st_transform(node.geom,%(target_srid)))
    FROM %(temp_schema).node JOIN 
    (
        SELECT node.id, unnest(arc.traffic_rules) as traffic_rule 
        FROM %(temp_schema).node JOIN %(temp_schema).arc ON (arc.node_from_id = node.id OR arc.node_to_id = node.id)
    ) q 
    ON (q.id = node.id) 
    GROUP BY node.id, node.geom 
);

do $$
begin
raise notice '==== 4. Road section table ===';
end$$;

SELECT setval('pgtempus.road_section_type_id_seq', (SELECT coalesce(max(id), 0) FROM pgtempus.road_section_type));
INSERT INTO pgtempus.road_section_type (name) SELECT 'Chemin ou sentier' EXCEPT SELECT name FROM pgtempus.road_section_type WHERE name = 'Chemin ou sentier';
INSERT INTO pgtempus.road_section_type (name) SELECT 'Escalier' EXCEPT SELECT name FROM pgtempus.road_section_type WHERE name = 'Escalier';
INSERT INTO pgtempus.road_section_type (name) SELECT 'Piste cyclable' EXCEPT SELECT name FROM pgtempus.road_section_type WHERE name = 'Piste cyclable';
INSERT INTO pgtempus.road_section_type (name) SELECT 'Liaison lacustre, fluviale ou maritime' EXCEPT SELECT name FROM pgtempus.road_section_type WHERE name = 'Liaison lacustre, fluviale ou maritime';
INSERT INTO pgtempus.road_section_type (name) SELECT 'Route empierrée' EXCEPT SELECT name FROM pgtempus.road_section_type WHERE name = 'Route empierrée';
INSERT INTO pgtempus.road_section_type (name) SELECT 'Route à 1 chaussée' EXCEPT SELECT name FROM pgtempus.road_section_type WHERE name = 'Route à 1 chaussée';
INSERT INTO pgtempus.road_section_type (name) SELECT 'Route à 2 chaussées' EXCEPT SELECT name FROM pgtempus.road_section_type WHERE name = 'Route à 2 chaussées';
INSERT INTO pgtempus.road_section_type (name) SELECT 'Autoroute, quasi-autoroute ou bretelle' EXCEPT SELECT name FROM pgtempus.road_section_type WHERE name = 'Autoroute, quasi-autoroute ou bretelle';

INSERT INTO tempus_networks.arc (id, sources_id, original_id, node_from_id, node_to_id, length, type, traffic_rules, diffusion, crossability, geom)
(
    SELECT id, sources_id, original_id, node_from_id, node_to_id, length, type, traffic_rules, diffusion, crossability, geom FROM %(temp_schema).arc
);

SELECT pgtempus.tempus_digitalization_direction_correction();

INSERT INTO tempus_networks.road_section_attributes(arc_id, road_number, street_name, total_lanes, section_type_id)
( 
    SELECT s.* FROM
    (
        SELECT  id_ft as id, 
                numero as road_number, 
                COALESCE(alias_g, alias_d, nom_1_g, nom_1_d, nom_2_g, nom_2_d) as street_name, 
                nb_voies as total_lanes, 
                CASE WHEN (nature = 'Chemin' OR nature = 'Sentier') THEN (SELECT id FROM pgtempus.road_section_type WHERE name = 'Chemin ou sentier')
                     WHEN nature = 'Bac ou liaison maritime' THEN (SELECT id FROM pgtempus.road_section_type WHERE name = 'Liaison lacustre, fluviale ou maritime')
                     ELSE (SELECT id FROM pgtempus.road_section_type WHERE name = nature)
                END AS section_type_id                 
        FROM %(temp_schema).troncon_de_route
        WHERE etat = 'En service' or etat is null
        UNION
        SELECT  id_tf as id,
                numero as road_number, 
                COALESCE(alias_g, alias_d, nom_1_g, nom_1_d, nom_2_g, nom_2_d) as street_name, 
                nb_voies as total_lanes, 
                CASE WHEN (nature = 'Chemin' OR nature = 'Sentier') THEN (SELECT id FROM pgtempus.road_section_type WHERE name = 'Chemin ou sentier')
                     WHEN (nature = 'Bac ou liaison maritime') THEN (SELECT id FROM pgtempus.road_section_type WHERE name = 'Liaison lacustre, fluviale ou maritime')
                     ELSE (SELECT id FROM pgtempus.road_section_type WHERE name = nature)
                END AS section_type_id
        FROM %(temp_schema).troncon_de_route
        WHERE etat = 'En service' or etat is null
    ) s JOIN tempus_networks.arc ON (s.id = arc.id)
);


do $$
begin
raise notice '==== 5. Speed data creation ===';
end$$;

-- Cost definition for each road arc, for motorized modes
-- For traffic_rules in ("Private car", "Taxi", "Carpooling", "Truck", "Coach") (speed_rule = 5)
-- For all days, definition of a static speed function, based on the light vehicles mean speed defined in BDTopo

-- 4.1 From -> To direction
INSERT INTO tempus_networks.road_section_speed(
                                                arc_id, 
                                                speed_rule, 
                                                value
                                              )
            SELECT arc.id, 
                   1, 
                   greatest(troncon_de_route.vit_moy_vl, 5::smallint) -- Cars drive at least at 5 km/h (if speed > 0)
            FROM tempus_networks.arc JOIN %(temp_schema).troncon_de_route ON ( troncon_de_route.id_ft = arc.id)
                                     JOIN tempus_networks.road_section_attributes ON (arc.id = road_section_attributes.arc_id)
            WHERE troncon_de_route.vit_moy_vl IS NOT NULL AND troncon_de_route.vit_moy_vl > 0 AND (arc.traffic_rules && (SELECT array_agg(id) FROM pgtempus.traffic_rule WHERE name = 'Car accepting toll roads' or name ='Taxi' or name = 'Car refusing toll roads' or name = 'Carpooling')); 

-- 4.2 To -> From direction
INSERT INTO tempus_networks.road_section_speed(
                                                arc_id, 
                                                speed_rule, 
                                                value
                                              )
            SELECT arc.id, 
                   1, 
                   greatest(troncon_de_route.vit_moy_vl, 5::smallint) -- Cars drive at least at 5 km/h (if speed > 0)
            FROM tempus_networks.arc JOIN %(temp_schema).troncon_de_route ON ( troncon_de_route.id_tf = arc.id)
                                     JOIN tempus_networks.road_section_attributes ON (arc.id = road_section_attributes.arc_id)
            WHERE troncon_de_route.vit_moy_vl IS NOT NULL AND troncon_de_route.vit_moy_vl > 0 AND (arc.traffic_rules && (SELECT array_agg(id) FROM pgtempus.traffic_rule WHERE name = 'Car accepting toll roads' or name ='Taxi' or name = 'Car refusing toll roads' or name = 'Carpooling')); 

do $$
begin
raise notice '==== 6. Road forbidden movements ===';
end$$;

DROP TABLE IF EXISTS %(temp_schema).arcs_sequence;
CREATE TABLE %(temp_schema).arcs_sequence (id bigserial, original_id character varying, arcs_id bigint[]);

ALTER SEQUENCE %(temp_schema).arcs_sequence_id_seq MINVALUE 0;
SELECT setval('%(temp_schema).arcs_sequence_id_seq', (SELECT coalesce(max(id), 0) FROM tempus_networks.arcs_sequence));

WITH q AS (
    SELECT troncon_entree.id_ft as troncon_entree_id_ft, 
           troncon_entree.id_tf as troncon_entree_id_tf, 
           troncon_entree.start_node as troncon_entree_start_node, 
           troncon_entree.end_node as troncon_entree_end_node, 
           troncon_sortie.id_ft as troncon_sortie_id_ft, 
           troncon_sortie.id_tf as troncon_sortie_id_tf, 
           troncon_sortie.start_node as troncon_sortie_start_node, 
           troncon_sortie.end_node as troncon_sortie_end_node, 
           non_communication.id as original_id
    FROM %(temp_schema).non_communication JOIN %(temp_schema).troncon_de_route troncon_entree ON (troncon_entree.id = non_communication.id_tr_ent)
                                          JOIN %(temp_schema).troncon_de_route troncon_sortie ON (troncon_sortie.id = non_communication.id_tr_sor)
)
INSERT INTO %(temp_schema).arcs_sequence ( original_id, arcs_id ) 
SELECT original_id, 
       ARRAY[
               CASE WHEN (troncon_entree_start_node = troncon_sortie_start_node) OR (troncon_entree_start_node = troncon_sortie_end_node) THEN troncon_entree_id_tf
                    WHEN (troncon_entree_end_node = troncon_sortie_start_node) OR (troncon_entree_end_node = troncon_sortie_end_node) THEN troncon_entree_id_ft
               END, 
               CASE WHEN (troncon_entree_start_node = troncon_sortie_start_node) OR (troncon_entree_end_node = troncon_sortie_start_node) THEN troncon_sortie_id_ft
                    WHEN (troncon_entree_start_node = troncon_sortie_end_node) OR (troncon_entree_end_node = troncon_sortie_end_node) THEN troncon_sortie_id_tf
               END
            ] as arcs_id
FROM q;

WITH r AS (
    WITH q AS (
        SELECT arcs_sequence.id, a.arc_id, a.o
        FROM %(temp_schema).arcs_sequence, unnest(arcs_sequence.arcs_id) WITH ORDINALITY a(arc_id, o)
    )    
    SELECT q.id, st_makeline(arc.geom ORDER BY q.o) as geom
    FROM q JOIN tempus_networks.arc ON (arc.id = q.arc_id)
    GROUP BY q.id
)
INSERT INTO tempus_networks.arcs_sequence( id, sources_id, original_id, arcs_id, geom ) 
SELECT arcs_sequence.id, 
       ARRAY[(SELECT id FROM pgtempus.source WHERE name = '%(source_name)')],
       arcs_sequence.original_id, 
       arcs_sequence.arcs_id, 
       r.geom
FROM %(temp_schema).arcs_sequence JOIN r ON (r.id = arcs_sequence.id);

INSERT INTO tempus_networks.arcs_sequence_time ( arcs_sequence_id, traffic_rules )
SELECT id, 
      (SELECT array_agg(id) FROM pgtempus.traffic_rule WHERE name = 'Car accepting toll roads' or name ='Taxi' or name = 'Car refusing toll roads' or name = 'Carpooling' or name = 'Truck' or name = 'Coach') 
FROM %(temp_schema).arcs_sequence; 




