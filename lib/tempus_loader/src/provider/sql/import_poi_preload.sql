do $$
begin
raise notice '==== Remove triggers ===';
end$$;

SELECT pgtempus.tempus_remove_triggers();

do $$
begin
raise notice '==== Create new source if needed ===';
end$$;

do
$$
begin
    IF %(merge) = False THEN
        DELETE FROM pgtempus.source
        WHERE name = '%(source_name)';
        
        INSERT INTO pgtempus.source(id, name, type, comment)
        VALUES((SELECT coalesce(max(id)+1, 1) FROM pgtempus.source), '%(source_name)'::character varying, 3, '%(source_comment)'::character varying);
        
        ALTER SEQUENCE pgtempus.source_id_seq MINVALUE 0;
        PERFORM setval('pgtempus.source_id_seq', (SELECT coalesce(max(id), 0) FROM pgtempus.source));
    END IF;
end
$$;

