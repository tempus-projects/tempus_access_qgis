
DO
$$
BEGIN
RAISE notice '==== 1. Make geometries valid ===';
END
$$; 

SELECT pgtempus.tempus_correction_geom('%(temp_schema)', 'commune', 'geom'); 
SELECT pgtempus.tempus_correction_geom('%(temp_schema)', 'departement', 'geom'); 
SELECT pgtempus.tempus_correction_geom('%(temp_schema)', 'region', 'geom'); 
SELECT pgtempus.tempus_correction_geom('%(temp_schema)', 'epci', 'geom'); 
SELECT pgtempus.tempus_correction_geom('%(temp_schema)', 'canton', 'geom'); 
SELECT pgtempus.tempus_correction_geom('%(temp_schema)', 'arrondissement_departemental', 'geom'); 

DO
$$
BEGIN
RAISE notice '==== 2. Rename table and fields, reproject and comment ===';
END
$$;

DROP TABLE IF EXISTS tempus_zoning.%(source_name)_commune;
CREATE TABLE tempus_zoning.%(source_name)_commune 
AS
SELECT gid as id, insee_com as original_id, nom_com as lib, st_transform(st_multi(st_force2d(geom)), %(target_srid))::Geometry(MultiPolygon, %(target_srid)) as geom, population
FROM %(temp_schema).commune
ORDER BY insee_com; 

DROP TABLE IF EXISTS tempus_zoning.%(source_name)_departement;
CREATE TABLE tempus_zoning.%(source_name)_departement 
AS
SELECT gid as id, insee_dep as original_id, nom_dep as lib, st_transform(st_multi(st_force2d(st_simplify(geom, %(geom_simplify)))), %(target_srid))::Geometry(MultiPolygon, %(target_srid)) as geom
FROM %(temp_schema).departement
ORDER BY insee_dep; 

DROP TABLE IF EXISTS tempus_zoning.%(source_name)_region;
CREATE TABLE tempus_zoning.%(source_name)_region 
AS
SELECT gid as id, insee_reg as original_id, nom_reg as lib, st_transform(st_multi(st_force2d(st_simplify(geom, %(geom_simplify)))), %(target_srid))::Geometry(MultiPolygon, %(target_srid)) as geom
FROM %(temp_schema).region
ORDER BY insee_reg; 

DROP TABLE IF EXISTS tempus_zoning.%(source_name)_epci;
CREATE TABLE tempus_zoning.%(source_name)_epci
AS
SELECT gid as id, code_epci as original_id, nom_epci as lib, st_transform(st_multi(st_force2d(st_simplify(geom, %(geom_simplify)))), %(target_srid))::Geometry(MultiPolygon, %(target_srid)) as geom
FROM %(temp_schema).epci
ORDER BY code_epci; 

DROP TABLE IF EXISTS tempus_zoning.%(source_name)_canton;
CREATE TABLE tempus_zoning.%(source_name)_canton
AS
SELECT gid as id, insee_dep || insee_can as original_id, '' as lib, st_transform(st_multi(st_force2d(st_simplify(geom, %(geom_simplify)))), %(target_srid))::Geometry(MultiPolygon, %(target_srid)) as geom
FROM %(temp_schema).canton
ORDER BY insee_dep || insee_can; 

DROP TABLE IF EXISTS tempus_zoning.%(source_name)_arrondissement_departemental;
CREATE TABLE tempus_zoning.%(source_name)_arrondissement_departemental
AS
SELECT gid as id, insee_dep || insee_arr as original_id, nom_arr as lib, st_transform(st_multi(st_force2d(st_simplify(geom, %(geom_simplify)))), %(target_srid))::Geometry(MultiPolygon, %(target_srid)) as geom
FROM %(temp_schema).arrondissement_departemental
ORDER BY insee_dep || insee_arr; 

COMMENT ON TABLE tempus_zoning.%(source_name)_commune IS 'Communes (%(source_name))';
COMMENT ON TABLE tempus_zoning.%(source_name)_departement IS 'Départements (%(source_name))';
COMMENT ON TABLE tempus_zoning.%(source_name)_region IS 'Régions (%(source_name))';
COMMENT ON TABLE tempus_zoning.%(source_name)_epci IS 'EPCI (%(source_name))';
COMMENT ON TABLE tempus_zoning.%(source_name)_canton IS 'Cantons (%(source_name))';
COMMENT ON TABLE tempus_zoning.%(source_name)_arrondissement_departemental IS 'Arrondissements départementaux (%(source_name))';

do $$
begin
raise notice '==== 3. Create indexes ===';
end$$;

CREATE INDEX %(source_name)_commune_original_id_idx ON tempus_zoning.%(source_name)_commune USING btree (original_id);
CREATE INDEX %(source_name)_commune_geom_idx ON tempus_zoning.%(source_name)_commune USING gist(geom);

CREATE INDEX %(source_name)_departement_original_id_idx ON tempus_zoning.%(source_name)_departement USING btree (original_id);
CREATE INDEX %(source_name)_departement_geom_idx ON tempus_zoning.%(source_name)_departement USING gist(geom);

CREATE INDEX %(source_name)_region_original_id_idx ON tempus_zoning.%(source_name)_region USING btree (original_id);
CREATE INDEX %(source_name)_region_geom_idx ON tempus_zoning.%(source_name)_region USING gist(geom);

CREATE INDEX %(source_name)_epci_original_id_idx ON tempus_zoning.%(source_name)_epci USING btree (original_id);
CREATE INDEX %(source_name)_epci_geom_idx ON tempus_zoning.%(source_name)_epci USING gist(geom);

CREATE INDEX %(source_name)_canton_original_id_idx ON tempus_zoning.%(source_name)_canton USING btree (original_id);
CREATE INDEX %(source_name)_canton_geom_idx ON tempus_zoning.%(source_name)_canton USING gist(geom);

CREATE INDEX %(source_name)_arrondissement_departemental_original_id_idx ON tempus_zoning.%(source_name)_arrondissement_departemental USING btree (original_id);
CREATE INDEX %(source_name)_arrondissement_departemental_geom_idx ON tempus_zoning.%(source_name)_arrondissement_departemental USING gist(geom);




