/*
        Substitutions options
        %(temp_schema): name of schema containing temporary data
*/
do $$
begin
raise notice '==== Data transfer from temporary to final schema ===';
end$$;

INSERT INTO pgtempus.data_format
SELECT * FROM %(temp_schema).data_format;

INSERT INTO pgtempus.agregate
SELECT * FROM %(temp_schema).agregate;

INSERT INTO pgtempus.modality
SELECT * FROM %(temp_schema).modality;

INSERT INTO pgtempus.base_obj_type
SELECT * FROM %(temp_schema).base_obj_type;

INSERT INTO pgtempus.base_obj_subtype
SELECT * FROM %(temp_schema).base_obj_subtype;

INSERT INTO pgtempus.indic
SELECT * FROM %(temp_schema).indic;

INSERT INTO tempus_networks.holiday_period
SELECT * FROM %(temp_schema).holiday_period;

DROP SCHEMA IF EXISTS %(temp_schema) CASCADE;
