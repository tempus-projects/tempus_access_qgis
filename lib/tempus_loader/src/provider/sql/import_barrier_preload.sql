do $$
begin
raise notice '==== Remove constraints, triggers and indexes (performances concern) ===';
end$$;

-- Remove all related constraints
ALTER TABLE tempus_networks.node DROP CONSTRAINT IF EXISTS node_source_id_fkey;
DROP INDEX IF EXISTS tempus_networks.node_id_idx;
DROP INDEX IF EXISTS tempus_networks.node_source_id_idx;
DROP INDEX IF EXISTS tempus_networks.node_geom_idx;

ALTER TABLE tempus_networks.arc DROP CONSTRAINT IF EXISTS arc_source_id_fkey;
ALTER TABLE tempus_networks.arc DROP CONSTRAINT IF EXISTS arc_node_from_id_fkey;
ALTER TABLE tempus_networks.arc DROP CONSTRAINT IF EXISTS arc_node_to_id_fkey;
DROP INDEX IF EXISTS tempus_networks.arc_id_idx;
DROP INDEX IF EXISTS tempus_networks.arc_network_id_idx;
DROP INDEX IF EXISTS tempus_networks.arc_node_from_id_idx;
DROP INDEX IF EXISTS tempus_networks.arc_node_to_id_idx;
DROP INDEX IF EXISTS tempus_networks.arc_geom_idx;

SELECT pgtempus.tempus_remove_triggers();

do $$
begin
raise notice '==== Create new source if needed ===';
end$$;

do
$$
begin
    IF %(merge) = False THEN
        DELETE FROM pgtempus.source
        WHERE name = '%(source_name)';
        
        INSERT INTO pgtempus.source(id, name, type, comment)
        VALUES((SELECT coalesce(max(id)+1, 1) FROM pgtempus.source), '%(source_name)'::character varying, 5, '%(source_comment)'::character varying);
        
        ALTER SEQUENCE pgtempus.source_id_seq MINVALUE 0;
        PERFORM setval('pgtempus.source_id_seq', (SELECT coalesce(max(id), 0) FROM pgtempus.source));
    END IF;
end
$$;