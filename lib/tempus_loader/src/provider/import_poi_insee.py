#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2012-2013 IFSTTAR (http://www.ifsttar.fr)
 *   Copyright (C) 2012-2013 Oslandia <infos@oslandia.com>
 *   Copyright (C) 2019-2020 Cerema (http://www.cerema.fr) 
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""

from provider.data_dir_manager import DataDirManager

# Module to load a POI shape file
class ImportPOIINSEEBPE(DataDirManager):
    """This class enables to load INSEE BPE POI data into a PostGIS database and link it to an existing network."""
    # SQL files to execute before loading GTFS data
    PRE_SQL = [ "import_preload.sql", "import_poi_preload.sql" ]    
    # Shapefile names to load, without the extension and prefix. 
    IMPORT_DBFSHPFILES = [
                          ('bpe_ensemble_xy', True),
                          ('varmod_ensemble_xy', True), 
                          ('contours-iris', False)
                         ]
    # SQL files to execute after loading shapefiles 
    POST_SQL = [ "import_poi_insee_bpe.sql", "import_poi_postload.sql", "import_postload.sql" ]
