#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2012-2013 IFSTTAR (http://www.ifsttar.fr)
 *   Copyright (C) 2012-2013 Oslandia <infos@oslandia.com>
 *   Copyright (C) 2020-2021 Cerema <modelisation-deplacements@cerema.fr>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""



#from provider.import_road_ign import ImportRoadIGNBDTopo_2_2
from provider.import_road_ign import ImportRoadIGNBDTopo_3
# from provider.import_road_ign import ImportRoadIGNBDCarto_3_2
# from provider.import_road_ign import ImportRoadIGNRoute120_1_1
# from provider.import_road_ign import ImportRoadIGNRoute500_2_1
# from provider.import_road_tomtom import ImportRoadMultinet
# from provider.import_road_tomtom import ImportRoadMultinet_1409
# from provider.import_road_navteq import ImportRoadNavstreets
# from provider.import_road_osm import ImportRoadOSM
# from provider.import_road_tempus import ImportRoadTempus
# from provider.import_road_visum import ImportRoadVisum

from provider.import_pt_gtfs import ImportPTGTFS
# from provider.import_pt_gtfs import ImportPTGTFSTemp
# from provider.import_pt_sncf import ImportPTSNCF

from provider.import_poi_generic import ImportPOIGeneric
from provider.import_poi_insee import ImportPOIINSEEBPE
from provider.import_poi_ign import ImportPOIIGNBDTopo_3

from provider.import_zoning_ign import ImportZoningIGNAdminExpress
from provider.import_zoning_generic import ImportZoningGeneric

from provider.import_barrier_ign import ImportBarrierIGNBDTopo_3

from provider.delete import Delete

# from provider.merge_pt import MergePT

from provider.export_poi_generic import ExportPOIGeneric
# from provider.export_road_tempus import ExportRoadTempus
from provider.export_pt_gtfs import ExportPTGTFS
from provider.export_pt_tempus import ExportPTTempus
# from provider.export_zoning_tempus import ExportZoningTempus

from provider.reset_database import ResetTempus
from provider.reset_database import ResetTempusAccess


