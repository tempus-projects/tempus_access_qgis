#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Tempus road data importer

import provider

# def import_road_ign_route120(args, shape_options):
    # """Load IGN (Route120) road data into a Tempus database."""
    # subs={}
    # if args.source_name is None:
        # sys.stderr.write("A road network name must be supplied. Use --source-name\n")
        # sys.exit(1)
    # subs['source_name'] = args.source_name
    # subs["source_srid"] = args.source_srid
    # subs["target_srid"] = args.target_srid
    # if args.source_comment is None:
        # subs["source_comment"] = ''
    # else:
        # subs["source_comment"] = args.source_comment
    # if args.noclean:
        # subs["no_clean"] = "True"
    # else:
        # subs["no_clean"] = "False"
    # if args.merge:
        # subs["merge"] = "True"
    # else:
        # subs["merge"] = "False"
    # subs["temp_schema"]=provider.config.TEMPSCHEMA
    # Importer = {
        # '1.1': provider.ImportRoadIGNRoute120_1_1,
        # None: provider.ImportRoadIGNRoute120_1_1
    # }[args.model_version]
    # rte120i = Importer(path=args.path, prefix=args.prefix, dbstring=args.dbstring, logfile=args.logfile, shape_options=shape_options, subs=subs)
    # return rte120i.run()

    
# def import_road_ign_route500(args, shape_options):
    # """Load IGN (Route500) road data into a Tempus database."""
    # subs={}
    # if args.source_name is None:
        # sys.stderr.write("A road network name must be supplied. Use --source-name\n")
        # sys.exit(1)
    # subs['source_name'] = args.source_name   
    # subs["source_srid"] = args.source_srid
    # subs["target_srid"] = args.target_srid 
    # if args.source_comment is None:
        # subs["source_comment"] = ''
    # else:
        # subs["source_comment"] = args.source_comment
    # if args.noclean:
        # subs["no_clean"] = "True"
    # else:
        # subs["no_clean"] = "False"
    # if args.merge:
        # subs["merge"] = "True"
    # else:
        # subs["merge"] = "False"
    # subs["temp_schema"]=provider.config.TEMPSCHEMA
    # Importer = {
        # '2.1': provider.ImportRoadIGNRoute500_2_1,
        # None: provider.ImportRoadIGNRoute500_2_1
    # }[args.model_version]
    
    # rte500i = Importer(path=args.path, prefix=args.prefix, dbstring=args.dbstring, logfile=args.logfile, shape_options=shape_options, subs=subs)
    # return rte500i.run()


def import_road_ign_bdtopo(args, shape_options):
    """Load IGN (BDTopo) road data into a Tempus database."""
    subs={}
    if args.source_name is None:
        sys.stderr.write("A road network name must be supplied. Use --source-name\n")
        sys.exit(1)        
    subs['source_name'] = args.source_name[0]
        
    if args.prefix is None:
        args.prefix = ''
        
    if args.source_srid is None:
        subs["source_srid"] = "2154"
    else:
        subs["source_srid"] = args.source_srid
        
    if args.target_srid is None:
        subs["target_srid"] = "2154"
    else: 
        subs["target_srid"] = args.target_srid
        
    if args.source_comment and args.source_comment is not None:
        subs["source_comment"] = args.source_comment
    else:
        subs["source_comment"] = ""
        
    if args.noclean:
        subs["no_clean"] = "True"
    else:
        subs["no_clean"] = "False"
    
    if args.merge:
        subs["merge"] = "True"
    else:
        subs["merge"] = "False"
        
    if args.filter and args.filter is not None:
        subs["filter"] = args.filter
    else:
        subs["filter"] = "True"
    
    if args.geom_simplify and args.geom_simplify is not None: 
        subs["geom_simplify"] = args.geom_simplify
    else: 
        subs["geom_simplify"] = "0"
    
    if args.noclean:
        subs["no_clean"] = "True"
    else:
        subs["no_clean"] = "False"
    
    subs["temp_schema"]=provider.config.TEMPSCHEMA
    
    Importer = {
        #'2.2': provider.ImportRoadIGNBDTopo_2_2,
        '3': provider.ImportRoadIGNBDTopo_3,
        None: provider.ImportRoadIGNBDTopo_3
    } [args.model_version]
    
    bdtopoi = Importer(path=args.path[0], prefix=args.prefix, dbstring=args.dbstring, logfile=args.logfile, options=shape_options, subs=subs)    
    return bdtopoi.run()

    
# def import_road_ign_bdcarto(args, shape_options):
    # """Load IGN (BDCarto) road data into a Tempus database."""
    # subs={}
    # if args.source_name is None:
        # sys.stderr.write("A road network name must be supplied. Use --source-name\n")
        # sys.exit(1)
    # subs['source_name'] = args.source_name
    # subs["source_srid"] = args.source_srid
    # subs["target_srid"] = args.target_srid
    # if args.source_comment is None:
        # subs["source_comment"] = ''
    # else:
        # subs["source_comment"] = args.source_comment
    # if args.noclean:
        # subs["no_clean"] = "True"
    # else:
        # subs["no_clean"] = "False"
    # if args.merge:
        # subs["merge"] = "True"
    # else:
        # subs["merge"] = "False"
    # subs["temp_schema"]=provider.config.TEMPSCHEMA
    # Importer = {
        # '3.2': provider.ImportRoadIGNBDCarto_3_2,
        # None: provider.ImportRoadIGNBDCarto_3_2
    # }[args.model_version]
    # bdcartoi = Importer(path=args.path, prefix=args.prefix, dbstring=args.dbstring, logfile=args.logfile, shape_options=shape_options, subs=subs)
    # return bdcartoi.run()
    
    
# def import_road_visum(args, shape_options):
    # """Load a Visum-extracted Shapefile into a Tempus database; wait for 4
    # distinct transportation modes (pedestrian, bike, private car, taxi)

    # Parameters
    # ----------
    # args: list
        # list of arguments passed to loader
    # shape_options: dict
        # geometry options passed to the ShapeLoader
    
    # """
    # subs={}
    # if args.source_name is None:
        # sys.stderr.write("A road network name must be supplied. Use --source-name\n")
        # sys.exit(1)
    # subs['source_name'] = args.source_name
    # subs["source_srid"] = args.source_srid
    # subs["target_srid"] = args.target_srid
    # if args.source_comment is None:
        # subs["source_comment"] = ''
    # else:
        # subs["source_comment"] = args.source_comment
    # if args.noclean:
        # subs["no_clean"] = "True"
    # else:
        # subs["no_clean"] = "False"
    # if args.merge:
        # subs["merge"] = "True"
    # else:
        # subs["merge"] = "False"
    # subs["temp_schema"]=provider.config.TEMPSCHEMA
    # splitted_modes = args.visum_modes.split(',')
    # if len(splitted_modes) != 4:
        # sys.stderr.write(("Need 4 comma-separated strings "
                          # "(command --visum-modes) for representing "
                          # "pedestrians, bikes, private vehicles and taxis\n"))
        # sys.exit(1)
    # visumi = Importer(path=args.path, prefix=args.prefix, dbstring=args.dbstring, logfile=args.logfile, shape_options=shape_options, subs=subs, splitted_modes=splitted_modes)
    # return visumi.run()

# def import_road_osm(args, shape_options):
    # """Load OpenStreetMap (as shapefile) road data into a Tempus database."""
    # subs={}
    # if args.source_name is None:
        # sys.stderr.write("A road network name must be supplied. Use --source-name\n")
        # sys.exit(1)
    # subs['source_name'] = args.source_name
    # subs["source_srid"] = args.source_srid
    # subs["target_srid"] = args.target_srid
    # if args.source_comment is None:
        # subs["source_comment"] = ''
    # else:
        # subs["source_comment"] = args.source_comment
    # if args.noclean:
        # subs["no_clean"] = "True"
    # else:
        # subs["no_clean"] = "False"
    # if args.merge:
        # subs["merge"] = "True"
    # else:
        # subs["merge"] = "False"
    # subs["temp_schema"]=provider.config.TEMPSCHEMA
    # osmi = provider.ImportRoadOSM(path=args.path, dbstring=args.dbstring, logfile=args.logfile, subs=subs)
    # return osmi.run()

# def import_road_tomtom(args, shape_options):
    # """Load Tomtom (Multinet) road data into a Tempus database."""
    # subs={}
    # if args.source_name is None:
        # sys.stderr.write("A road network name must be supplied. Use --source-name\n")
        # sys.exit(1)
    # subs['source_name'] = args.source_name
    # subs["source_srid"] = args.source_srid
    # subs["target_srid"] = args.target_srid
    # if args.source_comment is None:
        # subs["source_comment"] = ''
    # else:
        # subs["source_comment"] = args.source_comment
    # if args.noclean:
        # subs["no_clean"] = "True"
    # else:
        # subs["no_clean"] = "False"
    # if args.merge:
        # subs["merge"] = "True"
    # else:
        # subs["merge"] = "False"
    # subs["temp_schema"]=provider.config.TEMPSCHEMA
    # Importer = {
        # '1409': provider.ImportRoadMultinet_1409,
        # None: provider.ImportRoadMultinet
    # }[args.model_version]
    # shape_options['I'] = False
    # mni = Importer(path=args.path, prefix=args.prefix, dbstring=args.dbstring, logfile=args.logfile, shape_options=shape_options, subs=subs)
    # return mni.run()
    

# def import_road_navteq(args, shape_options):
    # """Load Navteq (Navstreets) road data into a Tempus database."""
    # subs={}
    # if args.source_name is None:
        # sys.stderr.write("A road network name must be supplied. Use --source-name\n")
        # sys.exit(1)
    # subs['source_name'] = args.source_name
    # subs["source_srid"] = args.source_srid
    # subs["target_srid"] = args.target_srid
    # if args.source_comment is None:
        # subs["source_comment"] = ''
    # else:
        # subs["source_comment"] = args.source_comment
    # if args.noclean:
        # subs["no_clean"] = "True"
    # else:
        # subs["no_clean"] = "False"
    # if args.merge:
        # subs["merge"] = "True"
    # else:
        # subs["merge"] = "False"
    # subs["temp_schema"]=provider.config.TEMPSCHEMA
    # ntqi = provider.ImportRoadNavstreets(path=args.path, prefix=args.prefix, dbstring=args.dbstring, logfile=args.logfile, shape_options=shape_options, subs=subs)
    # return ntqi.run()


# def import_road_tempus(args, shape_options):
    # """Load a Tempus format road file (as shapefile) into a Tempus database."""
    # subs={}
    # if args.source_name is None:
        # sys.stderr.write("A road network name must be supplied. Use --source-name\n")
        # sys.exit(1)
    # subs['source_name'] = args.source_name
    # subs["source_srid"] = args.source_srid
    # subs["target_srid"] = args.target_srid
    # if args.source_comment is None:
        # subs["source_comment"] = ''
    # else:
        # subs["source_comment"] = args.source_commentroadtempusi = provider.ImportRoadTempus(args.path, args.dbstring, args.logfile)
    # if args.noclean:
        # subs["no_clean"] = "True"
    # else:
        # subs["no_clean"] = "False"
    # if args.merge:
        # subs["merge"] = "True"
    # else:
        # subs["merge"] = "False"
    # subs["temp_schema"]=provider.config.TEMPSCHEMA
    # roadtempusi = provider.ImportRoadNavstreets(path=args.path, prefix=args.prefix, dbstring=args.dbstring, logfile=args.logfile, shape_options=shape_options, subs=subs)
    # return roadtempusi.run()


