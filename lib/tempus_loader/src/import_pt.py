#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Tempus public transport data importer

import provider

def import_pt_gtfs(args):
    """Load Public Transport from GTFS data into a Tempus database."""    
    subs={}
    if args.source_name is None:
        sys.stderr.write("A PT network name must be supplied. Use --source-name\n")
        sys.exit(1)
    subs['source_name'] = args.source_name[0]
    
    if args.prefix is None:
        args.prefix = ''
        
    subs["source_srid"] = "4326"
        
    if args.target_srid is None:
        subs["target_srid"] = "2154"
    else: 
        subs["target_srid"] = args.target_srid
    
    if args.source_comment is None:
        subs["source_comment"] = ""
    else:
        subs["source_comment"] = args.source_comment
    
    if args.noclean:
        subs["no_clean"] = "True"
    else:
        subs["no_clean"] = "False"
    
    if args.merge:
        subs["merge"] = "True"
    else:
        subs["merge"] = "False"
    
    if args.link:
        subs["link"] = "True"
    else:
        subs["link"] = "False"
    
    if args.max_dist:
        subs["max_dist"] = str(args.max_dist)
    else: 
        subs["max_dist"] = "50"
        
    if args.noclean:
        subs["no_clean"] = "True"
    else:
        subs["no_clean"] = "False"
    
    subs["temp_schema"]=provider.config.TEMPSCHEMA
    
    gtfsi = provider.ImportPTGTFS(path=args.path[0], prefix="", dbstring=args.dbstring, logfile=args.logfile, options={'g':'geom', 'D':True, 'I':True, 'S':True}, sep=",", encoding=args.encoding, copymode=args.copymode, subs=subs)

    r = gtfsi.run()
    return r
    
def import_pt_sncf(args, shape_options):
    """Load Public Transport from SNCF and IGN open-data into a Tempus database."""
    subs={}
    if args.source_name is None:
        sys.stderr.write("A PT network name must be supplied. Use --source-name\n")
        sys.exit(1)
    
    subs['source_name'] = "ter"
    
    if args.prefix is None:
        args.prefix = ''
    
    if args.target_srid is None:
        subs["target_srid"] = "2154"
    else: 
        subs["target_srid"] = args.target_srid
    
    if args.source_comment is None:
        subs["source_comment"] = ""
    else:
        subs["source_comment"] = args.source_comment
    
    if args.noclean:
        subs["no_clean"] = "True"
    else:
        subs["no_clean"] = "False"
    
    if args.merge:
        subs["merge"] = "True"
    else:
        subs["merge"] = "False"
    
    if args.max_dist:
        subs["max_dist"] = str(args.max_dist)
    else: 
        subs["max_dist"] = "50"
        
    if args.noclean:
        subs["no_clean"] = "True"
    else:
        subs["no_clean"] = "False"
    
    subs["temp_schema"]=provider.config.TEMPSCHEMA
    
    teri = provider.ImportPTGTFSTemp(path=args.path[0], dbstring=args.dbstring, logfile=args.logfile, sep=",", encoding='UTF8', copymode=args.copymode, subs=subs)
    teri.run()
    
    subs["source_name"] = "ic"
    
    ici = provider.ImportPTGTFSTemp(path=args.path[1], dbstring=args.dbstring, logfile=args.logfile, sep=",", encoding='UTF8', copymode=args.copymode, subs=subs)
    ici.run()
    
    subs["source_name"] = args.source_name
    
    sncfi = provider.ImportPTSNCF(args.path[2], args.prefix, args.dbstring, args.logfile, shape_options, subs=subs) 
    return sncfi.run()
    
# def import_pt_ntfs(args):    
    # subs={}
    # if args.source_name is None:
        # sys.stderr.write("A PT network name must be supplied. Use --source-name\n")
        # sys.exit(1)
    # subs['source_name'] = args.source_name
    # subs["source_srid"] = args.source_srid
    # subs["target_srid"] = args.target_srid
    # subs['max_dist'] = str(args.max_dist)
    # if args.noclean:
        # subs["no_clean"] = "True"
    # else:
        # subs["no_clean"] = "False"
    # subs["temp_schema"]=provider.config.TEMPSCHEMA
    # ntfsi = provider.ImportPTNTFS(path=args.path, dbstring=args.dbstring, logfile=args.logfile, sep=",", encoding=args.encoding, copymode=args.copymode, subs=subs)
    # return ntfsi.run()


    