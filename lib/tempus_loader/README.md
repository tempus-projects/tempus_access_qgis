Documentation Tempus Loader (fr)
=============

Cet outil est un importeur de données pour Tempus développé en Python. Il créé la base de données et la remplit avec différentes sources sur l'offre de transport, les points d'intérêt, les zonages, etc.  
La documentation de référence est disponible [ici](https://gitlab.com/tempus-projects/tempus_loader/wikis/home). 

-------------------------------------
# Tempus Loader documentation (en)

This is the data importer for the Tempus framework. It creates and feeds Tempus databases. It can be exploited to parse a wide range of data sources.  
Reference documentation is available [here](https://gitlab.com/tempus-projects/tempus_loader/wikis/home). 
