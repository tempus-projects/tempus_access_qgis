#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2018-2019 Cerema (https://www.cerema.fr)
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""

# import the PyQt and QGIS libraries
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from qgis.PyQt.QtWidgets import QDialog
from PyQt5.Qt import *
from PyQt5 import QtSql
from qgis.core import *
from qgis.utils import iface

import sys
import string
import os

from .new_db_dialog import new_db_dialog
from .db_properties_dialog import db_properties_dialog 

# Initialize Qt resources from file resources.py
from .config import *
from .functions import *

from .lib import pglite

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "\\forms")
from manage_db_dialog import Ui_Dialog


class manage_db_dialog(QDialog): 

    def __init__(self, caller, iface):
        QDialog.__init__(self)
        self.ui= Ui_Dialog()
        self.ui.setupUi(self)        
        
        self.caller = caller
        self.db = self.caller.db
        self.iface = self.caller.iface
        
        self.plugin_dir = self.caller.plugin_dir
        self.icon_dir = self.caller.icon_dir
        self.temp_db = QtSql.QSqlDatabase.addDatabase("QPSQL", connectionName="temp_db")
        
        self.ui.pushButtonInformation.setIcon(QIcon(self.icon_dir + "/icon_information.png")) 
        self.ui.pushButtonCreate.setIcon(QIcon(self.icon_dir + "/icon_add_db.png")) 
        self.ui.pushButtonDelete.setIcon(QIcon(self.icon_dir + "/icon_delete.png"))
        self.ui.pushButtonExport.setIcon(QIcon(self.icon_dir + "/icon_export.png"))
        self.ui.pushButtonLoad.setIcon(QIcon(self.icon_dir + "/icon_open.png"))
        self.ui.pushButtonInfo2.setIcon(QIcon(self.icon_dir + "/icon_information.png"))
        
        self.msq=QMessageBox()
        self.msq.setIcon(QMessageBox.Information)
        
        self.new_db_dialog=new_db_dialog(self, self.iface)
        self.new_db_dialog.setModal(True)
                
        self.db_properties_dialog=db_properties_dialog(self, self.iface)
        self.db_properties_dialog.setModal(True)
                
        self.ui.comboBoxDB.setModel(self.caller.modelDB)
        
        # Connexion des signaux et des slots
        self._connectSlots()
    
    def _connectSlots(self):
        self.ui.pushButtonExport.clicked.connect(self._slotPushButtonExportClicked)
        self.ui.pushButtonDelete.clicked.connect(self._slotPushButtonDeleteClicked)
        self.ui.pushButtonLoad.clicked.connect(self._slotPushButtonLoadClicked)
        self.ui.pushButtonCreate.clicked.connect(self._slotPushButtonCreateClicked)
        self.ui.pushButtonInformation.clicked.connect(self._slotPushButtonInformationClicked)
        self.ui.pushButtonInfo2.clicked.connect(self._slotPushButtonInfo2Clicked)
        self.ui.buttonBox.rejected.connect(self._slotClose)
    
    def updateDBConnection(self):
        self.caller.db.setHostName(self.caller.db.hostName())
        self.caller.db.setUserName( self.caller.db.userName() )
        self.caller.db.setPort(int(self.caller.db.port()))
        self.caller.db.setPassword(self.caller.db.password())
        self.caller.db.setDatabaseName( "tempusaccess_"+str(self.ui.comboBoxDB.currentText()) )
        r=self.caller.db.open()
        
        s="SELECT value FROM pgtempus.db_param WHERE name='srid'";
        q = QtSql.QSqlQuery(s, self.caller.db)
        q.next()
        
        self.caller.srid = int(q.value(0))
        
        box = QMessageBox()
        box.setModal(True)
        if r is False:
            box.setText(u"La connexion à la base a échoué : "+self.caller.db.lastError().text()+".")
            box.exec_()
    
    def _slotComboBoxDBIndexChanged(self):
        self.updateDBConnection()
        
    def _slotPushButtonCreateClicked(self):
        self.new_db_dialog.show()
    
    def _slotPushButtonExportClicked(self):
        nom_fichier = QFileDialog.getSaveFileName(caption = "Enregistrer la base de données sous...", directory=self.caller.data_dir, filter = "Backup files (*.backup)")
        
        if nom_fichier:
            cmd = [PGDUMP, "--host", self.caller.db.hostName(), "--port", str(self.caller.db.port()), "--username", self.caller.db.userName(), "--no-password", "--format", "custom", "--encoding", "UTF8", "--no-privileges", "--verbose", "--file", nom_fichier[0], "-d", self.caller.db.databaseName()]
            rc = execute_external_cmd( cmd )
            
            box = QMessageBox()
            box.setModal(True)
            if (rc==0):
                box.setText(u"La sauvegarde s'est terminée avec succès.")
            else:
                box.setText(u"La sauvegarde a échoué.")
            box.exec_()
    
    def _slotPushButtonDeleteClicked(self):
        s="select count(*) from pg_database\
            WHERE datname = 'tempusaccess_"+str(self.ui.comboBoxDB.currentText())+"'";
        q=QtSql.QSqlQuery(unicode(s), self.caller.db)
        q.next()
        
        if (int(q.value(0))>0):
            ret = QMessageBox.question(self, "TempusAccess", u"La base de données 'tempusaccess_"+str(self.ui.comboBoxDB.currentText())+u"' va être supprimée. \n Confirmez-vous cette opération ?", QMessageBox.Ok | QMessageBox.Cancel, QMessageBox.Cancel)
        else:
            ret = QMessageBox.warning(self, "TempusAccess", u"La base de données 'tempusaccess_"+str(self.ui.comboBoxDB.currentText())+u"' n'existe pas.", QMessageBox.Cancel, QMessageBox.Cancel)
        
        if (ret == QMessageBox.Ok):
            
            # Restart database server to be sure deleting "TempusAccess" database will be allowed (avoids still connected applications)
            pglite.stop_cluster()
            pglite.start_cluster()
            
            # Delete database
            cmd = [ DROPDB, "-h", self.caller.db.hostName(), "-U", self.caller.db.userName(), "-p", str(self.caller.db.port()), "tempusaccess_"+str(self.ui.comboBoxDB.currentText()) ]
            rc = execute_external_cmd( cmd )
            
            box = QMessageBox()
            if (rc==0):
                box.setText(u"La suppression s'est terminée avec succès.")
            else:
                box.setText(u"La suppression a échoué.")
            box.exec_()
            
            self.caller.set_db_connection_dialog.refreshDBList()
            self.caller.iface.mapCanvas().refreshAllLayers()
    
    def _slotPushButtonInfo2Clicked(self):
        self.msq.exec_()
    
    def _slotPushButtonInformationClicked(self):
        self.updateDBConnection()
        # Update size statistics
        s="SELECT table_schema, round(sum(pg_total_relation_size(table_schema || '.' || table_name))/(1024*1024),2) AS size\
           FROM information_schema.tables\
           WHERE table_schema IN ('tempus_networks')\
           GROUP BY table_schema"
        q=QtSql.QSqlQuery(unicode(s), self.caller.db)
        q.next()
        self.db_properties_dialog.ui.labelNetworksDataSize.setText(str(q.value(1)) + " Mo")
        
        s="SELECT table_schema, round(sum(pg_total_relation_size(table_schema || '.' || table_name))/(1024*1024), 2) AS size\
           FROM information_schema.tables\
           WHERE table_schema IN ('tempus_results')\
           GROUP BY table_schema"
        q=QtSql.QSqlQuery(unicode(s), self.caller.db)
        q.next()
        self.db_properties_dialog.ui.labelResultsDataSize.setText(str(q.value(1)) + " Mo")
        
        s="SELECT table_schema, round(sum(pg_total_relation_size(table_schema || '.' || table_name))/(1024*1024), 2) AS size\
           FROM information_schema.tables\
           WHERE table_schema IN ('tempus_networks')\
           GROUP BY table_schema"
        q=QtSql.QSqlQuery(unicode(s), self.caller.db)
        q.next()
        self.db_properties_dialog.ui.labelCostsDataSize.setText(str(q.value(1)) + " Mo") 
        
        self.db_properties_dialog.show()
    
    def _slotPushButtonLoadClicked(self):
        self.msq.setText("tempusaccess_"+self.ui.comboBoxDB.currentText())
        self.updateDBConnection()
        
        if self.caller.db.open():
            root = QgsProject.instance().layerTreeRoot()
            
            # Remove old layers
            self.caller.node_group = root.findGroup("Tempus Access")
            root.removeChildNode(self.caller.node_group)
            
            # Create new layers groups to display in the legend interface
            self.caller.node_group = root.insertGroup(0, "Tempus Access")
            self.caller.node_group.setExpanded(True)
            self.caller.node_indicators=self.caller.node_group.insertGroup(1, u"Indicateurs calculés")
            self.caller.node_indicators.setExpanded(False)
            self.caller.node_general = self.caller.node_group.insertGroup(2, u"Typologies et habillages")
            self.caller.node_general.setExpanded(False)
            self.caller.node_networks=self.caller.node_group.insertGroup(3, u"Réseaux de transport")
            self.caller.node_networks.setExpanded(False)
            self.caller.node_PT_networks=self.caller.node_networks.insertGroup(1, u"Transport collectif")
            self.caller.node_PT_networks.setExpanded(False)
            self.caller.node_road_networks=self.caller.node_networks.insertGroup(2, u"Route")
            self.caller.node_road_networks.setExpanded(False)
            self.caller.node_graph=self.caller.node_networks.insertGroup(3, u"Graphe")
            self.caller.node_graph.setExpanded(False)
            self.caller.node_graph.setItemVisibilityChecked(False) 
            # self.caller.node_zoning=self.caller.node_group.insertGroup(4, u"Zonages")
            # self.caller.node_zoning.setExpanded(False)
            
            # Update the map window
            self.caller.loadLayers()
            
            self.ui.pushButtonExport.setEnabled(True)
            self.ui.pushButtonDelete.setEnabled(True)
            self.ui.pushButtonLoad.setEnabled(True)
            self.caller.refreshSourcesModes()
            if (self.caller.modelPTSource.rowCount()>0): 
                self.caller.modelObjType.setQuery("SELECT name, id, def_name FROM pgtempus.base_obj_type ORDER BY id", self.caller.db)
            elif (self.caller.modelRoadSource.rowCount()>0): 
                self.caller.modelObjType.setQuery("SELECT name, id, def_name FROM pgtempus.base_obj_type WHERE needs_pt = False ORDER BY id", self.caller.db)
            self.caller.modelPerType.setQuery("SELECT mod_lib, mod_id FROM pgtempus.modality WHERE var_name = 'per_type' ORDER BY mod_id", self.caller.db)
            self.caller.dockwidget.ui.comboBoxPerType.setCurrentIndex(0)
            self.caller.modelAgreg.setQuery("SELECT name, id, func_name FROM pgtempus.agregate ORDER BY id", self.caller.db)
            self.caller.modelDayType.setQuery("SELECT mod_lib, mod_id FROM pgtempus.modality WHERE var_name = 'day_type' ORDER BY mod_id", self.caller.db)
            self.caller.dockwidget.ui.comboBoxDayType.setCurrentIndex(0)
            self.caller.modelEncoding.setQuery("SELECT mod_lib, mod_id FROM pgtempus.modality WHERE var_name = 'encoding' ORDER BY mod_id", self.caller.db)
            self.caller.modelNode.setQuery("SELECT id FROM tempus_networks.node ORDER BY id", self.caller.db)
            self.caller.modelVehicleParkingRule.setQuery("SELECT name, id FROM pgtempus.vehicle_parking_rule", self.caller.db)
            self.caller.modelPOIType.setQuery("SELECT name, id FROM pgtempus.poi_type ORDER BY id", self.caller.db)
            
            # Already calculated queries model
            self.caller.manage_indicators_dialog.refreshReq()
            self.caller.manage_indicators_dialog._slotComboBoxReqIndexChanged(0)
            
            self.caller.dockwidget.ui.comboBoxObjType.setCurrentIndex(0)
            
            # Update networks and modes
            self.caller.refreshSourcesModes()
            self.caller._slotPushButtonUpdateSourcesModesClicked()
            #self.caller.refreshZoningSources()
            
        else:
            box = QMessageBox()
            box.setModal(True)
            box.setText(u"La connexion à la base a échoué.")
            box.exec_()
        self.hide()
       
    def _slotClose(self):
        self.hide()

    
        