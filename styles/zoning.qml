<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyAlgorithm="0" simplifyDrawingTol="1" simplifyMaxScale="1" hasScaleBasedVisibilityFlag="0" styleCategories="AllStyleCategories" maxScale="0" simplifyDrawingHints="1" simplifyLocal="1" minScale="1e+08" readOnly="0" labelsEnabled="0" version="3.4.5-Madeira">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 type="singleSymbol" forceraster="0" enableorderby="0" symbollevels="0">
    <symbols>
      <symbol type="fill" alpha="0.5" force_rhr="0" clip_to_extent="1" name="0">
        <layer pass="0" enabled="1" class="SimpleFill" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="114,155,111,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="247,247,247,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory penWidth="0" minScaleDenominator="0" scaleDependency="Area" scaleBasedVisibility="0" enabled="0" diagramOrientation="Up" backgroundColor="#ffffff" minimumSize="0" sizeScale="3x:0,0,0,0,0,0" width="15" penColor="#000000" barWidth="5" lineSizeScale="3x:0,0,0,0,0,0" opacity="1" lineSizeType="MM" penAlpha="255" backgroundAlpha="255" height="15" rotationOffset="270" maxScaleDenominator="1e+08" labelPlacementMethod="XHeight" sizeType="MM">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings placement="1" linePlacementFlags="18" priority="0" obstacle="0" showAll="1" zIndex="0" dist="0">
    <properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="original_id">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="name">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="station_id">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="station_na">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="network_id">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="original_i">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="wheelchair">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pt_fare_zo">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="gtfs_locat">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pt_station">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pt_trip_ty">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pt_trip__1">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pt_agencie">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pt_agenc_1">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id" name="" index="0"/>
    <alias field="original_id" name="" index="1"/>
    <alias field="name" name="" index="2"/>
    <alias field="station_id" name="" index="3"/>
    <alias field="station_na" name="" index="4"/>
    <alias field="network_id" name="" index="5"/>
    <alias field="original_i" name="" index="6"/>
    <alias field="wheelchair" name="" index="7"/>
    <alias field="pt_fare_zo" name="" index="8"/>
    <alias field="gtfs_locat" name="" index="9"/>
    <alias field="pt_station" name="" index="10"/>
    <alias field="pt_trip_ty" name="" index="11"/>
    <alias field="pt_trip__1" name="" index="12"/>
    <alias field="pt_agencie" name="" index="13"/>
    <alias field="pt_agenc_1" name="" index="14"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default field="id" expression="" applyOnUpdate="0"/>
    <default field="original_id" expression="" applyOnUpdate="0"/>
    <default field="name" expression="" applyOnUpdate="0"/>
    <default field="station_id" expression="" applyOnUpdate="0"/>
    <default field="station_na" expression="" applyOnUpdate="0"/>
    <default field="network_id" expression="" applyOnUpdate="0"/>
    <default field="original_i" expression="" applyOnUpdate="0"/>
    <default field="wheelchair" expression="" applyOnUpdate="0"/>
    <default field="pt_fare_zo" expression="" applyOnUpdate="0"/>
    <default field="gtfs_locat" expression="" applyOnUpdate="0"/>
    <default field="pt_station" expression="" applyOnUpdate="0"/>
    <default field="pt_trip_ty" expression="" applyOnUpdate="0"/>
    <default field="pt_trip__1" expression="" applyOnUpdate="0"/>
    <default field="pt_agencie" expression="" applyOnUpdate="0"/>
    <default field="pt_agenc_1" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint unique_strength="0" field="id" notnull_strength="0" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" field="original_id" notnull_strength="0" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" field="name" notnull_strength="0" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" field="station_id" notnull_strength="0" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" field="station_na" notnull_strength="0" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" field="network_id" notnull_strength="0" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" field="original_i" notnull_strength="0" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" field="wheelchair" notnull_strength="0" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" field="pt_fare_zo" notnull_strength="0" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" field="gtfs_locat" notnull_strength="0" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" field="pt_station" notnull_strength="0" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" field="pt_trip_ty" notnull_strength="0" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" field="pt_trip__1" notnull_strength="0" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" field="pt_agencie" notnull_strength="0" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" field="pt_agenc_1" notnull_strength="0" constraints="0" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id" exp="" desc=""/>
    <constraint field="original_id" exp="" desc=""/>
    <constraint field="name" exp="" desc=""/>
    <constraint field="station_id" exp="" desc=""/>
    <constraint field="station_na" exp="" desc=""/>
    <constraint field="network_id" exp="" desc=""/>
    <constraint field="original_i" exp="" desc=""/>
    <constraint field="wheelchair" exp="" desc=""/>
    <constraint field="pt_fare_zo" exp="" desc=""/>
    <constraint field="gtfs_locat" exp="" desc=""/>
    <constraint field="pt_station" exp="" desc=""/>
    <constraint field="pt_trip_ty" exp="" desc=""/>
    <constraint field="pt_trip__1" exp="" desc=""/>
    <constraint field="pt_agencie" exp="" desc=""/>
    <constraint field="pt_agenc_1" exp="" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="">
    <columns>
      <column type="field" hidden="0" width="-1" name="id"/>
      <column type="field" hidden="0" width="-1" name="original_id"/>
      <column type="field" hidden="0" width="-1" name="name"/>
      <column type="field" hidden="0" width="-1" name="station_id"/>
      <column type="field" hidden="0" width="-1" name="station_na"/>
      <column type="field" hidden="0" width="-1" name="network_id"/>
      <column type="field" hidden="0" width="-1" name="original_i"/>
      <column type="field" hidden="0" width="-1" name="wheelchair"/>
      <column type="field" hidden="0" width="-1" name="pt_fare_zo"/>
      <column type="field" hidden="0" width="-1" name="gtfs_locat"/>
      <column type="field" hidden="0" width="-1" name="pt_station"/>
      <column type="field" hidden="0" width="-1" name="pt_trip_ty"/>
      <column type="field" hidden="0" width="-1" name="pt_trip__1"/>
      <column type="field" hidden="0" width="-1" name="pt_agencie"/>
      <column type="field" hidden="0" width="-1" name="pt_agenc_1"/>
      <column type="actions" hidden="1" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="gtfs_locat"/>
    <field editable="1" name="id"/>
    <field editable="1" name="name"/>
    <field editable="1" name="network_id"/>
    <field editable="1" name="original_i"/>
    <field editable="1" name="original_id"/>
    <field editable="1" name="pt_agenc_1"/>
    <field editable="1" name="pt_agencie"/>
    <field editable="1" name="pt_fare_zo"/>
    <field editable="1" name="pt_station"/>
    <field editable="1" name="pt_trip__1"/>
    <field editable="1" name="pt_trip_ty"/>
    <field editable="1" name="station_id"/>
    <field editable="1" name="station_na"/>
    <field editable="1" name="wheelchair"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="gtfs_locat"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="name"/>
    <field labelOnTop="0" name="network_id"/>
    <field labelOnTop="0" name="original_i"/>
    <field labelOnTop="0" name="original_id"/>
    <field labelOnTop="0" name="pt_agenc_1"/>
    <field labelOnTop="0" name="pt_agencie"/>
    <field labelOnTop="0" name="pt_fare_zo"/>
    <field labelOnTop="0" name="pt_station"/>
    <field labelOnTop="0" name="pt_trip__1"/>
    <field labelOnTop="0" name="pt_trip_ty"/>
    <field labelOnTop="0" name="station_id"/>
    <field labelOnTop="0" name="station_na"/>
    <field labelOnTop="0" name="wheelchair"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
