<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyDrawingHints="0" simplifyDrawingTol="1" maxScale="0" styleCategories="AllStyleCategories" labelsEnabled="0" readOnly="0" version="3.10.6-A Coruña" minScale="0" simplifyLocal="1" simplifyMaxScale="1" hasScaleBasedVisibilityFlag="0" simplifyAlgorithm="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 forceraster="0" symbollevels="0" enableorderby="0" type="singleSymbol">
    <symbols>
      <symbol clip_to_extent="1" name="0" force_rhr="0" alpha="1" type="marker">
        <layer class="SimpleMarker" enabled="1" pass="0" locked="0">
          <prop v="0" k="angle"/>
          <prop v="114,155,111,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="triangle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0,0,0,255" k="outline_color"/>
          <prop v="no" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="area" k="scale_method"/>
          <prop v="5" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>id</value>
      <value>"id"</value>
      <value>id</value>
    </property>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory backgroundAlpha="255" diagramOrientation="Up" rotationOffset="270" maxScaleDenominator="0" width="15" lineSizeType="MM" labelPlacementMethod="XHeight" height="15" sizeScale="3x:0,0,0,0,0,0" minimumSize="0" backgroundColor="#ffffff" penColor="#000000" penAlpha="255" penWidth="0" scaleBasedVisibility="0" barWidth="5" opacity="1" lineSizeScale="3x:0,0,0,0,0,0" enabled="0" scaleDependency="Area" sizeType="MM" minScaleDenominator="0">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute color="#000000" field="" label=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings dist="0" obstacle="0" linePlacementFlags="18" showAll="1" zIndex="0" placement="0" priority="0">
    <properties>
      <Option type="Map">
        <Option name="name" value="" type="QString"/>
        <Option name="properties"/>
        <Option name="type" value="collection" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="false" type="bool"/>
            <Option name="UseHtml" value="false" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="source_id">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option name="AllowMulti" value="false" type="bool"/>
            <Option name="AllowNull" value="false" type="bool"/>
            <Option name="FilterExpression" value="" type="QString"/>
            <Option name="Key" value="id" type="QString"/>
            <Option name="Layer" value="Sources_de_données_cd87a1dc_45e5_4024_9641_894c2f859004" type="QString"/>
            <Option name="LayerName" value="Sources de données" type="QString"/>
            <Option name="LayerProviderName" value="postgres" type="QString"/>
            <Option name="LayerSource" value="dbname='tempusaccess_corse' host=localhost port=55432 user='aurelie-p.bousquet' key='id' checkPrimaryKeyUnicity='1' table=&quot;pgtempus&quot;.&quot;source&quot; sql=" type="QString"/>
            <Option name="NofColumns" value="1" type="int"/>
            <Option name="OrderByValue" value="false" type="bool"/>
            <Option name="UseCompleter" value="false" type="bool"/>
            <Option name="Value" value="name" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="traffic_rules">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pt_agencies_id">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pt_trip_types_id">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="ID" field="id" index="0"/>
    <alias name="Source" field="source_id" index="1"/>
    <alias name="Règles de circulation" field="traffic_rules" index="2"/>
    <alias name="ID exploitants TC" field="pt_agencies_id" index="3"/>
    <alias name="ID types services TC" field="pt_trip_types_id" index="4"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default applyOnUpdate="0" field="id" expression=""/>
    <default applyOnUpdate="0" field="source_id" expression=""/>
    <default applyOnUpdate="0" field="traffic_rules" expression=""/>
    <default applyOnUpdate="0" field="pt_agencies_id" expression=""/>
    <default applyOnUpdate="0" field="pt_trip_types_id" expression=""/>
  </defaults>
  <constraints>
    <constraint notnull_strength="1" exp_strength="0" field="id" constraints="3" unique_strength="1"/>
    <constraint notnull_strength="0" exp_strength="0" field="source_id" constraints="0" unique_strength="0"/>
    <constraint notnull_strength="0" exp_strength="0" field="traffic_rules" constraints="0" unique_strength="0"/>
    <constraint notnull_strength="0" exp_strength="0" field="pt_agencies_id" constraints="0" unique_strength="0"/>
    <constraint notnull_strength="0" exp_strength="0" field="pt_trip_types_id" constraints="0" unique_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id" exp="" desc=""/>
    <constraint field="source_id" exp="" desc=""/>
    <constraint field="traffic_rules" exp="" desc=""/>
    <constraint field="pt_agencies_id" exp="" desc=""/>
    <constraint field="pt_trip_types_id" exp="" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortExpression="&quot;id&quot;" actionWidgetStyle="dropDown" sortOrder="0">
    <columns>
      <column hidden="1" type="actions" width="-1"/>
      <column hidden="0" name="id" type="field" width="-1"/>
      <column hidden="1" name="pt_agencies_id" type="field" width="111"/>
      <column hidden="1" name="pt_trip_types_id" type="field" width="125"/>
      <column hidden="1" name="source_id" type="field" width="-1"/>
      <column hidden="1" name="traffic_rules" type="field" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="id" editable="1"/>
    <field name="network_id" editable="1"/>
    <field name="pt_agencies_id" editable="1"/>
    <field name="pt_trip_types_id" editable="1"/>
    <field name="source_id" editable="1"/>
    <field name="traffic_rules" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="id" labelOnTop="0"/>
    <field name="network_id" labelOnTop="0"/>
    <field name="pt_agencies_id" labelOnTop="0"/>
    <field name="pt_trip_types_id" labelOnTop="0"/>
    <field name="source_id" labelOnTop="0"/>
    <field name="traffic_rules" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
