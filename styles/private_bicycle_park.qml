<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyDrawingTol="1" maxScale="0" simplifyAlgorithm="0" simplifyLocal="1" minScale="0" labelsEnabled="0" simplifyDrawingHints="0" styleCategories="AllStyleCategories" readOnly="0" version="3.10.6-A Coruña" hasScaleBasedVisibilityFlag="0" simplifyMaxScale="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 forceraster="0" type="singleSymbol" enableorderby="0" symbollevels="0">
    <symbols>
      <symbol force_rhr="0" clip_to_extent="1" name="0" type="marker" alpha="1">
        <layer enabled="1" locked="0" pass="0" class="FontMarker">
          <prop v="0" k="angle"/>
          <prop v="V" k="chr"/>
          <prop v="31,120,180,255" k="color"/>
          <prop v="Dingbats" k="font"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="miter" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="4" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>id</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory lineSizeScale="3x:0,0,0,0,0,0" minScaleDenominator="0" width="15" rotationOffset="270" barWidth="5" penWidth="0" scaleDependency="Area" labelPlacementMethod="XHeight" scaleBasedVisibility="0" opacity="1" height="15" minimumSize="0" lineSizeType="MM" enabled="0" penColor="#000000" backgroundAlpha="255" maxScaleDenominator="0" sizeScale="3x:0,0,0,0,0,0" penAlpha="255" sizeType="MM" backgroundColor="#ffffff" diagramOrientation="Up">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute label="" field="" color="#000000"/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings linePlacementFlags="18" showAll="1" dist="0" priority="0" obstacle="0" placement="0" zIndex="0">
    <properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties"/>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="network_id">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" name="AllowMulti" type="bool"/>
            <Option value="false" name="AllowNull" type="bool"/>
            <Option value="" name="FilterExpression" type="QString"/>
            <Option value="id" name="Key" type="QString"/>
            <Option value="Réseaux_98f98e9e_343a_4b4e_a213_0089db022a06" name="Layer" type="QString"/>
            <Option value="Réseaux" name="LayerName" type="QString"/>
            <Option value="1" name="NofColumns" type="int"/>
            <Option value="false" name="OrderByValue" type="bool"/>
            <Option value="false" name="UseCompleter" type="bool"/>
            <Option value="name" name="Value" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="name">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="poi_type">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="3" name="Parking voitures" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="4" name="Parking vélos" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="5" name="Station location voiture partagée" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="6" name="Station location vélo partagé" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="vehicle_parking_rule">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" name="AllowMulti" type="bool"/>
            <Option value="false" name="AllowNull" type="bool"/>
            <Option value="" name="FilterExpression" type="QString"/>
            <Option value="id" name="Key" type="QString"/>
            <Option value="Règles_de_stationnement_8bcae15e_64f6_40fe_8720_8517d1e47d85" name="Layer" type="QString"/>
            <Option value="Règles de stationnement" name="LayerName" type="QString"/>
            <Option value="1" name="NofColumns" type="int"/>
            <Option value="false" name="OrderByValue" type="bool"/>
            <Option value="false" name="UseCompleter" type="bool"/>
            <Option value="name" name="Value" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="node_id">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" name="AllowNull" type="bool"/>
            <Option value="2147483647" name="Max" type="int"/>
            <Option value="-2147483648" name="Min" type="int"/>
            <Option value="0" name="Precision" type="int"/>
            <Option value="1" name="Step" type="int"/>
            <Option value="SpinBox" name="Style" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="leaving_time">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="HH:mm:ss" name="display_format" type="QString"/>
            <Option value="HH:mm:ss" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="leaving_fare">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="taking_time">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="HH:mm:ss" name="display_format" type="QString"/>
            <Option value="HH:mm:ss" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="taking_fare">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="ID" field="id"/>
    <alias index="1" name="Réseau" field="network_id"/>
    <alias index="2" name="Nom" field="name"/>
    <alias index="3" name="Type de POI" field="poi_type"/>
    <alias index="4" name="Règle stationnement" field="vehicle_parking_rule"/>
    <alias index="5" name="ID noeud" field="node_id"/>
    <alias index="6" name="Temps stationnement" field="leaving_time"/>
    <alias index="7" name="Tarif stationnement" field="leaving_fare"/>
    <alias index="8" name="Temps prise en charge" field="taking_time"/>
    <alias index="9" name="Tarif prise en charge" field="taking_fare"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" field="id" applyOnUpdate="0"/>
    <default expression="" field="network_id" applyOnUpdate="0"/>
    <default expression="" field="name" applyOnUpdate="0"/>
    <default expression="" field="poi_type" applyOnUpdate="0"/>
    <default expression="" field="vehicle_parking_rule" applyOnUpdate="0"/>
    <default expression="" field="node_id" applyOnUpdate="0"/>
    <default expression="" field="leaving_time" applyOnUpdate="0"/>
    <default expression="" field="leaving_fare" applyOnUpdate="0"/>
    <default expression="" field="taking_time" applyOnUpdate="0"/>
    <default expression="" field="taking_fare" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint field="id" notnull_strength="1" constraints="3" unique_strength="1" exp_strength="0"/>
    <constraint field="network_id" notnull_strength="0" constraints="0" unique_strength="0" exp_strength="0"/>
    <constraint field="name" notnull_strength="0" constraints="0" unique_strength="0" exp_strength="0"/>
    <constraint field="poi_type" notnull_strength="0" constraints="0" unique_strength="0" exp_strength="0"/>
    <constraint field="vehicle_parking_rule" notnull_strength="0" constraints="0" unique_strength="0" exp_strength="0"/>
    <constraint field="node_id" notnull_strength="0" constraints="0" unique_strength="0" exp_strength="0"/>
    <constraint field="leaving_time" notnull_strength="0" constraints="0" unique_strength="0" exp_strength="0"/>
    <constraint field="leaving_fare" notnull_strength="0" constraints="0" unique_strength="0" exp_strength="0"/>
    <constraint field="taking_time" notnull_strength="0" constraints="0" unique_strength="0" exp_strength="0"/>
    <constraint field="taking_fare" notnull_strength="0" constraints="0" unique_strength="0" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="id" exp=""/>
    <constraint desc="" field="network_id" exp=""/>
    <constraint desc="" field="name" exp=""/>
    <constraint desc="" field="poi_type" exp=""/>
    <constraint desc="" field="vehicle_parking_rule" exp=""/>
    <constraint desc="" field="node_id" exp=""/>
    <constraint desc="" field="leaving_time" exp=""/>
    <constraint desc="" field="leaving_fare" exp=""/>
    <constraint desc="" field="taking_time" exp=""/>
    <constraint desc="" field="taking_fare" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="" actionWidgetStyle="dropDown" sortOrder="0">
    <columns>
      <column name="id" type="field" width="-1" hidden="0"/>
      <column name="node_id" type="field" width="100" hidden="0"/>
      <column name="network_id" type="field" width="-1" hidden="0"/>
      <column name="poi_type" type="field" width="130" hidden="0"/>
      <column name="name" type="field" width="174" hidden="0"/>
      <column name="vehicle_parking_rule" type="field" width="144" hidden="0"/>
      <column name="leaving_fare" type="field" width="159" hidden="0"/>
      <column name="taking_fare" type="field" width="194" hidden="0"/>
      <column type="actions" width="-1" hidden="1"/>
      <column name="leaving_time" type="field" width="154" hidden="0"/>
      <column name="taking_time" type="field" width="166" hidden="0"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="id" editable="1"/>
    <field name="leaving_fare" editable="1"/>
    <field name="leaving_time" editable="1"/>
    <field name="leaving_time_penalty" editable="1"/>
    <field name="name" editable="1"/>
    <field name="network_id" editable="1"/>
    <field name="node_id" editable="1"/>
    <field name="poi_type" editable="1"/>
    <field name="taking_fare" editable="1"/>
    <field name="taking_time" editable="1"/>
    <field name="taking_time_penalty" editable="1"/>
    <field name="transport_mode_id" editable="1"/>
    <field name="vehicle_parking_rule" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="id" labelOnTop="0"/>
    <field name="leaving_fare" labelOnTop="0"/>
    <field name="leaving_time" labelOnTop="0"/>
    <field name="leaving_time_penalty" labelOnTop="0"/>
    <field name="name" labelOnTop="0"/>
    <field name="network_id" labelOnTop="0"/>
    <field name="node_id" labelOnTop="0"/>
    <field name="poi_type" labelOnTop="0"/>
    <field name="taking_fare" labelOnTop="0"/>
    <field name="taking_time" labelOnTop="0"/>
    <field name="taking_time_penalty" labelOnTop="0"/>
    <field name="transport_mode_id" labelOnTop="0"/>
    <field name="vehicle_parking_rule" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
