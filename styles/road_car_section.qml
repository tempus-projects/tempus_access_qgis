<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyDrawingHints="1" simplifyLocal="1" version="3.10.6-A Coruña" simplifyDrawingTol="1" hasScaleBasedVisibilityFlag="0" simplifyAlgorithm="0" readOnly="0" minScale="1e+08" styleCategories="AllStyleCategories" maxScale="0" simplifyMaxScale="1" labelsEnabled="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 symbollevels="0" enableorderby="0" forceraster="0" type="RuleRenderer">
    <rules key="{808d0135-4f27-4436-ab4f-b322ba0959da}">
      <rule label="Section interdite aux piétons et vélos" key="{bf6c5382-7d5a-4a8e-b21f-0497d576eb4f}" filter="section_type_id>1 AND NOT (array_contains(traffic_rules, 1) OR array_contains(traffic_rules, 2))" symbol="0"/>
      <rule label="Section mixte" key="{9401d020-5085-4ddc-a689-a30d1043df30}" filter="section_type_id>1 AND (array_contains(traffic_rules, 1) OR array_contains(traffic_rules, 2))" symbol="1"/>
      <rule label="Réseau virtuel" key="{8f7e1764-f0e9-4b5f-8e28-ec0370c019b9}" filter="section_type_id=1" symbol="2"/>
      <rule label="Autres" key="{8796bde7-b417-4201-b9a2-a39818b44059}" filter="ELSE" symbol="3"/>
    </rules>
    <symbols>
      <symbol alpha="1" force_rhr="0" name="0" clip_to_extent="1" type="line">
        <layer locked="0" class="SimpleLine" pass="0" enabled="1">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="31,120,180,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="1.06"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="5.55112e-17"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer locked="0" class="MarkerLine" pass="0" enabled="1">
          <prop k="average_angle_length" v="4.2"/>
          <prop k="average_angle_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="average_angle_unit" v="MM"/>
          <prop k="interval" v="3"/>
          <prop k="interval_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="interval_unit" v="MM"/>
          <prop k="offset" v="-5.55112e-17"/>
          <prop k="offset_along_line" v="0"/>
          <prop k="offset_along_line_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_along_line_unit" v="MM"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="placement" v="centralpoint"/>
          <prop k="ring_filter" v="0"/>
          <prop k="rotate" v="0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
          <symbol alpha="1" force_rhr="0" name="@0@1" clip_to_extent="1" type="marker">
            <layer locked="0" class="SimpleMarker" pass="0" enabled="1">
              <prop k="angle" v="0"/>
              <prop k="color" v="102,103,105,255"/>
              <prop k="horizontal_anchor_point" v="1"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="name" v="filled_arrowhead"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="31,120,180,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="scale_method" v="diameter"/>
              <prop k="size" v="2.2"/>
              <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="size_unit" v="MM"/>
              <prop k="vertical_anchor_point" v="1"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" type="QString" value=""/>
                  <Option name="properties"/>
                  <Option name="type" type="QString" value="collection"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol alpha="1" force_rhr="0" name="1" clip_to_extent="1" type="line">
        <layer locked="0" class="SimpleLine" pass="0" enabled="1">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,127,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.6"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="-5.55112e-17"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer locked="0" class="MarkerLine" pass="0" enabled="1">
          <prop k="average_angle_length" v="4"/>
          <prop k="average_angle_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="average_angle_unit" v="MM"/>
          <prop k="interval" v="3"/>
          <prop k="interval_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="interval_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_along_line" v="0"/>
          <prop k="offset_along_line_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_along_line_unit" v="MM"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="placement" v="centralpoint"/>
          <prop k="ring_filter" v="0"/>
          <prop k="rotate" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
          <symbol alpha="1" force_rhr="0" name="@1@1" clip_to_extent="1" type="marker">
            <layer locked="0" class="SimpleMarker" pass="0" enabled="1">
              <prop k="angle" v="0"/>
              <prop k="color" v="255,127,0,255"/>
              <prop k="horizontal_anchor_point" v="1"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="name" v="filled_arrowhead"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="255,127,0,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="scale_method" v="diameter"/>
              <prop k="size" v="2"/>
              <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="size_unit" v="MM"/>
              <prop k="vertical_anchor_point" v="1"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" type="QString" value=""/>
                  <Option name="properties"/>
                  <Option name="type" type="QString" value="collection"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol alpha="1" force_rhr="0" name="2" clip_to_extent="1" type="line">
        <layer locked="0" class="MarkerLine" pass="0" enabled="1">
          <prop k="average_angle_length" v="4"/>
          <prop k="average_angle_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="average_angle_unit" v="MM"/>
          <prop k="interval" v="1"/>
          <prop k="interval_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="interval_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_along_line" v="0"/>
          <prop k="offset_along_line_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_along_line_unit" v="MM"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="placement" v="centralpoint"/>
          <prop k="ring_filter" v="0"/>
          <prop k="rotate" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
          <symbol alpha="1" force_rhr="0" name="@2@0" clip_to_extent="1" type="marker">
            <layer locked="0" class="SimpleMarker" pass="0" enabled="1">
              <prop k="angle" v="0"/>
              <prop k="color" v="255,0,0,255"/>
              <prop k="horizontal_anchor_point" v="1"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="name" v="arrowhead"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="scale_method" v="diameter"/>
              <prop k="size" v="1"/>
              <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="size_unit" v="MM"/>
              <prop k="vertical_anchor_point" v="1"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" type="QString" value=""/>
                  <Option name="properties"/>
                  <Option name="type" type="QString" value="collection"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" class="SimpleLine" pass="0" enabled="1">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="102,103,105,255"/>
          <prop k="line_style" v="dot"/>
          <prop k="line_width" v="0.26"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" force_rhr="0" name="3" clip_to_extent="1" type="line">
        <layer locked="0" class="SimpleLine" pass="0" enabled="1">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,158,23,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.26"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer locked="0" class="MarkerLine" pass="0" enabled="1">
          <prop k="average_angle_length" v="4"/>
          <prop k="average_angle_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="average_angle_unit" v="MM"/>
          <prop k="interval" v="3"/>
          <prop k="interval_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="interval_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_along_line" v="0"/>
          <prop k="offset_along_line_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_along_line_unit" v="MM"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="placement" v="centralpoint"/>
          <prop k="ring_filter" v="0"/>
          <prop k="rotate" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
          <symbol alpha="1" force_rhr="0" name="@3@1" clip_to_extent="1" type="marker">
            <layer locked="0" class="SimpleMarker" pass="0" enabled="1">
              <prop k="angle" v="0"/>
              <prop k="color" v="255,158,23,255"/>
              <prop k="horizontal_anchor_point" v="1"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="name" v="filled_arrowhead"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="255,158,23,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="scale_method" v="diameter"/>
              <prop k="size" v="2"/>
              <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="size_unit" v="MM"/>
              <prop k="vertical_anchor_point" v="1"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" type="QString" value=""/>
                  <Option name="properties"/>
                  <Option name="type" type="QString" value="collection"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <customproperties>
    <property key="dualview/previewExpressions" value="COALESCE( &quot;road_name&quot;, '&lt;NULL>' )"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory height="15" rotationOffset="270" sizeType="MM" penWidth="0" scaleBasedVisibility="0" diagramOrientation="Up" minScaleDenominator="0" maxScaleDenominator="1e+08" minimumSize="0" sizeScale="3x:0,0,0,0,0,0" backgroundAlpha="255" penColor="#000000" opacity="1" barWidth="5" backgroundColor="#ffffff" lineSizeScale="3x:0,0,0,0,0,0" lineSizeType="MM" labelPlacementMethod="XHeight" width="15" scaleDependency="Area" enabled="0" penAlpha="255">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute label="" color="#000000" field=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings placement="2" linePlacementFlags="2" showAll="1" zIndex="0" obstacle="0" dist="0" priority="0">
    <properties>
      <Option type="Map">
        <Option name="name" type="QString" value=""/>
        <Option name="properties"/>
        <Option name="type" type="QString" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="sources_id">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="node_from_id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="node_to_id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="diffusion">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option name="Pas de diffusion" type="QString" value="0"/>
              </Option>
              <Option type="Map">
                <Option name="Sur la droite seulement" type="QString" value="1"/>
              </Option>
              <Option type="Map">
                <Option name="Sur la gauche seulement" type="QString" value="2"/>
              </Option>
              <Option type="Map">
                <Option name="Des 2 côtés" type="QString" value="3"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="crossability">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option name="Depuis les 2 côtés" type="QString" value="0"/>
              </Option>
              <Option type="Map">
                <Option name="De gauche à droite" type="QString" value="1"/>
              </Option>
              <Option type="Map">
                <Option name="De droite à gauche" type="QString" value="2"/>
              </Option>
              <Option type="Map">
                <Option name="Non traversable" type="QString" value="3"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="section_type_id">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option name="AllowMulti" type="bool" value="false"/>
            <Option name="AllowNull" type="bool" value="false"/>
            <Option name="FilterExpression" type="QString" value=""/>
            <Option name="Key" type="QString" value="id"/>
            <Option name="Layer" type="QString" value="Types_sections_de_route_d794139b_29de_490f_8567_21ffc3e7a9ea"/>
            <Option name="LayerName" type="QString" value="Types sections de route"/>
            <Option name="NofColumns" type="int" value="1"/>
            <Option name="OrderByValue" type="bool" value="false"/>
            <Option name="UseCompleter" type="bool" value="false"/>
            <Option name="Value" type="QString" value="name"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="length">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="traffic_rules">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="car_speed_limit">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="street_name">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="road_number">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="lanes">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option name="AllowNull" type="bool" value="true"/>
            <Option name="Max" type="int" value="2147483647"/>
            <Option name="Min" type="int" value="-2147483648"/>
            <Option name="Precision" type="int" value="0"/>
            <Option name="Step" type="int" value="1"/>
            <Option name="Style" type="QString" value="SpinBox"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="total_lanes">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option name="AllowNull" type="bool" value="true"/>
            <Option name="Max" type="int" value="2147483647"/>
            <Option name="Min" type="int" value="-2147483648"/>
            <Option name="Precision" type="int" value="0"/>
            <Option name="Step" type="int" value="1"/>
            <Option name="Style" type="QString" value="SpinBox"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="max_slope">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="min_width">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="car_speed">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="ID" field="id"/>
    <alias index="1" name="" field="sources_id"/>
    <alias index="2" name="ID noeud initial" field="node_from_id"/>
    <alias index="3" name="ID noeud final" field="node_to_id"/>
    <alias index="4" name="Diffusion" field="diffusion"/>
    <alias index="5" name="Traversabilité" field="crossability"/>
    <alias index="6" name="Type section" field="section_type_id"/>
    <alias index="7" name="Longueur (m)" field="length"/>
    <alias index="8" name="Règles circulation" field="traffic_rules"/>
    <alias index="9" name="Vitesse max autorisée (km/h)" field="car_speed_limit"/>
    <alias index="10" name="Nom rue" field="street_name"/>
    <alias index="11" name="Numéro route" field="road_number"/>
    <alias index="12" name="Nb voies" field="lanes"/>
    <alias index="13" name="Nb voie 2 sens" field="total_lanes"/>
    <alias index="14" name="Pente max (%)" field="max_slope"/>
    <alias index="15" name="Largeur min (m)" field="min_width"/>
    <alias index="16" name="Vitesse voitures (km/h)" field="car_speed"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" applyOnUpdate="0" field="id"/>
    <default expression="" applyOnUpdate="0" field="sources_id"/>
    <default expression="" applyOnUpdate="0" field="node_from_id"/>
    <default expression="" applyOnUpdate="0" field="node_to_id"/>
    <default expression="" applyOnUpdate="0" field="diffusion"/>
    <default expression="" applyOnUpdate="0" field="crossability"/>
    <default expression="" applyOnUpdate="0" field="section_type_id"/>
    <default expression="" applyOnUpdate="0" field="length"/>
    <default expression="" applyOnUpdate="0" field="traffic_rules"/>
    <default expression="" applyOnUpdate="0" field="car_speed_limit"/>
    <default expression="" applyOnUpdate="0" field="street_name"/>
    <default expression="" applyOnUpdate="0" field="road_number"/>
    <default expression="" applyOnUpdate="0" field="lanes"/>
    <default expression="" applyOnUpdate="0" field="total_lanes"/>
    <default expression="" applyOnUpdate="0" field="max_slope"/>
    <default expression="" applyOnUpdate="0" field="min_width"/>
    <default expression="" applyOnUpdate="0" field="car_speed"/>
  </defaults>
  <constraints>
    <constraint unique_strength="1" notnull_strength="1" exp_strength="0" constraints="3" field="id"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="sources_id"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="node_from_id"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="node_to_id"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="diffusion"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="crossability"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="section_type_id"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="length"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="traffic_rules"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="car_speed_limit"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="street_name"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="road_number"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="lanes"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="total_lanes"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="max_slope"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="min_width"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="car_speed"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="id"/>
    <constraint desc="" exp="" field="sources_id"/>
    <constraint desc="" exp="" field="node_from_id"/>
    <constraint desc="" exp="" field="node_to_id"/>
    <constraint desc="" exp="" field="diffusion"/>
    <constraint desc="" exp="" field="crossability"/>
    <constraint desc="" exp="" field="section_type_id"/>
    <constraint desc="" exp="" field="length"/>
    <constraint desc="" exp="" field="traffic_rules"/>
    <constraint desc="" exp="" field="car_speed_limit"/>
    <constraint desc="" exp="" field="street_name"/>
    <constraint desc="" exp="" field="road_number"/>
    <constraint desc="" exp="" field="lanes"/>
    <constraint desc="" exp="" field="total_lanes"/>
    <constraint desc="" exp="" field="max_slope"/>
    <constraint desc="" exp="" field="min_width"/>
    <constraint desc="" exp="" field="car_speed"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortExpression="&quot;section_type_id&quot;" actionWidgetStyle="dropDown" sortOrder="0">
    <columns>
      <column name="id" type="field" hidden="0" width="47"/>
      <column name="node_from_id" type="field" hidden="0" width="90"/>
      <column name="node_to_id" type="field" hidden="0" width="88"/>
      <column name="section_type_id" type="field" hidden="0" width="122"/>
      <column name="length" type="field" hidden="0" width="82"/>
      <column name="traffic_rules" type="field" hidden="0" width="100"/>
      <column name="car_speed_limit" type="field" hidden="0" width="159"/>
      <column name="car_speed" type="field" hidden="0" width="115"/>
      <column name="street_name" type="field" hidden="0" width="271"/>
      <column name="road_number" type="field" hidden="0" width="82"/>
      <column name="lanes" type="field" hidden="0" width="57"/>
      <column name="total_lanes" type="field" hidden="0" width="82"/>
      <column name="max_slope" type="field" hidden="0" width="83"/>
      <column name="min_width" type="field" hidden="0" width="93"/>
      <column name="diffusion" type="field" hidden="0" width="143"/>
      <column name="crossability" type="field" hidden="0" width="118"/>
      <column type="actions" hidden="1" width="-1"/>
      <column name="sources_id" type="field" hidden="0" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles>
      <style rule="road_type = 5" text_color_alpha="255" text_color="#4a8c1d" name="">
        <font description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      </style>
    </rowstyles>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="car_speed"/>
    <field editable="1" name="car_speed_limit"/>
    <field editable="1" name="crossability"/>
    <field editable="1" name="diffusion"/>
    <field editable="1" name="edge_type_desc"/>
    <field editable="1" name="edge_type_id"/>
    <field editable="1" name="id"/>
    <field editable="1" name="lanes"/>
    <field editable="1" name="length"/>
    <field editable="1" name="max_slope"/>
    <field editable="1" name="min_width"/>
    <field editable="1" name="network_id"/>
    <field editable="1" name="network_name"/>
    <field editable="1" name="node_from_id"/>
    <field editable="1" name="node_to_id"/>
    <field editable="1" name="road_number"/>
    <field editable="1" name="section_type_id"/>
    <field editable="1" name="source_id"/>
    <field editable="1" name="sources_id"/>
    <field editable="1" name="street_name"/>
    <field editable="1" name="total_lanes"/>
    <field editable="1" name="traffic_rules"/>
    <field editable="1" name="traffic_rules_id"/>
    <field editable="1" name="traffic_rules_name"/>
    <field editable="1" name="traffic_rules_names"/>
    <field editable="1" name="traffic_rules_str"/>
    <field editable="1" name="transport_modes_id"/>
    <field editable="1" name="transport_modes_name"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="car_speed"/>
    <field labelOnTop="0" name="car_speed_limit"/>
    <field labelOnTop="0" name="crossability"/>
    <field labelOnTop="0" name="diffusion"/>
    <field labelOnTop="0" name="edge_type_desc"/>
    <field labelOnTop="0" name="edge_type_id"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="lanes"/>
    <field labelOnTop="0" name="length"/>
    <field labelOnTop="0" name="max_slope"/>
    <field labelOnTop="0" name="min_width"/>
    <field labelOnTop="0" name="network_id"/>
    <field labelOnTop="0" name="network_name"/>
    <field labelOnTop="0" name="node_from_id"/>
    <field labelOnTop="0" name="node_to_id"/>
    <field labelOnTop="0" name="road_number"/>
    <field labelOnTop="0" name="section_type_id"/>
    <field labelOnTop="0" name="source_id"/>
    <field labelOnTop="0" name="sources_id"/>
    <field labelOnTop="0" name="street_name"/>
    <field labelOnTop="0" name="total_lanes"/>
    <field labelOnTop="0" name="traffic_rules"/>
    <field labelOnTop="0" name="traffic_rules_id"/>
    <field labelOnTop="0" name="traffic_rules_name"/>
    <field labelOnTop="0" name="traffic_rules_names"/>
    <field labelOnTop="0" name="traffic_rules_str"/>
    <field labelOnTop="0" name="transport_modes_id"/>
    <field labelOnTop="0" name="transport_modes_name"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>COALESCE( "road_name", '&lt;NULL>' )</previewExpression>
  <mapTip>road_name</mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
