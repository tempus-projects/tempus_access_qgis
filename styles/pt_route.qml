<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="1e+08" readOnly="0" version="3.10.6-A Coruña" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0" maxScale="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>short_name</value>
      <value>"short_name"</value>
      <value>short_name</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowNull" value="true"/>
            <Option type="int" name="Max" value="2147483647"/>
            <Option type="int" name="Min" value="-2147483648"/>
            <Option type="int" name="Precision" value="0"/>
            <Option type="int" name="Step" value="1"/>
            <Option type="QString" name="Style" value="SpinBox"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="network_id">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowMulti" value="false"/>
            <Option type="bool" name="AllowNull" value="false"/>
            <Option type="QString" name="FilterExpression" value=""/>
            <Option type="QString" name="Key" value="id"/>
            <Option type="QString" name="Layer" value="Réseaux_38f53e0c_4185_4993_8685_003b060fc231"/>
            <Option type="QString" name="LayerName" value="Réseaux"/>
            <Option type="int" name="NofColumns" value="1"/>
            <Option type="bool" name="OrderByValue" value="false"/>
            <Option type="bool" name="UseCompleter" value="false"/>
            <Option type="QString" name="Value" value="name"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="original_id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="short_name">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="long_name">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pt_agency_id">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowMulti" value="false"/>
            <Option type="bool" name="AllowNull" value="false"/>
            <Option type="QString" name="FilterExpression" value=""/>
            <Option type="QString" name="Key" value="id"/>
            <Option type="QString" name="Layer" value="Exploitants_b4225857_395d_462f_ba8b_4042c2fbbcd3"/>
            <Option type="QString" name="LayerName" value="Exploitants"/>
            <Option type="int" name="NofColumns" value="1"/>
            <Option type="bool" name="OrderByValue" value="false"/>
            <Option type="bool" name="UseCompleter" value="false"/>
            <Option type="QString" name="Value" value="name"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pt_trip_types_id">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pt_trip_types_name">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="ID" index="0" field="id"/>
    <alias name="Réseau" index="1" field="network_id"/>
    <alias name="ID initial" index="2" field="original_id"/>
    <alias name="Nom court" index="3" field="short_name"/>
    <alias name="Nom long" index="4" field="long_name"/>
    <alias name="Exploitant" index="5" field="pt_agency_id"/>
    <alias name="ID types de services" index="6" field="pt_trip_types_id"/>
    <alias name="Noms types de services" index="7" field="pt_trip_types_name"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default applyOnUpdate="0" field="id" expression=""/>
    <default applyOnUpdate="0" field="network_id" expression=""/>
    <default applyOnUpdate="0" field="original_id" expression=""/>
    <default applyOnUpdate="0" field="short_name" expression=""/>
    <default applyOnUpdate="0" field="long_name" expression=""/>
    <default applyOnUpdate="0" field="pt_agency_id" expression=""/>
    <default applyOnUpdate="0" field="pt_trip_types_id" expression=""/>
    <default applyOnUpdate="0" field="pt_trip_types_name" expression=""/>
  </defaults>
  <constraints>
    <constraint constraints="3" notnull_strength="1" unique_strength="1" field="id" exp_strength="0"/>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" field="network_id" exp_strength="0"/>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" field="original_id" exp_strength="0"/>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" field="short_name" exp_strength="0"/>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" field="long_name" exp_strength="0"/>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" field="pt_agency_id" exp_strength="0"/>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" field="pt_trip_types_id" exp_strength="0"/>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" field="pt_trip_types_name" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="id"/>
    <constraint desc="" exp="" field="network_id"/>
    <constraint desc="" exp="" field="original_id"/>
    <constraint desc="" exp="" field="short_name"/>
    <constraint desc="" exp="" field="long_name"/>
    <constraint desc="" exp="" field="pt_agency_id"/>
    <constraint desc="" exp="" field="pt_trip_types_id"/>
    <constraint desc="" exp="" field="pt_trip_types_name"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortOrder="0" sortExpression="&quot;id&quot;" actionWidgetStyle="dropDown">
    <columns>
      <column type="field" name="id" width="52" hidden="0"/>
      <column type="field" name="network_id" width="90" hidden="0"/>
      <column type="field" name="original_id" width="74" hidden="1"/>
      <column type="field" name="short_name" width="-1" hidden="0"/>
      <column type="field" name="long_name" width="239" hidden="0"/>
      <column type="field" name="pt_agency_id" width="205" hidden="0"/>
      <column type="field" name="pt_trip_types_id" width="-1" hidden="1"/>
      <column type="field" name="pt_trip_types_name" width="159" hidden="0"/>
      <column type="actions" width="-1" hidden="1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="id" editable="1"/>
    <field name="long_name" editable="1"/>
    <field name="network_id" editable="1"/>
    <field name="original_id" editable="1"/>
    <field name="pt_agency_id" editable="1"/>
    <field name="pt_trip_types_id" editable="1"/>
    <field name="pt_trip_types_name" editable="1"/>
    <field name="short_name" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="id" labelOnTop="0"/>
    <field name="long_name" labelOnTop="0"/>
    <field name="network_id" labelOnTop="0"/>
    <field name="original_id" labelOnTop="0"/>
    <field name="pt_agency_id" labelOnTop="0"/>
    <field name="pt_trip_types_id" labelOnTop="0"/>
    <field name="pt_trip_types_name" labelOnTop="0"/>
    <field name="short_name" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>short_name</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>4</layerGeometryType>
</qgis>
