<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.10.6-A Coruña" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0" minScale="1e+08" readOnly="0" maxScale="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>name</value>
    </property>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="category">
      <editWidget type="Enumeration">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="name">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="traffic_rule">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowMulti" value="false"/>
            <Option type="bool" name="AllowNull" value="false"/>
            <Option type="QString" name="FilterExpression" value=""/>
            <Option type="QString" name="Key" value="id"/>
            <Option type="QString" name="Layer" value="Règles_de_circulation_0788b516_78fd_400a_8b15_0f1c97758b93"/>
            <Option type="QString" name="LayerName" value="Règles de circulation"/>
            <Option type="int" name="NofColumns" value="1"/>
            <Option type="bool" name="OrderByValue" value="false"/>
            <Option type="bool" name="UseCompleter" value="false"/>
            <Option type="QString" name="Value" value="name"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="speed_rule">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowMulti" value="false"/>
            <Option type="bool" name="AllowNull" value="false"/>
            <Option type="QString" name="FilterExpression" value=""/>
            <Option type="QString" name="Key" value="id"/>
            <Option type="QString" name="Layer" value="Règles_de_vitesse_611393e8_3080_4de8_8118_ff3b874f24eb"/>
            <Option type="QString" name="LayerName" value="Règles de vitesse"/>
            <Option type="int" name="NofColumns" value="1"/>
            <Option type="bool" name="OrderByValue" value="false"/>
            <Option type="bool" name="UseCompleter" value="false"/>
            <Option type="QString" name="Value" value="name"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="toll_rule">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowMulti" value="false"/>
            <Option type="bool" name="AllowNull" value="false"/>
            <Option type="QString" name="FilterExpression" value=""/>
            <Option type="QString" name="Key" value="id"/>
            <Option type="QString" name="Layer" value="Règles_de_péage_c8885763_12ef_47f4_aded_411eb67e600b"/>
            <Option type="QString" name="LayerName" value="Règles de péage"/>
            <Option type="int" name="NofColumns" value="1"/>
            <Option type="bool" name="OrderByValue" value="false"/>
            <Option type="bool" name="UseCompleter" value="false"/>
            <Option type="QString" name="Value" value="name"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="vehicle_parking_rule">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowMulti" value="false"/>
            <Option type="bool" name="AllowNull" value="false"/>
            <Option type="QString" name="FilterExpression" value=""/>
            <Option type="QString" name="Key" value="id"/>
            <Option type="QString" name="Layer" value="Règles_de_stationnement_d4f48b36_5888_4ae8_8c6d_7b59a934a82b"/>
            <Option type="QString" name="LayerName" value="Règles de stationnement"/>
            <Option type="int" name="NofColumns" value="1"/>
            <Option type="bool" name="OrderByValue" value="false"/>
            <Option type="bool" name="UseCompleter" value="false"/>
            <Option type="QString" name="Value" value="name"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="ID" field="id"/>
    <alias index="1" name="Catégorie" field="category"/>
    <alias index="2" name="Nom" field="name"/>
    <alias index="3" name="Règle de circulation" field="traffic_rule"/>
    <alias index="4" name="Règle de vitesse" field="speed_rule"/>
    <alias index="5" name="Règle de péage" field="toll_rule"/>
    <alias index="6" name="Règle de stationnement" field="vehicle_parking_rule"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default applyOnUpdate="0" field="id" expression=""/>
    <default applyOnUpdate="0" field="category" expression=""/>
    <default applyOnUpdate="0" field="name" expression=""/>
    <default applyOnUpdate="0" field="traffic_rule" expression=""/>
    <default applyOnUpdate="0" field="speed_rule" expression=""/>
    <default applyOnUpdate="0" field="toll_rule" expression=""/>
    <default applyOnUpdate="0" field="vehicle_parking_rule" expression=""/>
  </defaults>
  <constraints>
    <constraint notnull_strength="1" constraints="3" unique_strength="1" field="id" exp_strength="0"/>
    <constraint notnull_strength="0" constraints="0" unique_strength="0" field="category" exp_strength="0"/>
    <constraint notnull_strength="0" constraints="0" unique_strength="0" field="name" exp_strength="0"/>
    <constraint notnull_strength="0" constraints="0" unique_strength="0" field="traffic_rule" exp_strength="0"/>
    <constraint notnull_strength="0" constraints="0" unique_strength="0" field="speed_rule" exp_strength="0"/>
    <constraint notnull_strength="0" constraints="0" unique_strength="0" field="toll_rule" exp_strength="0"/>
    <constraint notnull_strength="0" constraints="0" unique_strength="0" field="vehicle_parking_rule" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="id"/>
    <constraint desc="" exp="" field="category"/>
    <constraint desc="" exp="" field="name"/>
    <constraint desc="" exp="" field="traffic_rule"/>
    <constraint desc="" exp="" field="speed_rule"/>
    <constraint desc="" exp="" field="toll_rule"/>
    <constraint desc="" exp="" field="vehicle_parking_rule"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="&quot;id&quot;">
    <columns>
      <column type="field" name="id" hidden="0" width="35"/>
      <column type="field" name="category" hidden="0" width="56"/>
      <column type="field" name="name" hidden="0" width="194"/>
      <column type="actions" hidden="1" width="-1"/>
      <column type="field" name="traffic_rule" hidden="0" width="136"/>
      <column type="field" name="speed_rule" hidden="0" width="127"/>
      <column type="field" name="toll_rule" hidden="0" width="110"/>
      <column type="field" name="vehicle_parking_rule" hidden="0" width="148"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="category"/>
    <field editable="1" name="engine_type_id"/>
    <field editable="1" name="id"/>
    <field editable="1" name="name"/>
    <field editable="1" name="need_parking"/>
    <field editable="1" name="return_shared_vehicle"/>
    <field editable="1" name="shared_vehicle"/>
    <field editable="1" name="speed_rule"/>
    <field editable="1" name="speed_rule_id"/>
    <field editable="1" name="toll_rule"/>
    <field editable="1" name="toll_rules_id"/>
    <field editable="1" name="traffic_rule"/>
    <field editable="1" name="traffic_rules_id"/>
    <field editable="1" name="vehicle_parking_rule"/>
  </editable>
  <labelOnTop>
    <field name="category" labelOnTop="0"/>
    <field name="engine_type_id" labelOnTop="0"/>
    <field name="id" labelOnTop="0"/>
    <field name="name" labelOnTop="0"/>
    <field name="need_parking" labelOnTop="0"/>
    <field name="return_shared_vehicle" labelOnTop="0"/>
    <field name="shared_vehicle" labelOnTop="0"/>
    <field name="speed_rule" labelOnTop="0"/>
    <field name="speed_rule_id" labelOnTop="0"/>
    <field name="toll_rule" labelOnTop="0"/>
    <field name="toll_rules_id" labelOnTop="0"/>
    <field name="traffic_rule" labelOnTop="0"/>
    <field name="traffic_rules_id" labelOnTop="0"/>
    <field name="vehicle_parking_rule" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>name</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>4</layerGeometryType>
</qgis>
