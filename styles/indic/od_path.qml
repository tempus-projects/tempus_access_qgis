<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="1e+08" simplifyAlgorithm="0" styleCategories="AllStyleCategories" simplifyLocal="0" simplifyMaxScale="1" version="3.10.6-A Coruña" labelsEnabled="0" hasScaleBasedVisibilityFlag="0" simplifyDrawingTol="1" readOnly="0" simplifyDrawingHints="1" maxScale="100000">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 type="singleSymbol" enableorderby="0" symbollevels="0" forceraster="0">
    <symbols>
      <symbol type="line" force_rhr="0" alpha="1" name="0" clip_to_extent="1">
        <layer pass="0" enabled="1" class="SimpleLine" locked="0">
          <prop v="square" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0,0,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="2.8" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>COALESCE("stop_id1", '&lt;NULL>')</value>
    </property>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory lineSizeScale="3x:0,0,0,0,0,0" minScaleDenominator="100000" width="15" penWidth="0" minimumSize="0" barWidth="5" rotationOffset="270" sizeType="MM" scaleDependency="Area" sizeScale="3x:0,0,0,0,0,0" penAlpha="255" lineSizeType="MM" labelPlacementMethod="XHeight" enabled="0" backgroundColor="#ffffff" diagramOrientation="Up" backgroundAlpha="255" maxScaleDenominator="1e+08" opacity="1" height="15" penColor="#000000" scaleBasedVisibility="0">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute field="" label="" color="#000000"/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings zIndex="0" placement="2" linePlacementFlags="2" priority="0" showAll="1" obstacle="0" dist="0">
    <properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="query_id">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option type="bool" value="true" name="AllowNull"/>
            <Option type="int" value="2147483647" name="Max"/>
            <Option type="int" value="-2147483648" name="Min"/>
            <Option type="int" value="0" name="Precision"/>
            <Option type="int" value="1" name="Step"/>
            <Option type="QString" value="SpinBox" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="path_o_node">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option type="bool" value="true" name="AllowNull"/>
            <Option type="int" value="2147483647" name="Max"/>
            <Option type="int" value="-2147483648" name="Min"/>
            <Option type="int" value="0" name="Precision"/>
            <Option type="int" value="1" name="Step"/>
            <Option type="QString" value="SpinBox" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="path_d_node">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option type="bool" value="true" name="AllowNull"/>
            <Option type="int" value="2147483647" name="Max"/>
            <Option type="int" value="-2147483648" name="Min"/>
            <Option type="int" value="0" name="Precision"/>
            <Option type="int" value="1" name="Step"/>
            <Option type="QString" value="SpinBox" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="orig_mode">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="AllowMulti"/>
            <Option type="bool" value="true" name="AllowNull"/>
            <Option type="QString" value="" name="FilterExpression"/>
            <Option type="QString" value="id" name="Key"/>
            <Option type="QString" value="Modes_de_transport_4d87eba3_4fb0_44df_85da_fdc1dae8f575" name="Layer"/>
            <Option type="QString" value="Modes de transport" name="LayerName"/>
            <Option type="QString" value="postgres" name="LayerProviderName"/>
            <Option type="QString" value="dbname='tempusaccess_corse' host=localhost port=55432 user='aurelie-p.bousquet' key='id' checkPrimaryKeyUnicity='1' table=&quot;pgtempus&quot;.&quot;transport_mode&quot; sql=" name="LayerSource"/>
            <Option type="int" value="1" name="NofColumns"/>
            <Option type="bool" value="false" name="OrderByValue"/>
            <Option type="bool" value="false" name="UseCompleter"/>
            <Option type="QString" value="name" name="Value"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="dest_mode">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="AllowMulti"/>
            <Option type="bool" value="true" name="AllowNull"/>
            <Option type="QString" value="" name="FilterExpression"/>
            <Option type="QString" value="id" name="Key"/>
            <Option type="QString" value="Modes_de_transport_4d87eba3_4fb0_44df_85da_fdc1dae8f575" name="Layer"/>
            <Option type="QString" value="Modes de transport" name="LayerName"/>
            <Option type="QString" value="postgres" name="LayerProviderName"/>
            <Option type="QString" value="dbname='tempusaccess_corse' host=localhost port=55432 user='aurelie-p.bousquet' key='id' checkPrimaryKeyUnicity='1' table=&quot;pgtempus&quot;.&quot;transport_mode&quot; sql=" name="LayerSource"/>
            <Option type="int" value="1" name="NofColumns"/>
            <Option type="bool" value="false" name="OrderByValue"/>
            <Option type="bool" value="false" name="UseCompleter"/>
            <Option type="QString" value="name" name="Value"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="theo_dep_time">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="theo_arr_time">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="departure_time">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="arrival_time">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="travel_time">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="waiting_time">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option type="bool" value="true" name="allow_null"/>
            <Option type="bool" value="true" name="calendar_popup"/>
            <Option type="QString" value="HH:mm:ss" name="display_format"/>
            <Option type="QString" value="HH:mm:ss" name="field_format"/>
            <Option type="bool" value="false" name="field_iso_format"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="fare">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="cost">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="mode_changes">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option type="bool" value="true" name="AllowNull"/>
            <Option type="int" value="2147483647" name="Max"/>
            <Option type="int" value="-2147483648" name="Min"/>
            <Option type="int" value="0" name="Precision"/>
            <Option type="int" value="1" name="Step"/>
            <Option type="QString" value="SpinBox" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pt_transfers">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option type="bool" value="true" name="AllowNull"/>
            <Option type="int" value="2147483647" name="Max"/>
            <Option type="int" value="-2147483648" name="Min"/>
            <Option type="int" value="0" name="Precision"/>
            <Option type="int" value="1" name="Step"/>
            <Option type="QString" value="SpinBox" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" field="id" name=""/>
    <alias index="1" field="query_id" name="ID requête"/>
    <alias index="2" field="path_o_node" name="Origine"/>
    <alias index="3" field="path_d_node" name="Destination"/>
    <alias index="4" field="orig_mode" name="Mode orig"/>
    <alias index="5" field="dest_mode" name="Mode dest"/>
    <alias index="6" field="theo_dep_time" name="Départ théo orig"/>
    <alias index="7" field="theo_arr_time" name="Arrivée théo dest"/>
    <alias index="8" field="departure_time" name="Départ réel orig"/>
    <alias index="9" field="arrival_time" name="Arrivée réelle dest"/>
    <alias index="10" field="travel_time" name="Temps parcours"/>
    <alias index="11" field="waiting_time" name="Temps attente"/>
    <alias index="12" field="fare" name="Tarif total"/>
    <alias index="13" field="cost" name="Coût total"/>
    <alias index="14" field="mode_changes" name="Changements mode"/>
    <alias index="15" field="pt_transfers" name="Correspondances TC"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" field="id" applyOnUpdate="0"/>
    <default expression="" field="query_id" applyOnUpdate="0"/>
    <default expression="" field="path_o_node" applyOnUpdate="0"/>
    <default expression="" field="path_d_node" applyOnUpdate="0"/>
    <default expression="" field="orig_mode" applyOnUpdate="0"/>
    <default expression="" field="dest_mode" applyOnUpdate="0"/>
    <default expression="" field="theo_dep_time" applyOnUpdate="0"/>
    <default expression="" field="theo_arr_time" applyOnUpdate="0"/>
    <default expression="" field="departure_time" applyOnUpdate="0"/>
    <default expression="" field="arrival_time" applyOnUpdate="0"/>
    <default expression="" field="travel_time" applyOnUpdate="0"/>
    <default expression="" field="waiting_time" applyOnUpdate="0"/>
    <default expression="" field="fare" applyOnUpdate="0"/>
    <default expression="" field="cost" applyOnUpdate="0"/>
    <default expression="" field="mode_changes" applyOnUpdate="0"/>
    <default expression="" field="pt_transfers" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint unique_strength="1" field="id" constraints="3" exp_strength="0" notnull_strength="1"/>
    <constraint unique_strength="0" field="query_id" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="path_o_node" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="path_d_node" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="orig_mode" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="dest_mode" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="theo_dep_time" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="theo_arr_time" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="departure_time" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="arrival_time" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="travel_time" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="waiting_time" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="fare" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="cost" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="mode_changes" constraints="0" exp_strength="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="pt_transfers" constraints="0" exp_strength="0" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="id" exp=""/>
    <constraint desc="" field="query_id" exp=""/>
    <constraint desc="" field="path_o_node" exp=""/>
    <constraint desc="" field="path_d_node" exp=""/>
    <constraint desc="" field="orig_mode" exp=""/>
    <constraint desc="" field="dest_mode" exp=""/>
    <constraint desc="" field="theo_dep_time" exp=""/>
    <constraint desc="" field="theo_arr_time" exp=""/>
    <constraint desc="" field="departure_time" exp=""/>
    <constraint desc="" field="arrival_time" exp=""/>
    <constraint desc="" field="travel_time" exp=""/>
    <constraint desc="" field="waiting_time" exp=""/>
    <constraint desc="" field="fare" exp=""/>
    <constraint desc="" field="cost" exp=""/>
    <constraint desc="" field="mode_changes" exp=""/>
    <constraint desc="" field="pt_transfers" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortExpression="&quot;transport_modes_name&quot;" actionWidgetStyle="dropDown" sortOrder="0">
    <columns>
      <column width="-1" type="actions" hidden="1"/>
      <column width="-1" type="field" hidden="1" name="id"/>
      <column width="61" type="field" hidden="0" name="query_id"/>
      <column width="48" type="field" hidden="0" name="path_o_node"/>
      <column width="68" type="field" hidden="0" name="path_d_node"/>
      <column width="63" type="field" hidden="0" name="orig_mode"/>
      <column width="63" type="field" hidden="0" name="dest_mode"/>
      <column width="140" type="field" hidden="0" name="theo_dep_time"/>
      <column width="121" type="field" hidden="0" name="theo_arr_time"/>
      <column width="86" type="field" hidden="0" name="departure_time"/>
      <column width="98" type="field" hidden="0" name="arrival_time"/>
      <column width="89" type="field" hidden="0" name="waiting_time"/>
      <column width="90" type="field" hidden="0" name="travel_time"/>
      <column width="57" type="field" hidden="0" name="fare"/>
      <column width="58" type="field" hidden="0" name="cost"/>
      <column width="111" type="field" hidden="0" name="pt_transfers"/>
      <column width="113" type="field" hidden="0" name="mode_changes"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="arrival_time"/>
    <field editable="1" name="cost"/>
    <field editable="1" name="d_node"/>
    <field editable="1" name="departure_time"/>
    <field editable="1" name="dest_mode"/>
    <field editable="1" name="fare"/>
    <field editable="1" name="final_mode"/>
    <field editable="1" name="gtfs_codes"/>
    <field editable="1" name="id"/>
    <field editable="1" name="initial_mode"/>
    <field editable="1" name="mode_changes"/>
    <field editable="1" name="o_node"/>
    <field editable="1" name="orig_mode"/>
    <field editable="1" name="path_d_node"/>
    <field editable="1" name="path_o_node"/>
    <field editable="1" name="pt_agencies_id"/>
    <field editable="1" name="pt_agencies_name"/>
    <field editable="1" name="pt_legs_num"/>
    <field editable="1" name="pt_routes_id"/>
    <field editable="1" name="pt_routes_long_name"/>
    <field editable="1" name="pt_routes_short_name"/>
    <field editable="1" name="pt_transfers"/>
    <field editable="1" name="pt_trip_types_id"/>
    <field editable="1" name="pt_trip_types_name"/>
    <field editable="1" name="query_id"/>
    <field editable="1" name="theo_arr_time"/>
    <field editable="1" name="theo_dep_time"/>
    <field editable="1" name="total_legs_num"/>
    <field editable="1" name="transport_modes_id"/>
    <field editable="1" name="transport_modes_name"/>
    <field editable="1" name="travel_time"/>
    <field editable="1" name="wait_time"/>
    <field editable="1" name="waiting_time"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="arrival_time"/>
    <field labelOnTop="0" name="cost"/>
    <field labelOnTop="0" name="d_node"/>
    <field labelOnTop="0" name="departure_time"/>
    <field labelOnTop="0" name="dest_mode"/>
    <field labelOnTop="0" name="fare"/>
    <field labelOnTop="0" name="final_mode"/>
    <field labelOnTop="0" name="gtfs_codes"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="initial_mode"/>
    <field labelOnTop="0" name="mode_changes"/>
    <field labelOnTop="0" name="o_node"/>
    <field labelOnTop="0" name="orig_mode"/>
    <field labelOnTop="0" name="path_d_node"/>
    <field labelOnTop="0" name="path_o_node"/>
    <field labelOnTop="0" name="pt_agencies_id"/>
    <field labelOnTop="0" name="pt_agencies_name"/>
    <field labelOnTop="0" name="pt_legs_num"/>
    <field labelOnTop="0" name="pt_routes_id"/>
    <field labelOnTop="0" name="pt_routes_long_name"/>
    <field labelOnTop="0" name="pt_routes_short_name"/>
    <field labelOnTop="0" name="pt_transfers"/>
    <field labelOnTop="0" name="pt_trip_types_id"/>
    <field labelOnTop="0" name="pt_trip_types_name"/>
    <field labelOnTop="0" name="query_id"/>
    <field labelOnTop="0" name="theo_arr_time"/>
    <field labelOnTop="0" name="theo_dep_time"/>
    <field labelOnTop="0" name="total_legs_num"/>
    <field labelOnTop="0" name="transport_modes_id"/>
    <field labelOnTop="0" name="transport_modes_name"/>
    <field labelOnTop="0" name="travel_time"/>
    <field labelOnTop="0" name="wait_time"/>
    <field labelOnTop="0" name="waiting_time"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>COALESCE("stop_id1", '&lt;NULL>')</previewExpression>
  <mapTip>stop_id1</mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
