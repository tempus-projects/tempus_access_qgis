<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis readOnly="0" maxScale="0" version="3.10.6-A Coruña" minScale="1e+08" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>COALESCE("gid", '&lt;NULL>')</value>
    </property>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="gid">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="id">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="2147483647" type="int" name="Max"/>
            <Option value="-2147483648" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="original_id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="short_name">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="long_name">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pt_trip_type_id">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="2147483647" type="int" name="Max"/>
            <Option value="-2147483648" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="gtfs_codes">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pt_agency_id">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="2147483647" type="int" name="Max"/>
            <Option value="-2147483648" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="source_id">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="AllowMulti"/>
            <Option value="false" type="bool" name="AllowNull"/>
            <Option value="" type="QString" name="FilterExpression"/>
            <Option value="id" type="QString" name="Key"/>
            <Option value="Sources_de_données_4ee626d9_c4f1_40b3_b1d8_fada5a36b8f6" type="QString" name="Layer"/>
            <Option value="Sources de données" type="QString" name="LayerName"/>
            <Option value="1" type="int" name="NofColumns"/>
            <Option value="false" type="bool" name="OrderByValue"/>
            <Option value="false" type="bool" name="UseCompleter"/>
            <Option value="name" type="QString" name="Value"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="days">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pt_serv_days">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="serv_num">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="veh_km">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" field="gid" name=""/>
    <alias index="1" field="id" name="ID"/>
    <alias index="2" field="original_id" name="ID initial ligne"/>
    <alias index="3" field="short_name" name="Nom court"/>
    <alias index="4" field="long_name" name="Nom long"/>
    <alias index="5" field="pt_trip_type_id" name="ID types services"/>
    <alias index="6" field="gtfs_codes" name="Codes GTFS"/>
    <alias index="7" field="pt_agency_id" name="ID exploitant"/>
    <alias index="8" field="source_id" name="Source"/>
    <alias index="9" field="days" name="Jours calcul"/>
    <alias index="10" field="pt_serv_days" name="Jours services"/>
    <alias index="11" field="serv_num" name="Nb services"/>
    <alias index="12" field="veh_km" name="véh.km/jour"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" applyOnUpdate="0" field="gid"/>
    <default expression="" applyOnUpdate="0" field="id"/>
    <default expression="" applyOnUpdate="0" field="original_id"/>
    <default expression="" applyOnUpdate="0" field="short_name"/>
    <default expression="" applyOnUpdate="0" field="long_name"/>
    <default expression="" applyOnUpdate="0" field="pt_trip_type_id"/>
    <default expression="" applyOnUpdate="0" field="gtfs_codes"/>
    <default expression="" applyOnUpdate="0" field="pt_agency_id"/>
    <default expression="" applyOnUpdate="0" field="source_id"/>
    <default expression="" applyOnUpdate="0" field="days"/>
    <default expression="" applyOnUpdate="0" field="pt_serv_days"/>
    <default expression="" applyOnUpdate="0" field="serv_num"/>
    <default expression="" applyOnUpdate="0" field="veh_km"/>
  </defaults>
  <constraints>
    <constraint notnull_strength="1" exp_strength="0" unique_strength="1" field="gid" constraints="3"/>
    <constraint notnull_strength="0" exp_strength="0" unique_strength="0" field="id" constraints="0"/>
    <constraint notnull_strength="0" exp_strength="0" unique_strength="0" field="original_id" constraints="0"/>
    <constraint notnull_strength="0" exp_strength="0" unique_strength="0" field="short_name" constraints="0"/>
    <constraint notnull_strength="0" exp_strength="0" unique_strength="0" field="long_name" constraints="0"/>
    <constraint notnull_strength="0" exp_strength="0" unique_strength="0" field="pt_trip_type_id" constraints="0"/>
    <constraint notnull_strength="0" exp_strength="0" unique_strength="0" field="gtfs_codes" constraints="0"/>
    <constraint notnull_strength="0" exp_strength="0" unique_strength="0" field="pt_agency_id" constraints="0"/>
    <constraint notnull_strength="0" exp_strength="0" unique_strength="0" field="source_id" constraints="0"/>
    <constraint notnull_strength="0" exp_strength="0" unique_strength="0" field="days" constraints="0"/>
    <constraint notnull_strength="0" exp_strength="0" unique_strength="0" field="pt_serv_days" constraints="0"/>
    <constraint notnull_strength="0" exp_strength="0" unique_strength="0" field="serv_num" constraints="0"/>
    <constraint notnull_strength="0" exp_strength="0" unique_strength="0" field="veh_km" constraints="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" field="gid" desc=""/>
    <constraint exp="" field="id" desc=""/>
    <constraint exp="" field="original_id" desc=""/>
    <constraint exp="" field="short_name" desc=""/>
    <constraint exp="" field="long_name" desc=""/>
    <constraint exp="" field="pt_trip_type_id" desc=""/>
    <constraint exp="" field="gtfs_codes" desc=""/>
    <constraint exp="" field="pt_agency_id" desc=""/>
    <constraint exp="" field="source_id" desc=""/>
    <constraint exp="" field="days" desc=""/>
    <constraint exp="" field="pt_serv_days" desc=""/>
    <constraint exp="" field="serv_num" desc=""/>
    <constraint exp="" field="veh_km" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortExpression="&quot;id&quot;" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column width="-1" hidden="1" type="actions"/>
      <column width="37" hidden="1" type="field" name="gid"/>
      <column width="35" hidden="0" type="field" name="id"/>
      <column width="100" hidden="1" type="field" name="original_id"/>
      <column width="85" hidden="0" type="field" name="source_id"/>
      <column width="73" hidden="0" type="field" name="short_name"/>
      <column width="235" hidden="0" type="field" name="long_name"/>
      <column width="-1" hidden="1" type="field" name="pt_trip_type_id"/>
      <column width="-1" hidden="1" type="field" name="gtfs_codes"/>
      <column width="-1" hidden="1" type="field" name="pt_agency_id"/>
      <column width="81" hidden="0" type="field" name="days"/>
      <column width="82" hidden="0" type="field" name="pt_serv_days"/>
      <column width="69" hidden="0" type="field" name="serv_num"/>
      <column width="74" hidden="0" type="field" name="veh_km"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles>
      <style background_color_alpha="102" rule="route_type=0" background_color="#008000" name="Tram">
        <font style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      </style>
      <style background_color_alpha="102" rule="route_type=1" background_color="#ff0000" name="Métro">
        <font style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      </style>
      <style background_color_alpha="102" rule="route_type=2" background_color="#0000ff" name="Fer">
        <font style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      </style>
      <style background_color_alpha="102" rule="route_type=3" background_color="#ffa500" name="Car ou bus">
        <font style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      </style>
      <style background_color_alpha="102" rule="route_type=4" background_color="#00c3fe" name="Ferry">
        <font style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      </style>
      <style background_color_alpha="102" rule="route_type=5 or route_type=6" background_color="#ffff00" name="Transport par câble">
        <font style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      </style>
      <style background_color_alpha="102" rule="route_type=7" background_color="#800080" name="Funiculaire">
        <font style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      </style>
      <style background_color_alpha="102" rule="route_type = 8" background_color="#000000" name="Tous modes">
        <font style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      </style>
    </rowstyles>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="days"/>
    <field editable="1" name="gid"/>
    <field editable="1" name="gtfs_codes"/>
    <field editable="1" name="id"/>
    <field editable="1" name="long_name"/>
    <field editable="1" name="network_id"/>
    <field editable="1" name="network_name"/>
    <field editable="1" name="original_id"/>
    <field editable="1" name="pt_agency_id"/>
    <field editable="1" name="pt_agency_name"/>
    <field editable="1" name="pt_serv_days"/>
    <field editable="1" name="pt_trip_type_id"/>
    <field editable="1" name="pt_trip_type_name"/>
    <field editable="1" name="pt_trip_types_id"/>
    <field editable="1" name="pt_trip_types_name"/>
    <field editable="1" name="serv_num"/>
    <field editable="1" name="short_name"/>
    <field editable="1" name="source_id"/>
    <field editable="1" name="symbol_color"/>
    <field editable="1" name="symbol_size"/>
    <field editable="1" name="veh_km"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="days"/>
    <field labelOnTop="0" name="gid"/>
    <field labelOnTop="0" name="gtfs_codes"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="long_name"/>
    <field labelOnTop="0" name="network_id"/>
    <field labelOnTop="0" name="network_name"/>
    <field labelOnTop="0" name="original_id"/>
    <field labelOnTop="0" name="pt_agency_id"/>
    <field labelOnTop="0" name="pt_agency_name"/>
    <field labelOnTop="0" name="pt_serv_days"/>
    <field labelOnTop="0" name="pt_trip_type_id"/>
    <field labelOnTop="0" name="pt_trip_type_name"/>
    <field labelOnTop="0" name="pt_trip_types_id"/>
    <field labelOnTop="0" name="pt_trip_types_name"/>
    <field labelOnTop="0" name="serv_num"/>
    <field labelOnTop="0" name="short_name"/>
    <field labelOnTop="0" name="source_id"/>
    <field labelOnTop="0" name="symbol_color"/>
    <field labelOnTop="0" name="symbol_size"/>
    <field labelOnTop="0" name="veh_km"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>COALESCE("gid", '&lt;NULL>')</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>4</layerGeometryType>
</qgis>
