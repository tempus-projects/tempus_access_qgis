<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis hasScaleBasedVisibilityFlag="0" version="3.10.6-A Coruña" minScale="1e+08" maxScale="0" styleCategories="AllStyleCategories" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>COALESCE("gid", '&lt;NULL>')</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="gid">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="id">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="2147483647" type="int" name="Max"/>
            <Option value="-2147483648" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="original_id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="name">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pt_trip_type_id">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="2147483647" type="int" name="Max"/>
            <Option value="-2147483648" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pt_trip_type_name">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="gtfs_codes">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="source_id">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="2147483647" type="int" name="Max"/>
            <Option value="-2147483648" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="days">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pt_serv_days">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="veh_km">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" field="gid" name=""/>
    <alias index="1" field="id" name="ID"/>
    <alias index="2" field="original_id" name="ID initial"/>
    <alias index="3" field="name" name="Nom"/>
    <alias index="4" field="pt_trip_type_id" name="ID types services"/>
    <alias index="5" field="pt_trip_type_name" name="Types services"/>
    <alias index="6" field="gtfs_codes" name="Code GTFS"/>
    <alias index="7" field="source_id" name="Source"/>
    <alias index="8" field="days" name="Jours calcul"/>
    <alias index="9" field="pt_serv_days" name="Jours desserte"/>
    <alias index="10" field="veh_km" name="véh.km/jour"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default field="gid" expression="" applyOnUpdate="0"/>
    <default field="id" expression="" applyOnUpdate="0"/>
    <default field="original_id" expression="" applyOnUpdate="0"/>
    <default field="name" expression="" applyOnUpdate="0"/>
    <default field="pt_trip_type_id" expression="" applyOnUpdate="0"/>
    <default field="pt_trip_type_name" expression="" applyOnUpdate="0"/>
    <default field="gtfs_codes" expression="" applyOnUpdate="0"/>
    <default field="source_id" expression="" applyOnUpdate="0"/>
    <default field="days" expression="" applyOnUpdate="0"/>
    <default field="pt_serv_days" expression="" applyOnUpdate="0"/>
    <default field="veh_km" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint unique_strength="1" exp_strength="0" notnull_strength="1" field="gid" constraints="3"/>
    <constraint unique_strength="0" exp_strength="0" notnull_strength="0" field="id" constraints="0"/>
    <constraint unique_strength="0" exp_strength="0" notnull_strength="0" field="original_id" constraints="0"/>
    <constraint unique_strength="0" exp_strength="0" notnull_strength="0" field="name" constraints="0"/>
    <constraint unique_strength="0" exp_strength="0" notnull_strength="0" field="pt_trip_type_id" constraints="0"/>
    <constraint unique_strength="0" exp_strength="0" notnull_strength="0" field="pt_trip_type_name" constraints="0"/>
    <constraint unique_strength="0" exp_strength="0" notnull_strength="0" field="gtfs_codes" constraints="0"/>
    <constraint unique_strength="0" exp_strength="0" notnull_strength="0" field="source_id" constraints="0"/>
    <constraint unique_strength="0" exp_strength="0" notnull_strength="0" field="days" constraints="0"/>
    <constraint unique_strength="0" exp_strength="0" notnull_strength="0" field="pt_serv_days" constraints="0"/>
    <constraint unique_strength="0" exp_strength="0" notnull_strength="0" field="veh_km" constraints="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="gid" exp=""/>
    <constraint desc="" field="id" exp=""/>
    <constraint desc="" field="original_id" exp=""/>
    <constraint desc="" field="name" exp=""/>
    <constraint desc="" field="pt_trip_type_id" exp=""/>
    <constraint desc="" field="pt_trip_type_name" exp=""/>
    <constraint desc="" field="gtfs_codes" exp=""/>
    <constraint desc="" field="source_id" exp=""/>
    <constraint desc="" field="days" exp=""/>
    <constraint desc="" field="pt_serv_days" exp=""/>
    <constraint desc="" field="veh_km" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortOrder="0" actionWidgetStyle="dropDown" sortExpression="&quot;gid&quot;">
    <columns>
      <column type="actions" width="-1" hidden="1"/>
      <column type="field" name="gid" width="-1" hidden="1"/>
      <column type="field" name="id" width="35" hidden="0"/>
      <column type="field" name="original_id" width="-1" hidden="1"/>
      <column type="field" name="source_id" width="90" hidden="0"/>
      <column type="field" name="name" width="183" hidden="0"/>
      <column type="field" name="pt_trip_type_id" width="-1" hidden="1"/>
      <column type="field" name="pt_trip_type_name" width="83" hidden="0"/>
      <column type="field" name="gtfs_codes" width="-1" hidden="1"/>
      <column type="field" name="days" width="100" hidden="0"/>
      <column type="field" name="pt_serv_days" width="100" hidden="0"/>
      <column type="field" name="veh_km" width="79" hidden="0"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles>
      <style rule="route_type=0" background_color="#008000" background_color_alpha="102" name="Tram">
        <font description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      </style>
      <style rule="route_type=1" background_color="#ff0000" background_color_alpha="102" name="Métro">
        <font description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      </style>
      <style rule="route_type=2" background_color="#0000ff" background_color_alpha="102" name="Fer">
        <font description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      </style>
      <style rule="route_type=3" background_color="#ffa500" background_color_alpha="102" name="Car ou bus">
        <font description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      </style>
      <style rule="route_type=4" background_color="#00c3fe" background_color_alpha="102" name="Ferry">
        <font description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      </style>
      <style rule="route_type=5 or route_type=6" background_color="#ffff00" background_color_alpha="102" name="Transport par câble">
        <font description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      </style>
      <style rule="route_type=7" background_color="#800080" background_color_alpha="102" name="Funiculaire">
        <font description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      </style>
      <style rule="route_type = 8" background_color="#000000" background_color_alpha="102" name="Tous modes">
        <font description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      </style>
    </rowstyles>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="days" editable="1"/>
    <field name="gid" editable="1"/>
    <field name="gtfs_codes" editable="1"/>
    <field name="id" editable="1"/>
    <field name="name" editable="1"/>
    <field name="network_id" editable="1"/>
    <field name="network_name" editable="1"/>
    <field name="original_id" editable="1"/>
    <field name="pt_serv_days" editable="1"/>
    <field name="pt_trip_type_id" editable="1"/>
    <field name="pt_trip_type_name" editable="1"/>
    <field name="source_id" editable="1"/>
    <field name="veh_km" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="days"/>
    <field labelOnTop="0" name="gid"/>
    <field labelOnTop="0" name="gtfs_codes"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="name"/>
    <field labelOnTop="0" name="network_id"/>
    <field labelOnTop="0" name="network_name"/>
    <field labelOnTop="0" name="original_id"/>
    <field labelOnTop="0" name="pt_serv_days"/>
    <field labelOnTop="0" name="pt_trip_type_id"/>
    <field labelOnTop="0" name="pt_trip_type_name"/>
    <field labelOnTop="0" name="source_id"/>
    <field labelOnTop="0" name="veh_km"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>COALESCE("gid", '&lt;NULL>')</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>4</layerGeometryType>
</qgis>
