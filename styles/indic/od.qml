<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyLocal="1" version="3.10.6-A Coruña" labelsEnabled="0" simplifyMaxScale="1" minScale="0" simplifyAlgorithm="0" simplifyDrawingTol="1" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0" simplifyDrawingHints="1" readOnly="0" maxScale="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 forceraster="0" symbollevels="0" enableorderby="0" type="singleSymbol">
    <symbols>
      <symbol force_rhr="0" clip_to_extent="1" name="0" type="line" alpha="1">
        <layer locked="0" pass="0" class="SimpleLine" enabled="1">
          <prop v="square" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0,0,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="0.46" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>path_o_node</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory backgroundAlpha="255" penColor="#000000" penAlpha="255" height="15" sizeScale="3x:0,0,0,0,0,0" labelPlacementMethod="XHeight" backgroundColor="#ffffff" width="15" barWidth="5" minimumSize="0" diagramOrientation="Up" maxScaleDenominator="0" minScaleDenominator="0" lineSizeScale="3x:0,0,0,0,0,0" rotationOffset="270" enabled="0" sizeType="MM" penWidth="0" scaleDependency="Area" scaleBasedVisibility="0" opacity="1" lineSizeType="MM">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute field="" color="#000000" label=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings zIndex="0" showAll="1" obstacle="0" dist="0" placement="2" priority="0" linePlacementFlags="18">
    <properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties"/>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="path_o_node">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" name="AllowNull" type="bool"/>
            <Option value="2147483647" name="Max" type="int"/>
            <Option value="-2147483648" name="Min" type="int"/>
            <Option value="0" name="Precision" type="int"/>
            <Option value="1" name="Step" type="int"/>
            <Option value="SpinBox" name="Style" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="path_d_node">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" name="AllowNull" type="bool"/>
            <Option value="2147483647" name="Max" type="int"/>
            <Option value="-2147483648" name="Min" type="int"/>
            <Option value="0" name="Precision" type="int"/>
            <Option value="1" name="Step" type="int"/>
            <Option value="SpinBox" name="Style" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="orig_mode">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" name="AllowMulti" type="bool"/>
            <Option value="false" name="AllowNull" type="bool"/>
            <Option value="" name="FilterExpression" type="QString"/>
            <Option value="id" name="Key" type="QString"/>
            <Option value="Modes_de_transport_a9f85794_a770_4c37_988e_e9cec8db31c5" name="Layer" type="QString"/>
            <Option value="Modes de transport" name="LayerName" type="QString"/>
            <Option value="postgres" name="LayerProviderName" type="QString"/>
            <Option value="dbname='tempusaccess_corse' host=localhost port=55432 user='aurelie-p.bousquet' key='id' checkPrimaryKeyUnicity='1' table=&quot;pgtempus&quot;.&quot;transport_mode&quot; sql=" name="LayerSource" type="QString"/>
            <Option value="1" name="NofColumns" type="int"/>
            <Option value="false" name="OrderByValue" type="bool"/>
            <Option value="false" name="UseCompleter" type="bool"/>
            <Option value="name" name="Value" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="dest_mode">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" name="AllowMulti" type="bool"/>
            <Option value="false" name="AllowNull" type="bool"/>
            <Option value="" name="FilterExpression" type="QString"/>
            <Option value="id" name="Key" type="QString"/>
            <Option value="Modes_de_transport_a9f85794_a770_4c37_988e_e9cec8db31c5" name="Layer" type="QString"/>
            <Option value="Modes de transport" name="LayerName" type="QString"/>
            <Option value="postgres" name="LayerProviderName" type="QString"/>
            <Option value="dbname='tempusaccess_corse' host=localhost port=55432 user='aurelie-p.bousquet' key='id' checkPrimaryKeyUnicity='1' table=&quot;pgtempus&quot;.&quot;transport_mode&quot; sql=" name="LayerSource" type="QString"/>
            <Option value="1" name="NofColumns" type="int"/>
            <Option value="false" name="OrderByValue" type="bool"/>
            <Option value="false" name="UseCompleter" type="bool"/>
            <Option value="name" name="Value" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="first_dep">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="HH:mm:ss" name="display_format" type="QString"/>
            <Option value="HH:mm:ss" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="last_dep">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="HH:mm:ss" name="display_format" type="QString"/>
            <Option value="HH:mm:ss" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="first_arr">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="HH:mm:ss" name="display_format" type="QString"/>
            <Option value="HH:mm:ss" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="last_arr">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="HH:mm:ss" name="display_format" type="QString"/>
            <Option value="HH:mm:ss" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="cost">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="fare">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="num_paths">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" name="AllowNull" type="bool"/>
            <Option value="2147483647" name="Max" type="int"/>
            <Option value="-2147483648" name="Min" type="int"/>
            <Option value="0" name="Precision" type="int"/>
            <Option value="1" name="Step" type="int"/>
            <Option value="SpinBox" name="Style" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="waiting_time">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="travel_time">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="mode_changes">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pt_transfers">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="path_o_node" name="Orig chemin" index="0"/>
    <alias field="path_d_node" name="Dest chemin" index="1"/>
    <alias field="orig_mode" name="Mode orig" index="2"/>
    <alias field="dest_mode" name="Mode dest" index="3"/>
    <alias field="first_dep" name="Premier départ" index="4"/>
    <alias field="last_dep" name="Dernier départ" index="5"/>
    <alias field="first_arr" name="Première arrivée" index="6"/>
    <alias field="last_arr" name="Dernière arrivée" index="7"/>
    <alias field="cost" name="Coût total" index="8"/>
    <alias field="fare" name="Tarif total" index="9"/>
    <alias field="num_paths" name="Nb chemins" index="10"/>
    <alias field="waiting_time" name="Temps attente" index="11"/>
    <alias field="travel_time" name="Temps parcours" index="12"/>
    <alias field="mode_changes" name="Changements mode" index="13"/>
    <alias field="pt_transfers" name="Correspondances TC" index="14"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default field="path_o_node" expression="" applyOnUpdate="0"/>
    <default field="path_d_node" expression="" applyOnUpdate="0"/>
    <default field="orig_mode" expression="" applyOnUpdate="0"/>
    <default field="dest_mode" expression="" applyOnUpdate="0"/>
    <default field="first_dep" expression="" applyOnUpdate="0"/>
    <default field="last_dep" expression="" applyOnUpdate="0"/>
    <default field="first_arr" expression="" applyOnUpdate="0"/>
    <default field="last_arr" expression="" applyOnUpdate="0"/>
    <default field="cost" expression="" applyOnUpdate="0"/>
    <default field="fare" expression="" applyOnUpdate="0"/>
    <default field="num_paths" expression="" applyOnUpdate="0"/>
    <default field="waiting_time" expression="" applyOnUpdate="0"/>
    <default field="travel_time" expression="" applyOnUpdate="0"/>
    <default field="mode_changes" expression="" applyOnUpdate="0"/>
    <default field="pt_transfers" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" field="path_o_node" constraints="1" unique_strength="0" notnull_strength="1"/>
    <constraint exp_strength="0" field="path_d_node" constraints="1" unique_strength="0" notnull_strength="1"/>
    <constraint exp_strength="0" field="orig_mode" constraints="1" unique_strength="0" notnull_strength="1"/>
    <constraint exp_strength="0" field="dest_mode" constraints="1" unique_strength="0" notnull_strength="1"/>
    <constraint exp_strength="0" field="first_dep" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="last_dep" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="first_arr" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="last_arr" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="cost" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="fare" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="num_paths" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="waiting_time" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="travel_time" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="mode_changes" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="pt_transfers" constraints="0" unique_strength="0" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="path_o_node" exp=""/>
    <constraint desc="" field="path_d_node" exp=""/>
    <constraint desc="" field="orig_mode" exp=""/>
    <constraint desc="" field="dest_mode" exp=""/>
    <constraint desc="" field="first_dep" exp=""/>
    <constraint desc="" field="last_dep" exp=""/>
    <constraint desc="" field="first_arr" exp=""/>
    <constraint desc="" field="last_arr" exp=""/>
    <constraint desc="" field="cost" exp=""/>
    <constraint desc="" field="fare" exp=""/>
    <constraint desc="" field="num_paths" exp=""/>
    <constraint desc="" field="waiting_time" exp=""/>
    <constraint desc="" field="travel_time" exp=""/>
    <constraint desc="" field="mode_changes" exp=""/>
    <constraint desc="" field="pt_transfers" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="&quot;num_paths&quot;" actionWidgetStyle="dropDown" sortOrder="1">
    <columns>
      <column hidden="0" name="path_o_node" width="76" type="field"/>
      <column hidden="0" name="path_d_node" width="79" type="field"/>
      <column hidden="0" name="orig_mode" width="65" type="field"/>
      <column hidden="0" name="dest_mode" width="70" type="field"/>
      <column hidden="0" name="first_dep" width="86" type="field"/>
      <column hidden="0" name="last_dep" width="85" type="field"/>
      <column hidden="0" name="first_arr" width="92" type="field"/>
      <column hidden="0" name="last_arr" width="88" type="field"/>
      <column hidden="0" name="cost" width="58" type="field"/>
      <column hidden="0" name="fare" width="54" type="field"/>
      <column hidden="0" name="waiting_time" width="79" type="field"/>
      <column hidden="0" name="travel_time" width="91" type="field"/>
      <column hidden="0" name="pt_transfers" width="116" type="field"/>
      <column hidden="0" name="mode_changes" width="114" type="field"/>
      <column hidden="0" name="num_paths" width="76" type="field"/>
      <column hidden="1" width="-1" type="actions"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="cost"/>
    <field editable="1" name="days"/>
    <field editable="1" name="dest_mode"/>
    <field editable="1" name="fare"/>
    <field editable="1" name="final_mode"/>
    <field editable="1" name="first_arr"/>
    <field editable="1" name="first_dep"/>
    <field editable="1" name="initial_mode"/>
    <field editable="1" name="last_arr"/>
    <field editable="1" name="last_dep"/>
    <field editable="1" name="mode_changes"/>
    <field editable="1" name="num_paths"/>
    <field editable="1" name="num_serv"/>
    <field editable="1" name="orig_mode"/>
    <field editable="1" name="path_d_node"/>
    <field editable="1" name="path_o_node"/>
    <field editable="1" name="pt_transfers"/>
    <field editable="1" name="query_id"/>
    <field editable="1" name="travel_time"/>
    <field editable="1" name="waiting_time"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="cost"/>
    <field labelOnTop="0" name="days"/>
    <field labelOnTop="0" name="dest_mode"/>
    <field labelOnTop="0" name="fare"/>
    <field labelOnTop="0" name="final_mode"/>
    <field labelOnTop="0" name="first_arr"/>
    <field labelOnTop="0" name="first_dep"/>
    <field labelOnTop="0" name="initial_mode"/>
    <field labelOnTop="0" name="last_arr"/>
    <field labelOnTop="0" name="last_dep"/>
    <field labelOnTop="0" name="mode_changes"/>
    <field labelOnTop="0" name="num_paths"/>
    <field labelOnTop="0" name="num_serv"/>
    <field labelOnTop="0" name="orig_mode"/>
    <field labelOnTop="0" name="path_d_node"/>
    <field labelOnTop="0" name="path_o_node"/>
    <field labelOnTop="0" name="pt_transfers"/>
    <field labelOnTop="0" name="query_id"/>
    <field labelOnTop="0" name="travel_time"/>
    <field labelOnTop="0" name="waiting_time"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>path_o_node</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
