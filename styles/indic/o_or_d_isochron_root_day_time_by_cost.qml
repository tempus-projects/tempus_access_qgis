<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis styleCategories="AllStyleCategories" labelsEnabled="0" simplifyMaxScale="1" minScale="1e+08" simplifyDrawingHints="1" simplifyDrawingTol="1" version="3.10.6-A Coruña" maxScale="0" simplifyAlgorithm="0" simplifyLocal="1" hasScaleBasedVisibilityFlag="0" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 enableorderby="0" symbollevels="0" type="RuleRenderer" forceraster="0">
    <rules key="{05b159c6-8810-4f13-b7a3-4de23603407c}">
      <rule label="Barrière" filter="&quot;symbol_color&quot; is null" symbol="0" key="{74fa4d2a-c599-4175-a08b-09b3bf9713a2}"/>
      <rule label="0 - 0,05 " filter="&quot;symbol_color&quot; > 0.000000 AND &quot;symbol_color&quot; &lt;= 0.050000" symbol="1" key="{9b88f9c8-5240-468a-88a6-68f366a529b7}"/>
      <rule label="0,05 - 0,1 " filter="&quot;symbol_color&quot; > 0.050000 AND &quot;symbol_color&quot; &lt;= 0.100000" symbol="2" key="{57271544-08f0-4356-ac20-7d9aee173073}"/>
      <rule label="0,1 - 0,15 " filter="&quot;symbol_color&quot; > 0.100000 AND &quot;symbol_color&quot; &lt;= 0.150000" symbol="3" key="{4fc091ca-07d1-4216-b179-443ca927d404}"/>
      <rule label="0,15 - 0,2 " filter="&quot;symbol_color&quot; > 0.150000 AND &quot;symbol_color&quot; &lt;= 0.200000" symbol="4" key="{d6fdf5f3-8b79-4188-8ebb-18751cd23770}"/>
      <rule label="0,2 - 0,25 " filter="&quot;symbol_color&quot; > 0.200000 AND &quot;symbol_color&quot; &lt;= 0.250000" symbol="5" key="{914cfb0d-9698-4c8a-953b-5f995f99f760}"/>
      <rule label="0,25 - 0,3 " filter="&quot;symbol_color&quot; > 0.250000 AND &quot;symbol_color&quot; &lt;= 0.300000" symbol="6" key="{e1c821e2-d284-43ee-94a5-b97cf03d84eb}"/>
      <rule label="0,3 - 0,35 " filter="&quot;symbol_color&quot; > 0.300000 AND &quot;symbol_color&quot; &lt;= 0.350000" symbol="7" key="{30f233e1-b867-4b1f-b403-852ba2c917f4}"/>
      <rule label="0,35 - 0,4 " filter="&quot;symbol_color&quot; > 0.350000 AND &quot;symbol_color&quot; &lt;= 0.400000" symbol="8" key="{47805e87-9c5e-4b8f-afc4-14ab97e57eb6}"/>
      <rule label="0,4 - 0,45 " filter="&quot;symbol_color&quot; > 0.400000 AND &quot;symbol_color&quot; &lt;= 0.450000" symbol="9" key="{e062e89b-23bd-445f-bd48-6cf9324caf0d}"/>
      <rule label="0,45 - 0,5 " filter="&quot;symbol_color&quot; > 0.450000 AND &quot;symbol_color&quot; &lt;= 0.500000" symbol="10" key="{1248ee3f-e339-4100-a25d-fc18492e439e}"/>
      <rule label="0,5 - 0,55 " filter="&quot;symbol_color&quot; > 0.500000 AND &quot;symbol_color&quot; &lt;= 0.550000" symbol="11" key="{dcb8b96b-1fe5-4dd1-bbbc-94a0960e882b}"/>
      <rule label="0,55 - 0,6 " filter="&quot;symbol_color&quot; > 0.550000 AND &quot;symbol_color&quot; &lt;= 0.600000" symbol="12" key="{b714e4a4-b470-46ae-9278-2a99d3f401c5}"/>
      <rule label="0,6 - 0,65 " filter="&quot;symbol_color&quot; > 0.600000 AND &quot;symbol_color&quot; &lt;= 0.650000" symbol="13" key="{80c2b791-53d7-4665-866b-20411932e884}"/>
      <rule label="0,65 - 0,7 " filter="&quot;symbol_color&quot; > 0.650000 AND &quot;symbol_color&quot; &lt;= 0.700000" symbol="14" key="{2b68fd0c-3c93-4999-ac88-cbfe683249ec}"/>
      <rule label="0,7 - 0,75 " filter="&quot;symbol_color&quot; > 0.700000 AND &quot;symbol_color&quot; &lt;= 0.750000" symbol="15" key="{e75e1413-e1f8-4fb7-8547-bdfb7eff4fbb}"/>
      <rule label="0,75 - 0,8 " filter="&quot;symbol_color&quot; > 0.750000 AND &quot;symbol_color&quot; &lt;= 0.800000" symbol="16" key="{322c8d0d-c941-42cc-b924-7acb0047b974}"/>
      <rule label="0,8 - 0,85 " filter="&quot;symbol_color&quot; > 0.800000 AND &quot;symbol_color&quot; &lt;= 0.850000" symbol="17" key="{8d266973-a239-42ec-b558-1971b28aed3e}"/>
      <rule label="0,85 - 0,9 " filter="&quot;symbol_color&quot; > 0.850000 AND &quot;symbol_color&quot; &lt;= 0.900000" symbol="18" key="{1026c18e-c31a-4d22-967e-afe4b2dd3ef8}"/>
      <rule label="0,9 - 0,95 " filter="&quot;symbol_color&quot; > 0.900000 AND &quot;symbol_color&quot; &lt;= 0.950000" symbol="19" key="{1034c3e8-0d4c-49ec-8dd8-2b6b40c3299b}"/>
      <rule label="0,95 - 1 " filter="&quot;symbol_color&quot; > 0.950000 AND &quot;symbol_color&quot; &lt;= 1.000000" symbol="20" key="{f12b6082-bc98-4da4-a2ac-d458b0fb194e}"/>
    </rules>
    <symbols>
      <symbol name="0" force_rhr="0" type="line" clip_to_extent="1" alpha="1">
        <layer pass="0" class="SimpleLine" enabled="1" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="0,0,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="1" force_rhr="0" type="line" clip_to_extent="1" alpha="1">
        <layer pass="0" class="SimpleLine" enabled="1" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="43,131,186,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="10" force_rhr="0" type="line" clip_to_extent="1" alpha="1">
        <layer pass="0" class="SimpleLine" enabled="1" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="247,252,188,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="11" force_rhr="0" type="line" clip_to_extent="1" alpha="1">
        <layer pass="0" class="SimpleLine" enabled="1" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,247,181,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="12" force_rhr="0" type="line" clip_to_extent="1" alpha="1">
        <layer pass="0" class="SimpleLine" enabled="1" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,230,161,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="13" force_rhr="0" type="line" clip_to_extent="1" alpha="1">
        <layer pass="0" class="SimpleLine" enabled="1" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="254,213,142,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="14" force_rhr="0" type="line" clip_to_extent="1" alpha="1">
        <layer pass="0" class="SimpleLine" enabled="1" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="254,196,122,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="15" force_rhr="0" type="line" clip_to_extent="1" alpha="1">
        <layer pass="0" class="SimpleLine" enabled="1" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="254,178,102,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="16" force_rhr="0" type="line" clip_to_extent="1" alpha="1">
        <layer pass="0" class="SimpleLine" enabled="1" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="247,151,86,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="17" force_rhr="0" type="line" clip_to_extent="1" alpha="1">
        <layer pass="0" class="SimpleLine" enabled="1" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="239,119,71,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="18" force_rhr="0" type="line" clip_to_extent="1" alpha="1">
        <layer pass="0" class="SimpleLine" enabled="1" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="231,88,57,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="19" force_rhr="0" type="line" clip_to_extent="1" alpha="1">
        <layer pass="0" class="SimpleLine" enabled="1" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="223,56,42,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="2" force_rhr="0" type="line" clip_to_extent="1" alpha="1">
        <layer pass="0" class="SimpleLine" enabled="1" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="70,150,182,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="20" force_rhr="0" type="line" clip_to_extent="1" alpha="1">
        <layer pass="0" class="SimpleLine" enabled="1" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="215,25,28,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="3" force_rhr="0" type="line" clip_to_extent="1" alpha="1">
        <layer pass="0" class="SimpleLine" enabled="1" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="97,169,177,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="4" force_rhr="0" type="line" clip_to_extent="1" alpha="1">
        <layer pass="0" class="SimpleLine" enabled="1" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="124,188,172,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="5" force_rhr="0" type="line" clip_to_extent="1" alpha="1">
        <layer pass="0" class="SimpleLine" enabled="1" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="151,207,168,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="6" force_rhr="0" type="line" clip_to_extent="1" alpha="1">
        <layer pass="0" class="SimpleLine" enabled="1" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="176,223,166,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="7" force_rhr="0" type="line" clip_to_extent="1" alpha="1">
        <layer pass="0" class="SimpleLine" enabled="1" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="193,230,171,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="8" force_rhr="0" type="line" clip_to_extent="1" alpha="1">
        <layer pass="0" class="SimpleLine" enabled="1" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="211,238,177,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="9" force_rhr="0" type="line" clip_to_extent="1" alpha="1">
        <layer pass="0" class="SimpleLine" enabled="1" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="229,245,183,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>id</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory maxScaleDenominator="1e+08" height="15" minScaleDenominator="0" backgroundAlpha="255" labelPlacementMethod="XHeight" penColor="#000000" scaleBasedVisibility="0" scaleDependency="Area" penWidth="0" minimumSize="0" lineSizeType="MM" opacity="1" enabled="0" barWidth="5" backgroundColor="#ffffff" penAlpha="255" sizeType="MM" sizeScale="3x:0,0,0,0,0,0" width="15" diagramOrientation="Up" lineSizeScale="3x:0,0,0,0,0,0" rotationOffset="270">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute label="" color="#000000" field=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" zIndex="0" placement="2" showAll="1" dist="0" linePlacementFlags="18" priority="0">
    <properties>
      <Option type="Map">
        <Option name="name" type="QString" value=""/>
        <Option name="properties"/>
        <Option name="type" type="QString" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="query_id">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option name="AllowNull" type="bool" value="true"/>
            <Option name="Max" type="int" value="2147483647"/>
            <Option name="Min" type="int" value="-2147483648"/>
            <Option name="Precision" type="int" value="0"/>
            <Option name="Step" type="int" value="1"/>
            <Option name="Style" type="QString" value="SpinBox"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="root_node">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="root_mode">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option name="AllowMulti" type="bool" value="false"/>
            <Option name="AllowNull" type="bool" value="false"/>
            <Option name="FilterExpression" type="QString" value=""/>
            <Option name="Key" type="QString" value="id"/>
            <Option name="Layer" type="QString" value="Modes_de_transport_b0c2590b_19f6_4b8e_9135_1c1c9f498ff1"/>
            <Option name="LayerName" type="QString" value="Modes de transport"/>
            <Option name="NofColumns" type="int" value="1"/>
            <Option name="OrderByValue" type="bool" value="false"/>
            <Option name="UseCompleter" type="bool" value="false"/>
            <Option name="Value" type="QString" value="name"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="root_timestamp">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option name="allow_null" type="bool" value="true"/>
            <Option name="calendar_popup" type="bool" value="true"/>
            <Option name="display_format" type="QString" value="yyyy-MM-dd HH:mm:ss"/>
            <Option name="field_format" type="QString" value="yyyy-MM-dd HH:mm:ss"/>
            <Option name="field_iso_format" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="arc_id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pred_node_id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="node_id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pred_cost">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="cost">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="departure_time">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option name="allow_null" type="bool" value="true"/>
            <Option name="calendar_popup" type="bool" value="true"/>
            <Option name="display_format" type="QString" value="HH:mm:ss"/>
            <Option name="field_format" type="QString" value="HH:mm:ss"/>
            <Option name="field_iso_format" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="arrival_time">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option name="allow_null" type="bool" value="true"/>
            <Option name="calendar_popup" type="bool" value="true"/>
            <Option name="display_format" type="QString" value="HH:mm:ss"/>
            <Option name="field_format" type="QString" value="HH:mm:ss"/>
            <Option name="field_iso_format" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="total_bicycle_time">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option name="allow_null" type="bool" value="true"/>
            <Option name="calendar_popup" type="bool" value="true"/>
            <Option name="display_format" type="QString" value="HH:mm:ss"/>
            <Option name="field_format" type="QString" value="HH:mm:ss"/>
            <Option name="field_iso_format" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="total_walking_time">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option name="allow_null" type="bool" value="true"/>
            <Option name="calendar_popup" type="bool" value="true"/>
            <Option name="display_format" type="QString" value="HH:mm:ss"/>
            <Option name="field_format" type="QString" value="HH:mm:ss"/>
            <Option name="field_iso_format" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="total_mode_changes">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option name="AllowNull" type="bool" value="true"/>
            <Option name="Max" type="int" value="2147483647"/>
            <Option name="Min" type="int" value="-2147483648"/>
            <Option name="Precision" type="int" value="0"/>
            <Option name="Step" type="int" value="1"/>
            <Option name="Style" type="QString" value="SpinBox"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="total_pt_transfers">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option name="AllowNull" type="bool" value="true"/>
            <Option name="Max" type="int" value="2147483647"/>
            <Option name="Min" type="int" value="-2147483648"/>
            <Option name="Precision" type="int" value="0"/>
            <Option name="Step" type="int" value="1"/>
            <Option name="Style" type="QString" value="SpinBox"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="total_fare">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="length">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option name="AllowNull" type="bool" value="true"/>
            <Option name="Max" type="double" value="1e+07"/>
            <Option name="Min" type="double" value="0"/>
            <Option name="Precision" type="int" value="2"/>
            <Option name="Step" type="double" value="1"/>
            <Option name="Style" type="QString" value="SpinBox"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="crossability">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option name="Dans les 2 sens" type="QString" value="0"/>
              </Option>
              <Option type="Map">
                <Option name="De la gauche vers la droite" type="QString" value="1"/>
              </Option>
              <Option type="Map">
                <Option name="De la droite vers la gauche" type="QString" value="2"/>
              </Option>
              <Option type="Map">
                <Option name="Non traversable" type="QString" value="3"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="diffusion">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option name="Pas de diffusion" type="QString" value="0"/>
              </Option>
              <Option type="Map">
                <Option name="Diffusion à droite de l'arc" type="QString" value="1"/>
              </Option>
              <Option type="Map">
                <Option name="Diffusion à gauche de l'arc" type="QString" value="2"/>
              </Option>
              <Option type="Map">
                <Option name="Diffusion des 2 côtés" type="QString" value="3"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="symbol_color">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="false"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="ID" field="id"/>
    <alias index="1" name="ID requête" field="query_id"/>
    <alias index="2" name="Noeud racine" field="root_node"/>
    <alias index="3" name="Mode racine" field="root_mode"/>
    <alias index="4" name="Date-Heure racine" field="root_timestamp"/>
    <alias index="5" name="ID arc" field="arc_id"/>
    <alias index="6" name="Noeud précédent" field="pred_node_id"/>
    <alias index="7" name="Noeud" field="node_id"/>
    <alias index="8" name="Coût précédent" field="pred_cost"/>
    <alias index="9" name="Coût" field="cost"/>
    <alias index="10" name="Heure départ" field="departure_time"/>
    <alias index="11" name="Heure arrivée" field="arrival_time"/>
    <alias index="12" name="Tps vélo cumulé" field="total_bicycle_time"/>
    <alias index="13" name="Tps marche cumulé" field="total_walking_time"/>
    <alias index="14" name="Changements mode" field="total_mode_changes"/>
    <alias index="15" name="Correspondances TC" field="total_pt_transfers"/>
    <alias index="16" name="Tarif cumulé" field="total_fare"/>
    <alias index="17" name="Longueur (m)" field="length"/>
    <alias index="18" name="Traversabilité" field="crossability"/>
    <alias index="19" name="Diffusion" field="diffusion"/>
    <alias index="20" name="" field="symbol_color"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" applyOnUpdate="0" field="id"/>
    <default expression="" applyOnUpdate="0" field="query_id"/>
    <default expression="" applyOnUpdate="0" field="root_node"/>
    <default expression="" applyOnUpdate="0" field="root_mode"/>
    <default expression="" applyOnUpdate="0" field="root_timestamp"/>
    <default expression="" applyOnUpdate="0" field="arc_id"/>
    <default expression="" applyOnUpdate="0" field="pred_node_id"/>
    <default expression="" applyOnUpdate="0" field="node_id"/>
    <default expression="" applyOnUpdate="0" field="pred_cost"/>
    <default expression="" applyOnUpdate="0" field="cost"/>
    <default expression="" applyOnUpdate="0" field="departure_time"/>
    <default expression="" applyOnUpdate="0" field="arrival_time"/>
    <default expression="" applyOnUpdate="0" field="total_bicycle_time"/>
    <default expression="" applyOnUpdate="0" field="total_walking_time"/>
    <default expression="" applyOnUpdate="0" field="total_mode_changes"/>
    <default expression="" applyOnUpdate="0" field="total_pt_transfers"/>
    <default expression="" applyOnUpdate="0" field="total_fare"/>
    <default expression="" applyOnUpdate="0" field="length"/>
    <default expression="" applyOnUpdate="0" field="crossability"/>
    <default expression="" applyOnUpdate="0" field="diffusion"/>
    <default expression="" applyOnUpdate="0" field="symbol_color"/>
  </defaults>
  <constraints>
    <constraint notnull_strength="1" unique_strength="1" exp_strength="0" constraints="3" field="id"/>
    <constraint notnull_strength="0" unique_strength="0" exp_strength="0" constraints="0" field="query_id"/>
    <constraint notnull_strength="0" unique_strength="0" exp_strength="0" constraints="0" field="root_node"/>
    <constraint notnull_strength="0" unique_strength="0" exp_strength="0" constraints="0" field="root_mode"/>
    <constraint notnull_strength="0" unique_strength="0" exp_strength="0" constraints="0" field="root_timestamp"/>
    <constraint notnull_strength="0" unique_strength="0" exp_strength="0" constraints="0" field="arc_id"/>
    <constraint notnull_strength="0" unique_strength="0" exp_strength="0" constraints="0" field="pred_node_id"/>
    <constraint notnull_strength="0" unique_strength="0" exp_strength="0" constraints="0" field="node_id"/>
    <constraint notnull_strength="0" unique_strength="0" exp_strength="0" constraints="0" field="pred_cost"/>
    <constraint notnull_strength="0" unique_strength="0" exp_strength="0" constraints="0" field="cost"/>
    <constraint notnull_strength="0" unique_strength="0" exp_strength="0" constraints="0" field="departure_time"/>
    <constraint notnull_strength="0" unique_strength="0" exp_strength="0" constraints="0" field="arrival_time"/>
    <constraint notnull_strength="0" unique_strength="0" exp_strength="0" constraints="0" field="total_bicycle_time"/>
    <constraint notnull_strength="0" unique_strength="0" exp_strength="0" constraints="0" field="total_walking_time"/>
    <constraint notnull_strength="0" unique_strength="0" exp_strength="0" constraints="0" field="total_mode_changes"/>
    <constraint notnull_strength="0" unique_strength="0" exp_strength="0" constraints="0" field="total_pt_transfers"/>
    <constraint notnull_strength="0" unique_strength="0" exp_strength="0" constraints="0" field="total_fare"/>
    <constraint notnull_strength="0" unique_strength="0" exp_strength="0" constraints="0" field="length"/>
    <constraint notnull_strength="0" unique_strength="0" exp_strength="0" constraints="0" field="crossability"/>
    <constraint notnull_strength="0" unique_strength="0" exp_strength="0" constraints="0" field="diffusion"/>
    <constraint notnull_strength="0" unique_strength="0" exp_strength="0" constraints="0" field="symbol_color"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="id"/>
    <constraint desc="" exp="" field="query_id"/>
    <constraint desc="" exp="" field="root_node"/>
    <constraint desc="" exp="" field="root_mode"/>
    <constraint desc="" exp="" field="root_timestamp"/>
    <constraint desc="" exp="" field="arc_id"/>
    <constraint desc="" exp="" field="pred_node_id"/>
    <constraint desc="" exp="" field="node_id"/>
    <constraint desc="" exp="" field="pred_cost"/>
    <constraint desc="" exp="" field="cost"/>
    <constraint desc="" exp="" field="departure_time"/>
    <constraint desc="" exp="" field="arrival_time"/>
    <constraint desc="" exp="" field="total_bicycle_time"/>
    <constraint desc="" exp="" field="total_walking_time"/>
    <constraint desc="" exp="" field="total_mode_changes"/>
    <constraint desc="" exp="" field="total_pt_transfers"/>
    <constraint desc="" exp="" field="total_fare"/>
    <constraint desc="" exp="" field="length"/>
    <constraint desc="" exp="" field="crossability"/>
    <constraint desc="" exp="" field="diffusion"/>
    <constraint desc="" exp="" field="symbol_color"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="&quot;root_mode&quot;" actionWidgetStyle="dropDown" sortOrder="0">
    <columns>
      <column name="id" type="field" width="39" hidden="1"/>
      <column name="query_id" type="field" width="59" hidden="0"/>
      <column name="root_node" type="field" width="79" hidden="0"/>
      <column name="root_mode" type="field" width="71" hidden="0"/>
      <column name="root_timestamp" type="field" width="123" hidden="0"/>
      <column name="arc_id" type="field" width="44" hidden="0"/>
      <column name="pred_node_id" type="field" width="100" hidden="0"/>
      <column name="node_id" type="field" width="48" hidden="0"/>
      <column name="length" type="field" width="87" hidden="0"/>
      <column name="pred_cost" type="field" width="87" hidden="0"/>
      <column name="cost" type="field" width="41" hidden="0"/>
      <column name="departure_time" type="field" width="75" hidden="0"/>
      <column name="arrival_time" type="field" width="82" hidden="0"/>
      <column name="total_bicycle_time" type="field" width="97" hidden="0"/>
      <column name="total_walking_time" type="field" width="116" hidden="0"/>
      <column name="total_mode_changes" type="field" width="117" hidden="0"/>
      <column name="total_pt_transfers" type="field" width="118" hidden="0"/>
      <column name="total_fare" type="field" width="72" hidden="0"/>
      <column name="crossability" type="field" width="100" hidden="0"/>
      <column name="diffusion" type="field" width="107" hidden="0"/>
      <column name="symbol_color" type="field" width="-1" hidden="1"/>
      <column type="actions" width="-1" hidden="1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="arc_id"/>
    <field editable="1" name="arrival_time"/>
    <field editable="1" name="automaton_state"/>
    <field editable="1" name="cost"/>
    <field editable="1" name="crossability"/>
    <field editable="1" name="departure_time"/>
    <field editable="1" name="diffusion"/>
    <field editable="1" name="id"/>
    <field editable="1" name="in_paths"/>
    <field editable="1" name="length"/>
    <field editable="1" name="node_id"/>
    <field editable="1" name="obj_id"/>
    <field editable="1" name="pred_cost"/>
    <field editable="1" name="pred_node_id"/>
    <field editable="1" name="pred_obj_id"/>
    <field editable="1" name="pt_trip_id"/>
    <field editable="1" name="query_id"/>
    <field editable="1" name="root_mode"/>
    <field editable="1" name="root_node"/>
    <field editable="1" name="root_timestamp"/>
    <field editable="1" name="symbol_color"/>
    <field editable="1" name="total_bicycle_time"/>
    <field editable="1" name="total_fare"/>
    <field editable="1" name="total_mode_changes"/>
    <field editable="1" name="total_pt_transfers"/>
    <field editable="1" name="total_walking_time"/>
    <field editable="1" name="transport_mode_id"/>
    <field editable="1" name="waiting_time"/>
  </editable>
  <labelOnTop>
    <field name="arc_id" labelOnTop="0"/>
    <field name="arrival_time" labelOnTop="0"/>
    <field name="automaton_state" labelOnTop="0"/>
    <field name="cost" labelOnTop="0"/>
    <field name="crossability" labelOnTop="0"/>
    <field name="departure_time" labelOnTop="0"/>
    <field name="diffusion" labelOnTop="0"/>
    <field name="id" labelOnTop="0"/>
    <field name="in_paths" labelOnTop="0"/>
    <field name="length" labelOnTop="0"/>
    <field name="node_id" labelOnTop="0"/>
    <field name="obj_id" labelOnTop="0"/>
    <field name="pred_cost" labelOnTop="0"/>
    <field name="pred_node_id" labelOnTop="0"/>
    <field name="pred_obj_id" labelOnTop="0"/>
    <field name="pt_trip_id" labelOnTop="0"/>
    <field name="query_id" labelOnTop="0"/>
    <field name="root_mode" labelOnTop="0"/>
    <field name="root_node" labelOnTop="0"/>
    <field name="root_timestamp" labelOnTop="0"/>
    <field name="symbol_color" labelOnTop="0"/>
    <field name="total_bicycle_time" labelOnTop="0"/>
    <field name="total_fare" labelOnTop="0"/>
    <field name="total_mode_changes" labelOnTop="0"/>
    <field name="total_pt_transfers" labelOnTop="0"/>
    <field name="total_walking_time" labelOnTop="0"/>
    <field name="transport_mode_id" labelOnTop="0"/>
    <field name="waiting_time" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
