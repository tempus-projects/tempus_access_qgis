<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis maxScale="100000" simplifyDrawingTol="1" simplifyMaxScale="1" labelsEnabled="0" readOnly="0" version="3.10.6-A Coruña" simplifyLocal="0" simplifyAlgorithm="0" hasScaleBasedVisibilityFlag="0" styleCategories="AllStyleCategories" minScale="1e+08" simplifyDrawingHints="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 symbollevels="0" type="singleSymbol" enableorderby="0" forceraster="0">
    <symbols>
      <symbol alpha="1" type="line" clip_to_extent="1" name="0" force_rhr="0">
        <layer class="SimpleLine" locked="0" enabled="1" pass="0">
          <prop v="square" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="116,116,116,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="0.8" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer class="MarkerLine" locked="0" enabled="1" pass="0">
          <prop v="4" k="average_angle_length"/>
          <prop v="3x:0,0,0,0,0,0" k="average_angle_map_unit_scale"/>
          <prop v="MM" k="average_angle_unit"/>
          <prop v="3" k="interval"/>
          <prop v="3x:0,0,0,0,0,0" k="interval_map_unit_scale"/>
          <prop v="MM" k="interval_unit"/>
          <prop v="0" k="offset"/>
          <prop v="0" k="offset_along_line"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_along_line_map_unit_scale"/>
          <prop v="MM" k="offset_along_line_unit"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="centralpoint" k="placement"/>
          <prop v="0" k="ring_filter"/>
          <prop v="1" k="rotate"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol alpha="1" type="marker" clip_to_extent="1" name="@0@1" force_rhr="0">
            <layer class="SimpleMarker" locked="0" enabled="1" pass="0">
              <prop v="0" k="angle"/>
              <prop v="116,116,116,255" k="color"/>
              <prop v="1" k="horizontal_anchor_point"/>
              <prop v="bevel" k="joinstyle"/>
              <prop v="filled_arrowhead" k="name"/>
              <prop v="0,0" k="offset"/>
              <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
              <prop v="MM" k="offset_unit"/>
              <prop v="116,116,116,255" k="outline_color"/>
              <prop v="solid" k="outline_style"/>
              <prop v="0" k="outline_width"/>
              <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
              <prop v="MM" k="outline_width_unit"/>
              <prop v="diameter" k="scale_method"/>
              <prop v="3" k="size"/>
              <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
              <prop v="MM" k="size_unit"/>
              <prop v="1" k="vertical_anchor_point"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>id</value>
    </property>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory enabled="0" diagramOrientation="Up" penWidth="0" barWidth="5" lineSizeScale="3x:0,0,0,0,0,0" rotationOffset="270" width="15" height="15" opacity="1" minScaleDenominator="100000" maxScaleDenominator="1e+08" backgroundColor="#ffffff" sizeScale="3x:0,0,0,0,0,0" penAlpha="255" penColor="#000000" lineSizeType="MM" sizeType="MM" scaleDependency="Area" backgroundAlpha="255" labelPlacementMethod="XHeight" minimumSize="0" scaleBasedVisibility="0">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute field="" color="#000000" label=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" placement="2" dist="0" priority="0" zIndex="0" showAll="1" linePlacementFlags="2">
    <properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option type="bool" value="true" name="AllowNull"/>
            <Option type="int" value="2147483647" name="Max"/>
            <Option type="int" value="-2147483648" name="Min"/>
            <Option type="int" value="0" name="Precision"/>
            <Option type="int" value="1" name="Step"/>
            <Option type="QString" value="SpinBox" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="source_id">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="AllowMulti"/>
            <Option type="bool" value="false" name="AllowNull"/>
            <Option type="QString" value="" name="FilterExpression"/>
            <Option type="QString" value="id" name="Key"/>
            <Option type="QString" value="Sources_de_données_7fbdbd0f_141c_4a24_8f9a_8b470c08bf8d" name="Layer"/>
            <Option type="QString" value="Sources de données" name="LayerName"/>
            <Option type="int" value="1" name="NofColumns"/>
            <Option type="bool" value="false" name="OrderByValue"/>
            <Option type="bool" value="false" name="UseCompleter"/>
            <Option type="QString" value="name" name="Value"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="node_from_id">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="AllowMulti"/>
            <Option type="bool" value="false" name="AllowNull"/>
            <Option type="QString" value="" name="FilterExpression"/>
            <Option type="QString" value="id" name="Key"/>
            <Option type="QString" value="Arrêts_d56c0d5d_3294_4ff3_a6d9_e753b47d1673" name="Layer"/>
            <Option type="QString" value="Arrêts" name="LayerName"/>
            <Option type="QString" value="postgres" name="LayerProviderName"/>
            <Option type="QString" value="dbname='tempusaccess_corse' host=localhost port=55432 user='aurelie-p.bousquet' key='id' checkPrimaryKeyUnicity='1' table=&quot;tempus_networks&quot;.&quot;pt_stop_geom&quot; (pt_stop_geom) sql=(source_id = ANY(ARRAY[1]::smallint[]))&#xa; AND (traffic_rules IS NULL OR traffic_rules &amp;&amp; (SELECT array_agg(traffic_rule) FROM pgtempus.transport_mode WHERE traffic_rule IS NOT NULL AND id = ANY(ARRAY[1, 3, 4, 5, 6]::smallint[])))&#xa;" name="LayerSource"/>
            <Option type="int" value="1" name="NofColumns"/>
            <Option type="bool" value="false" name="OrderByValue"/>
            <Option type="bool" value="false" name="UseCompleter"/>
            <Option type="QString" value="name" name="Value"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="node_to_id">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="AllowMulti"/>
            <Option type="bool" value="false" name="AllowNull"/>
            <Option type="QString" value="" name="FilterExpression"/>
            <Option type="QString" value="id" name="Key"/>
            <Option type="QString" value="Arrêts_d56c0d5d_3294_4ff3_a6d9_e753b47d1673" name="Layer"/>
            <Option type="QString" value="Arrêts" name="LayerName"/>
            <Option type="QString" value="postgres" name="LayerProviderName"/>
            <Option type="QString" value="dbname='tempusaccess_corse' host=localhost port=55432 user='aurelie-p.bousquet' key='id' checkPrimaryKeyUnicity='1' table=&quot;tempus_networks&quot;.&quot;pt_stop_geom&quot; (pt_stop_geom) sql=(source_id = ANY(ARRAY[1]::smallint[]))&#xa; AND (traffic_rules IS NULL OR traffic_rules &amp;&amp; (SELECT array_agg(traffic_rule) FROM pgtempus.transport_mode WHERE traffic_rule IS NOT NULL AND id = ANY(ARRAY[1, 3, 4, 5, 6]::smallint[])))&#xa;" name="LayerSource"/>
            <Option type="int" value="1" name="NofColumns"/>
            <Option type="bool" value="false" name="OrderByValue"/>
            <Option type="bool" value="false" name="UseCompleter"/>
            <Option type="QString" value="name" name="Value"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="length">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="sub_arcs">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="diffusion">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option type="QString" value="0" name="Pas de diffusion"/>
              </Option>
              <Option type="Map">
                <Option type="QString" value="1" name="Sur la droite"/>
              </Option>
              <Option type="Map">
                <Option type="QString" value="2" name="Sur la gauche"/>
              </Option>
              <Option type="Map">
                <Option type="QString" value="3" name="Des deux côtés"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="crossability">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option type="QString" value="0" name="Depuis les deux côtés"/>
              </Option>
              <Option type="Map">
                <Option type="QString" value="1" name="De gauche à droite"/>
              </Option>
              <Option type="Map">
                <Option type="QString" value="2" name="De droite à gauche"/>
              </Option>
              <Option type="Map">
                <Option type="QString" value="3" name="Intraversable"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="traffic_rules">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id" index="0" name="ID"/>
    <alias field="source_id" index="1" name="Source données"/>
    <alias field="node_from_id" index="2" name="Arrêt orig"/>
    <alias field="node_to_id" index="3" name="Arrêt dest"/>
    <alias field="length" index="4" name="Longueur (m)"/>
    <alias field="sub_arcs" index="5" name="Sous-sections"/>
    <alias field="diffusion" index="6" name="Diffusion"/>
    <alias field="crossability" index="7" name="Traversabilité"/>
    <alias field="traffic_rules" index="8" name="Accès services TC"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default field="id" expression="" applyOnUpdate="0"/>
    <default field="source_id" expression="" applyOnUpdate="0"/>
    <default field="node_from_id" expression="" applyOnUpdate="0"/>
    <default field="node_to_id" expression="" applyOnUpdate="0"/>
    <default field="length" expression="" applyOnUpdate="0"/>
    <default field="sub_arcs" expression="" applyOnUpdate="0"/>
    <default field="diffusion" expression="" applyOnUpdate="0"/>
    <default field="crossability" expression="" applyOnUpdate="0"/>
    <default field="traffic_rules" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint field="id" unique_strength="1" constraints="3" notnull_strength="1" exp_strength="0"/>
    <constraint field="source_id" unique_strength="0" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint field="node_from_id" unique_strength="0" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint field="node_to_id" unique_strength="0" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint field="length" unique_strength="0" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint field="sub_arcs" unique_strength="0" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint field="diffusion" unique_strength="0" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint field="crossability" unique_strength="0" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint field="traffic_rules" unique_strength="0" constraints="0" notnull_strength="0" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id" desc="" exp=""/>
    <constraint field="source_id" desc="" exp=""/>
    <constraint field="node_from_id" desc="" exp=""/>
    <constraint field="node_to_id" desc="" exp=""/>
    <constraint field="length" desc="" exp=""/>
    <constraint field="sub_arcs" desc="" exp=""/>
    <constraint field="diffusion" desc="" exp=""/>
    <constraint field="crossability" desc="" exp=""/>
    <constraint field="traffic_rules" desc="" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortOrder="0" actionWidgetStyle="dropDown" sortExpression="&quot;id&quot;">
    <columns>
      <column hidden="1" type="actions" width="-1"/>
      <column hidden="0" type="field" width="53" name="id"/>
      <column hidden="0" type="field" width="92" name="source_id"/>
      <column hidden="0" type="field" width="146" name="node_from_id"/>
      <column hidden="0" type="field" width="146" name="node_to_id"/>
      <column hidden="0" type="field" width="79" name="length"/>
      <column hidden="0" type="field" width="108" name="sub_arcs"/>
      <column hidden="0" type="field" width="114" name="diffusion"/>
      <column hidden="0" type="field" width="138" name="crossability"/>
      <column hidden="0" type="field" width="-1" name="traffic_rules"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="crossability"/>
    <field editable="1" name="diffusion"/>
    <field editable="1" name="id"/>
    <field editable="1" name="length"/>
    <field editable="1" name="network_id"/>
    <field editable="1" name="network_name"/>
    <field editable="1" name="node_from_id"/>
    <field editable="1" name="node_from_name"/>
    <field editable="1" name="node_to_id"/>
    <field editable="1" name="node_to_name"/>
    <field editable="1" name="pt_agencies_id"/>
    <field editable="1" name="pt_agencies_name"/>
    <field editable="1" name="pt_routes_id"/>
    <field editable="1" name="pt_routes_long_name"/>
    <field editable="1" name="pt_routes_short_name"/>
    <field editable="1" name="pt_trip_types_id"/>
    <field editable="1" name="pt_trip_types_name"/>
    <field editable="1" name="source_id"/>
    <field editable="1" name="sub_arcs"/>
    <field editable="1" name="sub_edges"/>
    <field editable="1" name="traffic_rules"/>
    <field editable="1" name="traffic_rules_id"/>
    <field editable="1" name="traffic_rules_name"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="crossability"/>
    <field labelOnTop="0" name="diffusion"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="length"/>
    <field labelOnTop="0" name="network_id"/>
    <field labelOnTop="0" name="network_name"/>
    <field labelOnTop="0" name="node_from_id"/>
    <field labelOnTop="0" name="node_from_name"/>
    <field labelOnTop="0" name="node_to_id"/>
    <field labelOnTop="0" name="node_to_name"/>
    <field labelOnTop="0" name="pt_agencies_id"/>
    <field labelOnTop="0" name="pt_agencies_name"/>
    <field labelOnTop="0" name="pt_routes_id"/>
    <field labelOnTop="0" name="pt_routes_long_name"/>
    <field labelOnTop="0" name="pt_routes_short_name"/>
    <field labelOnTop="0" name="pt_trip_types_id"/>
    <field labelOnTop="0" name="pt_trip_types_name"/>
    <field labelOnTop="0" name="source_id"/>
    <field labelOnTop="0" name="sub_arcs"/>
    <field labelOnTop="0" name="sub_edges"/>
    <field labelOnTop="0" name="traffic_rules"/>
    <field labelOnTop="0" name="traffic_rules_id"/>
    <field labelOnTop="0" name="traffic_rules_name"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id</previewExpression>
  <mapTip>stop_id1</mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
