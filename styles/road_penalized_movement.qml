<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0" simplifyDrawingTol="1" simplifyLocal="1" maxScale="0" simplifyAlgorithm="0" minScale="1e+08" labelsEnabled="0" simplifyDrawingHints="1" version="3.10.6-A Coruña" simplifyMaxScale="1" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 type="singleSymbol" symbollevels="0" forceraster="0" enableorderby="0">
    <symbols>
      <symbol alpha="1" force_rhr="0" type="line" name="0" clip_to_extent="1">
        <layer enabled="1" pass="0" class="SimpleLine" locked="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="221,93,140,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.66"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer enabled="1" pass="0" class="MarkerLine" locked="0">
          <prop k="average_angle_length" v="4"/>
          <prop k="average_angle_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="average_angle_unit" v="MM"/>
          <prop k="interval" v="3"/>
          <prop k="interval_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="interval_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_along_line" v="0"/>
          <prop k="offset_along_line_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_along_line_unit" v="MM"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="placement" v="centralpoint"/>
          <prop k="ring_filter" v="0"/>
          <prop k="rotate" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol alpha="1" force_rhr="0" type="marker" name="@0@1" clip_to_extent="1">
            <layer enabled="1" pass="0" class="SimpleMarker" locked="0">
              <prop k="angle" v="0"/>
              <prop k="color" v="221,93,140,255"/>
              <prop k="horizontal_anchor_point" v="1"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="name" v="filled_arrowhead"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="221,93,140,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="scale_method" v="diameter"/>
              <prop k="size" v="2"/>
              <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="size_unit" v="MM"/>
              <prop k="vertical_anchor_point" v="1"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option value="" type="QString" name="name"/>
                  <Option name="properties"/>
                  <Option value="collection" type="QString" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>id</value>
    </property>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory enabled="0" penColor="#000000" scaleDependency="Area" height="15" labelPlacementMethod="XHeight" sizeScale="3x:0,0,0,0,0,0" minScaleDenominator="0" opacity="1" backgroundAlpha="255" backgroundColor="#ffffff" rotationOffset="270" lineSizeScale="3x:0,0,0,0,0,0" width="15" scaleBasedVisibility="0" maxScaleDenominator="1e+08" sizeType="MM" penAlpha="255" barWidth="5" penWidth="0" minimumSize="0" lineSizeType="MM" diagramOrientation="Up">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute label="" field="" color="#000000"/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" dist="0" placement="2" zIndex="0" linePlacementFlags="18" priority="0" showAll="1">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="original_id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="sources_id">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="arcs_id">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="traffic_rules">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="time_value">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="2147483647" type="int" name="Max"/>
            <Option value="-2147483648" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="toll_rules">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="toll_value">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" field="id" name="ID"/>
    <alias index="1" field="original_id" name="ID initial"/>
    <alias index="2" field="sources_id" name="Sources données"/>
    <alias index="3" field="arcs_id" name="Liste arcs"/>
    <alias index="4" field="traffic_rules" name="Règles circulation"/>
    <alias index="5" field="time_value" name="Pénalité temporelle (s)"/>
    <alias index="6" field="toll_rules" name="Règles péage"/>
    <alias index="7" field="toll_value" name="Péage (€)"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default field="id" applyOnUpdate="0" expression=""/>
    <default field="original_id" applyOnUpdate="0" expression=""/>
    <default field="sources_id" applyOnUpdate="0" expression=""/>
    <default field="arcs_id" applyOnUpdate="0" expression=""/>
    <default field="traffic_rules" applyOnUpdate="0" expression=""/>
    <default field="time_value" applyOnUpdate="0" expression=""/>
    <default field="toll_rules" applyOnUpdate="0" expression=""/>
    <default field="toll_value" applyOnUpdate="0" expression=""/>
  </defaults>
  <constraints>
    <constraint unique_strength="1" field="id" notnull_strength="1" exp_strength="0" constraints="3"/>
    <constraint unique_strength="0" field="original_id" notnull_strength="0" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="sources_id" notnull_strength="0" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="arcs_id" notnull_strength="0" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="traffic_rules" notnull_strength="0" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="time_value" notnull_strength="0" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="toll_rules" notnull_strength="0" exp_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="toll_value" notnull_strength="0" exp_strength="0" constraints="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" desc="" field="id"/>
    <constraint exp="" desc="" field="original_id"/>
    <constraint exp="" desc="" field="sources_id"/>
    <constraint exp="" desc="" field="arcs_id"/>
    <constraint exp="" desc="" field="traffic_rules"/>
    <constraint exp="" desc="" field="time_value"/>
    <constraint exp="" desc="" field="toll_rules"/>
    <constraint exp="" desc="" field="toll_value"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="&quot;id&quot;">
    <columns>
      <column hidden="0" type="field" name="id" width="35"/>
      <column hidden="1" type="field" name="original_id" width="-1"/>
      <column hidden="0" type="field" name="sources_id" width="97"/>
      <column hidden="0" type="field" name="arcs_id" width="100"/>
      <column hidden="0" type="field" name="traffic_rules" width="102"/>
      <column hidden="0" type="field" name="time_value" width="133"/>
      <column hidden="0" type="field" name="toll_rules" width="88"/>
      <column hidden="0" type="field" name="toll_value" width="64"/>
      <column hidden="1" type="actions" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="arcs_id"/>
    <field editable="1" name="edges_id"/>
    <field editable="1" name="id"/>
    <field editable="1" name="network_id"/>
    <field editable="1" name="original_id"/>
    <field editable="1" name="sources_id"/>
    <field editable="1" name="time"/>
    <field editable="1" name="time_value"/>
    <field editable="1" name="toll"/>
    <field editable="1" name="toll_rule"/>
    <field editable="1" name="toll_rules"/>
    <field editable="1" name="toll_value"/>
    <field editable="1" name="traffic_rules"/>
    <field editable="1" name="traffic_rules_id"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="arcs_id"/>
    <field labelOnTop="0" name="edges_id"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="network_id"/>
    <field labelOnTop="0" name="original_id"/>
    <field labelOnTop="0" name="sources_id"/>
    <field labelOnTop="0" name="time"/>
    <field labelOnTop="0" name="time_value"/>
    <field labelOnTop="0" name="toll"/>
    <field labelOnTop="0" name="toll_rule"/>
    <field labelOnTop="0" name="toll_rules"/>
    <field labelOnTop="0" name="toll_value"/>
    <field labelOnTop="0" name="traffic_rules"/>
    <field labelOnTop="0" name="traffic_rules_id"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
