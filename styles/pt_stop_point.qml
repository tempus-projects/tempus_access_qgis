<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis styleCategories="AllStyleCategories" labelsEnabled="1" simplifyDrawingHints="0" simplifyLocal="1" maxScale="0" readOnly="0" simplifyAlgorithm="0" version="3.10.6-A Coruña" simplifyMaxScale="1" hasScaleBasedVisibilityFlag="0" minScale="1e+08" simplifyDrawingTol="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 forceraster="0" enableorderby="0" symbollevels="0" type="RuleRenderer">
    <rules key="{dc59625e-0d47-4f5c-91cf-1ef218d25b78}">
      <rule symbol="0" key="{7b187616-9664-4e36-8541-6c50236b9e27}" label="Non accessible PMR (ou accessibilité inconnue)" filter="NOT array_contains(boarding_traffic_rules, 2)"/>
      <rule symbol="1" key="{bfd4de8d-036d-4cb4-bdcd-97a09215fccd}" label="Accessible PMR" filter=" array_contains(boarding_traffic_rules, 2)"/>
    </rules>
    <symbols>
      <symbol force_rhr="0" alpha="1" clip_to_extent="1" type="marker" name="0">
        <layer locked="0" enabled="1" class="SimpleMarker" pass="0">
          <prop v="0" k="angle"/>
          <prop v="123,123,123,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="star" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0,0,0,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="4" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" alpha="1" clip_to_extent="1" type="marker" name="1">
        <layer locked="0" enabled="1" class="SimpleMarker" pass="0">
          <prop v="0" k="angle"/>
          <prop v="51,160,44,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="star" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0,0,0,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="4" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style fontStrikeout="0" textOpacity="1" fontWeight="50" fontSizeMapUnitScale="3x:0,0,0,0,0,0" namedStyle="Normal" multilineHeight="1" fontKerning="1" isExpression="0" blendMode="0" textColor="0,0,0,255" fieldName="name" textOrientation="horizontal" previewBkgrdColor="255,255,255,255" fontCapitals="0" fontUnderline="0" fontFamily="MS Shell Dlg 2" fontSize="8" fontSizeUnit="Point" useSubstitutions="0" fontLetterSpacing="0" fontItalic="0" fontWordSpacing="0">
        <text-buffer bufferColor="255,255,255,255" bufferNoFill="1" bufferSize="1" bufferSizeUnits="MM" bufferDraw="0" bufferOpacity="1" bufferBlendMode="0" bufferJoinStyle="128" bufferSizeMapUnitScale="3x:0,0,0,0,0,0"/>
        <background shapeRadiiX="0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeRotation="0" shapeOffsetUnit="MM" shapeType="0" shapeBorderColor="128,128,128,255" shapeFillColor="255,255,255,255" shapeBorderWidthUnit="MM" shapeRotationType="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiUnit="MM" shapeDraw="0" shapeSizeY="0" shapeBlendMode="0" shapeOffsetX="0" shapeSizeUnit="MM" shapeJoinStyle="64" shapeOffsetY="0" shapeRadiiY="0" shapeSizeX="0" shapeOpacity="1" shapeSVGFile="" shapeSizeType="0" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBorderWidth="0">
          <symbol force_rhr="0" alpha="1" clip_to_extent="1" type="marker" name="markerSymbol">
            <layer locked="0" enabled="1" class="SimpleMarker" pass="0">
              <prop v="0" k="angle"/>
              <prop v="125,139,143,255" k="color"/>
              <prop v="1" k="horizontal_anchor_point"/>
              <prop v="bevel" k="joinstyle"/>
              <prop v="circle" k="name"/>
              <prop v="0,0" k="offset"/>
              <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
              <prop v="MM" k="offset_unit"/>
              <prop v="35,35,35,255" k="outline_color"/>
              <prop v="solid" k="outline_style"/>
              <prop v="0" k="outline_width"/>
              <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
              <prop v="MM" k="outline_width_unit"/>
              <prop v="diameter" k="scale_method"/>
              <prop v="2" k="size"/>
              <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
              <prop v="MM" k="size_unit"/>
              <prop v="1" k="vertical_anchor_point"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option value="" type="QString" name="name"/>
                  <Option name="properties"/>
                  <Option value="collection" type="QString" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetAngle="135" shadowOffsetDist="1" shadowUnder="0" shadowDraw="0" shadowOffsetUnit="MM" shadowColor="0,0,0,255" shadowRadius="1.5" shadowScale="100" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowBlendMode="6" shadowOffsetGlobal="1" shadowRadiusUnit="MM" shadowRadiusAlphaOnly="0" shadowOpacity="0.7"/>
        <dd_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </dd_properties>
        <substitutions/>
      </text-style>
      <text-format reverseDirectionSymbol="0" useMaxLineLengthForAutoWrap="1" formatNumbers="0" multilineAlign="3" plussign="0" rightDirectionSymbol=">" autoWrapLength="0" decimals="3" wrapChar="" addDirectionSymbol="0" leftDirectionSymbol="&lt;" placeDirectionSymbol="0"/>
      <placement repeatDistance="0" offsetType="0" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" repeatDistanceUnits="MM" priority="5" centroidInside="0" preserveRotation="1" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" overrunDistanceUnit="MM" geometryGeneratorEnabled="0" xOffset="10" geometryGenerator="" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" offsetUnits="Pixel" dist="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" overrunDistance="0" distMapUnitScale="3x:0,0,0,0,0,0" maxCurvedCharAngleOut="-25" geometryGeneratorType="PointGeometry" yOffset="10" rotationAngle="0" layerType="PointGeometry" quadOffset="2" distUnits="MM" placement="1" maxCurvedCharAngleIn="25" placementFlags="10" centroidWhole="0" fitInPolygonOnly="0"/>
      <rendering zIndex="0" scaleVisibility="1" displayAll="0" scaleMax="5000" maxNumLabels="2000" limitNumLabels="0" fontLimitPixelSize="0" fontMinPixelSize="3" upsidedownLabels="0" scaleMin="0" minFeatureSize="0" fontMaxPixelSize="10000" drawLabels="1" labelPerPart="0" obstacleFactor="1" obstacleType="0" mergeLines="0" obstacle="1"/>
      <dd_properties>
        <Option type="Map">
          <Option value="" type="QString" name="name"/>
          <Option name="properties"/>
          <Option value="collection" type="QString" name="type"/>
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option value="pole_of_inaccessibility" type="QString" name="anchorPoint"/>
          <Option type="Map" name="ddProperties">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
          <Option value="false" type="bool" name="drawToAllParts"/>
          <Option value="0" type="QString" name="enabled"/>
          <Option value="&lt;symbol force_rhr=&quot;0&quot; alpha=&quot;1&quot; clip_to_extent=&quot;1&quot; type=&quot;line&quot; name=&quot;symbol&quot;>&lt;layer locked=&quot;0&quot; enabled=&quot;1&quot; class=&quot;SimpleLine&quot; pass=&quot;0&quot;>&lt;prop v=&quot;square&quot; k=&quot;capstyle&quot;/>&lt;prop v=&quot;5;2&quot; k=&quot;customdash&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;customdash_map_unit_scale&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;customdash_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;draw_inside_polygon&quot;/>&lt;prop v=&quot;bevel&quot; k=&quot;joinstyle&quot;/>&lt;prop v=&quot;60,60,60,255&quot; k=&quot;line_color&quot;/>&lt;prop v=&quot;solid&quot; k=&quot;line_style&quot;/>&lt;prop v=&quot;0.3&quot; k=&quot;line_width&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;line_width_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;offset&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;offset_map_unit_scale&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;offset_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;ring_filter&quot;/>&lt;prop v=&quot;0&quot; k=&quot;use_custom_dash&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;width_map_unit_scale&quot;/>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; type=&quot;QString&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; type=&quot;QString&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" type="QString" name="lineSymbol"/>
          <Option value="0" type="double" name="minLength"/>
          <Option value="3x:0,0,0,0,0,0" type="QString" name="minLengthMapUnitScale"/>
          <Option value="MM" type="QString" name="minLengthUnit"/>
          <Option value="0" type="double" name="offsetFromAnchor"/>
          <Option value="3x:0,0,0,0,0,0" type="QString" name="offsetFromAnchorMapUnitScale"/>
          <Option value="MM" type="QString" name="offsetFromAnchorUnit"/>
          <Option value="0" type="double" name="offsetFromLabel"/>
          <Option value="3x:0,0,0,0,0,0" type="QString" name="offsetFromLabelMapUnitScale"/>
          <Option value="MM" type="QString" name="offsetFromLabelUnit"/>
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>id</value>
    </property>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory penWidth="0" rotationOffset="270" labelPlacementMethod="XHeight" minScaleDenominator="0" lineSizeScale="3x:0,0,0,0,0,0" sizeType="MM" opacity="1" penColor="#000000" sizeScale="3x:0,0,0,0,0,0" scaleDependency="Area" diagramOrientation="Up" backgroundAlpha="255" width="15" backgroundColor="#ffffff" scaleBasedVisibility="0" penAlpha="255" lineSizeType="MM" barWidth="5" height="15" enabled="0" minimumSize="0" maxScaleDenominator="1e+08">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute color="#000000" label="" field=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings zIndex="0" placement="0" priority="0" showAll="1" linePlacementFlags="2" obstacle="0" dist="0">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="2147483647" type="int" name="Max"/>
            <Option value="-2147483648" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="name">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="station_id">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="2147483647" type="int" name="Max"/>
            <Option value="-2147483648" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="station_name">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="source_id">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="AllowMulti"/>
            <Option value="false" type="bool" name="AllowNull"/>
            <Option value="" type="QString" name="FilterExpression"/>
            <Option value="id" type="QString" name="Key"/>
            <Option value="Sources_de_données_45a195e9_9ca3_4c37_af41_3c24c2b2b2dc" type="QString" name="Layer"/>
            <Option value="Sources de données" type="QString" name="LayerName"/>
            <Option value="postgres" type="QString" name="LayerProviderName"/>
            <Option value="dbname='tempusaccess_corse' host=localhost port=55432 user='aurelie-p.bousquet' key='id' checkPrimaryKeyUnicity='1' table=&quot;pgtempus&quot;.&quot;source&quot; sql=" type="QString" name="LayerSource"/>
            <Option value="1" type="int" name="NofColumns"/>
            <Option value="false" type="bool" name="OrderByValue"/>
            <Option value="false" type="bool" name="UseCompleter"/>
            <Option value="name" type="QString" name="Value"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="original_id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="traffic_rules">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="boarding_traffic_rules">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pt_fare_zone_id">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="2147483647" type="int" name="Max"/>
            <Option value="-2147483648" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="gtfs_location_type">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="AllowNull"/>
            <Option value="2147483647" type="int" name="Max"/>
            <Option value="-2147483648" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="security_time">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" type="bool" name="allow_null"/>
            <Option value="true" type="bool" name="calendar_popup"/>
            <Option value="HH:mm:ss" type="QString" name="display_format"/>
            <Option value="HH:mm:ss" type="QString" name="field_format"/>
            <Option value="false" type="bool" name="field_iso_format"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pt_trip_types_id">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pt_trip_types_name">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pt_agencies_id">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pt_agencies_name">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pt_station_geom">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id" index="0" name="ID"/>
    <alias field="name" index="1" name="Nom"/>
    <alias field="station_id" index="2" name="ID zone arrêt"/>
    <alias field="station_name" index="3" name="Nom zone arrêt"/>
    <alias field="source_id" index="4" name="Source données"/>
    <alias field="original_id" index="5" name="ID initial"/>
    <alias field="traffic_rules" index="6" name="Accès à l'arrêt"/>
    <alias field="boarding_traffic_rules" index="7" name="Accès aux services TC"/>
    <alias field="pt_fare_zone_id" index="8" name="Zone tarifaire"/>
    <alias field="gtfs_location_type" index="9" name="Type arrêt GTFS"/>
    <alias field="security_time" index="10" name="Temps sécurité"/>
    <alias field="pt_trip_types_id" index="11" name="ID types services"/>
    <alias field="pt_trip_types_name" index="12" name="Type services"/>
    <alias field="pt_agencies_id" index="13" name="ID exploitants"/>
    <alias field="pt_agencies_name" index="14" name="Exploitants"/>
    <alias field="pt_station_geom" index="15" name=""/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" field="id" applyOnUpdate="0"/>
    <default expression="" field="name" applyOnUpdate="0"/>
    <default expression="" field="station_id" applyOnUpdate="0"/>
    <default expression="" field="station_name" applyOnUpdate="0"/>
    <default expression="" field="source_id" applyOnUpdate="0"/>
    <default expression="" field="original_id" applyOnUpdate="0"/>
    <default expression="" field="traffic_rules" applyOnUpdate="0"/>
    <default expression="" field="boarding_traffic_rules" applyOnUpdate="0"/>
    <default expression="" field="pt_fare_zone_id" applyOnUpdate="0"/>
    <default expression="" field="gtfs_location_type" applyOnUpdate="0"/>
    <default expression="" field="security_time" applyOnUpdate="0"/>
    <default expression="" field="pt_trip_types_id" applyOnUpdate="0"/>
    <default expression="" field="pt_trip_types_name" applyOnUpdate="0"/>
    <default expression="" field="pt_agencies_id" applyOnUpdate="0"/>
    <default expression="" field="pt_agencies_name" applyOnUpdate="0"/>
    <default expression="" field="pt_station_geom" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint constraints="3" unique_strength="1" exp_strength="0" field="id" notnull_strength="1"/>
    <constraint constraints="0" unique_strength="0" exp_strength="0" field="name" notnull_strength="0"/>
    <constraint constraints="0" unique_strength="0" exp_strength="0" field="station_id" notnull_strength="0"/>
    <constraint constraints="0" unique_strength="0" exp_strength="0" field="station_name" notnull_strength="0"/>
    <constraint constraints="0" unique_strength="0" exp_strength="0" field="source_id" notnull_strength="0"/>
    <constraint constraints="0" unique_strength="0" exp_strength="0" field="original_id" notnull_strength="0"/>
    <constraint constraints="0" unique_strength="0" exp_strength="0" field="traffic_rules" notnull_strength="0"/>
    <constraint constraints="0" unique_strength="0" exp_strength="0" field="boarding_traffic_rules" notnull_strength="0"/>
    <constraint constraints="0" unique_strength="0" exp_strength="0" field="pt_fare_zone_id" notnull_strength="0"/>
    <constraint constraints="0" unique_strength="0" exp_strength="0" field="gtfs_location_type" notnull_strength="0"/>
    <constraint constraints="0" unique_strength="0" exp_strength="0" field="security_time" notnull_strength="0"/>
    <constraint constraints="0" unique_strength="0" exp_strength="0" field="pt_trip_types_id" notnull_strength="0"/>
    <constraint constraints="0" unique_strength="0" exp_strength="0" field="pt_trip_types_name" notnull_strength="0"/>
    <constraint constraints="0" unique_strength="0" exp_strength="0" field="pt_agencies_id" notnull_strength="0"/>
    <constraint constraints="0" unique_strength="0" exp_strength="0" field="pt_agencies_name" notnull_strength="0"/>
    <constraint constraints="0" unique_strength="0" exp_strength="0" field="pt_station_geom" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="id"/>
    <constraint desc="" exp="" field="name"/>
    <constraint desc="" exp="" field="station_id"/>
    <constraint desc="" exp="" field="station_name"/>
    <constraint desc="" exp="" field="source_id"/>
    <constraint desc="" exp="" field="original_id"/>
    <constraint desc="" exp="" field="traffic_rules"/>
    <constraint desc="" exp="" field="boarding_traffic_rules"/>
    <constraint desc="" exp="" field="pt_fare_zone_id"/>
    <constraint desc="" exp="" field="gtfs_location_type"/>
    <constraint desc="" exp="" field="security_time"/>
    <constraint desc="" exp="" field="pt_trip_types_id"/>
    <constraint desc="" exp="" field="pt_trip_types_name"/>
    <constraint desc="" exp="" field="pt_agencies_id"/>
    <constraint desc="" exp="" field="pt_agencies_name"/>
    <constraint desc="" exp="" field="pt_station_geom"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortOrder="0" sortExpression="&quot;original_id&quot;" actionWidgetStyle="dropDown">
    <columns>
      <column width="-1" hidden="1" type="actions"/>
      <column width="46" hidden="0" type="field" name="id"/>
      <column width="100" hidden="1" type="field" name="original_id"/>
      <column width="97" hidden="0" type="field" name="source_id"/>
      <column width="197" hidden="0" type="field" name="name"/>
      <column width="100" hidden="1" type="field" name="station_id"/>
      <column width="182" hidden="0" type="field" name="station_name"/>
      <column width="163" hidden="0" type="field" name="traffic_rules"/>
      <column width="138" hidden="0" type="field" name="boarding_traffic_rules"/>
      <column width="100" hidden="1" type="field" name="pt_trip_types_id"/>
      <column width="79" hidden="0" type="field" name="pt_fare_zone_id"/>
      <column width="100" hidden="1" type="field" name="gtfs_location_type"/>
      <column width="76" hidden="0" type="field" name="pt_trip_types_name"/>
      <column width="100" hidden="1" type="field" name="pt_agencies_id"/>
      <column width="80" hidden="0" type="field" name="pt_agencies_name"/>
      <column width="-1" hidden="1" type="field" name="pt_station_geom"/>
      <column width="88" hidden="0" type="field" name="security_time"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="boarding_traffic_rules"/>
    <field editable="1" name="gtfs_location_type"/>
    <field editable="1" name="id"/>
    <field editable="1" name="name"/>
    <field editable="1" name="network_id"/>
    <field editable="1" name="network_name"/>
    <field editable="1" name="original_id"/>
    <field editable="1" name="pt_agencies_id"/>
    <field editable="1" name="pt_agencies_name"/>
    <field editable="1" name="pt_fare_zone_id"/>
    <field editable="1" name="pt_station_geom"/>
    <field editable="1" name="pt_trip_types_id"/>
    <field editable="1" name="pt_trip_types_name"/>
    <field editable="1" name="security_time"/>
    <field editable="1" name="source_id"/>
    <field editable="1" name="station_id"/>
    <field editable="1" name="station_name"/>
    <field editable="1" name="stop_type"/>
    <field editable="1" name="traffic_rules"/>
    <field editable="1" name="wheelchair"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="boarding_traffic_rules"/>
    <field labelOnTop="0" name="gtfs_location_type"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="name"/>
    <field labelOnTop="0" name="network_id"/>
    <field labelOnTop="0" name="network_name"/>
    <field labelOnTop="0" name="original_id"/>
    <field labelOnTop="0" name="pt_agencies_id"/>
    <field labelOnTop="0" name="pt_agencies_name"/>
    <field labelOnTop="0" name="pt_fare_zone_id"/>
    <field labelOnTop="0" name="pt_station_geom"/>
    <field labelOnTop="0" name="pt_trip_types_id"/>
    <field labelOnTop="0" name="pt_trip_types_name"/>
    <field labelOnTop="0" name="security_time"/>
    <field labelOnTop="0" name="source_id"/>
    <field labelOnTop="0" name="station_id"/>
    <field labelOnTop="0" name="station_name"/>
    <field labelOnTop="0" name="stop_type"/>
    <field labelOnTop="0" name="traffic_rules"/>
    <field labelOnTop="0" name="wheelchair"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id</previewExpression>
  <mapTip>stop_name</mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
