<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyLocal="1" hasScaleBasedVisibilityFlag="0" simplifyMaxScale="1" labelsEnabled="0" readOnly="0" simplifyDrawingTol="1" maxScale="0" version="3.10.6-A Coruña" styleCategories="AllStyleCategories" simplifyAlgorithm="0" minScale="1e+08" simplifyDrawingHints="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 enableorderby="0" symbollevels="0" type="singleSymbol" forceraster="0">
    <symbols>
      <symbol force_rhr="0" name="0" type="marker" alpha="1" clip_to_extent="1">
        <layer pass="0" class="SimpleMarker" enabled="1" locked="0">
          <prop k="angle" v="0"/>
          <prop k="color" v="0,0,0,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="star"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>name</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory minScaleDenominator="0" scaleDependency="Area" lineSizeScale="3x:0,0,0,0,0,0" scaleBasedVisibility="0" height="15" enabled="0" opacity="1" labelPlacementMethod="XHeight" penColor="#000000" lineSizeType="MM" sizeScale="3x:0,0,0,0,0,0" width="15" minimumSize="0" backgroundColor="#ffffff" penWidth="0" rotationOffset="270" backgroundAlpha="255" diagramOrientation="Up" barWidth="5" sizeType="MM" maxScaleDenominator="1e+08" penAlpha="255">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute color="#000000" label="" field=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings priority="0" placement="0" dist="0" showAll="1" zIndex="0" obstacle="0" linePlacementFlags="18">
    <properties>
      <Option type="Map">
        <Option name="name" value="" type="QString"/>
        <Option name="properties"/>
        <Option name="type" value="collection" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="false" type="bool"/>
            <Option name="UseHtml" value="false" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="original_id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="false" type="bool"/>
            <Option name="UseHtml" value="false" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="source_id">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option name="AllowMulti" value="false" type="bool"/>
            <Option name="AllowNull" value="false" type="bool"/>
            <Option name="FilterExpression" value="" type="QString"/>
            <Option name="Key" value="id" type="QString"/>
            <Option name="Layer" value="Sources_de_données_43e38f9b_6564_4c67_8c79_139b118e3a2d" type="QString"/>
            <Option name="LayerName" value="Sources de données" type="QString"/>
            <Option name="LayerProviderName" value="postgres" type="QString"/>
            <Option name="LayerSource" value="dbname='tempusaccess_corse' host=localhost port=55432 user='aurelie-p.bousquet' key='id' checkPrimaryKeyUnicity='1' table=&quot;pgtempus&quot;.&quot;source&quot; sql=" type="QString"/>
            <Option name="NofColumns" value="1" type="int"/>
            <Option name="OrderByValue" value="false" type="bool"/>
            <Option name="UseCompleter" value="false" type="bool"/>
            <Option name="Value" value="name" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pt_trip_types_id">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="name">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="false" type="bool"/>
            <Option name="UseHtml" value="false" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="traffic_rules">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="direction">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option name="AllowNull" value="true" type="bool"/>
            <Option name="Max" value="2147483647" type="int"/>
            <Option name="Min" value="-2147483648" type="int"/>
            <Option name="Precision" value="0" type="int"/>
            <Option name="Step" value="1" type="int"/>
            <Option name="Style" value="SpinBox" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="direction_headsign">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="false" type="bool"/>
            <Option name="UseHtml" value="false" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pt_route_id">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option name="AllowMulti" value="false" type="bool"/>
            <Option name="AllowNull" value="false" type="bool"/>
            <Option name="FilterExpression" value="" type="QString"/>
            <Option name="Key" value="id" type="QString"/>
            <Option name="Layer" value="Lignes_7e0887f1_448c_4807_93c6_3c7f0dc345af" type="QString"/>
            <Option name="LayerName" value="Lignes" type="QString"/>
            <Option name="NofColumns" value="1" type="int"/>
            <Option name="OrderByValue" value="false" type="bool"/>
            <Option name="UseCompleter" value="false" type="bool"/>
            <Option name="Value" value="long_name" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pt_agency_id">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option name="AllowMulti" value="false" type="bool"/>
            <Option name="AllowNull" value="false" type="bool"/>
            <Option name="FilterExpression" value="" type="QString"/>
            <Option name="Key" value="id" type="QString"/>
            <Option name="Layer" value="Exploitants_1c56aa7f_f136_4620_a40b_75e72041d17e" type="QString"/>
            <Option name="LayerName" value="Exploitants" type="QString"/>
            <Option name="NofColumns" value="1" type="int"/>
            <Option name="OrderByValue" value="false" type="bool"/>
            <Option name="UseCompleter" value="false" type="bool"/>
            <Option name="Value" value="name" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pt_stops_id">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pt_stops_name">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pt_sections_id">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="total_dist">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="false" type="bool"/>
            <Option name="UseHtml" value="false" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="speed">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="false" type="bool"/>
            <Option name="UseHtml" value="false" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="days">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="geom_arc">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="ID" field="id"/>
    <alias index="1" name="ID initial" field="original_id"/>
    <alias index="2" name="Source données" field="source_id"/>
    <alias index="3" name="Types de service" field="pt_trip_types_id"/>
    <alias index="4" name="Nom" field="name"/>
    <alias index="5" name="Accès (règles de circulation)" field="traffic_rules"/>
    <alias index="6" name="Direction" field="direction"/>
    <alias index="7" name="Libellé direction" field="direction_headsign"/>
    <alias index="8" name="Ligne" field="pt_route_id"/>
    <alias index="9" name="Exploitant" field="pt_agency_id"/>
    <alias index="10" name="Liste ID arrêts" field="pt_stops_id"/>
    <alias index="11" name="Liste noms arrêts" field="pt_stops_name"/>
    <alias index="12" name="Liste ID sections" field="pt_sections_id"/>
    <alias index="13" name="Distance totale (m)" field="total_dist"/>
    <alias index="14" name="Vitesse moy (km/h)" field="speed"/>
    <alias index="15" name="Jours de circulation" field="days"/>
    <alias index="16" name="" field="geom_arc"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" field="id" applyOnUpdate="0"/>
    <default expression="" field="original_id" applyOnUpdate="0"/>
    <default expression="" field="source_id" applyOnUpdate="0"/>
    <default expression="" field="pt_trip_types_id" applyOnUpdate="0"/>
    <default expression="" field="name" applyOnUpdate="0"/>
    <default expression="" field="traffic_rules" applyOnUpdate="0"/>
    <default expression="" field="direction" applyOnUpdate="0"/>
    <default expression="" field="direction_headsign" applyOnUpdate="0"/>
    <default expression="" field="pt_route_id" applyOnUpdate="0"/>
    <default expression="" field="pt_agency_id" applyOnUpdate="0"/>
    <default expression="" field="pt_stops_id" applyOnUpdate="0"/>
    <default expression="" field="pt_stops_name" applyOnUpdate="0"/>
    <default expression="" field="pt_sections_id" applyOnUpdate="0"/>
    <default expression="" field="total_dist" applyOnUpdate="0"/>
    <default expression="" field="speed" applyOnUpdate="0"/>
    <default expression="" field="days" applyOnUpdate="0"/>
    <default expression="" field="geom_arc" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" notnull_strength="1" unique_strength="1" field="id" constraints="3"/>
    <constraint exp_strength="0" notnull_strength="0" unique_strength="0" field="original_id" constraints="0"/>
    <constraint exp_strength="0" notnull_strength="0" unique_strength="0" field="source_id" constraints="0"/>
    <constraint exp_strength="0" notnull_strength="0" unique_strength="0" field="pt_trip_types_id" constraints="0"/>
    <constraint exp_strength="0" notnull_strength="0" unique_strength="0" field="name" constraints="0"/>
    <constraint exp_strength="0" notnull_strength="0" unique_strength="0" field="traffic_rules" constraints="0"/>
    <constraint exp_strength="0" notnull_strength="0" unique_strength="0" field="direction" constraints="0"/>
    <constraint exp_strength="0" notnull_strength="0" unique_strength="0" field="direction_headsign" constraints="0"/>
    <constraint exp_strength="0" notnull_strength="0" unique_strength="0" field="pt_route_id" constraints="0"/>
    <constraint exp_strength="0" notnull_strength="0" unique_strength="0" field="pt_agency_id" constraints="0"/>
    <constraint exp_strength="0" notnull_strength="0" unique_strength="0" field="pt_stops_id" constraints="0"/>
    <constraint exp_strength="0" notnull_strength="0" unique_strength="0" field="pt_stops_name" constraints="0"/>
    <constraint exp_strength="0" notnull_strength="0" unique_strength="0" field="pt_sections_id" constraints="0"/>
    <constraint exp_strength="0" notnull_strength="0" unique_strength="0" field="total_dist" constraints="0"/>
    <constraint exp_strength="0" notnull_strength="0" unique_strength="0" field="speed" constraints="0"/>
    <constraint exp_strength="0" notnull_strength="0" unique_strength="0" field="days" constraints="0"/>
    <constraint exp_strength="0" notnull_strength="0" unique_strength="0" field="geom_arc" constraints="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="id" exp=""/>
    <constraint desc="" field="original_id" exp=""/>
    <constraint desc="" field="source_id" exp=""/>
    <constraint desc="" field="pt_trip_types_id" exp=""/>
    <constraint desc="" field="name" exp=""/>
    <constraint desc="" field="traffic_rules" exp=""/>
    <constraint desc="" field="direction" exp=""/>
    <constraint desc="" field="direction_headsign" exp=""/>
    <constraint desc="" field="pt_route_id" exp=""/>
    <constraint desc="" field="pt_agency_id" exp=""/>
    <constraint desc="" field="pt_stops_id" exp=""/>
    <constraint desc="" field="pt_stops_name" exp=""/>
    <constraint desc="" field="pt_sections_id" exp=""/>
    <constraint desc="" field="total_dist" exp=""/>
    <constraint desc="" field="speed" exp=""/>
    <constraint desc="" field="days" exp=""/>
    <constraint desc="" field="geom_arc" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortOrder="0" actionWidgetStyle="dropDown" sortExpression="&quot;id&quot;">
    <columns>
      <column hidden="0" width="43" name="id" type="field"/>
      <column hidden="1" width="-1" name="original_id" type="field"/>
      <column hidden="0" width="100" name="source_id" type="field"/>
      <column hidden="0" width="96" name="pt_trip_types_id" type="field"/>
      <column hidden="0" width="100" name="name" type="field"/>
      <column hidden="0" width="163" name="traffic_rules" type="field"/>
      <column hidden="1" width="63" name="direction" type="field"/>
      <column hidden="0" width="136" name="direction_headsign" type="field"/>
      <column hidden="1" width="-1" name="pt_sections_id" type="field"/>
      <column hidden="1" width="160" name="pt_stops_id" type="field"/>
      <column hidden="0" width="283" name="pt_stops_name" type="field"/>
      <column hidden="0" width="106" name="pt_route_id" type="field"/>
      <column hidden="0" width="103" name="pt_agency_id" type="field"/>
      <column hidden="0" width="116" name="total_dist" type="field"/>
      <column hidden="0" width="110" name="speed" type="field"/>
      <column hidden="0" width="311" name="days" type="field"/>
      <column hidden="1" width="-1" type="actions"/>
      <column hidden="1" width="-1" name="geom_arc" type="field"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="days"/>
    <field editable="1" name="direction"/>
    <field editable="1" name="direction_headsign"/>
    <field editable="1" name="geom_arc"/>
    <field editable="1" name="id"/>
    <field editable="1" name="name"/>
    <field editable="1" name="network_id"/>
    <field editable="1" name="original_id"/>
    <field editable="1" name="pt_agency_id"/>
    <field editable="1" name="pt_route_id"/>
    <field editable="1" name="pt_sections_id"/>
    <field editable="1" name="pt_stops_id"/>
    <field editable="1" name="pt_stops_name"/>
    <field editable="1" name="pt_trip_types_id"/>
    <field editable="1" name="source_id"/>
    <field editable="1" name="speed"/>
    <field editable="1" name="total_dist"/>
    <field editable="1" name="traffic_rules"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="days"/>
    <field labelOnTop="0" name="direction"/>
    <field labelOnTop="0" name="direction_headsign"/>
    <field labelOnTop="0" name="geom_arc"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="name"/>
    <field labelOnTop="0" name="network_id"/>
    <field labelOnTop="0" name="original_id"/>
    <field labelOnTop="0" name="pt_agency_id"/>
    <field labelOnTop="0" name="pt_route_id"/>
    <field labelOnTop="0" name="pt_sections_id"/>
    <field labelOnTop="0" name="pt_stops_id"/>
    <field labelOnTop="0" name="pt_stops_name"/>
    <field labelOnTop="0" name="pt_trip_types_id"/>
    <field labelOnTop="0" name="source_id"/>
    <field labelOnTop="0" name="speed"/>
    <field labelOnTop="0" name="total_dist"/>
    <field labelOnTop="0" name="traffic_rules"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>name</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
