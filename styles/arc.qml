<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyMaxScale="1" simplifyDrawingHints="1" hasScaleBasedVisibilityFlag="0" labelsEnabled="0" version="3.10.6-A Coruña" maxScale="0" styleCategories="AllStyleCategories" minScale="1e+08" simplifyLocal="1" simplifyDrawingTol="1" simplifyAlgorithm="0" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 forceraster="0" enableorderby="0" symbollevels="0" type="singleSymbol">
    <symbols>
      <symbol force_rhr="0" alpha="1" name="0" clip_to_extent="1" type="line">
        <layer class="SimpleLine" pass="0" enabled="1" locked="0">
          <prop v="square" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="125,139,143,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="0.1" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style fontItalic="0" fontSizeUnit="Point" fontUnderline="0" fontWeight="50" fontWordSpacing="0" isExpression="0" fontSize="7" fontKerning="1" fontCapitals="0" textOrientation="horizontal" fontSizeMapUnitScale="3x:0,0,0,0,0,0" useSubstitutions="0" textOpacity="1" fieldName="id" previewBkgrdColor="255,255,255,255" blendMode="0" namedStyle="Normal" textColor="102,103,105,255" fontFamily="MS Shell Dlg 2" fontStrikeout="0" fontLetterSpacing="0" multilineHeight="1">
        <text-buffer bufferOpacity="1" bufferBlendMode="0" bufferDraw="0" bufferJoinStyle="128" bufferSize="1" bufferSizeUnits="MM" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferColor="255,255,255,255" bufferNoFill="1"/>
        <background shapeBorderColor="128,128,128,255" shapeRadiiUnit="MM" shapeOffsetX="0" shapeRotation="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeBlendMode="0" shapeSizeUnit="MM" shapeOffsetUnit="MM" shapeType="0" shapeRadiiX="0" shapeDraw="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeSizeType="0" shapeFillColor="255,255,255,255" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeRotationType="0" shapeSVGFile="" shapeSizeX="0" shapeBorderWidthUnit="MM" shapeJoinStyle="64" shapeRadiiY="0" shapeOpacity="1" shapeSizeY="0" shapeOffsetY="0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeBorderWidth="0">
          <symbol force_rhr="0" alpha="1" name="markerSymbol" clip_to_extent="1" type="marker">
            <layer class="SimpleMarker" pass="0" enabled="1" locked="0">
              <prop v="0" k="angle"/>
              <prop v="164,113,88,255" k="color"/>
              <prop v="1" k="horizontal_anchor_point"/>
              <prop v="bevel" k="joinstyle"/>
              <prop v="circle" k="name"/>
              <prop v="0,0" k="offset"/>
              <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
              <prop v="MM" k="offset_unit"/>
              <prop v="35,35,35,255" k="outline_color"/>
              <prop v="solid" k="outline_style"/>
              <prop v="0" k="outline_width"/>
              <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
              <prop v="MM" k="outline_width_unit"/>
              <prop v="diameter" k="scale_method"/>
              <prop v="2" k="size"/>
              <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
              <prop v="MM" k="size_unit"/>
              <prop v="1" k="vertical_anchor_point"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option value="" name="name" type="QString"/>
                  <Option name="properties"/>
                  <Option value="collection" name="type" type="QString"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetDist="1" shadowOffsetGlobal="1" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetUnit="MM" shadowRadius="1.5" shadowRadiusUnit="MM" shadowOpacity="0.7" shadowColor="0,0,0,255" shadowBlendMode="6" shadowDraw="0" shadowScale="100" shadowUnder="0" shadowOffsetAngle="135" shadowRadiusAlphaOnly="0"/>
        <dd_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </dd_properties>
        <substitutions/>
      </text-style>
      <text-format wrapChar="" addDirectionSymbol="0" rightDirectionSymbol=">" decimals="3" useMaxLineLengthForAutoWrap="1" formatNumbers="0" plussign="0" autoWrapLength="0" leftDirectionSymbol="&lt;" multilineAlign="4294967295" placeDirectionSymbol="0" reverseDirectionSymbol="0"/>
      <placement overrunDistanceUnit="MM" geometryGeneratorEnabled="0" offsetUnits="MM" yOffset="0" geometryGeneratorType="PointGeometry" repeatDistanceUnits="MM" geometryGenerator="" dist="0.2" offsetType="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" repeatDistance="0" distUnits="MM" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" rotationAngle="0" preserveRotation="1" quadOffset="4" centroidWhole="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" layerType="LineGeometry" maxCurvedCharAngleIn="25" fitInPolygonOnly="0" overrunDistance="0" priority="5" placement="2" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" centroidInside="0" distMapUnitScale="3x:0,0,0,0,0,0" maxCurvedCharAngleOut="-25" placementFlags="4" xOffset="0"/>
      <rendering minFeatureSize="0" fontMaxPixelSize="10000" labelPerPart="0" obstacleFactor="1" scaleMax="5000" obstacleType="0" drawLabels="1" mergeLines="0" scaleVisibility="1" scaleMin="0" fontMinPixelSize="3" obstacle="1" upsidedownLabels="0" maxNumLabels="2000" displayAll="0" zIndex="0" limitNumLabels="0" fontLimitPixelSize="0"/>
      <dd_properties>
        <Option type="Map">
          <Option value="" name="name" type="QString"/>
          <Option name="properties"/>
          <Option value="collection" name="type" type="QString"/>
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option value="pole_of_inaccessibility" name="anchorPoint" type="QString"/>
          <Option name="ddProperties" type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
          <Option value="false" name="drawToAllParts" type="bool"/>
          <Option value="0" name="enabled" type="QString"/>
          <Option value="&lt;symbol force_rhr=&quot;0&quot; alpha=&quot;1&quot; name=&quot;symbol&quot; clip_to_extent=&quot;1&quot; type=&quot;line&quot;>&lt;layer class=&quot;SimpleLine&quot; pass=&quot;0&quot; enabled=&quot;1&quot; locked=&quot;0&quot;>&lt;prop v=&quot;square&quot; k=&quot;capstyle&quot;/>&lt;prop v=&quot;5;2&quot; k=&quot;customdash&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;customdash_map_unit_scale&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;customdash_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;draw_inside_polygon&quot;/>&lt;prop v=&quot;bevel&quot; k=&quot;joinstyle&quot;/>&lt;prop v=&quot;60,60,60,255&quot; k=&quot;line_color&quot;/>&lt;prop v=&quot;solid&quot; k=&quot;line_style&quot;/>&lt;prop v=&quot;0.3&quot; k=&quot;line_width&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;line_width_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;offset&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;offset_map_unit_scale&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;offset_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;ring_filter&quot;/>&lt;prop v=&quot;0&quot; k=&quot;use_custom_dash&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;width_map_unit_scale&quot;/>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; name=&quot;name&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; name=&quot;type&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" name="lineSymbol" type="QString"/>
          <Option value="0" name="minLength" type="double"/>
          <Option value="3x:0,0,0,0,0,0" name="minLengthMapUnitScale" type="QString"/>
          <Option value="MM" name="minLengthUnit" type="QString"/>
          <Option value="0" name="offsetFromAnchor" type="double"/>
          <Option value="3x:0,0,0,0,0,0" name="offsetFromAnchorMapUnitScale" type="QString"/>
          <Option value="MM" name="offsetFromAnchorUnit" type="QString"/>
          <Option value="0" name="offsetFromLabel" type="double"/>
          <Option value="3x:0,0,0,0,0,0" name="offsetFromLabelMapUnitScale" type="QString"/>
          <Option value="MM" name="offsetFromLabelUnit" type="QString"/>
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>COALESCE( "road_name", '&lt;NULL>' )</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory lineSizeScale="3x:0,0,0,0,0,0" rotationOffset="270" maxScaleDenominator="1e+08" barWidth="5" height="15" diagramOrientation="Up" width="15" scaleDependency="Area" backgroundColor="#ffffff" minimumSize="0" scaleBasedVisibility="0" backgroundAlpha="255" sizeScale="3x:0,0,0,0,0,0" lineSizeType="MM" sizeType="MM" penColor="#000000" opacity="1" penWidth="0" enabled="0" labelPlacementMethod="XHeight" penAlpha="255" minScaleDenominator="0">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute color="#000000" label="" field=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings linePlacementFlags="2" dist="0" zIndex="0" obstacle="0" placement="2" priority="0" showAll="1">
    <properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties"/>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="original_id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="sources_id">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="node_from_id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="node_to_id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="crossability">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="0" name="Dans les 2 sens" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="1" name="De la gauche vers la droite" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="2" name="De la droite vers la gauche" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="3" name="Intraversable" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="diffusion">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="0" name="Interdite" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="1" name="Côté droit seulement" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="2" name="Côté gauche seulement" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="3" name="Des deux côtés" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="section_type_id">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option value="false" name="AllowMulti" type="bool"/>
            <Option value="false" name="AllowNull" type="bool"/>
            <Option value="" name="FilterExpression" type="QString"/>
            <Option value="id" name="Key" type="QString"/>
            <Option value="Types_sections_de_route_3134c6e8_aa44_4530_8615_3647221d8f2a" name="Layer" type="QString"/>
            <Option value="Types sections de route" name="LayerName" type="QString"/>
            <Option value="1" name="NofColumns" type="int"/>
            <Option value="false" name="OrderByValue" type="bool"/>
            <Option value="false" name="UseCompleter" type="bool"/>
            <Option value="name" name="Value" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="type">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="1" name="Route" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="2" name="Transport collectif" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="3" name="Barrière physique" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="length">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="traffic_rules">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pt_agencies_id">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pt_trip_types_id">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="ID" field="id"/>
    <alias index="1" name="ID initial" field="original_id"/>
    <alias index="2" name="Sources données" field="sources_id"/>
    <alias index="3" name="ID noeud initial" field="node_from_id"/>
    <alias index="4" name="ID noeud final" field="node_to_id"/>
    <alias index="5" name="Traversabilité" field="crossability"/>
    <alias index="6" name="Diffusion" field="diffusion"/>
    <alias index="7" name="Type section route" field="section_type_id"/>
    <alias index="8" name="Type arc" field="type"/>
    <alias index="9" name="Longueur (m)" field="length"/>
    <alias index="10" name="Règles de circulation autorisées" field="traffic_rules"/>
    <alias index="11" name="Exploitants" field="pt_agencies_id"/>
    <alias index="12" name="Types de services" field="pt_trip_types_id"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" applyOnUpdate="0" field="id"/>
    <default expression="" applyOnUpdate="0" field="original_id"/>
    <default expression="" applyOnUpdate="0" field="sources_id"/>
    <default expression="" applyOnUpdate="0" field="node_from_id"/>
    <default expression="" applyOnUpdate="0" field="node_to_id"/>
    <default expression="" applyOnUpdate="0" field="crossability"/>
    <default expression="" applyOnUpdate="0" field="diffusion"/>
    <default expression="" applyOnUpdate="0" field="section_type_id"/>
    <default expression="" applyOnUpdate="0" field="type"/>
    <default expression="" applyOnUpdate="0" field="length"/>
    <default expression="" applyOnUpdate="0" field="traffic_rules"/>
    <default expression="" applyOnUpdate="0" field="pt_agencies_id"/>
    <default expression="" applyOnUpdate="0" field="pt_trip_types_id"/>
  </defaults>
  <constraints>
    <constraint constraints="3" notnull_strength="1" exp_strength="0" unique_strength="1" field="id"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="original_id"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="sources_id"/>
    <constraint constraints="1" notnull_strength="1" exp_strength="0" unique_strength="0" field="node_from_id"/>
    <constraint constraints="1" notnull_strength="1" exp_strength="0" unique_strength="0" field="node_to_id"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="crossability"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="diffusion"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="section_type_id"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="type"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="length"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="traffic_rules"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="pt_agencies_id"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" unique_strength="0" field="pt_trip_types_id"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" desc="" field="id"/>
    <constraint exp="" desc="" field="original_id"/>
    <constraint exp="" desc="" field="sources_id"/>
    <constraint exp="" desc="" field="node_from_id"/>
    <constraint exp="" desc="" field="node_to_id"/>
    <constraint exp="" desc="" field="crossability"/>
    <constraint exp="" desc="" field="diffusion"/>
    <constraint exp="" desc="" field="section_type_id"/>
    <constraint exp="" desc="" field="type"/>
    <constraint exp="" desc="" field="length"/>
    <constraint exp="" desc="" field="traffic_rules"/>
    <constraint exp="" desc="" field="pt_agencies_id"/>
    <constraint exp="" desc="" field="pt_trip_types_id"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="&quot;is_pt&quot;" actionWidgetStyle="dropDown" sortOrder="0">
    <columns>
      <column width="50" hidden="0" name="id" type="field"/>
      <column width="-1" hidden="1" type="actions"/>
      <column width="178" hidden="1" name="original_id" type="field"/>
      <column width="97" hidden="0" name="sources_id" type="field"/>
      <column width="90" hidden="0" name="node_from_id" type="field"/>
      <column width="82" hidden="0" name="node_to_id" type="field"/>
      <column width="78" hidden="0" name="length" type="field"/>
      <column width="-1" hidden="0" name="crossability" type="field"/>
      <column width="-1" hidden="0" name="diffusion" type="field"/>
      <column width="-1" hidden="0" name="section_type_id" type="field"/>
      <column width="100" hidden="0" name="type" type="field"/>
      <column width="179" hidden="0" name="traffic_rules" type="field"/>
      <column width="-1" hidden="0" name="pt_agencies_id" type="field"/>
      <column width="100" hidden="0" name="pt_trip_types_id" type="field"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles>
      <style rule="road_type = 5" text_color_alpha="255" text_color="#4a8c1d" name="">
        <font description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      </style>
    </rowstyles>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="crossability"/>
    <field editable="1" name="diffusion"/>
    <field editable="1" name="edge_type_id"/>
    <field editable="1" name="id"/>
    <field editable="1" name="is_pt"/>
    <field editable="1" name="length"/>
    <field editable="1" name="network_id"/>
    <field editable="1" name="node_from_id"/>
    <field editable="1" name="node_to_id"/>
    <field editable="1" name="original_id"/>
    <field editable="1" name="pt"/>
    <field editable="1" name="pt_agencies_id"/>
    <field editable="1" name="pt_trip_types_id"/>
    <field editable="1" name="section_type_id"/>
    <field editable="1" name="source_id"/>
    <field editable="1" name="sources_id"/>
    <field editable="1" name="traffic_rules"/>
    <field editable="1" name="type"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="crossability"/>
    <field labelOnTop="0" name="diffusion"/>
    <field labelOnTop="0" name="edge_type_id"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="is_pt"/>
    <field labelOnTop="0" name="length"/>
    <field labelOnTop="0" name="network_id"/>
    <field labelOnTop="0" name="node_from_id"/>
    <field labelOnTop="0" name="node_to_id"/>
    <field labelOnTop="0" name="original_id"/>
    <field labelOnTop="0" name="pt"/>
    <field labelOnTop="0" name="pt_agencies_id"/>
    <field labelOnTop="0" name="pt_trip_types_id"/>
    <field labelOnTop="0" name="section_type_id"/>
    <field labelOnTop="0" name="source_id"/>
    <field labelOnTop="0" name="sources_id"/>
    <field labelOnTop="0" name="traffic_rules"/>
    <field labelOnTop="0" name="type"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>COALESCE( "road_name", '&lt;NULL>' )</previewExpression>
  <mapTip>road_name</mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
