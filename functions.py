#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2018-2019 Cerema (https://www.cerema.fr)
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""

import sys
import os
import string
import subprocess

from qgis.core import QgsEditorWidgetSetup


def transform_external_cmd( cmd ):
    line_cmd = (cmd[0] + ' "' +'" "'.join(cmd[1:]) + '"').replace( '\\', '/' )#.encode('latin1')
    return line_cmd

def execute_external_cmd( cmd ):
    line_cmd = transform_external_cmd( cmd ) 
    print(line_cmd)    
    r = subprocess.run( line_cmd , capture_output=True )
    print(r.stderr)
    return r.returncode

def field_to_value_relation(source_layer, source_field_name, target_layer, target_field_name):
    for source_field in source_layer.fields():
        for target_field in target_layer.fields():
            if ((source_field.name() == source_field_name) and (target_field.name() == target_field_name)):
                try:
                    config = {'AllowMulti': False,
                              'AllowNull': False,
                              'FilterExpression': '',
                              'Key': 'id',
                              'Layer': target_layer.id(),
                              'LayerName': target_layer.name(), 
                              'NofColumns': 1,
                              'OrderByValue': False,
                              'UseCompleter': False,
                              'Value': target_field.name()}
                    widget_setup = QgsEditorWidgetSetup('ValueRelation', config)
                    source_layer.setEditorWidgetSetup(source_layer.fields().indexFromName( source_field_name ), widget_setup)
                                        
                except:
                    pass
    else:
        return False
    return True   

def field_to_value_map(layer, field_name, map):
    for field in layer.fields():
        if (field.name() == field_name):
            try:
                widget_setup = QgsEditorWidgetSetup('ValueMap', map)
                layer.setEditorWidgetSetup(layer.fields.indexFromName( field_name ), widget_setup)
                
            except:
                pass
            
            