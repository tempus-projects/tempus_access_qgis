# -*- coding: utf-8 -*-
"""
/***************************************************************************
 PluginTempusAccess
                                 A QGIS plugin
 Analyse de l'offre de transport en commun
                              -------------------
        begin                : 2016-10-22
        git sha              : $Format:%H$
        copyright            : (C) 2016 by Cerema
        email                : Aurelie.bousquet@cerema.fr, Patrick.Palmier@cerema.fr, helene.ly@cerema.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# import the PyQt and QGIS libraries
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5 import Qt, QtSql
from qgis.PyQt.QtWidgets import QDialog, QApplication, QWidget, QPushButton, QMessageBox, QFileDialog
from PyQt5.QtGui import *
from qgis.core import *
from qgis.gui import *

# Initialize Qt resources from file resources.py
from .config import *
from .functions import *

import sys
import string
import os

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "\\forms")
from delete_dialog import Ui_Dialog

class delete_dialog(QDialog): 

    def __init__(self, caller, iface):
        QDialog.__init__(self)
        self.ui= Ui_Dialog()
        self.ui.setupUi(self)
        self.caller = caller
        self.db = caller.db
        self.iface = caller.iface
        self.plugin_dir = self.caller.plugin_dir
        self.icon_dir = self.caller.icon_dir
        
        self.ui.comboBoxSourceName.setModel(self.caller.modelAllSources)
        self.ui.comboBoxSourceName.setCurrentIndex(0)
        
        self._connectSlots() 
        
        self.ui.pushButtonExecute.setIcon(QIcon(self.icon_dir + "/icon_exe.svg"))
        self.ui.pushButtonGenerate.setIcon(QIcon(self.icon_dir + "/icon_generate.png"))
    
    def _connectSlots(self):
        self.ui.pushButtonExecute.clicked.connect(self._slotPushButtonExecuteClicked)
        self.ui.pushButtonGenerate.clicked.connect(self._slotPushButtonGenerateClicked)
    
    def prepareQuery(self):
        dbstring = "host="+self.caller.db.hostName()+" user="+self.caller.db.userName()+" dbname="+self.caller.db.databaseName()+" port="+str(self.caller.db.port())
        self.source_name = self.caller.modelAllSources.record(self.ui.comboBoxSourceName.currentIndex()).value("name")        
        self.cmd=[PYTHON, TEMPUSLOADER, "--action", "delete", "--source-name", self.source_name, '--dbstring', dbstring]
        print(self.cmd)
    
    def _slotPushButtonGenerateClicked(self):
        self.prepareQuery()
        line_cmd = transform_external_cmd( self.cmd )
        
        NomFichierComplet=["", ""]
        NomFichierComplet = QFileDialog.getSaveFileName(caption = "Enregistrer sous...", directory=self.plugin_dir, filter = "BATCH files (*.bat)")
        if (NomFichierComplet[0]!=""):
            f = open(NomFichierComplet[0], "w")
            f.write("call C:/OSGeo4W64/bin/o4w_env.bat\ncall py3_env.bat\n" + unicode(line_cmd) + "\npause")
            f.close()
            
            box = QMessageBox()
            box.setText(u"La requête a été générée dans le fichier cible")
            box.exec_()
    
    def _slotPushButtonExecuteClicked(self):
        ret = QMessageBox.question(self, "Tempus Access", u"La source de données sélectionnée va être supprimée. \n Confirmez-vous cette opération ?", QMessageBox.Ok | QMessageBox.Cancel,QMessageBox.Cancel)
        
        if (ret == QMessageBox.Ok):
            self.prepareQuery()
            
            rc = execute_external_cmd( self.cmd )
            
            box = QMessageBox()
            box.setModal(True)
            if (rc==0):
                box.setText(u"Source supprimée avec succès")
                self.caller.manage_db_dialog._slotPushButtonLoadClicked()
            else:
                box.setText(u"Erreur pendant la suppression. \nPour en savoir plus, générez la requête et exécutez-la dans un terminal. ")
            box.exec_()
    
    