#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2018-2019 Cerema (https://www.cerema.fr)
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""

# import the PyQt and QGIS libraries
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from qgis.PyQt.QtWidgets import QDialog
from PyQt5.Qt import *
from PyQt5 import QtSql
from qgis.core import *
from qgis.utils import iface

import sys
import string
import os

# Initialize Qt resources from file resources.py
from .config import *
from .functions import *

from .lib import pglite

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "\\forms")
from new_db_dialog import Ui_Dialog

class new_db_dialog(QDialog): 

    def __init__(self, caller, iface):
        QDialog.__init__(self)
        self.ui= Ui_Dialog()
        self.ui.setupUi(self)        
        
        self.caller = caller
        self.main = caller.caller
        self.iface = self.caller.iface
        
        self.plugin_dir = self.caller.plugin_dir
        self.icon_dir = self.caller.icon_dir
        self.temp_db = QtSql.QSqlDatabase.addDatabase("QPSQL", connectionName="temp_db")
        
        self.ui.pushButtonExecute.setIcon(QIcon(self.icon_dir + "/icon_exe.svg"))
        self.ui.pushButtonGenerate.setIcon(QIcon(self.icon_dir + "/icon_generate.png"))
        
        # Connexion des signaux et des slots
        self._connectSlots()
        
    def _connectSlots(self):
        self.ui.pushButtonExecute.clicked.connect(self._slotPushButtonExecuteClicked)
        self.ui.pushButtonGenerate.clicked.connect(self._slotPushButtonGenerateClicked)
        
    def updateDBConnection(self):
        self.caller.db.setDatabaseName(self.ui.labelLoadedDB.text()) 
    
    def prepareRestoreQuery(self):
        s="SELECT count(*) from pg_database\
            WHERE datname = 'tempusaccess_"+self.ui.lineEditNewDB.text()+"'";
        q=QtSql.QSqlQuery(unicode(s), self.caller.db)
        q.next()
        exists=False
        
        if (int(q.value(0))>0):
            ret = QMessageBox.question(self, "TempusAccess", u"La base de données 'tempusaccess_"+self.ui.lineEditNewDB.text()+u"' existe déjà et va être réinitialisée : toutes les données présentes seront écrasées. \n Confirmez-vous cette opération ?", QMessageBox.Ok | QMessageBox.Cancel, QMessageBox.Cancel)
            exists = True
        else:
            ret = QMessageBox.Ok
        
        if (ret == QMessageBox.Ok):
            # Restart database server to be sure deleting "TempusAccess" database will be allowed (avoids still connected applications)
            nom_fichier = QFileDialog.getOpenFileName(caption = "Restaurer la base de données...", directory=self.main.data_dir, filter = "Backup files (*.backup)")
            
            if (exists == True):
                # Restart database server to be sure deleting "TempusAccess" database will be allowed (avoids still connected applications)
                
                # Delete database
                self.dropdb_cmd = [ DROPDB, "-h", self.caller.db.hostName(), "-U", self.caller.db.userName(), "-p", str(self.caller.db.port()), "tempusaccess_"+str(self.ui.lineEditNewDB.text()) ]
            else:
                self.dropdb_cmd = []
            
            self.createdb_cmd = [ CREATEDB, "-h", self.caller.db.hostName(), "-U", self.caller.db.userName(), "-p", str(self.caller.db.port()), "tempusaccess_"+self.ui.lineEditNewDB.text() ]
                      
            self.restoredb_cmd = [ PGRESTORE,  "-h", self.caller.db.hostName(), "-U", self.caller.db.userName(), "-p", str(self.caller.db.port()), "-d", "tempusaccess_"+self.ui.lineEditNewDB.text(), "-w", "-O", "-x", "-v", nom_fichier[0] ]
    
    def prepareCreateQuery(self):
        if (self.ui.lineEditNewDB.text() == ''):
            box = QMessageBox()
            box.setText(unicode(u"Spécifiez le nom de la base à créer."))
            box.exec_()
        else:
            s="SELECT count(*) from pg_database\
                WHERE datname = 'tempusaccess_"+self.ui.lineEditNewDB.text()+"'";
            q=QtSql.QSqlQuery(unicode(s), self.caller.db)
            q.next()
            create = True
            
            if (int(q.value(0))>0):
                ret = QMessageBox.question(self, "TempusAccess", u"La base de données 'tempusaccess_"+self.ui.lineEditNewDB.text()+u"' existe déjà et va être réinitialisée : toutes les données présentes seront écrasées. \n Confirmez-vous cette opération ?", QMessageBox.Ok | QMessageBox.Cancel, QMessageBox.Cancel)
                create = False
            else:
                ret = QMessageBox.Ok
            
            if (ret == QMessageBox.Ok):            
                if (create == True):
                    self.createdb_cmd = [ CREATEDB, "-h", self.caller.db.hostName(), "-U", self.caller.db.userName(), "-p", str(self.caller.db.port()), "tempusaccess_"+self.ui.lineEditNewDB.text() ]
                else:
                    self.createdb_cmd = []
                
                dbstring = "host="+self.caller.db.hostName()+" dbname=tempusaccess_"+self.ui.lineEditNewDB.text()+" port="+str(self.caller.db.port())
                self.initdb_cmd = [PYTHON, TEMPUSLOADER, "--action", "reset", "--target-srid", str(self.ui.spinBoxSRID.value()), "--tempusaccess", "--path", self.plugin_dir + "/lib/tempus_loader/data/system.zip", "--sep", ";", "--encoding", "UTF8", "--dbstring", dbstring]
    
    def _slotPushButtonExecuteClicked(self): 
        if (self.ui.radioButton_base_vierge.isChecked()):
            self.prepareCreateQuery()             
            if (self.createdb_cmd != [] ):
                rc = execute_external_cmd( self.createdb_cmd )
            rc = execute_external_cmd( self.initdb_cmd )
        
        elif (self.ui.radioButton_restore.isChecked()):
            self.prepareImportQuery()
        
            if (self.dropdb_cmd != []):
                pglite.stop_cluster()
                pglite.start_cluster()            
                rc = execute_external_cmd( self.dropdb_cmd )
            rc = execute_external_cmd( self.createdb_cmd )
            rc = execute_external_cmd( self.restoredb_cmd )
        
        box = QMessageBox()
        box.setModal(True)
        if (rc==0):
            self.iface.mapCanvas().refreshMap()
            box.setText(u"La base a été créée. Vous pouvez maintenant l'ouvrir dans QGIS, puis y importer des données.")                    
            self.caller.ui.comboBoxDB.setCurrentIndex( self.caller.ui.comboBoxDB.findText( self.ui.lineEditNewDB.text() ) )
            self.caller.ui.pushButtonLoad.setEnabled(True)
            self.caller.ui.pushButtonDelete.setEnabled(True)
            self.caller.ui.pushButtonExport.setEnabled(True)
            self.caller.ui.pushButtonInformation.setEnabled(True)
            self.caller.ui.pushButtonInfo2.setEnabled(True)
            self.hide()
        else:
            box.setText(u"Erreur pendant la création de la base.\nCode de retour = "+str(rc)+"\nPour en savoir plus, ouvrir la console Python dans QGIS et relancer la commande.")
        box.exec_()
        
        self.main.set_db_connection_dialog.refreshDBList()
        self.caller.ui.comboBoxDB.setCurrentIndex(self.caller.ui.comboBoxDB.findText(self.ui.lineEditNewDB.text()))
        
    def _slotPushButtonGenerateClicked(self):
        NomFichierComplet=["", ""]
        NomFichierComplet = QFileDialog.getSaveFileName(caption = "Enregistrer sous...", directory=self.plugin_dir, filter = "BATCH files (*.bat)")
        if (NomFichierComplet[0]!=""):
            f = open(NomFichierComplet[0], "w") 
            if (self.ui.radioButton_base_vierge.isChecked()):
                self.prepareCreateQuery()
                if (self.createdb_cmd != []):
                    line_cmd = transform_external_cmd( self.createdb_cmd )
                    f.write(unicode(line_cmd)+"\n")
                
                line_cmd = "call C:/OSGeo4W64/bin/o4w_env.bat\ncall py3_env.bat\n" + transform_external_cmd( self.initdb_cmd )+ "\npause"
                f.write(unicode(line_cmd))
                
            elif (self.ui.radioButton_restore.isChecked()):
                self.prepareRestoreQuery()               
                if (self.dropdb_cmd != []):
                    line_cmd = transform_external_cmd( self.dropdb_cmd )
                    f.write(unicode(line_cmd))
                
                line_cmd = transform_external_cmd( self.createdb_cmd )
                f.write(unicode(line_cmd))
                
                line_cmd = transform_external_cmd( self.restoredb_cmd )
                f.write(unicode(line_cmd))
                
            f.close()
                
            box = QMessageBox()
            box.setText(u"La requête a été générée dans le fichier cible")
            box.exec_()
        
        